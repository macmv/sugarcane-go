module gitlab.com/macmv/sugarcane

go 1.16

require (
	github.com/aws/aws-sdk-go v1.35.19
	github.com/golang/protobuf v1.4.3
	github.com/klauspost/compress v1.11.2
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/stretchr/testify v1.7.0
	gitlab.com/macmv/cbuf v0.0.0-20210305202403-2a374264e8e5
	gitlab.com/macmv/log v0.0.0-20210317223941-4b5f30cdc234
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
)
