package registry_test

import (
  "testing"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/registry"
)

func TestRegistryAdd(t *testing.T) {
  r := registry.New()
  r.Add(23, "gaming")
  r.Add(50, "I am a string")
  r.Add(42, "Yet another string")

  item, index, ok := r.Get(23)
  assert.Equal(t, "gaming", item)
  assert.Equal(t, int32(0), index)
  assert.Equal(t, true, ok)

  item, index, ok = r.Get(50)
  assert.Equal(t, "I am a string", item)
  assert.Equal(t, int32(1), index)
  assert.Equal(t, true, ok)

  item, index, ok = r.Get(42)
  assert.Equal(t, "Yet another string", item)
  assert.Equal(t, int32(2), index)
  assert.Equal(t, true, ok)
}

func TestRegistryGet(t *testing.T) {
  r := registry.New()
  r.Add(23, "gaming")
  r.Add(50, "I am a string")
  r.Add(42, "Yet another string")

  item, index, ok := r.Get(23)
  assert.Equal(t, "gaming", item)
  assert.Equal(t, int32(0), index)
  assert.Equal(t, true, ok)

  item, index, ok = r.Get(1234)
  assert.Equal(t, false, ok)

  item, id, ok := r.GetReverse(1)
  assert.Equal(t, "I am a string", item)
  assert.Equal(t, int32(50), id)
  assert.Equal(t, true, ok)

  item, id, ok = r.GetReverse(4)
  assert.Equal(t, false, ok)
}

func TestRegistryList(t *testing.T) {
  r := registry.New()
  r.Add(23, "gaming")
  r.Add(50, "I am a string")
  r.Add(42, "Yet another string")

  assert.Equal(t, []interface{}{
    "gaming",
    "I am a string",
    "Yet another string",
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryAddAt(t *testing.T) {
  r := registry.New()
  r.AddAt(5, 23, "gaming")

  assert.Equal(t, []interface{}{
    nil,
    nil,
    nil,
    nil,
    "gaming",
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryInsert(t *testing.T) {
  r := registry.New()
  r.Add(23, "gaming")
  r.Add(50, "I am a string")
  r.Add(42, "Yet another string")
  r.Insert(1, 123, "Inserted!")

  assert.Equal(t, []interface{}{
    "gaming",
    "Inserted!",
    "I am a string",
    "Yet another string",
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryRemove(t *testing.T) {
  r := registry.New()
  r.Add(23, "gaming")
  r.Add(50, "I am a string")
  r.Add(42, "Yet another string")
  r.Add(123, "More strings")
  r.Remove(1)

  assert.Equal(t, []interface{}{
    "gaming",
    "Yet another string",
    "More strings",
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryMove(t *testing.T) {
  r := registry.New()
  r.Add(23, "1")
  r.Add(50, "2")
  r.Add(42, "3")
  r.Add(12, "4")
  r.Add(85, "5")
  r.Add(142, "6")
  r.Add(123, "7")
  // Moves item upwards, shifts other items down
  r.Move(142, 1)

  list := r.List()
  assert.Equal(t, []interface{}{
    "1",
    "6",
    "2",
    "3",
    "4",
    "5",
    "7",
  }, list)
  t.Log(r)
  assert.Nil(t, r.Validate())
  for i := range list {
    val, _, ok := r.GetReverse(int32(i))
    assert.True(t, ok)
    assert.Equal(t, list[i], val)
  }

  r = registry.New()
  r.Add(23, "1")
  r.Add(50, "2")
  r.Add(42, "3")
  r.Add(12, "4")
  r.Add(85, "5")
  r.Add(142, "6")
  r.Add(123, "7")
  // Moves item downwards, shifts other items up
  r.Move(50, 5)

  list = r.List()
  assert.Equal(t, []interface{}{
    "1",
    "3",
    "4",
    "5",
    "6",
    "2",
    "7",
  }, list)
  t.Log(r)
  assert.Nil(t, r.Validate())
  for i := range list {
    val, _, ok := r.GetReverse(int32(i))
    assert.True(t, ok)
    assert.Equal(t, list[i], val)
  }

  r = registry.New()
  r.Add(23, "1")
  r.Add(50, "2")
  r.Add(42, "3")
  r.Add(12, "4")
  r.Add(85, "5")
  r.Add(142, "6")
  r.Add(123, "7")
  // An invalid id should do nothign
  r.Move(123445, 5)

  list = r.List()
  assert.Equal(t, []interface{}{
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
  }, list)
  t.Log(r)
  assert.Nil(t, r.Validate())
  for i := range list {
    val, _, ok := r.GetReverse(int32(i))
    assert.True(t, ok)
    assert.Equal(t, list[i], val)
  }
}
