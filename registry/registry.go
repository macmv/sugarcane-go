package registry

import (
  "fmt"
  "strings"
)

type Registry struct {
  // List of registered items
  items []register
  // Map of new ids to item indices
  id_map map[int32]int32
}

type register struct {
  // Registered item
  item interface{}
  // New id of item
  id int32
}

func New() *Registry {
  return &Registry{
    items: make([]register, 0),
    id_map: make(map[int32]int32),
  }
}

func (r *Registry) Copy() *Registry {
  new_reg := &Registry{
    items: make([]register, len(r.items)),
    id_map: make(map[int32]int32),
  }
  // Copying function pointers is fine, we just want a different slice.
  for k, v := range r.items {
    new_reg.items[k] = v
  }
  for k, v := range r.id_map {
    new_reg.id_map[k] = v
  }
  return new_reg
}

// This registers a new item. id should be a constant, defined in user-space
// code (probably with iota). id is not used for sorting, the order of items
// depends on the order that Add is called. So, for packets, ID would be the
// grpc packet id, which doesn't need to be the same as any minecraft packet
// id. If id is -1, then this is treated as a placeholder, and nil will be
// inserted into the internal array.
func (r *Registry) Add(id int32, item interface{}) {
  // If this is true, then this is a placeholder item.
  // So, we don't want to add it to id_map.
  if id == -1 {
    r.items = append(r.items, register{nil, -1})
    return
  }
  // Must stay in this order!
  r.id_map[id] = int32(len(r.items))
  r.items = append(r.items, register{item, id})
}

// This is an odd function. It turns out that 1.8 starts entity ids off at id 50,
// so I don't want to call Add(-1, nil) 50 times. This function will do a couple
// things: If index is <= len(r.items), then this is a nop. If it is
// == len(r.items) + 1, then this is the same as Add(). If it is anything else,
// then it will add a bunch of nil entries, and then item at the end. So this is
// simply the avoid calling Add(-1, nil) a lot. For example, after you call
// AddAt(id, 50, item), then the next call to Add() will put that new item at index 51.
func (r *Registry) AddAt(index, id int32, item interface{}) {
  if index <= int32(len(r.items)) { return }
  for i := int32(len(r.items)); i < index - 1; i++ {
    r.items = append(r.items, register{nil, -1})
  }
  r.Add(id, item)
}

// This inserts a new item at index, shifting all packets with
// a greater index down by one. Useful if a new version adds an item
// near the beginning, and you need to shift all the other items
// down by one.
//
// Index should be the old item id (say, and old block id), and id
// should be the constant defined in user space code (a custom constant refering to that block).
// The old ids greater than index will all be shifted down by one, and
// then this new item will be inserted. The new ids will not be modified.
func (r *Registry) Insert(index, id int32, item interface{}) {
  // Increase length by one.
  r.items = append(r.items, register{nil, -1})
  // Shift all the packets down by one.
  copy(r.items[index+1:], r.items[index:])
  for id, i := range r.id_map {
    if i >= index {
      // Increase all the indecies to match up with r.items
      r.id_map[id]++
    }
  }
  // If this is true, then we are inserting a placeholder packet.
  // This means we do not want to add it to packet_map.
  if id == -1 {
    r.items[index].item = nil
    r.items[index].id = -1
    return
  }
  // Insert the new packet.
  r.id_map[id] = index
  r.items[index] = register{item, id}
}

// Will remove an item from the registry. This will decrease all items that
// have a greater index down by one.
func (r *Registry) Remove(index int32) {
  if index < 0 || index >= int32(len(r.items)) {
    return
  }
  // This call must stay here, because item is removed below.
  item := r.items[index]
  // Remove the item from the list
  copy(r.items[index:], r.items[index+1:])
  r.items = r.items[:len(r.items)-1]
  // Remove the item from the map
  for id, i := range r.id_map {
    if i > index {
      // Decrease all the indecies to match up with r.items
      r.id_map[id]--
    }
  }
  if item.id != -1 {
    delete(r.id_map, item.id)
  }
}

// This moves an item around within the registry. Useful for when
// a packet changes id, or an entity changes name. The first argument
// is the user defined id that you want to access. The second argument
// is the new index you would like to move that item to. This does the
// same thing as Remove() then Insert(), but it is more efficient.
func (r *Registry) Move(id, new_index int32) {
  current_index, ok := r.id_map[id]
  if !ok {
    return
  }
  item := r.items[current_index]
  if new_index > current_index {
    // We are moving the item downwards in the list, so we want to
    // shift everything between current and new up by one.
    copy(r.items[current_index:], r.items[current_index+1:new_index+1])
    for id, index := range r.id_map {
      if index > current_index && index <= new_index {
        r.id_map[id]--
      }
    }
    // Insert the item
    r.items[new_index] = item
    r.id_map[item.id] = new_index
  } else if new_index < current_index {
    // We are moving the item upwards in the list, so we want to
    // shift everything between new and current down by one.
    copy(r.items[new_index+1:], r.items[new_index:current_index])
    for id, index := range r.id_map {
      if index >= new_index && index < current_index {
        r.id_map[id]++
      }
    }
    // Insert the item
    r.items[new_index] = item
    r.id_map[item.id] = new_index
  }
  // If new_index and current_index were the same, this is a nop.
}

// This overrides an item. Useful for if a packet slightly changes between versions.
//
// The old id is infered, since this should only be used to override packets.
// If you call this function, and id is not in the registry, it will panic.
// To add a new item, use Add or Insert instead.
func (r *Registry) Set(id int32, item interface{}) {
  index := r.id_map[id]
  r.items[index].item = item
  if item == nil {
    delete(r.id_map, id)
    r.items[index].id = -1
  }
}

// This gets the item at the id, and returns false if the id is not within the registry.
func (r *Registry) Get(id int32) (item interface{}, index int32, ok bool) {
  index, ok = r.id_map[id]
  if !ok {
    return nil, -1, false
  }
  return r.items[index].item, index, ok
}

// This gets an item based on it's index. The returned int32 is the new id.
func (r *Registry) GetReverse(index int32) (item interface{}, id int32, ok bool) {
  if index >= int32(len(r.items)) || r.items[index].id == -1 {
    return nil, -1, false
  }
  return r.items[index].item, r.items[index].id, true
}

// This lists all elements added to the registry, in the order that they were addded.
func (r *Registry) List() []interface{} {
  list := make([]interface{}, len(r.items))
  for i, item := range r.items {
    list[i] = item.item
  }
  return list
}

func (r *Registry) String() string {
  var b strings.Builder
  fmt.Fprintf(&b, "Registry {\n")
  for index, item := range r.items {
    if item.id == -1 {
      fmt.Fprintf(&b, "  Index: 0x%x: Placeholder <nil>\n", index)
    } else {
      fmt.Fprintf(&b, "  Index: 0x%x: ID: 0x%x, Value: %T\n", index, item.id, item.item)
    }
  }
  fmt.Fprintf(&b, "}\n")
  return b.String()
}

func (r *Registry) Validate() error {
  for index, item := range r.items {
    if item.id == -1 {
      continue
    }
    if r.id_map[item.id] != int32(index) {
      return fmt.Errorf(
        "ID map does not match r.items! id 0x%x should have index 0x%x, but id map shows 0x%x",
        item.id,
        index,
        r.id_map[item.id],
      )
    }
  }
  return nil
}
