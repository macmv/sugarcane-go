FROM golang:1.15-alpine3.12 AS build

WORKDIR /go/src/cubiness/sugarcane

RUN apk update && apk upgrade && \
    apk add --no-cache git protobuf-dev

# Installs the go generator for grpc
RUN go get -d -v github.com/golang/protobuf/protoc-gen-go && \
    go install -v github.com/golang/protobuf/protoc-gen-go

COPY . .

# Generates the .pb.go files
RUN PATH=$PATH:/go/bin protoc proto/*.proto --go_out=plugins=grpc:/go/src/

# Installs and compiles everything
RUN go get -d -v ./... && \
  go install -v ./...

# Blank image, for final build
FROM alpine:3.12 AS proxy

WORKDIR /go/bin

# Only copy the needed files
COPY --from=build /go/bin/sugarcane .

EXPOSE 25565
# CMD can easily be overriden, whereas entrypoint cannot
ENTRYPOINT ["/go/bin/sugarcane"]
CMD ["-local"]
