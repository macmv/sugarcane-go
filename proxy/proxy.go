package proxy

import (
  "io"
  "os"
  "sync"
  "bytes"
  "strings"
  "context"
  "image/png"
  go_net "net"
  "sync/atomic"
  "encoding/base64"

  "github.com/nfnt/resize"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/proxy/net"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  pb "gitlab.com/macmv/sugarcane/proto"

  "google.golang.org/grpc"
  _ "google.golang.org/grpc/encoding/gzip"
)

type Proxy struct {
  connections []*net.Connection
  grpc_connections map[string]*grpc.ClientConn
  connections_mutex sync.Mutex
  compression bool
  encryption bool
  use_mojang_auth bool

  // Base64 encoded icon
  icon string
}

func NewProxy(compression, encryption, use_mojang_auth bool) *Proxy {
  p := Proxy{}
  p.compression = compression
  p.encryption = encryption
  p.use_mojang_auth = use_mojang_auth
  p.grpc_connections = make(map[string]*grpc.ClientConn)
  return &p
}

func (p *Proxy) load_icon() {
  f, err := os.Open("icon.png")
  wd, _ := os.Getwd()
  if err != nil {
    log.Info("No icon found at ", wd, "/icon.png")
    return
  }
  defer f.Close()
  log.Info("Loading icon from ", wd, "/icon.png")

  im, err := png.Decode(f)
  if err != nil {
    log.Error("Unable to load icon: ", err)
    return
  }
  if im.Bounds().Dx() != 64 || im.Bounds().Dy() != 64 {
    log.Warn("Icon is not 64x64 pixels! Resizing...")
    im = resize.Resize(64, 64, im, resize.Lanczos2)
  }
  buf := bytes.NewBufferString("data:image/png;base64,")
  w := base64.NewEncoder(base64.StdEncoding, buf)
  // This encodes the resized png, by writing it into the base64 buffer,
  // which writes the base64 data into the string bufer.
  png.Encode(w, im)
  p.icon = buf.String()
}

func (p *Proxy) Listen(port string, get_server_ip func() (string, error)) {
  p.load_icon()
  net.GenerateRSAKey()
  ln, err := go_net.Listen("tcp", port)
  if err != nil {
    panic(err)
  }
  log.Info("Listening on port " + port)
  for {
    conn, err := ln.Accept()
    if err != nil {
      log.Error("Error while accepting tcp conn: ", err.Error())
      if conn != nil {
        conn.Close()
      }
      continue
    }
    ip, err := get_server_ip()
    if err != nil {
      log.Error("Error while getting server ip: ", err.Error())
      if conn != nil {
        conn.Close()
      }
      continue
    }
    go p.connect_client_to_server(conn, strings.Split(conn.RemoteAddr().String(), ":")[0] + port, ip)
  }
}

func (p *Proxy) connect_client_to_server(client_conn go_net.Conn, client_ip string, server_ip string) {
  conn := p.find_grpc_conn(server_ip)
  if conn == nil {
    return
  }

  connection := net.NewConnection(client_conn, p.compression, p.encryption, 256, p.use_mojang_auth, p.icon)
  needs_server_conn := connection.DoHandshake()

  if needs_server_conn {
    client := pb.NewMinecraftClient(conn)
    grpc_ctx, grpc_cancel := context.WithCancel(context.Background())
    grpc_stream, err := client.Connection(grpc_ctx)
    if err != nil {
      log.Warn("Resetting grpc connection with ", server_ip)
      conn = p.reset_grpc_conn(server_ip)
      client = pb.NewMinecraftClient(conn)

      grpc_ctx, grpc_cancel = context.WithCancel(context.Background())
      grpc_stream, err = client.Connection(grpc_ctx)
      if err != nil {
        log.Error("Could not open a stream with server!")
        log.Error(err.Error())
        connection.Disconnect(util.NewChatFromString("Could not open a connection with the server!"))
        conn.Close()
        return
      }
    }
    log.Info("Opening new connection with server (", server_ip, "), and client (", client_ip, ")")
    connection.ServerPacketStream = grpc_stream

    server_connect_packet := &pb.Packet{Id: 0xff}
    server_connect_packet.Strings = append(server_connect_packet.Strings, connection.Username())
    server_connect_packet.Uuids = append(server_connect_packet.Uuids, &pb.UUID{BigEndianData: connection.UUID().GetBytes()})
    server_connect_packet.Bools = append(server_connect_packet.Bools, true)
    server_connect_packet.Ints = append(server_connect_packet.Ints, int32(connection.Version))
    grpc_stream.Send(server_connect_packet)

    closed := &atomic.Value{}
    closed.Store(false)
    // client_ctx, client_cancel := context.WithCancel(context.Background())
    go p.startOugoingPacketListener(connection, closed, grpc_ctx, grpc_cancel)
    go connection.RecieveLoop(closed, grpc_cancel)
    <- grpc_ctx.Done()
    grpc_cancel()
    if !closed.Load().(bool) {
      log.Warn("grpc connection was closed without the closed flag getting set!")
    }
    log.Info("Closing connection with server (", server_ip, "), and client (", client_ip, ")")
  }

  connection.OutgoingPackets.Close()
  connection.IncomingPackets.Close()
}

func (p *Proxy) find_grpc_conn(ip string) *grpc.ClientConn {
  p.connections_mutex.Lock()
  conn, ok := p.grpc_connections[ip]
  if !ok {
    var err error
    conn, err = grpc.Dial(ip, grpc.WithInsecure())//, grpc.WithDefaultCallOptions(grpc.UseCompressor("gzip")))
    log.Info("Opening a new grpc connection with ", ip)
    if err != nil {
      p.connections_mutex.Unlock()
      log.Error("Could not open connection with server at " + ip)
      log.Error(err.Error())
      return nil
    }
    p.grpc_connections[ip] = conn
  }
  p.connections_mutex.Unlock()
  return conn
}

func (p *Proxy) reset_grpc_conn(ip string) *grpc.ClientConn {
  p.connections_mutex.Lock()
  _, ok := p.grpc_connections[ip]
  if ok {
    delete(p.grpc_connections, ip)
  }
  p.connections_mutex.Unlock()
  return p.find_grpc_conn(ip)
}

// Listens for incoming grpc packets, and sends the according tcp packets to the client
func (p *Proxy) startOugoingPacketListener(connection *net.Connection,
closed *atomic.Value,
grpc_ctx context.Context, grpc_cancel context.CancelFunc) {
  for {
    in, err := connection.ServerPacketStream.Recv()
    if closed.Load().(bool) || err == io.EOF {
      break
    }
    if err != nil {
      log.Error("Got error while listening for packets from server: " + err.Error())
      break
    }
    if in.Id == 0xfe { // switch server packet
      log.Info("Switching server!")

      var stream pb.Minecraft_ConnectionClient
      var new_grpc_cancel context.CancelFunc
      var new_grpc_ctx context.Context
      conn := p.find_grpc_conn(in.Strings[0])
      if conn != nil {
        client := pb.NewMinecraftClient(conn)
        var err error
        new_grpc_ctx, new_grpc_cancel = context.WithCancel(context.Background())
        stream, err = client.Connection(new_grpc_ctx)
        if err != nil {
          log.Error("Error while opening stream with ", in.Strings[0], ", : ", err.Error())
          conn.Close()
          conn = nil
        }
      }

      if conn != nil {
        connection.ServerPacketStream = stream
        grpc_cancel()
        grpc_cancel = new_grpc_cancel
        grpc_ctx = new_grpc_ctx

        respawn := connection.OutgoingPackets.NewPacketID(0x3b)
        respawn.WriteInt(-1)
        respawn.WriteLong(0)
        respawn.WriteByte(1)
        respawn.WriteString("flat")
        connection.OutgoingPackets.Write(respawn)

        respawn = connection.OutgoingPackets.NewPacketID(0x3b)
        respawn.WriteInt(0)
        respawn.WriteLong(0)
        respawn.WriteByte(1)
        respawn.WriteString("flat")
        connection.OutgoingPackets.Write(respawn)

        server_connect := &pb.Packet{Id: 0xff}
        server_connect.Strings = append(server_connect.Strings, connection.Username())
        server_connect.Uuids = append(server_connect.Uuids, connection.UUID().ToProto())
        server_connect.Bools = append(server_connect.Bools, false)
        connection.ServerPacketStream.Send(server_connect)
        log.Info("Sending connection packets")
      } else {
        error_message := connection.OutgoingPackets.NewPacketID(0x0f)
        error_message.WriteString(
          `{"text":"Something went wrong while trying to connect you to server ` + in.Strings[0] + `","color":"red"}`)
        error_message.WriteByte(0)
        connection.OutgoingPackets.Write(error_message)
      }
    } else {
      connection.OutgoingPackets.SendProto(connection.Version, in)
    }
  }
  closed.Store(true)
  grpc_cancel()
}

