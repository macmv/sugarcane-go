package packet_stream

import (
  "fmt"
  "crypto/aes"
  "crypto/cipher"
  "encoding/base64"
)

// CFB stream with 8 bit segment size
// See http://csrc.nist.gov/publications/nistpubs/800-38a/sp800-38a.pdf
type CFB8 struct {
  block     cipher.Block
  blockSize int
  in        []byte
  out       []byte

  decrypt bool
}

func CipherTest() {
  key, err := base64.StdEncoding.DecodeString("i/cgbcLWUbiqifsXodxPAw==")
  if err != nil {
    panic(err)
  }
  block, err := aes.NewCipher(key)
  if err != nil {
    panic(err)
  }
  cipher := NewCFB8Encrypter(block, key)
  data, err := base64.StdEncoding.DecodeString("")
  if err != nil {
    panic(err)
  }
  fmt.Println("Got key: ", base64.StdEncoding.EncodeToString(key))
  fmt.Println("Got decrypted message: ", base64.StdEncoding.EncodeToString(data))
  cipher.XORKeyStream(data, data)
  fmt.Println("Got encrypted message: ", base64.StdEncoding.EncodeToString(data))
}

func (x *CFB8) XORKeyStream(dst, src []byte) {
  for i := range src {
    x.block.Encrypt(x.out, x.in)
    copy(x.in[:x.blockSize-1], x.in[1:])
    if x.decrypt {
      x.in[x.blockSize-1] = src[i]
      dst[i] = src[i] ^ x.out[0]
    } else {
      dst[i] = src[i] ^ x.out[0]
      x.in[x.blockSize-1] = dst[i]
    }
  }
}

func (x *CFB8) BlockSize() int {
  return x.blockSize
}

func NewCFB8Encrypter(block cipher.Block, iv []byte) *CFB8 {
  return newCFB8(block, iv, false)
}

func NewCFB8Decrypter(block cipher.Block, iv []byte) *CFB8 {
  return newCFB8(block, iv, true)
}

func newCFB8(block cipher.Block, iv []byte, decrypt bool) *CFB8 {
  blockSize := block.BlockSize()
  if len(iv) != blockSize {
    // stack trace will indicate whether it was de or encryption
    panic("cipher.newCFB: IV length must equal block size")
  }
  x := &CFB8{
    block:     block,
    blockSize: blockSize,
    out:       make([]byte, blockSize),
    in:        make([]byte, blockSize),
    decrypt:   decrypt,
  }
  copy(x.in, iv)

  return x
}
