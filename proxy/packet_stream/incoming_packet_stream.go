package packet_stream

import (
  "io"
  "net"
  "bytes"
  "io/ioutil"
  "sync/atomic"
  "encoding/hex"
  "crypto/cipher"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/cbuf"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/util"

  "github.com/klauspost/compress/zlib"
)

type IncomingPacketStream struct {
  // This is a large buffer that is written to when a new packet is recieved, and read from when a packet is parsed.
  buf *cbuf.CyclicBuffer

  // Is a bool that is set whenever the connection needs to closed
  closed *atomic.Value

  state byte // 0 = handshake, 1 = status, 2 = login, 3 = play
  conn net.Conn
  reader io.Reader
  compression int32
  async_read bool
}

func NewIncomingPacketStream(conn net.Conn, closed *atomic.Value) *IncomingPacketStream {
  s := IncomingPacketStream{}
  s.conn = conn
  s.reader = conn
  s.state = 0
  s.buf = cbuf.New()
  s.closed = closed
  s.closed.Store(false)
  s.async_read = false
  return &s
}

func (s *IncomingPacketStream) SetState(state byte) {
  s.state = state
}
func (s *IncomingPacketStream) GetState() byte {
  return s.state
}

func (s *IncomingPacketStream) ReadData() bool {
  // We need to check this here, as this is the only way that connection.RecieveLoop is closed
  if s.closed.Load().(bool) {
    return true
  }
  if s.async_read {
    return false
  } else {
    packet := make([]byte, 64)
    length, err := s.reader.Read(packet)
    if err != nil {
      log.Error("Error while reading tpc packet, closing connection:", err)
      s.closed.Store(true)
      return true
    } else {
      s.buf.Push(packet[:length])
    }
    return false
  }
}

func (s *IncomingPacketStream) GetPacket() *packet.IncomingPacket {
  buf_length := s.buf.Len()
  if buf_length != 0 {
    packet_length, total_read := util.ReadVarInt(s.buf)
    if total_read == -1 {
      return nil
    }
    if buf_length < int(packet_length + total_read) {
      return nil
    }
    if s.compression > 0 {
      uncompressed_length, num_read := util.ReadVarInt(s.buf)
      if uncompressed_length == 0 {
        packet := packet.NewIncomingPacket(s.buf.Pop(int(packet_length - num_read)))
        return packet
      } else {
        compressed_buf := s.buf.Pop(int(packet_length))
        b := bytes.NewReader(compressed_buf)
        r, err := zlib.NewReader(b)
        if err != nil {
          log.Error("Error while decompressing packet:", err)
          log.Error("Packet: ", hex.EncodeToString(compressed_buf))
          return nil
        }
        uncompressed, _ := ioutil.ReadAll(r)
        r.Close()

        return packet.NewIncomingPacket(uncompressed)
      }
    } else {
      return packet.NewIncomingPacket(s.buf.Pop(int(packet_length)))
    }
  }
  return nil
}

func (s *IncomingPacketStream) Close() {
  s.closed.Store(true)
  s.conn.Close()
}

func (s *IncomingPacketStream) SetCipher(stream cipher.Stream) {
  s.reader = cipher.StreamReader {
    S: stream,
    R: s.conn,
  }
}

func (s *IncomingPacketStream) SetCompression(threshold int32) {
  s.compression = threshold
}

func (s *IncomingPacketStream) EnableAsyncRead() {
  if s.async_read {
    panic("Async read is already enabled!")
  }
  s.async_read = true
  go func() {
    for {
      if s.closed.Load().(bool) {
        return
      }
      packet := make([]byte, 64)
      length, err := s.reader.Read(packet)
      if err != nil {
        s.Close()
        return
      } else {
        s.buf.Push(packet[:length])
      }
    }
  }()
}
