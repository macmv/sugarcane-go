package packet_stream

import (
  "os"
  "time"
  "testing"
  "sync/atomic"
  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type buffer struct {
  chunks [][]byte
}

func (b *buffer) Close() error {
  panic("We should never close this buffer!")
  return nil
}

func (b *buffer) Write(d []byte) (int, error) {
  if b.chunks == nil {
    b.chunks = [][]byte{}
  }
  // Make sure we copy the data
  data := make([]byte, len(d))
  copy(data, d)
  b.chunks = append(b.chunks, data)
  return len(d), nil
}

// This test is fairly timing sensitive. The closest gap between calls is 10 milliseconds. So, if you are
// running this test on a very slow computer, then you may run into issues with multithreading
// and these timings. I have never encountered any issues on any of my machines (I have a relatively slow
// laptop) so I hope this doesn't become a problem.
func TestWriteData(t *testing.T) {
  val := &atomic.Value{}
  log := util.NewLog(os.Stdout)
  buffer := &buffer{}
  // We just want to test length overflow, not time overflow, so we set a very large value for time
  out := NewOutgoingPacketStream(buffer, val, log, 16, 50 * time.Millisecond)

  // This is equal to threshold, so we expect this as a chunk
  out.write_data([]byte("aaaaaaaaaaaaaaaa"))
  // This is greater than threshold, so we expect this as a chunk
  out.write_data([]byte("bbbbbbbbbbbbbbbbbbbbbb"))
  // This is smaller than threshold, so we expect this as a part of a chunk
  out.write_data([]byte("cccc"))
  // Still smaller, so we expect it as a part of a chunk
  out.write_data([]byte("dddd"))
  // This plus c and d is larger than a threshold, we we expect c, d and e as a chunk
  out.write_data([]byte("eeeeeeeee"))

  // This makes the test really slow, but it's worth it to test the timeout functionality
  // We first write the ffff chunk, and then wait long enough that it should be flushed automatically.
  // So we expect the ffff and gggg chunks in separate packets.
  out.write_data([]byte("ffff"))
  time.Sleep(100 * time.Millisecond)

  // gggg will be written, and then we wait 25 millis. Once hhhh is written, the length threshold
  // is hit. So, we want to cancel the flush that was about to happen, and write gggg and hhhh in one packet.
  // NOTE: This test has failed every now and then, when the processor is busy. Most of the time,
  // it is consistent.
  out.write_data([]byte("gggg"))
  time.Sleep(25 * time.Millisecond)
  out.write_data([]byte("hhhhhhhhhhhhhhhh"))

  // This should be written in it's own packet, after 50 milliseconds. Since we are only waiting 40 millis,
  // the only way this could be written is if the auto flush (from gggg) was not cancelled.
  out.write_data([]byte("jjjj"))
  time.Sleep(40 * time.Millisecond)

  expected := [][]byte{}
  expected = append(expected, []byte("aaaaaaaaaaaaaaaa"))
  expected = append(expected, []byte("bbbbbbbbbbbbbbbbbbbbbb"))
  expected = append(expected, []byte("ccccddddeeeeeeeee"))
  expected = append(expected, []byte("ffff"))
  expected = append(expected, []byte("gggghhhhhhhhhhhhhhhh"))

  assert.Equal(t, expected, buffer.chunks, "Outgoing packet stream produced the wrong data on write_data")
}
