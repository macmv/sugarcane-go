package packet_stream

import (
  "io"
  // "fmt"
  "time"
  "bytes"
  "sync/atomic"
  "crypto/cipher"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/proxy/types"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/klauspost/compress/zlib"
)

type OutgoingPacketStream struct {
  conn io.WriteCloser
  writer io.Writer
  compression int32
  channel chan *packet.OutgoingPacket
  closed *atomic.Value
  buffer []byte
  secondary_buffer []byte
  zlib_writer *zlib.Writer

  outgoing_buffer []byte
  outgoing_buffer_index int
  length_threshold int
  time_threshold time.Duration
  last_flush_time time.Time

  flusher chan time.Duration
  flush_cancel chan struct{}
}

func NewOutgoingPacketStream(
  conn io.WriteCloser,
  closed *atomic.Value,
  length_threshold int,
  time_threshold time.Duration,
) *OutgoingPacketStream {
  s := OutgoingPacketStream{}
  s.conn = conn
  s.writer = conn
  s.channel = make(chan *packet.OutgoingPacket, 8000)
  s.closed = closed
  s.closed.Store(false)
  // 8kb buffer, used for writing packets.
  // This reduces mallocs for each new packet.
  s.buffer = make([]byte, 1024 * 8)
  // This is used for random other buffers the packet generator might need.
  // It is primarily used in the chunk data packet.
  s.secondary_buffer = make([]byte, 1024 * 8)
  s.zlib_writer = zlib.NewWriter(nil)

  // This will automatically get expanded to as big as it needs to be
  s.outgoing_buffer = []byte{}
  s.length_threshold = length_threshold
  s.time_threshold = time_threshold
  s.flusher = make(chan time.Duration)
  s.flush_cancel = make(chan struct{})
  p := &s
  go p.start_flusher()
  // Wait until the other goroutine is started
  s.flusher <- 0
  return p
}

func (s *OutgoingPacketStream) SendProto(version mpacket.Version, proto *pb.Packet) {
  out := types.OutgoingFromProto(version, proto, s.buffer, s.secondary_buffer)
  if out != nil {
    s.Write(out)
  }
}

func (s *OutgoingPacketStream) NewPacketID(id int32) *packet.OutgoingPacket {
  return packet.NewOutgoingPacketFromSliceID(s.buffer, -1, id)
}

func (s *OutgoingPacketStream) NewPacket() *packet.OutgoingPacket {
  return packet.NewOutgoingPacketFromSlice(s.buffer)
}

func (s *OutgoingPacketStream) listen() {
  panic("Don't call this!")
  for {
    packet := <- s.channel
    if packet != nil {
      data, err := packet.GetData(s.zlib_writer, s.compression)
      if err != nil {
        log.Error("Error while generating packet:", err)
        s.Close()
        return
      }
      s.writer.Write(data)
    }
    if s.closed.Load().(bool) {
      break
    }
  }
}

func (s *OutgoingPacketStream) Write(packet *packet.OutgoingPacket) {
  // s.log.Info("Sending packet ", packet.ID(), ", with old_id: 0x" + fmt.Sprintf("%x", packet.OldID()))
  data, err := packet.GetData(s.zlib_writer, s.compression)
  if err != nil {
    log.Error("Error while generating packet:", err)
    s.Close()
    return
  }
  s.write_data(data)
}

// This is seperate so that we can test the buffering/flushing system without dealing with actual packets
func (s *OutgoingPacketStream) write_data(data []byte) {
  len_of_data := len(data) + s.outgoing_buffer_index
  if len_of_data > len(s.outgoing_buffer) {
    split_index := len(s.outgoing_buffer) - s.outgoing_buffer_index
    copy(s.outgoing_buffer[s.outgoing_buffer_index:], data[:split_index])
    s.outgoing_buffer = append(s.outgoing_buffer, data[split_index:]...)
    s.outgoing_buffer_index += len(data)
  } else {
    copy(s.outgoing_buffer[s.outgoing_buffer_index:], data)
    s.outgoing_buffer_index += len(data)
  }
  // If the total data in the buffer is either too much data, or too old, then we flush the whole buffer
  // If the last write was long enough ago, we also want to write the new data, as the long wait time
  // could mean that there are not many packets going on this conneciton. If that is that case, then Write
  // might not be called for another whole second.
  if s.outgoing_buffer_index >= s.length_threshold || time.Since(s.last_flush_time) > s.time_threshold {
    s.Flush()
  } else {
    // This will trigger a flush in at least time_threshold, or do nothing if there is already
    // a flush scheduled.
    select {
    case s.flusher <- s.time_threshold:
    default:
    }
  }
}

func (s *OutgoingPacketStream) start_flusher() {
  for {
    d := <-s.flusher
    // This will finish if either d time has passed, or if flush_cancel is written to.
    // If enough time has passed, then it will flush the stream. If something else flushes
    // the stream, then the flush_cancel will be written to.
    if s.closed.Load().(bool) {
      break
    }
    select {
    case <-s.flush_cancel:
    case <-time.After(d):
      s.Flush()
    }
  }
}

func (s *OutgoingPacketStream) Flush() {
  if s.outgoing_buffer_index == 0 {
    return
  }
  // This will cancel any flush that was waiting to happen, or do nothing if there is no flush waiting.
  select {
  case s.flush_cancel <- struct{}{}:
  default:
  }
  s.writer.Write(s.outgoing_buffer[:s.outgoing_buffer_index])
  // We can leave s.outgoing_buffer as garbage data, as it will all be overriten in future Write calls
  s.outgoing_buffer_index = 0
  s.last_flush_time = time.Now()
}

func (s *OutgoingPacketStream) EnableAsyncSend() {
  log.Warn("Async sending has been disabled for outgoing packet streams!")
}

func (s *OutgoingPacketStream) SetCipher(stream cipher.Stream) {
  s.writer = cipher.StreamWriter{
    S: stream,
    W: s.conn,
  }
}

func (s *OutgoingPacketStream) SetCompression(threshold int32) {
  s.compression = threshold
}

// This is not thread safe! Do not call Close() on different threads close to each other.
// If you call it once, it is safe to call again, but you should not call close twice at the same time.
func (s *OutgoingPacketStream) Close() {
  if !s.closed.Load().(bool) {
    s.closed.Store(true)
    s.flusher <- 0 // Makes sure the flusher goroutine closes
    s.conn.Close()
    // Call reset here with a valid io.Writer, so that Close does not panic
    s.zlib_writer.Reset(&bytes.Buffer{})
    s.zlib_writer.Close()
  }
}
