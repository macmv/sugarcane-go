package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

// Takes a 1.12.2 spec 
func Make1_13Types(spec *registry.Registry) *registry.Registry {
  // Because mojang
  spec.Move(int32(mpacket.Serverbound_TabComplete), 0x05)
  // Query block NBT. This is used when pressing shift+f3+i and looking at a block.
  spec.Insert(0x01, int32(mpacket.Serverbound_QueryBlockNBT), func(in *packet.IncomingPacket) {
  })
  spec.Insert(0x0b, int32(mpacket.Serverbound_EditBook), func(in *packet.IncomingPacket) {
  })
  // Query entity NBT. Should probably add this, but it doesn't exist for newer versions.
  // This is used when pressing shift+f3+i and looking at an entity.
  spec.Insert(0x0c, -1, nil)
  // Middle click when in survival.
  spec.Insert(0x15, int32(mpacket.Serverbound_PickItem), func(in *packet.IncomingPacket) {
  })
  spec.Insert(0x1c, int32(mpacket.Serverbound_NameItem), func(in *packet.IncomingPacket) {
  })
  spec.Insert(0x1f, int32(mpacket.Serverbound_SelectTrade), func(in *packet.IncomingPacket) {
  })
  spec.Insert(0x20, int32(mpacket.Serverbound_SetBeaconEffect), func(in *packet.IncomingPacket) {
  })
  // Why the hell is this different from command block minecart!
  spec.Insert(0x22, int32(mpacket.Serverbound_UpdateCommandBlock), func(in *packet.IncomingPacket) {
  })
  spec.Insert(0x23, int32(mpacket.Serverbound_UpdateCommandBlockMinecart), func(in *packet.IncomingPacket) {
  })
  // Ah yes. More fun.
  spec.Insert(0x25, int32(mpacket.Serverbound_UpdateStructureBlock), func(in *packet.IncomingPacket) {
  })
  return spec
}
