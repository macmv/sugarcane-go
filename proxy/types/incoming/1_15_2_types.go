package incoming

// import (
//   "gitlab.com/macmv/sugarcane/minecraft/packet"
// )
// 
// func setup_1_15_2_IncomingPacketTypes() {
//   packet_types := make(map[packet.Serverbound]func(*IncomingPacket))
//   packet_types[0x00] = func(in *IncomingPacket) { // teleport confirm
//     in.CopyVarInt()
//   }
//   packet_types[0x03] = func(in *IncomingPacket) { // chat
//     in.CopyProtocolString()
//   }
//   packet_types[0x04] = func(in *IncomingPacket) { // client status (statistics request, or respawn request)
//     in.CopyVarInt()
//   }
//   packet_types[0x05] = func(in *IncomingPacket) { // client settings
//     in.CopyProtocolString()
//   }
//   packet_types[0x06] = func(in *IncomingPacket) { // tab complete
//     in.CopyVarInt()
//     in.CopyProtocolString()
//   }
//   packet_types[0x09] = func(in *IncomingPacket) { // click window
//     in.CopyByte()
//     in.CopyShort()
//     in.CopyByte()
//     in.CopyShort()
//     in.CopyVarInt()
//     in.CopyItem()
//   }
//   packet_types[0x0b] = func(in *IncomingPacket) { // plugin message
//     in.CopyProtocolString()
//   }
//   packet_types[0x0e] = func(in *IncomingPacket) { // interact entity
//     in.CopyVarInt()
//     val := in.CopyVarInt()
//     if val == 2 {
//       in.CopyFloat()
//       in.CopyFloat()
//       in.CopyFloat()
//       in.CopyVarInt()
//     } else if val == 0 {
//       in.CopyVarInt()
//     }
//   }
//   packet_types[0x0f] = func(in *IncomingPacket) { // keep alive
//     in.WriteInt(int32(in.ReadLong()))
//   }
//   packet_types[0x11] = func(in *IncomingPacket) { // player position
//     in.CopyDouble()
//     in.CopyDouble()
//     in.CopyDouble()
//     in.CopyBool()
//   }
//   packet_types[0x12] = func(in *IncomingPacket) { // player position and rotation
//     in.CopyDouble()
//     in.CopyDouble()
//     in.CopyDouble()
//     in.CopyFloat()
//     in.CopyFloat()
//     in.CopyBool()
//   }
//   packet_types[0x13] = func(in *IncomingPacket) { // player rotation
//     in.CopyFloat()
//     in.CopyFloat()
//     in.CopyBool()
//   }
//   packet_types[0x14] = func(in *IncomingPacket) { // player on ground change
//     in.CopyBool()
//   }
//   packet_types[0x19] = func(in *IncomingPacket) { // player abilities
//     in.CopyByte()
//     in.CopyFloat()
//     in.CopyFloat()
//   }
//   packet_types[0x1a] = func(in *IncomingPacket) { // player digging
//     in.CopyVarInt()
//     in.CopyPosition()
//     in.CopyByte()
//   }
//   packet_types[0x1b] = func(in *IncomingPacket) { // entity action (sprint, croutch, get in bed, start elytra, etc)
//     in.CopyVarInt()
//     in.CopyVarInt()
//     in.CopyVarInt()
//   }
//   packet_types[0x23] = func(in *IncomingPacket) { // held item change
//     in.CopyShort()
//   }
//   packet_types[0x26] = func(in *IncomingPacket) { // creative inventory action
//     in.CopyShort()
//     in.CopyItem()
//   }
//   packet_types[0x2a] = func(in *IncomingPacket) { // animation (arm swing)
//     in.CopyVarInt()
//   }
//   packet_types[0x2c] = func(in *IncomingPacket) { // block place
//     in.CopyVarInt()
//     in.CopyPosition()
//     in.CopyVarInt()
//     in.CopyFloat()
//     in.CopyFloat()
//     in.CopyFloat()
//     in.CopyBool()
//   }
//   packet_types[0x2d] = func(in *IncomingPacket) { // use item (right click with item)
//     in.CopyVarInt()
//   }
//   incoming_packet_types[578] = &incoming_version{packets: packet_types, same_id: true}
// }
