package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

// Takes a 1.12 spec (1.12.1 and 1.12 are the same)
func Make1_12_2Types(spec *registry.Registry) *registry.Registry {
  spec.Move(int32(mpacket.Serverbound_CraftRecipeRequest), 0x12)
  spec.Set(int32(mpacket.Serverbound_CraftRecipeRequest), func(in *packet.IncomingPacket) { // 0x12
    in.CopyByte() // Window id
    in.CopyVarInt() // Recipe ID
    in.CopyBool() // Make all
  })
  spec.Set(int32(mpacket.Serverbound_KeepAlive), func(in *packet.IncomingPacket) { // 0x0b
    in.WriteInt(int32(in.ReadLong())) // ID
  })
  return spec
}
