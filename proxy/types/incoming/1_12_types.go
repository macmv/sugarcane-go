package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

func Make1_12Types() *registry.Registry {
  spec := registry.New()
  spec.Add(int32(mpacket.Serverbound_TeleportConfirm), func(in *packet.IncomingPacket) { // 0x00
    in.CopyVarInt()
  })
  // In this version, this packet is very different. Not going to bother implementing.
  spec.Add(int32(mpacket.Serverbound_CraftRecipeRequest), nil) // 0x01
  spec.Add(int32(mpacket.Serverbound_TabComplete), func(in *packet.IncomingPacket) { // 0x02
    in.CopyProtocolString() // text
    in.CopyBool() // assume command
    if in.ReadBool() { // has position
      in.CopyPosition() // position of the block that the player is looking at
    }
  })
  spec.Add(int32(mpacket.Serverbound_ChatMessage), func(in *packet.IncomingPacket) { // 0x03
    in.CopyProtocolString()
  })
  spec.Add(int32(mpacket.Serverbound_ClientStatus), func(in *packet.IncomingPacket) { // 0x04
    // statistics request, or respawn request
    in.CopyVarInt()
  })
  spec.Add(int32(mpacket.Serverbound_ClientSettings), func(in *packet.IncomingPacket) { // 0x05
    in.CopyProtocolString() // Lang
    in.CopyByte() // View distance
    in.CopyVarInt() // Chat mode
    in.CopyBool() // Enable chat colors
    in.CopyByte() // Skin parts bitmask
    in.CopyVarInt() // Main hand. 0 -> left, 1 -> right
  })
  spec.Add(int32(mpacket.Serverbound_WindowConfirmation), func(in *packet.IncomingPacket) { // 0x06
    in.CopyByte() // Window id
    in.CopyShort() // Action number
    in.CopyBool() // If action was accepted
  })
  // In this version, it is only used to enchant items
  spec.Add(int32(mpacket.Serverbound_ClickWindowButton), func(in *packet.IncomingPacket) { // 0x07
    in.CopyByte() // Window id
    in.CopyByte() // Position in the enchant table window
    // In newer versions, this is the same format, but depending on the open window,
    // the second byte can mean different things.
  })
  // Clicking on an item in a window
  spec.Add(int32(mpacket.Serverbound_ClickWindow), func(in *packet.IncomingPacket) { // 0x08
    in.CopyByte() // Window ID
    in.CopyShort() // Slot
    in.CopyByte() // Button
    in.CopyShort() // Action number
    in.CopyVarInt() // Mode
    in.CopyItem() // Item that was clicked
  })
  spec.Add(int32(mpacket.Serverbound_CloseWindow), func(in *packet.IncomingPacket) { // 0x09
    in.CopyByte() // Window ID
  })
  spec.Add(int32(mpacket.Serverbound_PluginMessage), func(in *packet.IncomingPacket) { // 0x0a
    in.CopyProtocolString() // Channel
    in.CopyRemainingBytes() // Copy all the bytes in the packet
  })
  spec.Add(int32(mpacket.Serverbound_InteractEntity), func(in *packet.IncomingPacket) { // 0x0b
    in.CopyVarInt() // Target EID
    val := in.CopyVarInt() // Type (0 -> interact, 1 -> attack, 2 -> interact at)
    if val == 2 {
      // Click coordinates
      in.CopyFloat()
      in.CopyFloat()
      in.CopyFloat()
    }
    if val == 0 || val == 1 {
      in.CopyVarInt() // Which hand (0 -> main hand, 1 -> off hand)
    }
  })
  spec.Add(int32(mpacket.Serverbound_KeepAlive), func(in *packet.IncomingPacket) { // 0x0c
    in.CopyVarInt() // ID
  })
  spec.Add(int32(mpacket.Serverbound_PlayerOnGround), func(in *packet.IncomingPacket) { // 0x0d
    in.CopyBool() // On ground
  })
  spec.Add(int32(mpacket.Serverbound_PlayerPosition), func(in *packet.IncomingPacket) { // 0x0e
    in.CopyDouble() // X
    in.CopyDouble() // Y (feet pos)
    in.CopyDouble() // Z
    in.CopyBool() // On ground
  })
  spec.Add(int32(mpacket.Serverbound_PlayerPositionAndRotation), func(in *packet.IncomingPacket) { // 0x0f
    in.CopyDouble() // X
    in.CopyDouble() // Y (feet pos)
    in.CopyDouble() // Z
    in.CopyFloat() // Yaw
    in.CopyFloat() // Pitch
    in.CopyBool() // On ground
  })
  spec.Add(int32(mpacket.Serverbound_PlayerRotation), func(in *packet.IncomingPacket) { // 0x10
    in.CopyFloat() // Yaw
    in.CopyFloat() // Pitch
    in.CopyBool() // On ground
  })
  spec.Add(int32(mpacket.Serverbound_VehicleMove), func(in *packet.IncomingPacket) { // 0x11
    // Used for all entites (including boats)
    in.CopyDouble() // X
    in.CopyDouble() // Y (feet pos)
    in.CopyDouble() // Z
    in.CopyFloat() // Yaw
    in.CopyFloat() // Pitch
  })
  spec.Add(int32(mpacket.Serverbound_SteerBoat), func(in *packet.IncomingPacket) { // 0x12
    // Only changes the paddle animation, does not move it
    in.CopyBool() // Right paddling
    in.CopyBool() // Left paddling
  })
  spec.Add(int32(mpacket.Serverbound_PlayerAbilities), func(in *packet.IncomingPacket) { // 0x13
    in.CopyByte() // Flags
    in.CopyFloat() // Flying speed
    in.CopyFloat() // Walking speed
  })
  spec.Add(int32(mpacket.Serverbound_PlayerDigging), func(in *packet.IncomingPacket) { // 0x14
    in.CopyVarInt() // Status
    in.CopyPosition() // Location where the player is digging
    in.CopyByte() // Face which is being hit, or 255 for no face.
  })
  spec.Add(int32(mpacket.Serverbound_EntityAction), func(in *packet.IncomingPacket) { // 0x15
    in.CopyVarInt() // EID
    in.CopyVarInt() // Action id
    in.CopyVarInt() // Only used for start horse jump; if action is anything else, this is 0
  })
  spec.Add(int32(mpacket.Serverbound_SteerVehicle), func(in *packet.IncomingPacket) { // 0x16
    // Sort of an input packet. Unsure when this is used.
    in.CopyFloat() // Sideways
    in.CopyFloat() // Forward
    in.CopyByte() // Flags
  })
  spec.Add(int32(mpacket.Serverbound_RecipeBookData), func(in *packet.IncomingPacket) { // 0x17
    // Very old version of this packet; most fields are missing
    t := in.CopyVarInt()
    switch t {
    case 0:
      // A recipe id; modern versions use a string
      in.CopyInt()
    case 1:
      // Status of the crafting book
      in.CopyBool() // Crafting book open
      in.CopyBool() // Crafting book filter
      // Modern versions have many more fields, for the other recipe books.
    }
  })
  spec.Add(int32(mpacket.Serverbound_ResourcePackStatus), func(in *packet.IncomingPacket) { // 0x18
    // Result; 0 -> successful, 1 -> declined, 2 -> failed downloaded, 3 -> accepted.
    in.CopyVarInt()
  })
  spec.Add(int32(mpacket.Serverbound_AdvancementTab), func(in *packet.IncomingPacket) { // 0x19
    // 0 -> opened tab, 1 -> closed
    t := in.CopyVarInt()
    if t == 0 {
      // Advancement tab that was opened.
      in.CopyProtocolString()
    }
  })
  spec.Add(int32(mpacket.Serverbound_HeldItemChange), func(in *packet.IncomingPacket) { // 0x1a
    in.CopyShort() // Slot
  })
  spec.Add(int32(mpacket.Serverbound_CreativeInventoryAction), func(in *packet.IncomingPacket) { // 0x1b
    in.CopyShort() // Slot
    in.CopyItem() // Item that was clicked
  })
  spec.Add(int32(mpacket.Serverbound_UpdateSign), func(in *packet.IncomingPacket) { // 0x1c
    in.CopyPosition()
    in.CopyProtocolString() // Lines of sign
    in.CopyProtocolString()
    in.CopyProtocolString()
    in.CopyProtocolString()
  })
  spec.Add(int32(mpacket.Serverbound_Animation), func(in *packet.IncomingPacket) { // 0x1d
    in.CopyVarInt() // Hand
  })
  spec.Add(int32(mpacket.Serverbound_Spectate), func(in *packet.IncomingPacket) { // 0x1e
    in.CopyUUID() // Player you are spectating
  })
  spec.Add(int32(mpacket.Serverbound_PlayerBlockPlace), func(in *packet.IncomingPacket) { // 0x1f
    // In newer versions, hand comes before face
    x, y, z := in.ReadPosition() // Pos
    face := in.ReadVarInt()
    hand := in.ReadVarInt()
    in.WriteInt(hand)
    in.WritePosition(x, y, z)
    in.WriteInt(face)
    in.CopyFloat() // X position of click
    in.CopyFloat() // Y position of click
    in.CopyFloat() // Z position of click
    // In newer versions, there is a head inside block field,
    // which the server knows is not always sent. So it is ok
    // to leave this bool blank, to specify that this version
    // does not send that.
  })
  spec.Add(int32(mpacket.Serverbound_UseItem), func(in *packet.IncomingPacket) { // 0x20
    in.CopyVarInt() // Hand
  })
  return spec
}
