package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

// Takes a 1.13 spec
func Make1_13_1Types(spec *registry.Registry) *registry.Registry {
  // Added a hand field to this packet
  spec.Set(int32(mpacket.Serverbound_EditBook), func(in *packet.IncomingPacket) {
  })
  return spec
}
