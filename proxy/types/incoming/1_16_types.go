package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

// Takes a 1.14 spec
func Make1_16Types(spec *registry.Registry) *registry.Registry {
  // Called generate structure. Unclear if this is needed.
  spec.Insert(0x0f, -1, nil)
  // They removed flying speed and walking speed from this packet
  spec.Set(int32(mpacket.Serverbound_PlayerAbilities), func(in *packet.IncomingPacket) {
    in.CopyByte() // Flags
  })
  return spec
}
