package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

// Takes a 1.13.1 spec
func Make1_14Types(spec *registry.Registry) *registry.Registry {
  // Sent when the client changes the difficulty
  spec.Insert(0x02, int32(mpacket.Serverbound_SetDifficulty), func(in *packet.IncomingPacket) {
  })
  // Send when the client clicks the little lock icon next to the difficulty
  spec.Insert(0x10, int32(mpacket.Serverbound_LockDifficulty), func(in *packet.IncomingPacket) {
  })
  // They moved this after the other movement packets
  spec.Move(int32(mpacket.Serverbound_PlayerOnGround), 0x14)
  // More fun times
  spec.Insert(0x27, int32(mpacket.Serverbound_UpdateJigsawBlock), func(in *packet.IncomingPacket) {
  })
  // They changed the order of the fields in this packet
  spec.Set(int32(mpacket.Serverbound_PlayerBlockPlace), func(in *packet.IncomingPacket) {
    in.CopyVarInt() // Hand
    in.CopyPosition() // Block location
    in.CopyVarInt() // Face
    in.CopyFloat() // Cursor X
    in.CopyFloat() // Cursor Y
    in.CopyFloat() // Cursor Z
    in.CopyBool() // Inside block?
  })
  return spec
}
