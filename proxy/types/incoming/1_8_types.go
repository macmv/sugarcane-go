package incoming

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
)

func Make1_8Types() *registry.Registry {
  spec := registry.New()
  spec.Add(int32(mpacket.Serverbound_KeepAlive), func(in *packet.IncomingPacket) { // 0x00
    in.CopyVarInt()
  })
  spec.Add(int32(mpacket.Serverbound_ChatMessage), func(in *packet.IncomingPacket) { // 0x01
    in.CopyProtocolString()
  })
  // Called use entity
  spec.Add(int32(mpacket.Serverbound_InteractEntity), func(in *packet.IncomingPacket) { // 0x02
    in.CopyVarInt()
    t := in.CopyVarInt() // 0 -> interact, 1 -> attack, 2 -> interact at
    if t == 2 {
      // Interact at location
      in.CopyFloat()
      in.CopyFloat()
      in.CopyFloat()
    }
  })
  spec.Add(int32(mpacket.Serverbound_PlayerOnGround), func(in *packet.IncomingPacket) { // 0x03
    in.CopyBool()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerPosition), func(in *packet.IncomingPacket) { // 0x04
    in.CopyDouble()
    in.CopyDouble()
    in.CopyDouble()
    in.CopyBool()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerRotation), func(in *packet.IncomingPacket) { // 0x05
    in.CopyFloat()
    in.CopyFloat()
    in.CopyBool()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerPositionAndRotation), func(in *packet.IncomingPacket) { // 0x06
    in.CopyDouble()
    in.CopyDouble()
    in.CopyDouble()
    in.CopyFloat()
    in.CopyFloat()
    in.CopyBool()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerDigging), func(in *packet.IncomingPacket) { // 0x07
    in.WriteInt(int32(in.ReadByte()))
    in.CopyPosition()
    in.CopyByte()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerBlockPlace), func(in *packet.IncomingPacket) { // 0x08
    x, y, z := in.ReadPosition()
    face := in.ReadByte()

    // If the packet has x, y, z and face all set to -1, then it is a UseItem packet
    // If the face is set to 255 (-1), we can just assume the position is -1 on all axis
    if face == 255 {
      in.SetID(mpacket.Serverbound_UseItem)
      in.WriteInt(0) // main hand
      return
    }
    // main hand
    in.WriteInt(0)
    in.WritePosition(x, y, z)
    in.WriteInt(int32(face))

    // the item they used (ignored by server)
    in.ReadItem()

    // cursor position is a float in new version, so we need to convert it
    in.WriteFloat(float32(in.ReadByte()) / 256)
    in.WriteFloat(float32(in.ReadByte()) / 256)
    in.WriteFloat(float32(in.ReadByte()) / 256)

    // not inside block
    in.WriteBool(false)
  })
  spec.Add(int32(mpacket.Serverbound_HeldItemChange), func(in *packet.IncomingPacket) { // 0x09
    in.CopyShort()
  })
  // Used for player arm swing
  spec.Add(int32(mpacket.Serverbound_Animation), func(in *packet.IncomingPacket) { // 0x0a
    in.WriteInt(0) // Main hand
  })
  spec.Add(int32(mpacket.Serverbound_EntityAction), func(in *packet.IncomingPacket) { // 0x0b
    in.CopyVarInt() // EID of player
    in.CopyVarInt() // Action
    in.CopyVarInt() // Used by horse jump
  })
  spec.Add(int32(mpacket.Serverbound_SteerVehicle), func(in *packet.IncomingPacket) { // 0x0c
    // Sort of an input packet. Unsure when this is used.
    in.CopyFloat() // Sideways
    in.CopyFloat() // Forward
    in.CopyByte() // Flags
  })
  spec.Add(int32(mpacket.Serverbound_CloseWindow), func(in *packet.IncomingPacket) { // 0x0d
    in.CopyByte() // Window ID
  })
  spec.Add(int32(mpacket.Serverbound_ClickWindow), func(in *packet.IncomingPacket) { // 0x0e
    in.CopyByte() // Window ID
    in.CopyShort() // Slot
    in.CopyByte() // Button
    in.CopyShort() // Action number
    in.WriteInt(int32(in.ReadByte())) // Mode
    in.CopyItem() // Item that was clicked
  })
  spec.Add(int32(mpacket.Serverbound_WindowConfirmation), func(in *packet.IncomingPacket) { // 0x0f
    in.CopyByte() // Window ID
    in.CopyShort() // Action number
    in.CopyBool() // Accepted
  })
  spec.Add(int32(mpacket.Serverbound_CreativeInventoryAction), func(in *packet.IncomingPacket) { // 0x10
    in.CopyShort() // Slot
    in.CopyItem() // New item
  })
  // In this version, it is only used to enchant items
  spec.Add(int32(mpacket.Serverbound_ClickWindowButton), func(in *packet.IncomingPacket) { // 0x11
    in.CopyByte() // Window id
    in.CopyByte() // Position in the enchant table window
    // In newer versions, this is the same format, but depending on the open window,
    // the second byte can mean different things.
  })
  spec.Add(int32(mpacket.Serverbound_UpdateSign), func(in *packet.IncomingPacket) { // 0x12
    in.CopyPosition() // Block position
    in.CopyProtocolString() // Lines of sign
    in.CopyProtocolString()
    in.CopyProtocolString()
    in.CopyProtocolString()
  })
  spec.Add(int32(mpacket.Serverbound_PlayerAbilities), func(in *packet.IncomingPacket) { // 0x13
    in.CopyByte() // Flags
    in.CopyFloat() // Flying speed
    in.CopyFloat() // Walking speed
  })
  spec.Add(int32(mpacket.Serverbound_TabComplete), func(in *packet.IncomingPacket) { // 0x14
    in.CopyProtocolString() // Text
    // There is no assume command field in this version
    if in.ReadBool() { // Has position
      in.CopyPosition() // Position of the block that the player is looking at
    }
  })
  spec.Add(int32(mpacket.Serverbound_ClientSettings), func(in *packet.IncomingPacket) { // 0x15
    in.CopyProtocolString() // Lang
    in.CopyByte() // View distance
    in.WriteInt(int32(in.ReadByte())) // Chat mode (varint in newer versions)
    in.CopyBool() // Enable chat colors
    in.CopyByte() // Skin parts bitmask
    // Main hand setting doesn't exist in this version
    in.WriteInt(1) // Default to right hand
  })
  spec.Add(int32(mpacket.Serverbound_ClientStatus), func(in *packet.IncomingPacket) { // 0x16
    // Statistics request, or respawn request
    in.CopyVarInt()
  })
  spec.Add(int32(mpacket.Serverbound_PluginMessage), func(in *packet.IncomingPacket) { // 0x17
    in.CopyProtocolString() // Channel
    in.CopyRemainingBytes() // Copy all the bytes in the packet
  })
  spec.Add(int32(mpacket.Serverbound_Spectate), func(in *packet.IncomingPacket) { // 0x18
    in.CopyUUID() // Player you are spectating (can also be entity uuid)
  })
  spec.Add(int32(mpacket.Serverbound_ResourcePackStatus), func(in *packet.IncomingPacket) { // 0x18
    // This is the hash sent by Clientbound_ResourcePack. This was removed in later versions,
    // so the server will ignore it.
    in.ReadProtocolString()
    // Result; 0 -> successful, 1 -> declined, 2 -> failed downloaded, 3 -> accepted.
    in.CopyVarInt()
  })

  return spec
}
