package types

import (
  "fmt"

  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/proxy/types/incoming"
  "gitlab.com/macmv/sugarcane/proxy/types/outgoing"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// type outgoing_version struct {
//   packets map[packet.Clientbound]func(*pb.Packet, *OutgoingPacket)
//   same_id bool
// }
//
// type incoming_version struct {
//   packets map[packet.Serverbound]func(*IncomingPacket)
//   same_id bool
// }

var outgoing_packet_specs map[mpacket.Version]*registry.Registry
var incoming_packet_specs map[mpacket.Version]*registry.Registry

func init() {
  outgoing_packet_specs = make(map[mpacket.Version]*registry.Registry)
  incoming_packet_specs = make(map[mpacket.Version]*registry.Registry)

  // 1.8 is the oldest version I am going to support.
  // The combat makes this version almost essential.
  // Most of hypixel's players use 1.8, so I think
  // it is worthwhile to implement it.
  incoming_packet_specs[mpacket.V1_8] = incoming.Make1_8Types()
  outgoing_packet_specs[mpacket.V1_8] = outgoing.Make1_8Types()

  // 1.12.2 is very different from 1.8
  incoming_packet_specs[mpacket.V1_12] = incoming.Make1_12Types()
  outgoing_packet_specs[mpacket.V1_12] = outgoing.Make1_12Types()
  incoming_packet_specs[mpacket.V1_12_1] = incoming_packet_specs[mpacket.V1_12]
  outgoing_packet_specs[mpacket.V1_12_1] = outgoing_packet_specs[mpacket.V1_12]
  incoming_packet_specs[mpacket.V1_12_2] = incoming.Make1_12_2Types(incoming_packet_specs[mpacket.V1_12].Copy())
  outgoing_packet_specs[mpacket.V1_12_2] = outgoing.Make1_12_2Types(outgoing_packet_specs[mpacket.V1_12].Copy())

  incoming_packet_specs[mpacket.V1_13] = incoming.Make1_13Types(incoming_packet_specs[mpacket.V1_12_2].Copy())
  outgoing_packet_specs[mpacket.V1_13] = outgoing.Make1_13Types(outgoing_packet_specs[mpacket.V1_12_2].Copy())
  incoming_packet_specs[mpacket.V1_13_1] = incoming.Make1_13_1Types(incoming_packet_specs[mpacket.V1_13].Copy())
  outgoing_packet_specs[mpacket.V1_13_1] = outgoing_packet_specs[mpacket.V1_13]
  incoming_packet_specs[mpacket.V1_13_2] = incoming_packet_specs[mpacket.V1_13_1]
  outgoing_packet_specs[mpacket.V1_13_2] = outgoing_packet_specs[mpacket.V1_13_1]

  incoming_packet_specs[mpacket.V1_14] = incoming.Make1_14Types(incoming_packet_specs[mpacket.V1_13_2].Copy())
  outgoing_packet_specs[mpacket.V1_14] = outgoing.Make1_14Types(outgoing_packet_specs[mpacket.V1_13_2].Copy())
  incoming_packet_specs[mpacket.V1_14_1] = incoming_packet_specs[mpacket.V1_14]
  outgoing_packet_specs[mpacket.V1_14_1] = outgoing_packet_specs[mpacket.V1_14]
  incoming_packet_specs[mpacket.V1_14_2] = incoming_packet_specs[mpacket.V1_14_1]
  outgoing_packet_specs[mpacket.V1_14_2] = outgoing_packet_specs[mpacket.V1_14_1]
  incoming_packet_specs[mpacket.V1_14_3] = incoming_packet_specs[mpacket.V1_14_2]
  outgoing_packet_specs[mpacket.V1_14_3] = outgoing.Make1_14_3Types(outgoing_packet_specs[mpacket.V1_14_2].Copy())
  incoming_packet_specs[mpacket.V1_14_4] = incoming_packet_specs[mpacket.V1_14_3]
  outgoing_packet_specs[mpacket.V1_14_4] = outgoing.Make1_14_4Types(outgoing_packet_specs[mpacket.V1_14_3].Copy())

  incoming_packet_specs[mpacket.V1_15] = incoming_packet_specs[mpacket.V1_14_4]
  outgoing_packet_specs[mpacket.V1_15] = outgoing.Make1_15Types(outgoing_packet_specs[mpacket.V1_14_4].Copy())
  incoming_packet_specs[mpacket.V1_15_1] = incoming_packet_specs[mpacket.V1_15]
  outgoing_packet_specs[mpacket.V1_15_1] = outgoing_packet_specs[mpacket.V1_15]
  incoming_packet_specs[mpacket.V1_15_2] = incoming_packet_specs[mpacket.V1_15_1]
  outgoing_packet_specs[mpacket.V1_15_2] = outgoing_packet_specs[mpacket.V1_15_1]

  incoming_packet_specs[mpacket.V1_16] = incoming.Make1_16Types(incoming_packet_specs[mpacket.V1_15_2].Copy())
  outgoing_packet_specs[mpacket.V1_16] = outgoing.Make1_16Types(outgoing_packet_specs[mpacket.V1_15_2].Copy())
  incoming_packet_specs[mpacket.V1_16_1] = incoming_packet_specs[mpacket.V1_16]
  outgoing_packet_specs[mpacket.V1_16_1] = outgoing_packet_specs[mpacket.V1_16]
  incoming_packet_specs[mpacket.V1_16_2] = incoming_packet_specs[mpacket.V1_16_1]
  outgoing_packet_specs[mpacket.V1_16_2] = outgoing.Make1_16_2Types(outgoing_packet_specs[mpacket.V1_16_1].Copy())
  incoming_packet_specs[mpacket.V1_16_3] = incoming_packet_specs[mpacket.V1_16_2]
  outgoing_packet_specs[mpacket.V1_16_3] = outgoing_packet_specs[mpacket.V1_16_2]
  incoming_packet_specs[mpacket.V1_16_4] = incoming_packet_specs[mpacket.V1_16_3]
  outgoing_packet_specs[mpacket.V1_16_4] = outgoing_packet_specs[mpacket.V1_16_3]

  s := outgoing_packet_specs[mpacket.V1_16_2]
  for index := range s.List() {
    _, id, _ := s.GetReverse(int32(index))
    _, other, _ := s.Get(id)
    if int32(index) != other {
      fmt.Printf("Invalid index! Expected: 0x%x, got: 0x%x\n", index, other)
    }
    fmt.Printf("Index: 0x%x, ID: %v\n", index, mpacket.Clientbound(id))
  }

  //incoming_packet_specs[578] = incoming.Make1_15_2Types(incoming_packet_specs[340].Copy())
  //outgoing_packet_specs[578] = outgoing.Make1_15_2Types(outgoing_packet_specs[340].Copy())
}

func HasVersion(v mpacket.Version) bool {
  _, ok := outgoing_packet_specs[v]
  return ok
}

func IncomingToProto(p *packet.IncomingPacket, version mpacket.Version) (*pb.Packet, error) {
  spec, ok := incoming_packet_specs[version]
  if !ok {
    return nil, fmt.Errorf("Unknown version while trying to convert packet to protobuf: %d", version)
  }
  function, id, ok := spec.GetReverse(p.ID())
  if !ok {
    return nil, fmt.Errorf("Unknown incoming recieved from client: 0x%x", p.ID())
  }
  p.SetVersion(version)
  // The handler can change the packet id, so we want to use that.
  p.SetID(mpacket.Serverbound(id))
  function.(func(*packet.IncomingPacket))(p)
  // If the handler changed the packet id, we want to use that
  p.Proto.Id = int32(p.NewID())
  if p.Err != nil {
    return nil, fmt.Errorf("Error while parsing %v, old id: 0x%x: %w", p.NewID(), p.ID(), p.Err)
  }
  return p.Proto, nil
}

func OutgoingFromProto(version mpacket.Version, proto *pb.Packet, buffer, garbage_buffer []byte) *packet.OutgoingPacket {
  spec, ok := outgoing_packet_specs[version]
  if !ok {
    panic("Unknown version while trying to convert protobuf to packet: " + fmt.Sprintf("%d", version))
  }
  id := mpacket.Clientbound(proto.Id)
  function, old_id, ok := spec.Get(int32(id))
  if !ok {
    // fmt.Printf("Unknown packet recieved from server: 0x%x\n", proto.Id)
    return nil
  }
  fmt.Printf("Sending packet 0x%x (%v)\n", old_id, id)
  out := packet.NewOutgoingPacketFromSliceID(buffer, id, old_id)
  out.GarbageBuffer = garbage_buffer
  out.SetVersion(version)
  function.(func(*pb.Packet, *packet.OutgoingPacket))(proto, out)
  return out
}

// This generates a disconnect packet for the given version, during play state
// If the version is invalid, then a login state disconnect packet is sent, which is always id 0, and doesn't use this function
// This is used for if the backend server disconnects, and we need to disconnect a client for some reason.
// If a backend server crashes, we can usually just redirect the client, so this is not used very often.
// This is only used if the proxy cannot find any server to connect the client to, which should never happen.
func DisconnectPacket(version mpacket.Version, message *util.Chat) *packet.OutgoingPacket {
  spec, ok := outgoing_packet_specs[version]
  if !ok {
    panic("Unknown version while trying to convert protobuf to packet: " + fmt.Sprintf("%d", version))
  }
  out := packet.NewOutgoingPacketFromSlice(make([]byte, 10))

  id := mpacket.Clientbound_Disconnect
  function, old_id, _ := spec.Get(int32(id))
  out.WriteVarInt(old_id)
  function.(func(*pb.Packet, *packet.OutgoingPacket))(&pb.Packet{Id: int32(id), Strings: []string{message.ToJSON()}}, out)
  return out
}
