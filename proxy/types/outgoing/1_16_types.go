package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

// Takes a 1.15 spec
func Make1_16Types(spec *registry.Registry) *registry.Registry {
  // Why did they remove this
  spec.Remove(int32(mpacket.Clientbound_SpawnWeatherEntity))
  spec.Set(int32(mpacket.Clientbound_ChatMessage), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0e
    out.WriteString(packet.Strings[0]) // Chat json
    out.WriteByte(packet.Bytes[0]) // Position 0 -> chat box, 1 - > system message, 2 -> above hotbar
    // CHANGE: Added the sender, so clients can disable chat from certain people
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
  })
  // Yay more fun times. This time they added a field which makes the client
  // recalculate lighting data.
  spec.Set(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x21
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true) // Full chunk
    // CHANGE: Added this bool
    out.WriteBool(true) // Ignore old data

    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask) // bitmask

    heightmap := nbt.NewCompoundTag("")
    heightmap.CompoundAddChild("MOTION_BLOCKING", 0x0c).WriteLongArray(chunk.Heightmap)
    out.WriteByteArray(heightmap.Serialize())

    // Biome data. CHANGE: Its 3D now. Yay
    for i := 0; i < 1024; i++ {
      out.WriteInt(127) // void (grass looks ugly, should change)
    }

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteShort(uint16(section.NonAirBlocks))
      buf.WriteByte(byte(section.BitsPerBlock))
      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      }

      // section.Data is a byte array, but it is parsed as a long array, so we give it a length in longs
      buf.WriteVarInt(int32(len(section.Data) / 8))
      // section.Data is a big endian encoded long array
      buf.WriteByteArray(section.Data)
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    out.WriteVarInt(0)
  })
  // Added a trust edges field, which does something.
  spec.Set(int32(mpacket.Clientbound_UpdateLight), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x24
    out.WriteVarInt(packet.Ints[0]) // X
    out.WriteVarInt(packet.Ints[1]) // Z
    // CHANGE: Added this bool.
    out.WriteBool(true) // Called Trust Edges. Not clear what it does.
    out.WriteVarInt(packet.Ints[2]) // bitmask for sky light included in byte array
    out.WriteVarInt(packet.Ints[3]) // bitmask for block light included in byte array
    out.WriteVarInt(packet.Ints[4]) // bitmask for sky light is 0
    out.WriteVarInt(packet.Ints[5]) // bitmask for block light is 0
    light_data := make([]byte, 2048)
    for i := 0; i < len(light_data); i++ {
      light_data[i] = 0xff
    }
    bitmask := packet.Ints[2]
    for i := 0; i < 18; i++ {
      if bitmask & (1 << i) != 0 {
        out.WriteVarInt(int32(len(light_data)))
        out.WriteByteArray(light_data)
      }
    }
  })
  // They reworked this whole packet. It seems like multi-world support is finally coming client-side.
  spec.Set(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x25
    out.WriteInt(packet.Ints[0]) // Player's EID
    out.WriteByte(packet.Bytes[0]) // Gamemode
    // CHANGE: Added a lot of fields here
    out.WriteByte(1) // Previous gamemode (creative for now)
    out.WriteVarInt(1) // World count
    out.WriteString("minecraft:overworld") // One string for each world name

    tag := nbt.NewCompoundTag("")
    list := tag.CompoundAddChild("dimension", nbt.TAG_List)
    item := list.ListAddFirst(nbt.TAG_Compound)
    item.CompoundAddChild("name",                 nbt.TAG_String).WriteString("minecraft:overworld")
    item.CompoundAddChild("ambient_light",        nbt.TAG_Float).WriteFloat(0)
    item.CompoundAddChild("infiniburn",           nbt.TAG_String).WriteString("") // Firetick is off
    item.CompoundAddChild("logical_height",       nbt.TAG_Int).WriteInt(256) // 256 block tall world
    item.CompoundAddChild("has_raids",            nbt.TAG_Byte).WriteByte(0)
    item.CompoundAddChild("respawn_anchor_works", nbt.TAG_Byte).WriteByte(1)
    item.CompoundAddChild("bed_works",            nbt.TAG_Byte).WriteByte(0)
    item.CompoundAddChild("piglin_safe",          nbt.TAG_Byte).WriteByte(0)
    item.CompoundAddChild("shrunk",               nbt.TAG_Byte).WriteByte(0) // Set to 1 in the nether. Effects portal coordinates.
    item.CompoundAddChild("natural",              nbt.TAG_Byte).WriteByte(1) // Set if this is one of the 'normal' vanilla worlds
    item.CompoundAddChild("ultrawarm",            nbt.TAG_Byte).WriteByte(0)
    item.CompoundAddChild("has_ceiling",          nbt.TAG_Byte).WriteByte(0)
    item.CompoundAddChild("has_skylight",         nbt.TAG_Byte).WriteByte(1)
    out.WriteByteArray(tag.Serialize())

    // CHANGE: This used to be an int enum
    out.WriteString("minecraft:overworld") // Dimension
    out.WriteString("minecraft:overworld") // World name you are spawning into

    // This stuff is the same
    out.WriteLong(packet.Longs[0]) // Hashed seed
    out.WriteByte(0) // Ignored
    // CHANGE: Removed level type
    // out.WriteString(packet.Strings[0]) // Level type
    out.WriteVarInt(packet.Ints[2]) // View distance
    out.WriteBool(packet.Bools[0]) // Reduced debug info
    out.WriteBool(packet.Bools[1]) // Enable respawn screen

    // CHANGE: Added these fields
    out.WriteBool(false) // True if this is a debug mode world (makes the world unmodifiable, and set with predefined blocks)
    out.WriteBool(false) // True if this is a flat world (changes horizon and void fog)
  })
  // Yay fun time.
  spec.Move(int32(mpacket.Clientbound_SpawnPosition), 0x42)
  return spec
}
