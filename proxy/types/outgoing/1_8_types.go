package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

func Make1_8Types() *registry.Registry {
  spec := registry.New()
  spec.Add(int32(mpacket.Clientbound_KeepAlive), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x00
    out.WriteVarInt(packet.Ints[0])
  })
  spec.Add(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x01
    out.WriteInt(packet.Ints[0]) // eid
    out.WriteByte(packet.Bytes[0]) // gamemode
    out.WriteByte(byte(packet.Ints[1])) // dimension
    out.WriteByte(2) // difficulty
    out.WriteByte(255) // max players (used to draw tab list)
    out.WriteString(packet.Strings[0]) // level type
    out.WriteBool(packet.Bools[0]) // reduced debug info
  })
  spec.Add(int32(mpacket.Clientbound_ChatMessage), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x02
    out.WriteString(packet.Strings[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_TimeUpdate), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x03
    // World age in ticks; not affected by vanilla commands
    out.WriteLong(packet.Longs[0])
    // Time of day. If negative, then the sun will stop moving,
    // and stay at abs(time).
    out.WriteLong(packet.Longs[1])
  })
  spec.Add(int32(mpacket.Clientbound_EntityEquipment), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x04
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteShort(uint16(packet.Ints[1])) // Slot
    item := &pb.Item{}
    err := ptypes.UnmarshalAny(packet.Other[0], item)
    if err != nil {
      panic(err)
    }
    out.WriteItem(item)
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x05
    out.WritePosition(packet.Positions[0])
  })
  spec.Add(int32(mpacket.Clientbound_UpdateHealth), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x06
    out.WriteFloat(packet.Floats[0])
    out.WriteVarInt(packet.Ints[0])
    out.WriteFloat(packet.Floats[1])
  })
  spec.Add(int32(mpacket.Clientbound_Respawn), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x07
    out.WriteInt(packet.Ints[0]) // Dimension
    out.WriteByte(0) // Peaceful
    out.WriteByte(packet.Bytes[0]) // Gamemode
    out.WriteString("flat")
  })
  spec.Add(int32(mpacket.Clientbound_PlayerPositionAndLook), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x08
    out.WriteDouble(packet.Doubles[0]) // Z
    out.WriteDouble(packet.Doubles[1]) // Y
    out.WriteDouble(packet.Doubles[2]) // Z
    out.WriteFloat(packet.Floats[0]) // Yaw
    out.WriteFloat(packet.Floats[1]) // Pitch
    out.WriteByte(packet.Bytes[0]) // Flags
    // Teleport id is here, added in later version
  })
  spec.Add(int32(mpacket.Clientbound_HeldItemChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x09
    out.WriteByte(packet.Bytes[0]) // Slot
  })
  // Use bed.
  // TODO: Implement this on the server.
  spec.Add(-1, nil) // 0x0a
  spec.Add(int32(mpacket.Clientbound_EntityAnimation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0b
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPlayer), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0c
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // Y
    out.WriteFixedFloat(packet.Doubles[2]) // Z
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteShort(0) // This is the item in their hand. Default it to empty
    out.WriteByte(0x7f) // Empty metadata
  })
  spec.Add(int32(mpacket.Clientbound_CollectItem), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0d
    out.WriteVarInt(packet.Ints[0]) // Collected item EID
    out.WriteVarInt(packet.Ints[1]) // Collector EID
    // Pickup item amount is not in this version
  })
  // Called spawn object
  spec.Add(int32(mpacket.Clientbound_SpawnEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0e
    out.WriteVarInt(packet.Ints[0]) // EID
    // UUID is not in this version
    out.WriteByte(byte(packet.Ints[1])) // Type
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // Y
    out.WriteFixedFloat(packet.Doubles[2]) // Z
    out.WriteByte(packet.Bytes[0]) // Pitch
    out.WriteByte(packet.Bytes[1]) // Yaw
    out.WriteInt(packet.Ints[2]) // Data
    if packet.Ints[2] != 0 {
      out.WriteShort(uint16(packet.Shorts[0])) // Velocity X
      out.WriteShort(uint16(packet.Shorts[1])) // Velocity Y
      out.WriteShort(uint16(packet.Shorts[2])) // Velocity Z
    }
  })
  // Called spawn mob. Does the same thing
  spec.Add(int32(mpacket.Clientbound_SpawnLivingEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0f
    out.WriteVarInt(packet.Ints[0]) // EID
    // UUID is not in this version
    out.WriteByte(byte(packet.Ints[1])) // Type
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // X
    out.WriteFixedFloat(packet.Doubles[2]) // X
    // TODO: Yaw and piutch are in a different order, I need to make this consistent somewhere.
    out.WriteByte(packet.Bytes[0]) // Yaw
    out.WriteByte(packet.Bytes[1]) // Pitch
    out.WriteByte(packet.Bytes[2]) // Head Pitch
    out.WriteShort(uint16(packet.Shorts[0])) // Velocity X
    out.WriteShort(uint16(packet.Shorts[1])) // Velocity Y
    out.WriteShort(uint16(packet.Shorts[2])) // Velocity Z
    out.WriteByte(0x7f) // No metadata
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPainting), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x10
    out.WriteVarInt(packet.Ints[0]) // EID
    // UUID is not in this version
    out.WriteString("") // Name of painting
    // Painting id is not in this version
    out.WritePosition(packet.Positions[0]) // Center pos
    out.WriteByte(packet.Bytes[0]) // Direction
  })
  spec.Add(int32(mpacket.Clientbound_SpawnExpOrb), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x11
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // Y
    out.WriteFixedFloat(packet.Doubles[2]) // Z
    out.WriteShort(uint16(packet.Ints[1])) // Amount of xp
  })
  spec.Add(int32(mpacket.Clientbound_EntityVelocity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x12
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteShort(uint16(packet.Shorts[0])) // Velocity X
    out.WriteShort(uint16(packet.Shorts[1])) // Velocity Y
    out.WriteShort(uint16(packet.Shorts[2])) // Velocity Z
  })
  spec.Add(int32(mpacket.Clientbound_DestroyEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x13
    out.WriteVarInt(int32(len(packet.IntArrays[0].Ints))) // Length
    for _, value := range packet.IntArrays[0].Ints {
      out.WriteVarInt(value) // EID
    }
  })
  // This packet is called 'Entity', and just says there is an entity in the world.
  spec.Add(-1, nil) // 0x14
  spec.Add(int32(mpacket.Clientbound_EntityPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x15
    out.WriteVarInt(packet.Ints[0]) // EID
    // This only allows for four blocks of movement, while the shorts allow for 8 blocks.
    // So, the server should probably figure this out.
    out.WriteByte(byte(packet.Shorts[0])) // Relative X
    out.WriteByte(byte(packet.Shorts[1])) // Relative Y
    out.WriteByte(byte(packet.Shorts[2])) // Relative Z
    out.WriteBool(packet.Bools[0]) // On ground
  })
  spec.Add(int32(mpacket.Clientbound_EntityRotation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x16
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Yaw
    out.WriteByte(packet.Bytes[1]) // Pitch
    out.WriteBool(packet.Bools[0]) // On ground
  })
  spec.Add(int32(mpacket.Clientbound_EntityPositionAndRotation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x17
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(byte(packet.Shorts[0]))  // Relative X
    out.WriteByte(byte(packet.Shorts[1]))  // Relative Y
    out.WriteByte(byte(packet.Shorts[2]))  // Relative Z
    out.WriteByte(packet.Bytes[0]) // Yaw
    out.WriteByte(packet.Bytes[1]) // Pitch
    out.WriteBool(packet.Bools[0]) // On ground
  })
  spec.Add(int32(mpacket.Clientbound_EntityTeleport), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x18
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // Y
    out.WriteFixedFloat(packet.Doubles[2]) // Z
    out.WriteByte(packet.Bytes[0]) // Yaw
    out.WriteByte(packet.Bytes[1]) // Pitch
    out.WriteBool(packet.Bools[0]) // On ground
  })
  spec.Add(int32(mpacket.Clientbound_EntityHeadLook), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x19
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Head Yaw
  })
  spec.Add(int32(mpacket.Clientbound_EntityStatus), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1a
    // Things like damage animation
    out.WriteInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Status
  })
  spec.Add(int32(mpacket.Clientbound_AttachEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1b
    out.WriteInt(packet.Ints[0]) // EID
    out.WriteInt(packet.Ints[0]) // Target EID
    out.WriteBool(true) // Leash? Unclear what this is. It was removed in later versions.
  })
  // TERRIBLE SOLUTION
  // TODO: Need to fix all the types server side, but the way
  // those types are encoded (the multiple values in one byte thing)
  // will still be implemented here.
  spec.Add(int32(mpacket.Clientbound_EntityMetadata), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1c
    out.WriteVarInt(packet.Ints[0])
    meta := &pb.EntityMetadata{}
    err := ptypes.UnmarshalAny(packet.Other[0], meta)
    if err != nil {
      panic(err)
    }
    for index, value := range meta.Values {
      old_type := 0
      switch value.Type {
      case metadata.EntityType_Byte:
        old_type = 0 // byte
      case metadata.EntityType_VarInt:
        old_type = 1 // int
      case metadata.EntityType_Float:
        old_type = 3 // float
      case metadata.EntityType_String:
        old_type = 4 // string
      case metadata.EntityType_Chat:
        old_type = -1 // invalid
      case metadata.EntityType_OptChat:
        old_type = -1 // invalid
      case metadata.EntityType_Item:
        old_type = 5 // item
      case metadata.EntityType_Bool:
        old_type = 0 // byte
      case metadata.EntityType_Rotation:
        old_type = 7 // rotation
      case metadata.EntityType_Position:
        old_type = 6 // position
      case metadata.EntityType_OptPosition:
        old_type = -1 // invalid
      case metadata.EntityType_Direction:
        old_type = 1 // int
      case metadata.EntityType_OptUUID:
        old_type = -1 // invalid
      case metadata.EntityType_BlockID:
        old_type = 1 // int
      case metadata.EntityType_NBT:
        old_type = -1 // invalid
      case metadata.EntityType_Particle:
        old_type = -1 // invalid
      case metadata.EntityType_VillagerData:
        old_type = -1 // invalid
      case metadata.EntityType_OptVarInt:
        old_type = 1 // int
      case metadata.EntityType_Pose:
        old_type = -1 // invalid (1.8 does not have pose)
      }
      if old_type != -1 {
        out.WriteByte(byte(old_type << 5) | byte(index & 0x1f))
        out.WriteByteArray(value.Data)
      }
    }
    // terminates the metadata list
    out.WriteByte(0x7f)
  })
  spec.Add(int32(mpacket.Clientbound_EntityEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1d
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Effect ID
    out.WriteByte(packet.Bytes[0]) // Level (displayed as this number + 1)
    out.WriteVarInt(packet.Ints[0]) // Duration
    // GRPC version uses flags, but this version only knows hide particles.
    out.WriteBool(packet.Bytes[0] & 0x02 != 0)
  })
  spec.Add(int32(mpacket.Clientbound_RemoveEntityEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1e
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Effect ID
  })
  spec.Add(int32(mpacket.Clientbound_SetExp), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1f
    out.WriteFloat(packet.Floats[0]) // Experience bar
    out.WriteVarInt(packet.Ints[0]) // Level
    out.WriteVarInt(packet.Ints[1]) // Total
  })
  spec.Add(int32(mpacket.Clientbound_EntityProperties), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x20
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteInt(int32(len(packet.Other))) // Length
    for _, proto := range packet.Other {
      prop := &pb.EntityProperty{}
      err := ptypes.UnmarshalAny(proto, prop)
      if err != nil {
        panic(err)
      }
      out.WriteString(prop.Name) // Property name
      out.WriteDouble(prop.Value) // Property value
      out.WriteVarInt(0) // No elements in the array
    }
  })
  spec.Add(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x21
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }

    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true) // means that this is a new chunk
    biomes := true // Always true with new chunk set
    skylight := true // Assume overworld

    bitmask := uint16(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteShort(bitmask)

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    // makes an ordered list of chunk sections
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    // iterates through chunks in order, from ground up
    num_sections := 0
    for _, section := range section_list {
      if section == nil {
        continue
      }
      num_sections++
      // This is a list of block ids, encoded as little endian shorts
      // We assume the server has formatted this correctly
      buf.WriteByteArray(section.Data)
    }
    // Light data
    for i := 0; i < num_sections * 16*16*16 / 2; i++ {
      // Each lighting value is 1/2 byte
      buf.WriteByte(0xff)
    }
    if skylight {
      for i := 0; i < num_sections * 16*16*16 / 2; i++ {
        // Each lighting value is 1/2 byte
        buf.WriteByte(0xff)
      }
    }
    if biomes {
      for i := 0; i < 256; i++ {
        buf.WriteByte(127) // Void biome
      }
    }

    // Not needed. Leaving commented out for reference, although I don't
    // assume anyone would need this.
    // expected := num_sections * 16*16*16 * 2 // Block data
    // expected += num_sections * 16*16*16 / 2 // Block light data
    // if skylight {
    //   expected += num_sections * 16*16*16 / 2 // Sky light data
    // }
    // if biomes {
    //   expected += 256 // Biome data
    // }
    // if buf.Length() != int32(expected) {
    //   fmt.Println("ERROR: Incorrectly serialized chunk! Expected length:", expected, "actual length:", buf.Length())
    // }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    // fmt.Println("Packet data: ", hex.EncodeToString(out.GetData()))
  })
  spec.Add(int32(mpacket.Clientbound_MultiBlockChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x22
    out.WriteInt(packet.Ints[0]) // Chunk X
    out.WriteInt(packet.Ints[1]) // Chunk Z
    out.WriteVarInt(int32(len(packet.Other)))
    for _, val := range packet.Other {
      multi_block_change := &pb.MultiBlockChange{}
      err := ptypes.UnmarshalAny(val, multi_block_change)
      if err != nil {
        panic(err)
      }
      horizontal := byte(multi_block_change.X & 0x0f) << 4 | byte(multi_block_change.Z & 0x0f)
      out.WriteByte(horizontal)
      out.WriteByte(byte(multi_block_change.Y))
      out.WriteVarInt(multi_block_change.BlockState)
    }
  })
  spec.Add(int32(mpacket.Clientbound_BlockChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x23
    out.WritePosition(packet.Positions[0])
    out.WriteVarInt(packet.Ints[0])
  })
  spec.Add(int32(mpacket.Clientbound_BlockAction), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x24
    // This is things like chest opening, pistons pushing, etc.
    out.WritePosition(packet.Positions[0]) // Block pos
    out.WriteByte(packet.Bytes[0]) // Varies
    out.WriteByte(packet.Bytes[1]) // Varies
    out.WriteVarInt(packet.Ints[0] >> 4) // Block type (doesn't include damage value)
  })
  spec.Add(int32(mpacket.Clientbound_BlockBreakAnimation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x25
    out.WriteVarInt(packet.Ints[0]) // EID that is breaking the block
    out.WritePosition(packet.Positions[0]) // Block pos
    out.WriteByte(packet.Bytes[0]) // 0-9 for progress
  })
  // Map chunk bulk. This allowed multiple chunks to be
  // sent at the same time. Doesn't seem worthwhile implementing
  // at all.
  spec.Add(-1, nil) // 0x26
  spec.Add(int32(mpacket.Clientbound_Explosion), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x27
    out.WriteFloat(packet.Floats[0]) // X
    out.WriteFloat(packet.Floats[1]) // Y
    out.WriteFloat(packet.Floats[2]) // Z
    out.WriteFloat(packet.Floats[3]) // Radius (unused)
    out.WriteInt(int32(len(packet.ByteArrays[0]))) // Length
    out.WriteByteArray(packet.ByteArrays[0]) // Array of 3 bytes at a time; each byte is an (x,y,z) offset from the center
    out.WriteFloat(packet.Floats[4]) // X Velocity
    out.WriteFloat(packet.Floats[5]) // Y Velocity
    out.WriteFloat(packet.Floats[6]) // Z Velocity
  })
  spec.Add(int32(mpacket.Clientbound_Effect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x28
    // These are (usually) sounds that come from blocks/entities.
    out.WriteInt(packet.Ints[0]) // Effect ID
    out.WritePosition(packet.Positions[0]) // Location of the effect
    out.WriteInt(packet.Ints[1]) // Extra data
    out.WriteBool(packet.Bools[0]) // Disable relative volume
  })
  spec.Add(int32(mpacket.Clientbound_SoundEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x29
    out.WriteString("") // Sound name
    // There is no id or category in this version
    out.WriteInt(packet.Ints[2] * 8) // Effect X multiplied by 8 (why????)
    out.WriteInt(packet.Ints[3] * 8) // Effect Y multiplied by 8
    out.WriteInt(packet.Ints[4] * 8) // Effect Z multiplied by 8
    out.WriteFloat(packet.Floats[0]) // Volume
    out.WriteByte(byte(packet.Floats[1] * 64)) // Pitch
  })
  spec.Add(int32(mpacket.Clientbound_Particle), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2a
    out.WriteInt(packet.Ints[0]) // Particle id
    out.WriteBool(packet.Bools[0]) // If true, distance increases from 256 to 65536
    out.WriteFloat(float32(packet.Doubles[0])) // X
    out.WriteFloat(float32(packet.Doubles[1])) // Y
    out.WriteFloat(float32(packet.Doubles[2])) // Z
    out.WriteFloat(packet.Floats[0]) // Offset x
    out.WriteFloat(packet.Floats[1]) // Offset y
    out.WriteFloat(packet.Floats[2]) // Offset z
    out.WriteFloat(packet.Floats[3]) // Particle data
    out.WriteInt(packet.Ints[0]) // Particle count
    out.WriteByteArray(packet.ByteArrays[0]) // Data for some particles. Length varies.
  })
  spec.Add(int32(mpacket.Clientbound_ChangeGameState), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2b
    // Things like bed can't be used or weather change.
    out.WriteByte(packet.Bytes[0]) // Reason
    out.WriteFloat(packet.Floats[0]) // Extra data
  })
  // Called spawn global entity
  spec.Add(int32(mpacket.Clientbound_SpawnWeatherEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2c
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(1) // This might be changed in the future for other weather entities, but for now thunderbolt is the only option
    out.WriteFixedFloat(packet.Doubles[0]) // X
    out.WriteFixedFloat(packet.Doubles[1]) // Y
    out.WriteFixedFloat(packet.Doubles[2]) // Z
  })
  spec.Add(int32(mpacket.Clientbound_OpenWindow), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2d
    // Window id is varint in newer versions
    out.WriteByte(byte(packet.Ints[0]))
    // Window type is hard; it is a varint in newer versions, but a string
    // in this version. Need to update grpc spec.
    out.WriteString("minecraft:crafting")
    // Title
    out.WriteString(packet.Strings[0])
    // Number of slots. Doesn't make sense with newer window types.
    // Will always be 0 with non-storage windows.
    out.WriteByte(0)
    // EID is next, but that is only used in horse windows.
  })
  spec.Add(int32(mpacket.Clientbound_CloseWindow), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2e
    out.WriteByte(packet.Bytes[0]) // Window id
  })
  spec.Add(int32(mpacket.Clientbound_SetSlot), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2f
    out.WriteByte(packet.Bytes[0]) // Window id
    out.WriteShort(uint16(packet.Shorts[0])) // Slot
    item := &pb.Item{}
    err := ptypes.UnmarshalAny(packet.Other[0], item)
    if err != nil {
      panic(err)
    }
    out.WriteItem(item) // Item
  })
  spec.Add(int32(mpacket.Clientbound_WindowItems), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x30
    // this is the 'multi block change' of inventory items: It changes multiple items, but otherwise is the same as SetSlot
    out.WriteByte(packet.Bytes[0]) // Window id
    out.WriteShort(uint16(len(packet.Other))) // Length
    for _, val := range packet.Other {
      item := &pb.Item{}
      err := ptypes.UnmarshalAny(val, item)
      if err != nil {
        panic(err)
      }
      out.WriteItem(item) // Item
    }
  })
  spec.Add(int32(mpacket.Clientbound_WindowProperty), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x31
    // This is things like the little arrow in the furnace
    out.WriteByte(packet.Bytes[0]) // Window id
    out.WriteShort(uint16(packet.Shorts[0])) // Property
    out.WriteShort(uint16(packet.Shorts[1])) // Value
  })
  // Called confirm transaction
  spec.Add(int32(mpacket.Clientbound_WindowConfirm), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x32
    out.WriteByte(packet.Bytes[0]) // Window id
    out.WriteShort(uint16(packet.Shorts[0])) // Action id
    out.WriteBool(packet.Bools[0]) // Accepted
  })
  // Called update sign. Is sent any time a sign is loaded.
  // In newer versions, the packet update block entity with action 9
  // is sent instead.
  spec.Add(-1, nil) // 0x33
  spec.Add(int32(mpacket.Clientbound_MapData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x34
    map_data := &pb.Map{}
    err := ptypes.UnmarshalAny(packet.Other[0], map_data)
    if err != nil {
      panic(err)
    }
    out.WriteVarInt(map_data.Id) // Item damage value (id)
    out.WriteByte(byte(map_data.Scale)) // Scale
    // out.WriteBool(map_data.ShowPlayer) Added in later versions
    // out.WriteBool(map_data.Locked) Added in later versions
    out.WriteVarInt(int32(len(map_data.Icons))) // Length
    for _, icon := range map_data.Icons {
      if icon.Type > 15 {
        // Default to white arrow if it's too new of an icon
        icon.Type = 0
      }
      out.WriteByte(byte(icon.Direction & 0x0f) | byte(icon.Type & 0x0f) << 4)
      out.WriteByte(byte(icon.X))
      out.WriteByte(byte(icon.Z))
      // out.WriteBool(icon.HasName)
      // out.WriteString(icon.Name)
    }
    out.WriteByte(byte(map_data.Columns))
    if map_data.Columns != 0 {
      out.WriteByte(byte(map_data.Rows))
      out.WriteByte(byte(map_data.XOffset))
      out.WriteByte(byte(map_data.ZOffset))
      out.WriteVarInt(int32(len(map_data.Data)))
      out.WriteByteArray(map_data.Data)
    }
  })
  // Called update block entity
  spec.Add(int32(mpacket.Clientbound_BlockEntityData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x35
    out.WritePosition(packet.Positions[0]) // Block pos
    out.WriteByte(packet.Bytes[0]) // Action
    out.WriteByteArray(packet.NBTTags[0]) // NBT data
  })
  spec.Add(int32(mpacket.Clientbound_OpenSignEditor), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x36
    out.WritePosition(packet.Positions[0]) // Sign position
  })
  spec.Add(int32(mpacket.Clientbound_Statistics), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x37
    // TODO: Implement statistics with a protobuf
    out.WriteVarInt(0) // Number of elements in the array
  })
  // Called player list item; updates tab list
  spec.Add(int32(mpacket.Clientbound_PlayerInfo), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x38
    // Updates the tab list, and must be sent before SpawnPlayer
    out.WriteVarInt(packet.Ints[0])
    out.WriteVarInt(packet.Ints[1])
    out.WriteByteArray(packet.ByteArrays[0])
  })
  spec.Add(int32(mpacket.Clientbound_PlayerAbilities), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x39
    out.WriteByte(packet.Bytes[0]) // Bit field
    out.WriteFloat(packet.Floats[0]) // Fly speed
    out.WriteFloat(packet.Floats[1]) // Walk speed
  })
  spec.Add(int32(mpacket.Clientbound_TabComplete), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3a
    // TODO: Fix tab completes
    out.WriteVarInt(2)
    out.WriteString("big")
    out.WriteString("gaming")
  })
  spec.Add(int32(mpacket.Clientbound_ScoreboardObjective), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3b
    out.WriteString(packet.Strings[0]) // Objective name
    mode := packet.Bytes[0]
    out.WriteByte(mode) // 0 to create, 1 to remove, 2 to update
    if mode == 0 || mode == 2 {
      out.WriteString(packet.Strings[1])
      out.WriteVarInt(packet.Ints[0])
    }
  })
  spec.Add(int32(mpacket.Clientbound_UpdateScore), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3c
    out.WriteString(packet.Strings[0]) // Score name
    action := packet.Bytes[0]
    out.WriteByte(action) // 0 to create/update, 1 to remove
    out.WriteString(packet.Strings[1]) // Objective name
    if action == 0 {
      out.WriteVarInt(packet.Ints[0]) // New score to be displayed
    }
  })
  spec.Add(int32(mpacket.Clientbound_DisplayScoreboard), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3d
    out.WriteByte(packet.Bytes[0]) // Position: 0 -> list, 1 -> sidebar, 2 -> below name
    out.WriteString(packet.Strings[0]) // Name of the scoreboard
  })
  spec.Add(int32(mpacket.Clientbound_Teams), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3e
    out.WriteString(packet.Strings[0]) // Team name
    mode := packet.Bytes[0]
    out.WriteByte(mode)
    switch mode {
    case 0:
      // Create team
      out.WriteString(packet.Strings[1]) // display name
      out.WriteString(packet.Strings[2]) // prefix
      out.WriteString(packet.Strings[3]) // suffix
      out.WriteByte(packet.Bytes[0]) // flags
      out.WriteString(packet.Strings[4]) // name tag visibility rule
      out.WriteString(packet.Strings[5]) // collision rule
      out.WriteByte(packet.Bytes[1]) // color code. -1 resets the color
      // List of indentifiers for the entities on the team.
      // Players -> username
      // Entities -> UUID string
      out.WriteVarInt(int32(len(packet.StringArrays[0].Strings)))
      for _, value := range packet.StringArrays[0].Strings {
        out.WriteString(value)
      }
    case 1:
      // Remove team; no fields
    case 2:
      // Update team info
      out.WriteString(packet.Strings[1]) // display name
      out.WriteString(packet.Strings[2]) // prefix
      out.WriteString(packet.Strings[3]) // suffix
      out.WriteByte(packet.Bytes[0]) // flags
      out.WriteString(packet.Strings[4]) // name tag visibility rule
      out.WriteString(packet.Strings[5]) // collision rule
      out.WriteByte(packet.Bytes[1]) // color code. -1 resets the color
    case 3, 4:
      // Add/Remove entities from team
      out.WriteVarInt(int32(len(packet.StringArrays[0].Strings)))
      for _, value := range packet.StringArrays[0].Strings {
        out.WriteString(value)
      }
    }
  })
  spec.Add(int32(mpacket.Clientbound_PluginMessage), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3f
    out.WriteString(packet.Strings[0]) // Channel
    out.WriteByteArray(packet.ByteArrays[0]) // Data
  })
  spec.Add(int32(mpacket.Clientbound_Disconnect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x40
    out.WriteString(packet.Strings[0]) // Kick message
  })
  spec.Add(int32(mpacket.Clientbound_ServerDifficulty), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x41
    // This updates the button in the escape menu
    out.WriteByte(packet.Bytes[0]) // Difficulty
    // Locked is not in this version
  })
  spec.Add(int32(mpacket.Clientbound_EnterCombat), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x42
    event := packet.Ints[0]
    out.WriteVarInt(event)
    switch event {
    case 0:
      // Enter combat, no fields
    case 1:
      // End combat
      out.WriteVarInt(packet.Ints[1]) // duration
      out.WriteInt(packet.Ints[2]) // EID
    case 2:
      // Entity dead
      out.WriteVarInt(packet.Ints[1]) // player id
      out.WriteInt(packet.Ints[2]) // EID
      out.WriteString(packet.Strings[0]) // chat message
    }
  })
  spec.Add(int32(mpacket.Clientbound_Camera), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x43
    // Used when you spectate an entity
    out.WriteVarInt(packet.Ints[0]) // EID
  })
  spec.Add(int32(mpacket.Clientbound_WorldBorder), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x44
    action := packet.Ints[0]
    out.WriteVarInt(action)
    switch action {
    case 0:
      // Set size
      out.WriteDouble(packet.Doubles[0])
    case 1:
      // Lerp size
      out.WriteDouble(packet.Doubles[0]) // old size
      out.WriteDouble(packet.Doubles[1]) // new size
      out.WriteVarInt(packet.Ints[0]) // speed in ticks
    case 2:
      // Set center
      out.WriteDouble(packet.Doubles[0]) // X
      out.WriteDouble(packet.Doubles[1]) // Z
    case 3:
      // Init
      out.WriteDouble(packet.Doubles[0]) // X
      out.WriteDouble(packet.Doubles[1]) // Z
      out.WriteDouble(packet.Doubles[2]) // old size
      out.WriteDouble(packet.Doubles[3]) // new size
      out.WriteVarInt(packet.Ints[0]) // speed in ticks
      // Portal teleport boundry. Resulting coordinates from a portal
      // teleport are limited to +-packetInts[1]. Usually 29999984
      out.WriteVarInt(packet.Ints[1])
      out.WriteVarInt(packet.Ints[2]) // warning time in seconds
      out.WriteVarInt(packet.Ints[3]) // warning distance in blocks
    case 4:
      // Set warning time
      out.WriteVarInt(packet.Ints[0]) // warning time in seconds
    case 5:
      // Set warning distance
      out.WriteVarInt(packet.Ints[0]) // warning distance in blocks
    }
  })
  spec.Add(int32(mpacket.Clientbound_Title), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x45
    action := packet.Ints[0]
    out.WriteVarInt(action)
    // Action bar doesn't exist, so we will send a subtitle instead
    if action >= 2 {
      action--
    }
    switch action {
    case 0, 1:
      // Set title or subtitle
      out.WriteString(packet.Strings[0])
    case 2:
      // Set times
      out.WriteInt(packet.Ints[1])
      out.WriteInt(packet.Ints[2])
      out.WriteInt(packet.Ints[3])
    }
  })
  // Set compression. Don't use this. It appears to be horribly broken.
  spec.Add(-1, nil) // 0x46
  spec.Add(int32(mpacket.Clientbound_PlayerListHeader), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x47
    out.WriteString(packet.Strings[0]) // Header
    out.WriteString(packet.Strings[1]) // Footer
  })
  spec.Add(int32(mpacket.Clientbound_ResourcePack), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x48
    out.WriteString(packet.Strings[0]) // URL
    out.WriteString(packet.Strings[1]) // Hash (acts as a unique id)
  })
  // Update entity NBT. Unclear what this is used for, and it doesn't exist in newer versions.
  spec.Add(-1, nil)

  return spec
}
