package outgoing

// import (
//   "gitlab.com/macmv/sugarcane/minecraft/nbt"
//   "gitlab.com/macmv/sugarcane/minecraft/util"
//   "gitlab.com/macmv/sugarcane/minecraft/packet"
// 
//   pb "gitlab.com/macmv/sugarcane/proto"
// 
//   "github.com/golang/protobuf/ptypes"
// )

// func Make1_15_2Types(v1_12_2 *packet.OutgoingSpec) *packet.OutgoingSpec {
//   packet_types[0xff] = func(packet *pb.Packet, out *OutgoingPacket) { // login packet
//     out.Buf.WriteProtocolString(util.NewUUIDFromProto(packet.Uuids[0]).ToDashedString())
//     out.Buf.WriteProtocolString(packet.Strings[0])
//   }
//   packet_types[packet.Clientbound_SpawnEntity] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x00
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteInt(packet.Ints[2])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//     out.Buf.WriteShort(uint16(packet.Shorts[2]))
//   }
//   packet_types[packet.Clientbound_SpawnExpOrb] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x01
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteShort(uint16(packet.Ints[1]))
//   }
//   packet_types[packet.Clientbound_SpawnWeatherEntity] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x02
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteByte(1) // this might be changed in the future for other weather entities, but for now thunderbolt is the only option
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//   }
//   packet_types[packet.Clientbound_SpawnLivingEntity] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x03
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteByte(packet.Bytes[2])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//     out.Buf.WriteShort(uint16(packet.Shorts[2]))
//   }
//   packet_types[packet.Clientbound_SpawnPainting] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x04
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_SpawnPlayer] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x05
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//   }
//   packet_types[packet.Clientbound_EntityAnimation] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x06
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_AcknowledgePlayerDigging] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x08
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_BlockBreakAnimation] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x09
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_BlockEntityData] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0a
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByteArray(packet.NBTTags[0])
//   }
//   packet_types[packet.Clientbound_BlockAction] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0b
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteVarInt(packet.Ints[0])
//   }
//   packet_types[packet.Clientbound_BlockChange] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0c
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteVarInt(packet.Ints[0])
//   }
//   packet_types[packet.Clientbound_BossBar] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0d
//     action := packet.Ints[0]
//     boss_bar := &pb.BossBar{}
//     err := ptypes.UnmarshalAny(packet.Other[0], boss_bar)
//     if err != nil {
//       panic(err)
//     }
//     out.Buf.WriteUUID(util.NewUUIDFromProto(boss_bar.Uuid))
//     out.Buf.WriteVarInt(action)
//     if action == 0 { // add
//       out.Buf.WriteProtocolString(boss_bar.Title)
//       out.Buf.WriteFloat(boss_bar.Value)
//       out.Buf.WriteVarInt(boss_bar.Color)
//       out.Buf.WriteVarInt(boss_bar.Division)
//       out.Buf.WriteByte(byte(boss_bar.Flags))
//     } else if action == 1 { // remove
//     } else if action == 2 { // update health
//       out.Buf.WriteFloat(boss_bar.Value)
//     } else if action == 3 { // update title
//       out.Buf.WriteProtocolString(boss_bar.Title)
//     } else if action == 4 { // update style
//       out.Buf.WriteVarInt(boss_bar.Color)
//       out.Buf.WriteVarInt(boss_bar.Division)
//     } else if action == 5 { // update flags
//       out.Buf.WriteByte(byte(boss_bar.Flags))
//     } else {
//       panic("Invalid boss bar action!")
//     }
//   }
//   packet_types[packet.Clientbound_ServerDifficulty] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0e
//     // This updates the button in the escape menu
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_ChatMessage] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x0f
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_MultiBlockChange] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x10
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteInt(packet.Ints[1])
//     out.Buf.WriteVarInt(int32(len(packet.Other)))
//     for _, val := range packet.Other {
//       multi_block_change := &pb.MultiBlockChange{}
//       err := ptypes.UnmarshalAny(val, multi_block_change)
//       if err != nil {
//         panic(err)
//       }
//       horizontal := byte(multi_block_change.X & 0x0f) << 4 | byte(multi_block_change.Z & 0x0f)
//       out.Buf.WriteByte(horizontal)
//       out.Buf.WriteByte(byte(multi_block_change.Y))
//       out.Buf.WriteVarInt(multi_block_change.BlockState)
//     }
//   }
//   packet_types[packet.Clientbound_TabComplete] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x11
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteVarInt(packet.Ints[2])
//     out.Buf.WriteVarInt(int32(len(packet.Other)))
//     for _, val := range packet.Other {
//       node := &pb.TabCompleteSection{}
//       err := ptypes.UnmarshalAny(val, node)
//       if err != nil {
//         panic(err)
//       }
//       out.Buf.WriteProtocolString(node.Match)
//       out.Buf.WriteBool(node.HasTooltip)
//       if node.HasTooltip {
//         out.Buf.WriteProtocolString(node.Tooltip)
//       }
//     }
//   }
//   packet_types[packet.Clientbound_DeclareCommands] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x12
//     out.Buf.WriteVarInt(int32(len(packet.Other)))
//     for _, val := range packet.Other {
//       node := &pb.Node{}
//       err := ptypes.UnmarshalAny(val, node)
//       if err != nil {
//         panic(err)
//       }
//       out.Buf.WriteByte(byte(node.Flags))
//       out.Buf.WriteVarInt(int32(len(node.Children)))
//       for _, child := range node.Children {
//         out.Buf.WriteVarInt(child)
//       }
//       if node.Flags & 0x08 > 0 {
//         out.Buf.WriteVarInt(node.Redirect)
//       }
//       if node.Flags & 0x03 == 1 {
//         out.Buf.WriteProtocolString(node.Name)
//       } else if node.Flags & 0x03 == 2 {
//         out.Buf.WriteProtocolString(node.Name)
//         out.Buf.WriteProtocolString(node.Parser)
//         out.Buf.WriteByteArray(node.Properties)
//       }
//       if node.Flags & 0x10 > 0 {
//         out.Buf.WriteProtocolString(node.Suggestion)
//       }
//     }
//     out.Buf.WriteVarInt(packet.Ints[0])
//   }
//   packet_types[packet.Clientbound_WindowConfirm] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x13
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_CloseWindow] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x14
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_WindowItems] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x15
//     // this is the 'multi block change' of inventory items: It changes multiple items, but otherwise is the same as SetSlot
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteShort(uint16(len(packet.Other)))
//     for _, val := range packet.Other {
//       item := &pb.Item{}
//       err := ptypes.UnmarshalAny(val, item)
//       if err != nil {
//         panic(err)
//       }
//       out.Buf.WriteItem(item)
//     }
//   }
//   packet_types[packet.Clientbound_WindowProperty] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x16
//     // This is things like the little arrow in the furnace
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//   }
//   packet_types[packet.Clientbound_SetSlot] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x17
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     item := &pb.Item{}
//     err := ptypes.UnmarshalAny(packet.Other[0], item)
//     if err != nil {
//       panic(err)
//     }
//     out.Buf.WriteItem(item)
//   }
//   packet_types[packet.Clientbound_SetCooldown] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x18
//     // Is the little cooldown on ender pearls
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//   }
//   packet_types[packet.Clientbound_PluginMessage] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x19
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     out.Buf.WriteByteArray(packet.ByteArrays[0])
//   }
//   packet_types[packet.Clientbound_NamedSoundEffect] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1a
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteInt(packet.Ints[1])
//     out.Buf.WriteInt(packet.Ints[2])
//     out.Buf.WriteInt(packet.Ints[3])
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//   }
//   packet_types[packet.Clientbound_Disconnect] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1b
//     out.Buf.WriteProtocolString(packet.Strings[0])
//   }
//   packet_types[packet.Clientbound_EntityStatus] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1c
//     // Things like damage animation
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_Explosion] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1d
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//     out.Buf.WriteFloat(packet.Floats[2])
//     out.Buf.WriteFloat(packet.Floats[3])
//     out.Buf.WriteInt(int32(len(packet.ByteArrays[0])))
//     out.Buf.WriteByteArray(packet.ByteArrays[0])
//     out.Buf.WriteFloat(packet.Floats[4])
//     out.Buf.WriteFloat(packet.Floats[5])
//     out.Buf.WriteFloat(packet.Floats[6])
//   }
//   packet_types[packet.Clientbound_UnloadChunk] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1e
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteInt(packet.Ints[1])
//   }
//   packet_types[packet.Clientbound_ChangeGameState] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x1f
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteFloat(packet.Floats[0])
//   }
//   packet_types[packet.Clientbound_KeepAlive] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x21
//     out.Buf.WriteLong(uint64(packet.Ints[0]))
//   }
//   packet_types[packet.Clientbound_ChunkData] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x22
//     chunk := &pb.Chunk{}
//     err := ptypes.UnmarshalAny(packet.Other[0], chunk)
//     if err != nil {
//       panic(err)
//     }
//     out.Buf.WriteInt(chunk.X)
//     out.Buf.WriteInt(chunk.Z)
//     out.Buf.WriteBool(true)
//     bitmaks := int32(0)
//     for y, _ := range chunk.Sections {
//       bitmaks |= (1 << y)
//     }
//     out.Buf.WriteVarInt(bitmaks) // bitmask
//     heightmap := nbt.NewCompoundTag("")
//     heightmap.CompoundAddChild("MOTION_BLOCKING", 0x0c).WriteLongArray(chunk.Heightmap)
//     out.Buf.WriteByteArray(heightmap.Serialize())
//     // biome data
//     for i := 0; i < 1024; i++ {
//       out.Buf.WriteInt(127) // void (grass looks ugly, should change)
//     }
//     buf, _ := util.NewBufferFromSlice(out.garbage_buffer, 0)
//     section_list := make([]*pb.Chunk_Section, 16)
//     for i, section := range chunk.Sections {
//       section_list[i] = section
//     }
//     for _, section := range section_list {
//       if section == nil {
//         continue
//       }
//       buf.WriteShort(uint16(section.NonAirBlocks))
//       buf.WriteByte(byte(section.BitsPerBlock))
//       if section.BitsPerBlock <= 8 {
//         buf.WriteVarInt(int32(len(section.Palette)))
//         for _, id := range section.Palette {
//           buf.WriteVarInt(int32(id))
//         }
//       }
// 
//       // section.Data is a byte array, but it is parsed as a long array, so we give it a length in longs
//       buf.WriteVarInt(int32(len(section.Data) / 8))
//       // section.Data is a big endian encoded long array
//       buf.WriteByteArray(section.Data)
//     }
//     out.Buf.WriteVarInt(buf.Length())
//     out.Buf.WriteByteArray(buf.GetData())
//     out.Buf.WriteVarInt(0)
//   }
//   packet_types[packet.Clientbound_Effect] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x23
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteLong(packet.Positions[0])
//     out.Buf.WriteInt(packet.Ints[1])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_Particle] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x24
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteBool(packet.Bools[0])
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//     out.Buf.WriteFloat(packet.Floats[2])
//     out.Buf.WriteFloat(packet.Floats[3])
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteByteArray(packet.ByteArrays[0])
//   }
//   packet_types[packet.Clientbound_UpdateLight] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x25
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteVarInt(packet.Ints[2]) // bitmask for sky light included in byte array
//     out.Buf.WriteVarInt(packet.Ints[3]) // bitmask for block light included in byte array
//     out.Buf.WriteVarInt(packet.Ints[4]) // bitmask for sky light is 0
//     out.Buf.WriteVarInt(packet.Ints[5]) // bitmask for block light is 0
//     light_data := make([]byte, 2048)
//     for i := 0; i < 2048; i++ {
//       light_data[i] = 0xff
//     }
//     bitmask := packet.Ints[2]
//     for i := 0; i < 18; i++ {
//       if bitmask & (1 << i) != 0 {
//         out.Buf.WriteVarInt(2048)
//         out.Buf.WriteByteArray(light_data)
//       }
//     }
//   }
//   packet_types[packet.Clientbound_JoinGame] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x26
//     out.Buf.WriteInt(packet.Ints[0]) // eid
//     out.Buf.WriteByte(packet.Bytes[0]) // gamemode
//     out.Buf.WriteInt(packet.Ints[1]) // dimension
//     out.Buf.WriteLong(packet.Longs[0]) // hashed seed
//     out.Buf.WriteByte(0) // ignored
//     out.Buf.WriteProtocolString(packet.Strings[0]) // level type
//     out.Buf.WriteVarInt(packet.Ints[2]) // view distance
//     out.Buf.WriteBool(packet.Bools[0]) // reduced debug info
//     out.Buf.WriteBool(packet.Bools[1]) // enable respawn screen
//   }
//   packet_types[packet.Clientbound_MapData] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x27
//     map_data := &pb.Map{}
//     err := ptypes.UnmarshalAny(packet.Other[0], map_data)
//     if err != nil {
//       panic(err)
//     }
//     out.Buf.WriteVarInt(map_data.Id)
//     out.Buf.WriteByte(byte(map_data.Scale))
//     out.Buf.WriteBool(map_data.ShowPlayer)
//     out.Buf.WriteBool(map_data.Locked)
//     out.Buf.WriteVarInt(int32(len(map_data.Icons)))
//     for _, icon := range map_data.Icons {
//       out.Buf.WriteVarInt(icon.Type)
//       out.Buf.WriteByte(byte(icon.X))
//       out.Buf.WriteByte(byte(icon.Z))
//       out.Buf.WriteByte(byte(icon.Direction))
//       out.Buf.WriteBool(icon.HasName)
//       out.Buf.WriteProtocolString(icon.Name)
//     }
//     out.Buf.WriteByte(byte(map_data.Columns))
//     if map_data.Columns != 0 {
//       out.Buf.WriteByte(byte(map_data.Rows))
//       out.Buf.WriteByte(byte(map_data.XOffset))
//       out.Buf.WriteByte(byte(map_data.ZOffset))
//       out.Buf.WriteVarInt(int32(len(map_data.Data)))
//       out.Buf.WriteByteArray(map_data.Data)
//     }
//   }
//   packet_types[packet.Clientbound_EntityPosition] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x29
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//     out.Buf.WriteShort(uint16(packet.Shorts[2]))
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_EntityPositionAndRotation] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x2a
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//     out.Buf.WriteShort(uint16(packet.Shorts[2]))
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_EntityRotation] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x2b
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_OpenWindow] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x2f
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteProtocolString(packet.Strings[0])
//   }
//   packet_types[packet.Clientbound_PlayerAbilities] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x32
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//   }
//   packet_types[packet.Clientbound_PlayerInfo] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x34
//     // Updates the tab list, and must be sent before SpawnPlayer
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     out.Buf.WriteByteArray(packet.ByteArrays[0])
//   }
//   packet_types[packet.Clientbound_PlayerPositionAndLook] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x36
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteVarInt(packet.Ints[0])
//   }
//   packet_types[packet.Clientbound_DestroyEntity] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x38
//     out.Buf.WriteVarInt(int32(len(packet.IntArrays[0].Ints)))
//     for _, value := range packet.IntArrays[0].Ints {
//       out.Buf.WriteVarInt(value)
//     }
//   }
//   packet_types[packet.Clientbound_ResourcePack] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x3a
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     out.Buf.WriteProtocolString(packet.Strings[1])
//   }
//   packet_types[packet.Clientbound_Respawn] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x3b
//     out.Buf.WriteInt(packet.Ints[0])
//     out.Buf.WriteLong(0)
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteProtocolString("flat")
//   }
//   packet_types[packet.Clientbound_EntityHeadLook] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x3c
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteByte(packet.Bytes[0])
//   }
//   packet_types[packet.Clientbound_UpdateViewPosition] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x41
//     // Sent when you a player moves between chunks
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//   }
//   packet_types[packet.Clientbound_DisplayScoreboard] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x43
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteProtocolString(packet.Strings[0])
//   }
//   packet_types[packet.Clientbound_EntityMetadata] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x44
//     out.Buf.WriteVarInt(packet.Ints[0])
//     meta := &pb.EntityMetadata{}
//     err := ptypes.UnmarshalAny(packet.Other[0], meta)
//     if err != nil {
//       panic(err)
//     }
//     for index, value := range meta.Values {
//       out.Buf.WriteByte(byte(index))
//       out.Buf.WriteVarInt(value.Type)
//       out.Buf.WriteByteArray(value.Data)
//     }
//     // terminates the metadata list
//     out.Buf.WriteByte(0xff)
//   }
//   packet_types[packet.Clientbound_EntityVelocity] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x46
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteShort(uint16(packet.Shorts[0]))
//     out.Buf.WriteShort(uint16(packet.Shorts[1]))
//     out.Buf.WriteShort(uint16(packet.Shorts[2]))
//   }
//   packet_types[packet.Clientbound_EntityEquipment] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x47
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteVarInt(packet.Ints[1])
//     item := &pb.Item{}
//     err := ptypes.UnmarshalAny(packet.Other[0], item)
//     if err != nil {
//       panic(err)
//     }
//     out.Buf.WriteItem(item)
//   }
//   packet_types[packet.Clientbound_UpdateHealth] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x49
//     out.Buf.WriteFloat(packet.Floats[0])
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteFloat(packet.Floats[1])
//   }
//   packet_types[packet.Clientbound_ScoreboardObjective] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x4a
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     mode := packet.Bytes[0]
//     out.Buf.WriteByte(mode)
//     if mode == 0 || mode == 2 {
//       out.Buf.WriteProtocolString(packet.Strings[1])
//       out.Buf.WriteVarInt(packet.Ints[0])
//     }
//   }
//   packet_types[packet.Clientbound_UpdateScore] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x4d
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     action := packet.Bytes[0]
//     out.Buf.WriteByte(action)
//     out.Buf.WriteProtocolString(packet.Strings[1])
//     if action == 0 {
//       out.Buf.WriteVarInt(packet.Ints[0])
//     }
//   }
//   packet_types[packet.Clientbound_SpawnPosition] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x4e
//     out.Buf.WriteLong(packet.Positions[0])
//   }
//   packet_types[packet.Clientbound_Title] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x50
//     action := packet.Ints[0]
//     out.Buf.WriteVarInt(action)
//     switch action {
//     case 0, 1, 2:
//       out.Buf.WriteProtocolString(packet.Strings[0])
//     case 3:
//       out.Buf.WriteInt(packet.Ints[1])
//       out.Buf.WriteInt(packet.Ints[2])
//       out.Buf.WriteInt(packet.Ints[3])
//     }
//   }
//   packet_types[packet.Clientbound_PlayerListHeader] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x54
//     out.Buf.WriteProtocolString(packet.Strings[0])
//     out.Buf.WriteProtocolString(packet.Strings[1])
//   }
//   packet_types[packet.Clientbound_EntityTeleport] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x57
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteDouble(packet.Doubles[0])
//     out.Buf.WriteDouble(packet.Doubles[1])
//     out.Buf.WriteDouble(packet.Doubles[2])
//     out.Buf.WriteByte(packet.Bytes[0])
//     out.Buf.WriteByte(packet.Bytes[1])
//     out.Buf.WriteBool(packet.Bools[0])
//   }
//   packet_types[packet.Clientbound_EntityProperties] = func(packet *pb.Packet, out *OutgoingPacket) { // 0x59
//     out.Buf.WriteVarInt(packet.Ints[0])
//     out.Buf.WriteInt(int32(len(packet.Other)))
//     for _, proto := range packet.Other {
//       prop := &pb.EntityProperty{}
//       err := ptypes.UnmarshalAny(proto, prop)
//       if err != nil {
//         panic(err)
//       }
//       out.Buf.WriteProtocolString(prop.Name)
//       out.Buf.WriteDouble(prop.Value)
//       out.Buf.WriteVarInt(0)
//     }
//   }
//   outgoing_packet_types[578] = &outgoing_version{packets: packet_types, same_id: true}
//   return spec
// }
