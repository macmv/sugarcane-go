package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/proxy/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

func Make1_12Types() *registry.Registry {
  spec := registry.New()
  spec.Add(int32(mpacket.Clientbound_SpawnEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x00
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    out.WriteByte(byte(packet.Ints[1]))
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteInt(packet.Ints[2])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
  })
  spec.Add(int32(mpacket.Clientbound_SpawnExpOrb), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x01
    out.WriteVarInt(packet.Ints[0])
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteShort(uint16(packet.Ints[1]))
  })
  // Called spawn global entity. Does the same thing.
  spec.Add(int32(mpacket.Clientbound_SpawnWeatherEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x02
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(1) // this might be changed in the future for other weather entities, but for now thunderbolt is the only option
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
  })
  // Called spawn mob. Does the same thing
  spec.Add(int32(mpacket.Clientbound_SpawnLivingEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x03
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    out.WriteVarInt(packet.Ints[1])
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteByte(packet.Bytes[2])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPainting), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x04
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    out.WriteVarInt(packet.Ints[1])
    out.WritePosition(packet.Positions[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPlayer), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x05
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
  })
  spec.Add(int32(mpacket.Clientbound_EntityAnimation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x06
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_Statistics), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x07
    out.WritePosition(packet.Positions[0])
    out.WriteByte(packet.Bytes[0])
    out.WriteByteArray(packet.NBTTags[0])
  })
  // spec.Add(packet.Clientbound_AcknowledgePlayerDigging, func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x08
  //   out.WriteLong(packet.Positions[0])
  //   out.WriteVarInt(packet.Ints[0])
  //   out.WriteVarInt(packet.Ints[1])
  //   out.WriteBool(packet.Bools[0])
  // })
  spec.Add(int32(mpacket.Clientbound_BlockBreakAnimation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x08
    out.WriteVarInt(packet.Ints[0])
    out.WritePosition(packet.Positions[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_BlockEntityData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x09
    out.WritePosition(packet.Positions[0])
    out.WriteByte(packet.Bytes[0])
    out.WriteByteArray(packet.NBTTags[0])
  })
  spec.Add(int32(mpacket.Clientbound_BlockAction), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0a
    out.WritePosition(packet.Positions[0])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteVarInt(packet.Ints[0])
  })
  spec.Add(int32(mpacket.Clientbound_BlockChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0b
    out.WritePosition(packet.Positions[0])
    // In this version, this int is the same format as 1.8 -> (type << 4 | meta)
    out.WriteVarInt(packet.Ints[0])
  })
  spec.Add(int32(mpacket.Clientbound_BossBar), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0c
    action := packet.Ints[0]
    boss_bar := &pb.BossBar{}
    err := ptypes.UnmarshalAny(packet.Other[0], boss_bar)
    if err != nil {
      panic(err)
    }
    out.WriteUUID(util.NewUUIDFromProto(boss_bar.Uuid))
    out.WriteVarInt(action)
    if action == 0 { // add
      out.WriteString(boss_bar.Title)
      out.WriteFloat(boss_bar.Value)
      out.WriteVarInt(boss_bar.Color)
      out.WriteVarInt(boss_bar.Division)
      out.WriteByte(byte(boss_bar.Flags))
    } else if action == 1 { // remove
    } else if action == 2 { // update health
      out.WriteFloat(boss_bar.Value)
    } else if action == 3 { // update title
      out.WriteString(boss_bar.Title)
    } else if action == 4 { // update style
      out.WriteVarInt(boss_bar.Color)
      out.WriteVarInt(boss_bar.Division)
    } else if action == 5 { // update flags
      out.WriteByte(byte(boss_bar.Flags))
    } else {
      panic("Invalid boss bar action!")
    }
  })
  spec.Add(int32(mpacket.Clientbound_ServerDifficulty), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0d
    // This updates the button in the escape menu
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_TabComplete), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0e
    // TODO: Fix tab completes
    out.WriteVarInt(2)
    out.WriteString("big")
    out.WriteString("gaming")
  })
  spec.Add(int32(mpacket.Clientbound_ChatMessage), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0f
    out.WriteString(packet.Strings[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_MultiBlockChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x10
    out.WriteInt(packet.Ints[0])
    out.WriteInt(packet.Ints[1])
    out.WriteVarInt(int32(len(packet.Other)))
    for _, val := range packet.Other {
      multi_block_change := &pb.MultiBlockChange{}
      err := ptypes.UnmarshalAny(val, multi_block_change)
      if err != nil {
        panic(err)
      }
      horizontal := byte(multi_block_change.X & 0x0f) << 4 | byte(multi_block_change.Z & 0x0f)
      out.WriteByte(horizontal)
      out.WriteByte(byte(multi_block_change.Y))
      out.WriteVarInt(multi_block_change.BlockState)
    }
  })
  // Called confirm transaction in 1.12
  spec.Add(int32(mpacket.Clientbound_WindowConfirm), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x11
    out.WriteByte(packet.Bytes[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_CloseWindow), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x12
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_OpenWindow), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x13
    // Window id is varint in newer versions
    out.WriteByte(byte(packet.Ints[0]))
    // Window type is hard; it is a varint in newer versions, but a string
    // in this version. Need to update grpc spec.
    out.WriteString("minecraft:crafting")
    // Title
    out.WriteString(packet.Strings[0])
    // Number of slots. Doesn't make sense with newer window types.
    // Will always be 0 with non-storage windows.
    out.WriteByte(0)
    // EID is next, but that is only used in horse windows.
  })
  spec.Add(int32(mpacket.Clientbound_WindowItems), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x14
    // this is the 'multi block change' of inventory items: It changes multiple items, but otherwise is the same as SetSlot
    out.WriteByte(packet.Bytes[0])
    out.WriteShort(uint16(len(packet.Other)))
    for _, val := range packet.Other {
      item := &pb.Item{}
      err := ptypes.UnmarshalAny(val, item)
      if err != nil {
        panic(err)
      }
      out.WriteItem(item)
    }
  })
  spec.Add(int32(mpacket.Clientbound_WindowProperty), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x15
    // This is things like the little arrow in the furnace
    out.WriteByte(packet.Bytes[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
  })
  spec.Add(int32(mpacket.Clientbound_SetSlot), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x16
    out.WriteByte(packet.Bytes[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    item := &pb.Item{}
    err := ptypes.UnmarshalAny(packet.Other[0], item)
    if err != nil {
      panic(err)
    }
    out.WriteItem(item)
  })
  spec.Add(int32(mpacket.Clientbound_SetCooldown), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x17
    // Is the little cooldown on ender pearls
    out.WriteVarInt(packet.Ints[0])
    out.WriteVarInt(packet.Ints[1])
  })
  spec.Add(int32(mpacket.Clientbound_PluginMessage), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x18
    out.WriteString(packet.Strings[0])
    out.WriteByteArray(packet.ByteArrays[0])
  })
  spec.Add(int32(mpacket.Clientbound_NamedSoundEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x19
    out.WriteString(packet.Strings[0])
    out.WriteVarInt(packet.Ints[0])
    out.WriteInt(packet.Ints[1])
    out.WriteInt(packet.Ints[2])
    out.WriteInt(packet.Ints[3])
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
  })
  spec.Add(int32(mpacket.Clientbound_Disconnect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1a
    out.WriteString(packet.Strings[0])
  })
  spec.Add(int32(mpacket.Clientbound_EntityStatus), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1b
    // Things like damage animation
    out.WriteInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_Explosion), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1c
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
    out.WriteFloat(packet.Floats[2])
    out.WriteFloat(packet.Floats[3])
    out.WriteInt(int32(len(packet.ByteArrays[0])))
    out.WriteByteArray(packet.ByteArrays[0])
    out.WriteFloat(packet.Floats[4])
    out.WriteFloat(packet.Floats[5])
    out.WriteFloat(packet.Floats[6])
  })
  spec.Add(int32(mpacket.Clientbound_UnloadChunk), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1d
    out.WriteInt(packet.Ints[0])
    out.WriteInt(packet.Ints[1])
  })
  spec.Add(int32(mpacket.Clientbound_ChangeGameState), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1e
    out.WriteByte(packet.Bytes[0])
    out.WriteFloat(packet.Floats[0])
  })
  spec.Add(int32(mpacket.Clientbound_KeepAlive), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1f
    out.WriteVarInt(packet.Ints[0])
  })
  // TODO: This packet is most likely broken. Need more testing.
  spec.Add(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x20
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true)
    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask)

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    // Make an ordered list of chunk sections
    // chunk.Sections is a map
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    // iterates through chunks in order, from ground up
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteByte(byte(section.BitsPerBlock))

      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      } else {
        // Dummy palette; section.Data includes global block ids.
        buf.WriteVarInt(0)
      }

      buf.WriteVarInt(int32(len(section.Data)) / 8) // Number of longs
      buf.WriteByteArray(section.Data)

      // Light data
      for i := 0; i < 16*16*16; i++ {
        // We are sending both block light and sky light data (half a byte each)
        buf.WriteByte(0xff)
      }
    }
    // Biome data
    for i := 0; i < 256; i++ {
      buf.WriteByte(127) // Void biome
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    // No tile entities
    out.WriteVarInt(0)
  })
  spec.Add(int32(mpacket.Clientbound_Effect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x21
    out.WriteInt(packet.Ints[0])
    out.WritePosition(packet.Positions[0])
    out.WriteInt(packet.Ints[1])
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_Particle), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x22
    out.WriteInt(packet.Ints[0])
    out.WriteBool(packet.Bools[0])
    out.WriteFloat(float32(packet.Doubles[0]))
    out.WriteFloat(float32(packet.Doubles[1]))
    out.WriteFloat(float32(packet.Doubles[2]))
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
    out.WriteFloat(packet.Floats[2])
    out.WriteFloat(packet.Floats[3])
    out.WriteInt(packet.Ints[0])
    out.WriteByteArray(packet.ByteArrays[0])
  })
  // Included with chunk data
  // spec.Add(int32(mpacket.Clientbound_UpdateLight), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x23
  //   out.WriteVarInt(packet.Ints[0])
  //   out.WriteVarInt(packet.Ints[1])
  //   out.WriteVarInt(packet.Ints[2]) // bitmask for sky light included in byte array
  //   out.WriteVarInt(packet.Ints[3]) // bitmask for block light included in byte array
  //   out.WriteVarInt(packet.Ints[4]) // bitmask for sky light is 0
  //   out.WriteVarInt(packet.Ints[5]) // bitmask for block light is 0
  //   light_data := make([]byte, 2048)
  //   for i := 0; i < 2048; i++ {
  //     light_data[i] = 0xff
  //   }
  //   bitmask := packet.Ints[2]
  //   for i := 0; i < 18; i++ {
  //     if bitmask & (1 << i) != 0 {
  //       out.WriteVarInt(2048)
  //       out.WriteByteArray(light_data)
  //     }
  //   }
  // })
  spec.Add(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x23
    out.WriteInt(packet.Ints[0]) // eid
    out.WriteByte(packet.Bytes[0]) // gamemode
    out.WriteInt(packet.Ints[1]) // dimension
    // out.WriteLong(packet.Longs[0]) // hashed seed
    out.WriteByte(0) // difficulty (peaceful)
    out.WriteByte(0) // ignored
    out.WriteString(packet.Strings[0]) // level type
    // out.WriteVarInt(packet.Ints[2]) // view distance
    // out.WriteBool(packet.Bools[0]) // reduced debug info
    // out.WriteBool(packet.Bools[1]) // enable respawn screen
    out.WriteBool(false) // Reduced debug info
  })
  spec.Add(int32(mpacket.Clientbound_MapData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x24
    map_data := &pb.Map{}
    err := ptypes.UnmarshalAny(packet.Other[0], map_data)
    if err != nil {
      panic(err)
    }
    out.WriteVarInt(map_data.Id)
    out.WriteByte(byte(map_data.Scale))
    out.WriteBool(map_data.ShowPlayer)
    // out.WriteBool(map_data.Locked) Added in later versions
    out.WriteVarInt(int32(len(map_data.Icons)))
    for _, icon := range map_data.Icons {
      if icon.Type > 15 {
        // Default to white arrow if it's too new of an icon
        icon.Type = 0
      }
      out.WriteByte(byte(icon.Direction & 0x0f) | byte(icon.Type & 0x0f) << 4)
      out.WriteByte(byte(icon.X))
      out.WriteByte(byte(icon.Z))
      // out.WriteBool(icon.HasName)
      // out.WriteString(icon.Name)
    }
    out.WriteByte(byte(map_data.Columns))
    if map_data.Columns != 0 {
      out.WriteByte(byte(map_data.Rows))
      out.WriteByte(byte(map_data.XOffset))
      out.WriteByte(byte(map_data.ZOffset))
      out.WriteVarInt(int32(len(map_data.Data)))
      out.WriteByteArray(map_data.Data)
    }
  })
  // This packet is called 'Entity', and just says there is an entity in the world.
  // Later, an on_ground field is added to this packet.
  spec.Add(int32(mpacket.Clientbound_EntityOnGround), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x25
  })
  spec.Add(int32(mpacket.Clientbound_EntityPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x26
    out.WriteVarInt(packet.Ints[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_EntityPositionAndRotation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x27
    out.WriteVarInt(packet.Ints[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_EntityRotation), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x28
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_VehicleMove), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x29
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
  })
  spec.Add(int32(mpacket.Clientbound_OpenSignEditor), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2a
    out.WritePosition(packet.Positions[0])
  })
  spec.Add(int32(mpacket.Clientbound_PlayerAbilities), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2b
    out.WriteByte(packet.Bytes[0])
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
  })
  spec.Add(int32(mpacket.Clientbound_EnterCombat), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2c
    event := packet.Ints[0]
    out.WriteVarInt(event)
    switch event {
    case 0:
      // Enter combat, no fields
    case 1:
      // End combat
      out.WriteVarInt(packet.Ints[1]) // duration
      out.WriteInt(packet.Ints[2]) // EID
    case 2:
      // Entity dead
      out.WriteVarInt(packet.Ints[1]) // player id
      out.WriteInt(packet.Ints[2]) // EID
      out.WriteString(packet.Strings[0]) // chat message
    }
  })
  // Called player list item
  spec.Add(int32(mpacket.Clientbound_PlayerInfo), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2d
    // Updates the tab list, and must be sent before SpawnPlayer
    out.WriteVarInt(packet.Ints[0])
    out.WriteVarInt(packet.Ints[1])
    out.WriteByteArray(packet.ByteArrays[0])
  })
  spec.Add(int32(mpacket.Clientbound_PlayerPositionAndLook), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x2e
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteFloat(packet.Floats[0])
    out.WriteFloat(packet.Floats[1])
    out.WriteByte(packet.Bytes[0])
    out.WriteVarInt(packet.Ints[0])
  })
  // Use bed. This is different in newer versions, unclear how.
  // TODO: Find out how beds work in newer versions.
  spec.Add(-1, nil) // 0x2f
  spec.Add(int32(mpacket.Clientbound_UnlockRecipies), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x30
    out.WriteVarInt(packet.Ints[0])
    out.WriteBool(packet.Bools[0]) // crafting book open
    out.WriteBool(packet.Bools[1]) // crafting book filter enabled
    // Smelting book was added later
    // Number of array elements
    out.WriteVarInt(0)
    // Is the type init?
    if packet.Ints[0] == 0 {
      // Number of array elements
      out.WriteVarInt(0)
    }
  })
  spec.Add(int32(mpacket.Clientbound_DestroyEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x31
    out.WriteVarInt(int32(len(packet.IntArrays[0].Ints)))
    for _, value := range packet.IntArrays[0].Ints {
      out.WriteVarInt(value)
    }
  })
  spec.Add(int32(mpacket.Clientbound_RemoveEntityEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x32
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_ResourcePack), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x33
    out.WriteString(packet.Strings[0])
    out.WriteString(packet.Strings[1])
  })
  spec.Add(int32(mpacket.Clientbound_Respawn), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x34
    out.WriteInt(packet.Ints[0])
    out.WriteLong(0)
    out.WriteByte(packet.Bytes[0])
    out.WriteString("flat")
  })
  spec.Add(int32(mpacket.Clientbound_EntityHeadLook), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x35
    out.WriteVarInt(packet.Ints[0])
    out.WriteByte(packet.Bytes[0])
  })
  spec.Add(int32(mpacket.Clientbound_SelectAdvancementTab), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x36
    // Identifier will always be present
    out.WriteBool(true)
    out.WriteString(packet.Strings[0])
  })
  spec.Add(int32(mpacket.Clientbound_WorldBorder), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x37
    action := packet.Ints[0]
    out.WriteVarInt(action)
    switch action {
    case 0:
      // Set size
      out.WriteDouble(packet.Doubles[0])
    case 1:
      // Lerp size
      out.WriteDouble(packet.Doubles[0]) // old size
      out.WriteDouble(packet.Doubles[1]) // new size
      out.WriteVarInt(packet.Ints[0]) // speed in ticks
    case 2:
      // Set center
      out.WriteDouble(packet.Doubles[0]) // X
      out.WriteDouble(packet.Doubles[1]) // Z
    case 3:
      // Init
      out.WriteDouble(packet.Doubles[0]) // X
      out.WriteDouble(packet.Doubles[1]) // Z
      out.WriteDouble(packet.Doubles[2]) // old size
      out.WriteDouble(packet.Doubles[3]) // new size
      out.WriteVarInt(packet.Ints[0]) // speed in ticks
      // Portal teleport boundry. Resulting coordinates from a portal
      // teleport are limited to +-packetInts[1]. Usually 29999984
      out.WriteVarInt(packet.Ints[1])
      out.WriteVarInt(packet.Ints[2]) // warning time in seconds
      out.WriteVarInt(packet.Ints[3]) // warning distance in blocks
    case 4:
      // Set warning time
      out.WriteVarInt(packet.Ints[0]) // warning time in seconds
    case 5:
      // Set warning distance
      out.WriteVarInt(packet.Ints[0]) // warning distance in blocks
    }
  })
  spec.Add(int32(mpacket.Clientbound_Camera), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x38
    // Used when you spectate an entity
    out.WriteVarInt(packet.Ints[0]) // EID
  })
  spec.Add(int32(mpacket.Clientbound_HeldItemChange), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x39
    out.WriteByte(packet.Bytes[0]) // Slot
  })
  // Doesn't exist in 1.12
  // spec.Add(int32(mpacket.Clientbound_UpdateViewPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x41
  //   // Sent when you a player moves between chunks
  //   out.WriteVarInt(packet.Ints[0])
  //   out.WriteVarInt(packet.Ints[1])
  // })
  spec.Add(int32(mpacket.Clientbound_DisplayScoreboard), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3a
    out.WriteByte(packet.Bytes[0])
    out.WriteString(packet.Strings[0])
  })
  spec.Add(int32(mpacket.Clientbound_EntityMetadata), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3b
    out.WriteVarInt(packet.Ints[0])
    meta := &pb.EntityMetadata{}
    err := ptypes.UnmarshalAny(packet.Other[0], meta)
    if err != nil {
      panic(err)
    }
    for index, value := range meta.Values {
      out.WriteByte(byte(index))
      out.WriteVarInt(value.Type)
      out.WriteByteArray(value.Data)
    }
    // terminates the metadata list
    out.WriteByte(0xff)
  })
  spec.Add(int32(mpacket.Clientbound_AttachEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3c
    out.WriteInt(packet.Ints[0])
    out.WriteInt(packet.Ints[0])
  })
  spec.Add(int32(mpacket.Clientbound_EntityVelocity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3d
    out.WriteVarInt(packet.Ints[0])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
  })
  spec.Add(int32(mpacket.Clientbound_EntityEquipment), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3e
    out.WriteVarInt(packet.Ints[0])
    out.WriteVarInt(packet.Ints[1])
    item := &pb.Item{}
    err := ptypes.UnmarshalAny(packet.Other[0], item)
    if err != nil {
      panic(err)
    }
    out.WriteItem(item)
  })
  spec.Add(int32(mpacket.Clientbound_SetExp), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x3f
    out.WriteFloat(packet.Floats[0])
    out.WriteVarInt(packet.Ints[0])
    out.WriteVarInt(packet.Ints[1])
  })
  spec.Add(int32(mpacket.Clientbound_UpdateHealth), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x40
    out.WriteFloat(packet.Floats[0])
    out.WriteVarInt(packet.Ints[0])
    out.WriteFloat(packet.Floats[1])
  })
  spec.Add(int32(mpacket.Clientbound_ScoreboardObjective), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x41
    out.WriteString(packet.Strings[0])
    mode := packet.Bytes[0]
    out.WriteByte(mode)
    if mode == 0 || mode == 2 {
      out.WriteString(packet.Strings[1])
      out.WriteVarInt(packet.Ints[0])
    }
  })
  spec.Add(int32(mpacket.Clientbound_SetPassengers), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x42
    // Vehicle EID
    out.WriteVarInt(packet.Ints[0])
    // Passengers
    out.WriteVarInt(int32(len(packet.IntArrays[0].Ints)))
    for _, value := range packet.IntArrays[0].Ints {
      out.WriteVarInt(value)
    }
  })
  spec.Add(int32(mpacket.Clientbound_Teams), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x43
    out.WriteString(packet.Strings[0])
    mode := packet.Bytes[0]
    out.WriteByte(mode)
    switch mode {
    case 0:
      // Create team
      out.WriteString(packet.Strings[1]) // display name
      out.WriteString(packet.Strings[2]) // prefix
      out.WriteString(packet.Strings[3]) // suffix
      out.WriteByte(packet.Bytes[0]) // flags
      out.WriteString(packet.Strings[4]) // name tag visibility rule
      out.WriteString(packet.Strings[5]) // collision rule
      out.WriteByte(packet.Bytes[1]) // color code. -1 resets the color
      // List of indentifiers for the entities on the team.
      // Players -> username
      // Entities -> UUID string
      out.WriteVarInt(int32(len(packet.StringArrays[0].Strings)))
      for _, value := range packet.StringArrays[0].Strings {
        out.WriteString(value)
      }
    case 1:
      // Remove team; no fields
    case 2:
      // Update team info
      out.WriteString(packet.Strings[1]) // display name
      out.WriteString(packet.Strings[2]) // prefix
      out.WriteString(packet.Strings[3]) // suffix
      out.WriteByte(packet.Bytes[0]) // flags
      out.WriteString(packet.Strings[4]) // name tag visibility rule
      out.WriteString(packet.Strings[5]) // collision rule
      out.WriteByte(packet.Bytes[1]) // color code. -1 resets the color
    case 3, 4:
      // Add/Remove entities from team
      out.WriteVarInt(int32(len(packet.StringArrays[0].Strings)))
      for _, value := range packet.StringArrays[0].Strings {
        out.WriteString(value)
      }
    }
  })
  spec.Add(int32(mpacket.Clientbound_UpdateScore), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x44
    out.WriteString(packet.Strings[0])
    action := packet.Bytes[0]
    out.WriteByte(action)
    out.WriteString(packet.Strings[1])
    if action == 0 {
      out.WriteVarInt(packet.Ints[0])
    }
  })
  spec.Add(int32(mpacket.Clientbound_SpawnPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x45
    out.WritePosition(packet.Positions[0])
  })
  spec.Add(int32(mpacket.Clientbound_TimeUpdate), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x46
    // World age in ticks; not affected by vanilla commands
    out.WriteLong(packet.Longs[0])
    // Time of day. If negative, then the sun will stop moving,
    // and stay at abs(time).
    out.WriteLong(packet.Longs[1])
  })
  spec.Add(int32(mpacket.Clientbound_Title), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x47
    action := packet.Ints[0]
    out.WriteVarInt(action)
    switch action {
    case 0, 1, 2:
      // Set title, subtitle, and action bar
      out.WriteString(packet.Strings[0])
    case 3:
      // Set times
      out.WriteInt(packet.Ints[1])
      out.WriteInt(packet.Ints[2])
      out.WriteInt(packet.Ints[3])
    }
  })
  spec.Add(int32(mpacket.Clientbound_SoundEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x48
    out.WriteVarInt(packet.Ints[0]) // Sound id
    out.WriteVarInt(packet.Ints[1]) // Sound category
    out.WriteInt(packet.Ints[2]) // X
    out.WriteInt(packet.Ints[3]) // Y
    out.WriteInt(packet.Ints[4]) // Z
    out.WriteFloat(packet.Floats[0]) // Volume
    out.WriteFloat(packet.Floats[1]) // Pitch
  })
  spec.Add(int32(mpacket.Clientbound_PlayerListHeader), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x49
    out.WriteString(packet.Strings[0])
    out.WriteString(packet.Strings[1])
  })
  // This just plays the pickup animation
  spec.Add(int32(mpacket.Clientbound_CollectItem), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x4a
    out.WriteVarInt(packet.Ints[0]) // Collected item EID
    out.WriteVarInt(packet.Ints[1]) // Collector EID
    out.WriteVarInt(packet.Ints[2]) // Pickup item count
  })
  spec.Add(int32(mpacket.Clientbound_EntityTeleport), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x4b
    out.WriteVarInt(packet.Ints[0])
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteBool(packet.Bools[0])
  })
  spec.Add(int32(mpacket.Clientbound_Advancements), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x4c
    out.WriteBool(packet.Bools[0]) // Reset/clear
    // TODO: Add advancement protobuf
    out.WriteVarInt(0)
    out.WriteVarInt(0)
    out.WriteVarInt(0)
  })
  spec.Add(int32(mpacket.Clientbound_EntityProperties), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x4d
    out.WriteVarInt(packet.Ints[0])
    out.WriteInt(int32(len(packet.Other)))
    for _, proto := range packet.Other {
      prop := &pb.EntityProperty{}
      err := ptypes.UnmarshalAny(proto, prop)
      if err != nil {
        panic(err)
      }
      out.WriteString(prop.Name)
      out.WriteDouble(prop.Value)
      out.WriteVarInt(0)
    }
  })
  spec.Add(int32(mpacket.Clientbound_EntityEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x4e
    out.WriteVarInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Effect ID
    out.WriteByte(packet.Bytes[0]) // Level (displayed as this number + 1)
    out.WriteVarInt(packet.Ints[0]) // Duration
    // 0x01: Is from beacon?
    // 0x02: Show particles?
    out.WriteByte(packet.Bytes[0]) // Flags
  })
  // login packet (custom, captured by proxy)
  spec.Add(int32(mpacket.Clientbound_Login), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteString(util.NewUUIDFromProto(packet.Uuids[0]).ToDashedString())
    out.WriteString(packet.Strings[0])
  })
  return spec
}
