package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

// Takes a 1.13 spec
func Make1_14Types(spec *registry.Registry) *registry.Registry {
  spec.Set(int32(mpacket.Clientbound_SpawnEntity), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x00
    out.WriteVarInt(packet.Ints[0])
    out.WriteUUID(util.NewUUIDFromProto(packet.Uuids[0]))
    // CHANGE: Entity type is now a varint
    out.WriteVarInt(packet.Ints[1])
    out.WriteDouble(packet.Doubles[0])
    out.WriteDouble(packet.Doubles[1])
    out.WriteDouble(packet.Doubles[2])
    out.WriteByte(packet.Bytes[0])
    out.WriteByte(packet.Bytes[1])
    out.WriteInt(packet.Ints[2])
    out.WriteShort(uint16(packet.Shorts[0]))
    out.WriteShort(uint16(packet.Shorts[1]))
    out.WriteShort(uint16(packet.Shorts[2]))
  })
  spec.Set(int32(mpacket.Clientbound_ServerDifficulty), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x0d
    // This updates the button in the escape menu
    out.WriteByte(packet.Bytes[0])
    // CHANGE: Added this field, which is if the difficulty is locked
    out.WriteBool(packet.Bools[0])
  })
  // Ah yes, mojang.
  spec.Move(int32(mpacket.Clientbound_NBTQueryResponse), 0x54 - 6) // This ends up being 0x54 after all the inserts below
  spec.Move(int32(mpacket.Clientbound_OpenWindow), 0x2e - 4) // This ends up at 0x2e after all the inserts below
  // Yay, this is different from open inventory now.
  spec.Insert(0x1f, int32(mpacket.Clientbound_OpenHorseWindow), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // MORE CHUNK CHANGES WOOOOOOOOOOOOOO
  spec.Set(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x21
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true) // Full chunk

    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask) // bitmask

    // CHANGE: Added this thing
    heightmap := nbt.NewCompoundTag("")
    heightmap.CompoundAddChild("MOTION_BLOCKING", 0x0c).WriteLongArray(chunk.Heightmap)
    out.WriteByteArray(heightmap.Serialize())

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteShort(uint16(section.NonAirBlocks))
      buf.WriteByte(byte(section.BitsPerBlock))
      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      }

      // section.Data is a byte array, but it is parsed as a long array, so we give it a length in longs
      buf.WriteVarInt(int32(len(section.Data) / 8))
      // section.Data is a big endian encoded long array
      buf.WriteByteArray(section.Data)
    }
    // Biome data. It becomes 3D in 1.15. WOOOOOO
    for i := 0; i < 256; i++ {
      buf.WriteInt(127) // void (grass looks ugly, should change)
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    out.WriteVarInt(0)
  })
  // Now, this is seperate from chunk data. This makes things difficult with seperate versions, but it's probably better this way.
  spec.Insert(0x24, int32(mpacket.Clientbound_UpdateLight), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteVarInt(packet.Ints[0]) // X
    out.WriteVarInt(packet.Ints[1]) // Z
    out.WriteVarInt(packet.Ints[2]) // bitmask for sky light included in byte array
    out.WriteVarInt(packet.Ints[3]) // bitmask for block light included in byte array
    out.WriteVarInt(packet.Ints[4]) // bitmask for sky light is 0
    out.WriteVarInt(packet.Ints[5]) // bitmask for block light is 0
    light_data := make([]byte, 2048)
    for i := 0; i < 2048; i++ {
      light_data[i] = 0xff
    }
    bitmask := packet.Ints[2]
    for i := 0; i < 18; i++ {
      if bitmask & (1 << i) != 0 {
        out.WriteVarInt(2048)
        out.WriteByteArray(light_data)
      }
    }
  })
  spec.Set(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x25
    out.WriteInt(packet.Ints[0]) // EID
    out.WriteByte(packet.Bytes[0]) // Gamemode
    out.WriteInt(packet.Ints[1]) // Dimension
    // out.WriteByte(0) // CHANGE: Removed difficulty value
    out.WriteByte(0) // Ignored
    out.WriteString(packet.Strings[0]) // Level type
    out.WriteVarInt(packet.Ints[2]) // CHANGE: Added view distance
    out.WriteBool(packet.Bools[0]) // Reduced debug info
  })
  // Villager trade list. Very self explanatory.
  spec.Insert(0x27, int32(mpacket.Clientbound_TradeList), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // This was moved after the movement packets, and the trade list was inserted where it was before.
  spec.Move(int32(mpacket.Clientbound_EntityOnGround), 0x2b)
  // Opens a book ui on the client.
  spec.Insert(0x2d, int32(mpacket.Clientbound_OpenBook), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // The use bed packet, which no longer exists.
  spec.Remove(0x36)
  // Sent when moving between chunks. I guess this needs to be a thing.
  spec.Insert(0x40, int32(mpacket.Clientbound_UpdateViewPosition), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteVarInt(packet.Ints[0]) // Chunk X
    out.WriteVarInt(packet.Ints[1]) // Chunk Z
  })
  // Update view distance. Never used on the dedicated server, so it is unclear if this is a usable packet.
  // This is only used internally when the client is in singleplayer, and changes their view distance.
  spec.Insert(0x41, -1, nil)
  // Entity sound effect. For some reason, this is seperate from sound effect. Cool.
  spec.Insert(0x50, int32(mpacket.Clientbound_EntitySoundEffect), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteVarInt(packet.Ints[0]) // Sound id
    out.WriteVarInt(packet.Ints[1]) // Category
    out.WriteVarInt(packet.Ints[2]) // EID
    out.WriteFloat(packet.Floats[0]) // Volume
    out.WriteFloat(packet.Floats[1]) // Pitch
  })
  // Switched stop sound and sound effect. Thank you, Mojang.
  spec.Move(int32(mpacket.Clientbound_StopSound), 0x51)
  return spec
}
