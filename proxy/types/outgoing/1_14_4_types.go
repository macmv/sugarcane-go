package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// Takes a 1.14.3 spec
func Make1_14_4Types(spec *registry.Registry) *registry.Registry {
  spec.Set(int32(mpacket.Clientbound_TradeList), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1f
    // Added Demand to this packet
  })
  // Yay more random packets. This is actually a useful packet, as it gives the server
  // more control over the clientside animations.
  spec.Add(int32(mpacket.Clientbound_AcknowledgePlayerDigging), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x5c
    out.WriteLong(packet.Positions[0]) // Block pos
    out.WriteVarInt(packet.Ints[0]) // Block id
    out.WriteVarInt(packet.Ints[1]) // Status 0 -> start digging, 1 -> cancel digging, 2 -> finish digging
    out.WriteBool(packet.Bools[0]) // Successful (if false, the client will undo changes locally)
  })
  spec.Move(int32(mpacket.Clientbound_Login), 0x5d)
  return spec
}
