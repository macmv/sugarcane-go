package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// Takes a 1.14 spec (1.14, 1.14.1, and 1.14.2 are the same (entity metadata changed, but the protocol didn't))
func Make1_14_3Types(spec *registry.Registry) *registry.Registry {
  spec.Set(int32(mpacket.Clientbound_TradeList), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1f
    // Added Can Restock to this packet
  })
  return spec
}
