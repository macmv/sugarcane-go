package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

// Takes a 1.14.4 spec
func Make1_15Types(spec *registry.Registry) *registry.Registry {
  spec.Move(int32(mpacket.Clientbound_AcknowledgePlayerDigging), 0x08)
  // MORE CHUNK CHANGES WOOOOOOOOOOOOOO
  // This time, they added 3D biomes. More fun times, especially because now biomes are only accurate to
  // 4x4x4 areas. I mean this is great for the nether, but is very annoying in the overworld.
  spec.Set(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x22
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true) // Full chunk

    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask) // bitmask

    heightmap := nbt.NewCompoundTag("")
    heightmap.CompoundAddChild("MOTION_BLOCKING", 0x0c).WriteLongArray(chunk.Heightmap)
    out.WriteByteArray(heightmap.Serialize())

    // Biome data. CHANGE: Its 3D now. Yay
    for i := 0; i < 1024; i++ {
      out.WriteInt(127) // void (grass looks ugly, should change)
    }

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteShort(uint16(section.NonAirBlocks))
      buf.WriteByte(byte(section.BitsPerBlock))
      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      }

      // section.Data is a byte array, but it is parsed as a long array, so we give it a length in longs
      buf.WriteVarInt(int32(len(section.Data) / 8))
      // section.Data is a big endian encoded long array
      buf.WriteByteArray(section.Data)
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    out.WriteVarInt(0)
  })
  spec.Set(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x26
    out.WriteInt(packet.Ints[0]) // Player's EID
    out.WriteByte(packet.Bytes[0]) // Gamemode
    out.WriteInt(packet.Ints[1]) // Dimension
    out.WriteLong(packet.Longs[0]) // Hashed seed
    out.WriteByte(0) // Ignored
    out.WriteString(packet.Strings[0]) // Level type
    out.WriteVarInt(packet.Ints[2]) // View distance
    out.WriteBool(packet.Bools[0]) // Reduced debug info
    out.WriteBool(packet.Bools[1]) // Enable respawn screen
  })
  return spec
}
