package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// Takes a 1.12 spec (1.12.1 and 1.12 are the same)
func Make1_12_2Types(spec *registry.Registry) *registry.Registry {
  spec.Set(int32(mpacket.Clientbound_KeepAlive), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x1f
    // It was a varint, now it's a long
    out.WriteLong(uint64(packet.Ints[0]))
  })
  spec.Insert(0x2b, int32(mpacket.Clientbound_CraftRecipeResponse), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteByte(packet.Bytes[0])
    // In newer versions, this is a string (identifier for the recipe)
    // TODO: Implement both recipe names and int ids.
    out.WriteVarInt(packet.Ints[0])
  })
  return spec
}
