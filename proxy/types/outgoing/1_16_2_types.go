package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

// Takes a 1.16 spec
func Make1_16_2Types(spec *registry.Registry) *registry.Registry {
  // Yay more fun times. This time they removed the changes in 1.16, and added a 'biome length' field.
  spec.Set(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x20
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true) // Full chunk
    // CHANGE: Removed this bool

    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask) // bitmask

    heightmap := nbt.NewCompoundTag("")
    heightmap.CompoundAddChild("MOTION_BLOCKING", 0x0c).WriteLongArray(chunk.Heightmap)
    out.WriteByteArray(heightmap.Serialize())

    // Biome data
    // CHANGE: Added length
    out.WriteVarInt(1024)
    biome := chunk.X ^ chunk.Z
    biome = biome % 2
    if biome < 0 {
      biome += 2
    }
    for i := 0; i < 1024; i++ {
      // CHANGE: VarInt instead of int
      out.WriteVarInt(biome) // The biome defined below
    }

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteShort(uint16(section.NonAirBlocks))
      buf.WriteByte(byte(section.BitsPerBlock))
      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      }

      // section.Data is a byte array, but it is parsed as a long array, so we give it a length in longs
      buf.WriteVarInt(int32(len(section.Data) / 8))
      // section.Data is a big endian encoded long array
      buf.WriteByteArray(section.Data)
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    out.WriteVarInt(0)
  })
  // They changed this again. Now, 'dimension' is an nbt tag, instead of a string.
  spec.Set(int32(mpacket.Clientbound_JoinGame), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x24
    out.WriteInt(packet.Ints[0]) // Player's EID
    // CHANGE: Added this bool
    out.WriteBool(false) // Is hardcore
    out.WriteByte(packet.Bytes[0]) // Gamemode
    out.WriteByte(1) // Previous gamemode (creative for now)
    out.WriteVarInt(1) // World count
    out.WriteString("minecraft:overworld") // One string for each world name

    // CHANGE: This is totally different now
    tag := nbt.NewCompoundTag("")
    // Biomes
    biomes := tag.CompoundAddChild("minecraft:worldgen/biome", nbt.TAG_Compound)
    biomes.CompoundAddChild("type", nbt.TAG_String).WriteString("minecraft:worldgen/biome")
    list := biomes.CompoundAddChild("value", nbt.TAG_List)

    item := list.ListAddFirst(nbt.TAG_Compound)
    item.CompoundAddChild("name", nbt.TAG_String).WriteString("minecraft:plains")
    item.CompoundAddChild("id",   nbt.TAG_Int).WriteInt(0)
    elem := item.CompoundAddChild("element", nbt.TAG_Compound)
    elem.CompoundAddChild("precipitation", nbt.TAG_String).WriteString("rain")
    elem.CompoundAddChild("category",      nbt.TAG_String).WriteString("mesa")
    elem.CompoundAddChild("depth",         nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("temperature",   nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("downfall",      nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("scale",         nbt.TAG_Float).WriteFloat(1)
    effects := elem.CompoundAddChild("effects", nbt.TAG_Compound)
    effects.CompoundAddChild("sky_color",       nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("water_fog_color", nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("fog_color",       nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("water_color",     nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("foliage_color",   nbt.TAG_Int).WriteInt(0xff0000)
    effects.CompoundAddChild("grass_color",     nbt.TAG_Int).WriteInt(0xff0000)

    item = list.ListAddChild()
    item.CompoundAddChild("name", nbt.TAG_String).WriteString("sugarcane:blue")
    item.CompoundAddChild("id",   nbt.TAG_Int).WriteInt(1)
    elem = item.CompoundAddChild("element", nbt.TAG_Compound)
    elem.CompoundAddChild("precipitation", nbt.TAG_String).WriteString("rain")
    elem.CompoundAddChild("category",      nbt.TAG_String).WriteString("mesa")
    elem.CompoundAddChild("depth",         nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("temperature",   nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("downfall",      nbt.TAG_Float).WriteFloat(1)
    elem.CompoundAddChild("scale",         nbt.TAG_Float).WriteFloat(1)
    effects = elem.CompoundAddChild("effects", nbt.TAG_Compound)
    effects.CompoundAddChild("sky_color",       nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("water_fog_color", nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("fog_color",       nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("water_color",     nbt.TAG_Int).WriteInt(0x7FA1FF)
    effects.CompoundAddChild("foliage_color",   nbt.TAG_Int).WriteInt(0x0000ff)
    effects.CompoundAddChild("grass_color",     nbt.TAG_Int).WriteInt(0x0000ff)

    // Dimensions
    dimensions := tag.CompoundAddChild("minecraft:dimension_type", nbt.TAG_Compound)
    dimensions.CompoundAddChild("type", nbt.TAG_String).WriteString("minecraft:dimension_type")
    list = dimensions.CompoundAddChild("value", nbt.TAG_List)

    item = list.ListAddFirst(nbt.TAG_Compound)
    item.CompoundAddChild("name", nbt.TAG_String).WriteString("minecraft:overworld")
    item.CompoundAddChild("id",   nbt.TAG_Int).WriteInt(0)

    elem = item.CompoundAddChild("element", nbt.TAG_Compound)
    elem.CompoundAddChild("ambient_light",        nbt.TAG_Float).WriteFloat(0)
    elem.CompoundAddChild("infiniburn",           nbt.TAG_String).WriteString("") // Firetick is off
    elem.CompoundAddChild("logical_height",       nbt.TAG_Int).WriteInt(256) // 256 block tall world
    elem.CompoundAddChild("has_raids",            nbt.TAG_Byte).WriteByte(0)
    elem.CompoundAddChild("respawn_anchor_works", nbt.TAG_Byte).WriteByte(1)
    elem.CompoundAddChild("bed_works",            nbt.TAG_Byte).WriteByte(0)
    elem.CompoundAddChild("piglin_safe",          nbt.TAG_Byte).WriteByte(0)
    elem.CompoundAddChild("natural",              nbt.TAG_Byte).WriteByte(1) // Set if this is one of the 'normal' vanilla worlds
    elem.CompoundAddChild("ultrawarm",            nbt.TAG_Byte).WriteByte(0)
    elem.CompoundAddChild("has_ceiling",          nbt.TAG_Byte).WriteByte(0)
    elem.CompoundAddChild("has_skylight",         nbt.TAG_Byte).WriteByte(1)
    // CHANGE: Replaced 'shrunk' with 'coordinate_scale'
    elem.CompoundAddChild("coordinate_scale",     nbt.TAG_Float).WriteFloat(1)
    // CHANGE: Added this. Unknown what it does
    out.WriteByteArray(tag.Serialize())

    // CHANGE: This used to be a string, now it's an nbt tag
    out.WriteByteArray(elem.Serialize())

    // World name you are spawning into
    out.WriteString("minecraft:overworld")

    // This stuff is the same
    out.WriteLong(packet.Longs[0]) // Hashed seed
    // CHANGE: This is now a varint
    out.WriteVarInt(0) // Ignored
    // CHANGE: Removed level type
    // out.WriteString(packet.Strings[0]) // Level type
    out.WriteVarInt(packet.Ints[2]) // View distance
    out.WriteBool(packet.Bools[0]) // Reduced debug info
    out.WriteBool(packet.Bools[1]) // Enable respawn screen

    // CHANGE: Added these fields
    out.WriteBool(false) // True if this is a debug mode world (makes the world unmodifiable, and set with predefined blocks)
    out.WriteBool(false) // True if this is a flat world (changes horizon and void fog)
  })
  // Ok cool.
  spec.Move(int32(mpacket.Clientbound_MultiBlockChange), 0x3b)
  return spec
}
