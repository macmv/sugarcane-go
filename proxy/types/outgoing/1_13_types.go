package outgoing

import (
  "gitlab.com/macmv/sugarcane/registry"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  mpacket "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

// Takes a 1.12.2 spec
func Make1_13Types(spec *registry.Registry) *registry.Registry {
  // Yay more chunk data changes.
  spec.Set(int32(mpacket.Clientbound_ChunkData), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x22
    chunk := &pb.Chunk{}
    err := ptypes.UnmarshalAny(packet.Other[0], chunk)
    if err != nil {
      panic(err)
    }
    out.WriteInt(chunk.X)
    out.WriteInt(chunk.Z)
    out.WriteBool(true)

    bitmask := int32(0)
    for y, _ := range chunk.Sections {
      bitmask |= (1 << y)
    }
    out.WriteVarInt(bitmask)

    buf, _ := util.NewBufferFromSlice(out.GarbageBuffer, 0)
    // Make an ordered list of chunk sections (chunk.Sections is a map)
    section_list := make([]*pb.Chunk_Section, 16)
    for i, section := range chunk.Sections {
      section_list[i] = section
    }
    // Iterates through chunks in order, from ground up
    for _, section := range section_list {
      if section == nil {
        continue
      }
      buf.WriteByte(byte(section.BitsPerBlock))

      // CHANGE: When using a global palette, we don't write a zero here anymore.
      if section.BitsPerBlock <= 8 {
        buf.WriteVarInt(int32(len(section.Palette)))
        for _, id := range section.Palette {
          buf.WriteVarInt(int32(id))
        }
      }

      buf.WriteVarInt(int32(len(section.Data)) / 8) // Number of longs
      buf.WriteByteArray(section.Data)

      // Light data
      for i := 0; i < 16*16*16; i++ {
        // We are sending both block light and sky light data (half a byte each)
        buf.WriteByte(0xff)
      }
    }
    // Biome data
    for i := 0; i < 256; i++ {
      // CHANGE: Int array instead of byte array
      buf.WriteInt(127) // Void biome
    }
    out.WriteVarInt(buf.Length())
    out.WriteByteArray(buf.GetData())
    // No tile entities
    out.WriteVarInt(0)
  })
  // They moved this packet from 0x0e to 0x10, because mojang
  spec.Move(int32(mpacket.Clientbound_TabComplete), 0x10)
  // New packet! This is the command tree we all know and love.
  spec.Insert(0x11, int32(mpacket.Clientbound_DeclareCommands), func(packet *pb.Packet, out *packet.OutgoingPacket) {
    out.WriteVarInt(int32(len(packet.Other)))
    for _, val := range packet.Other {
      node := &pb.Node{}
      err := ptypes.UnmarshalAny(val, node)
      if err != nil {
        panic(err)
      }
      out.WriteByte(byte(node.Flags))
      out.WriteVarInt(int32(len(node.Children)))
      for _, child := range node.Children {
        out.WriteVarInt(child)
      }
      if node.Flags & 0x08 > 0 {
        out.WriteVarInt(node.Redirect)
      }
      if node.Flags & 0x03 == 1 {
        out.WriteString(node.Name)
      } else if node.Flags & 0x03 == 2 {
        out.WriteString(node.Name)
        out.WriteString(node.Parser)
        out.WriteByteArray(node.Properties)
      }
      if node.Flags & 0x10 > 0 {
        out.WriteString(node.Suggestion)
      }
    }
    out.WriteVarInt(packet.Ints[0])
  })
  // Another new packet. This is kinda funky mode, and I don't think I will use it much.
  spec.Insert(0x1d, int32(mpacket.Clientbound_NBTQueryResponse), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // Yet another new packet. This one is neat, but seems pointless.
  spec.Insert(0x31, int32(mpacket.Clientbound_FacePlayer), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // Why is this a new packet? I thought this was always a thing.
  spec.Insert(0x4c, int32(mpacket.Clientbound_StopSound), func(packet *pb.Packet, out *packet.OutgoingPacket) {
  })
  // Finally. Individual recipe requests are a pain.
  spec.Add(int32(mpacket.Clientbound_DeclareRecipies), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x54
  })
  // whyyyyyyyyyyyyyyy
  spec.Add(int32(mpacket.Clientbound_Tags), func(packet *pb.Packet, out *packet.OutgoingPacket) { // 0x55
  })
  // Should always be the last packet (so that no ids are shifted)
  spec.Move(int32(mpacket.Clientbound_Login), 0x56)
  return spec
}
