package net

import (
  "fmt"
  "net"
  "time"
  "bytes"
  "context"
  "net/http"
  "crypto/md5"
  "crypto/aes"
  "sync/atomic"
  "crypto/rand"
  "encoding/hex"
  "encoding/json"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/proxy/types"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/proxy/packet_stream"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type Connection struct {
  IncomingPackets *packet_stream.IncomingPacketStream
  OutgoingPackets *packet_stream.OutgoingPacketStream
  ServerPacketStream pb.Minecraft_ConnectionClient
  username string
  uuid util.UUID
  verify_token []byte
  compression, encryption bool
  compression_level int32
  use_mojang_auth bool
  Version packet.Version

  closed *atomic.Value

  // Server icon. This is the text 'data:image/png;base64,' plus a
  // base64 encoded png image. Will be displayed in the server list.
  icon string
}

func NewConnection(client_conn net.Conn,
compression, encryption bool,
compression_level int32,
use_mojang_auth bool,
icon string) *Connection {
  c := Connection{}
  c.closed = &atomic.Value{}
  c.closed.Store(false)
  c.IncomingPackets = packet_stream.NewIncomingPacketStream(client_conn, c.closed)
  // We want to flush every 4096 bytes, or if the data is 20 milliseconds old
  c.OutgoingPackets = packet_stream.NewOutgoingPacketStream(client_conn, c.closed, 4096, 20 * time.Millisecond)
  c.compression = compression
  c.encryption = encryption
  c.compression_level = compression_level
  c.use_mojang_auth = use_mojang_auth
  c.icon = icon
  return &c
}

func (c *Connection) Username() string {
  return c.username
}

func (c *Connection) UUID() util.UUID {
  return c.uuid
}

func (c *Connection) RecieveLoop(closed *atomic.Value, cancel context.CancelFunc) {
  for {
    // closed is set whenever the connection should close, so we check it every time
    if closed.Load().(bool) {
      log.Info("Context closed, leaving recieve loop")
      break
    }
    // Used when async_read is false (it grabs a packet from tcp)
    if c.IncomingPackets.ReadData() {
      break
    }
    // Used when async_read is true (it attempts to parse a packet from the buffer)
    if c.updatePackets() {
      log.Info("Proxy has closed the connection")
      out := pb.Packet{}
      out.Id = 0xfe
      c.ServerPacketStream.Send(&out)
      break
    }
    time.Sleep(10 * time.Millisecond)
  }
  closed.Store(true)
  cancel()
  // We don't closed outgoing_packet_stream because we may want to send a disconnect message
}

// returns true if the proxy needs to create a connection with the server
func (c *Connection) DoHandshake() bool {
  for {
    incoming_packet := c.IncomingPackets.GetPacket()
    if incoming_packet == nil {
      if c.IncomingPackets.ReadData() {
        return false
      }
      continue
    }

    id := incoming_packet.ID()
    state := c.IncomingPackets.GetState()

    if (state == 0) { // handshake state
      if id == 0x00 {
        c.Version = packet.Version(incoming_packet.ReadVarInt())
        // ip := incoming_packet.ReadProtocolString()
        incoming_packet.ReadProtocolString()
        // port := incoming_packet.ReadShort()
        incoming_packet.ReadShort()
        nextState := incoming_packet.ReadVarInt()

        if (nextState == 2) { // login
          if !types.HasVersion(c.Version) {
            log.Warn("Unsupported version: ", c.Version)
            // Makes sure c.Disconnect sends the correct disconnect packet
            c.IncomingPackets.SetState(2)
            c.Disconnect(util.NewChatFromString(fmt.Sprintf("Unsupported version %v", c.Version)))
            return false
          }
          log.Info("Client has logged in with version: ", c.Version)
          c.IncomingPackets.SetState(2)
        } else if (nextState == 1) {
          c.IncomingPackets.SetState(1)
        }
      } else {
        log.Warn("Got unknown handshake packet id: " + hex.EncodeToString([]byte{byte(id)}))
      }
    } else if (state == 1) { // status state
      if id == 0x00 { // request packet
        status := make(map[string]interface{})
        status["version"] = map[string]interface{}{
          "name": "1.15.2",
          "protocol": c.Version,
        }
        status["players"] = map[string]interface{}{
          "max": 1,
          "online": 3,
          "sample": []map[string]interface{}{
            {
              "name": "macmv",
              "id": "a0ebbc8d-e0b0-4c23-a965-efba61ff0ae8",
            },
            {
              "name": "Big Gaming",
              "id": util.NewUUID().ToDashedString(),
            },
          },
        }
        desc := util.NewChat()
        desc.AddSectionColor("--", "dark_green")
        desc.AddSectionColor(" Sugarcane Dev Server ", "green")
        desc.AddSectionColor("--", "dark_green")
        status["description"] = desc.JSONObject()
        if c.icon != "" {
          status["favicon"] = c.icon
        }
        status_data, err := json.Marshal(status)
        if err != nil {
          log.Error("Error marshalling server status: ", err)
        }

        out := c.OutgoingPackets.NewPacketID(0x00)
        out.WriteString(string(status_data))
        c.OutgoingPackets.Write(out)
        // Server should not close connection here. Client will send a ping after it gets the status packet.
      } else if id == 0x01 { // ping packet
        ping_data := incoming_packet.ReadLong()

        out := c.OutgoingPackets.NewPacketID(0x01)
        out.WriteLong(ping_data)
        c.OutgoingPackets.Write(out)
        return false
      }
    } else if (state == 2) { // login state
      if id == 0x00 { // start login
        c.username = incoming_packet.ReadProtocolString()
        c.verify_token = make([]byte, 4)
        rand.Read(c.verify_token)

        if c.encryption {
          out := c.OutgoingPackets.NewPacketID(0x01)
          out.WriteString("")
          out.WriteVarInt(int32(len(asn1_public_key)))
          out.WriteByteArray(asn1_public_key)
          out.WriteVarInt(4)
          out.WriteByteArray(c.verify_token)
          c.OutgoingPackets.Write(out)
        } else {
          if c.compression {
            out := c.OutgoingPackets.NewPacketID(0x03) // set compression
            out.WriteVarInt(c.compression_level)
            c.OutgoingPackets.Write(out)

            c.OutgoingPackets.SetCompression(c.compression_level)
            c.IncomingPackets.SetCompression(c.compression_level)
          }
          out := c.OutgoingPackets.NewPacketID(0x02) // login success
          if c.Version >= packet.V1_16 {
            out.WriteUUID(c.uuid)
          } else {
            out.WriteString(c.uuid.ToDashedString())
          }
          out.WriteString(c.username)
          c.OutgoingPackets.Write(out)

          c.uuid = util.NewUUIDFromBytes(md5.Sum([]byte(c.username)))

          c.IncomingPackets.SetState(3)
          c.IncomingPackets.EnableAsyncRead()
          // c.OutgoingPackets.EnableAsyncSend()
          return true
        }
      } else if id == 0x01 { // encryption response
        // cipher, err := aes.NewCipher(rsa_key)
        // if err != nil {
        //   panic(err)
        // }
        shared_secret_length := incoming_packet.ReadVarInt()
        shared_secret_encrypted := incoming_packet.ReadBytes(shared_secret_length)
        shared_secret, err := rsa_key.Decrypt(rand.Reader, shared_secret_encrypted, nil)
        if err != nil {
          panic(err)
        }

        token_length := incoming_packet.ReadVarInt()
        token_encrypted := incoming_packet.ReadBytes(token_length)
        token, err := rsa_key.Decrypt(rand.Reader, token_encrypted, nil)
        if err != nil {
          panic(err)
        }

        // Verifies that this is the same client
        if !bytes.Equal(token, c.verify_token) {
          log.Error("User sent encryption response with invalid verify token!")
          return false
        }

        if c.use_mojang_auth {
          // Get user uuid and skin from mojang
          // if sending a non empty string for the server id (above), then add that to the start of this byte array
          hash := authDigest(append(shared_secret, asn1_public_key...))
          url := "https://sessionserver.mojang.com/session/minecraft/hasJoined?username=" + c.username +
          "&serverId=" + hash

          client := &http.Client{}
          req, err := http.NewRequest("GET", url, nil)
          // mojang api hates freedom. Will give 403 if you don't use this
          req.Header.Add("User-Agent", "Wget/1.20.3")

          // c.log.Info("Sending authentication to mojang api...")
          resp, err := client.Do(req)
          if (err != nil) {
            panic(err)
          }
          defer resp.Body.Close()

          // Parse the mojang api data into json
          data := make([]byte, 2048)
          length, err := resp.Body.Read(data)
          data = data[:length]
          if resp.StatusCode != 200 {
            log.Error("Mojang session servers did not give http response code 200! (The user probably did not authenticate)")
            return false
          }
          json_data := make(map[string]interface{})
          err = json.Unmarshal(data, &json_data)
          if err != nil {
            panic(err)
          }
          // skin, err := base64.StdEncoding.DecodeString(json_data["properties"].([]interface{})[0].(map[string]interface{})["value"].(string))
          // c.log.Info("Got data auth server: ", skin)
          // c.log.Info("Got username from auth server: ", json_data["name"])
          // c.log.Info("Got uuid from auth server: ", json_data["id"])
          // c.log.Info("Got response code from auth server: ", resp.StatusCode)
          c.uuid = util.NewUUIDFromString(json_data["id"].(string))
          c.username = json_data["name"].(string)
        } else {
          c.uuid = util.NewUUIDFromBytes(md5.Sum([]byte(c.username)))
        }

        // Create an aes cipher to encrypt packets
        aes_cipher, err := aes.NewCipher(shared_secret)
        if err != nil {
          panic(err)
        }
        c.OutgoingPackets.SetCipher(packet_stream.NewCFB8Encrypter(aes_cipher, shared_secret))
        c.IncomingPackets.SetCipher(packet_stream.NewCFB8Decrypter(aes_cipher, shared_secret))

        if c.compression {
          out := c.OutgoingPackets.NewPacketID(0x03) // set compression
          out.WriteVarInt(c.compression_level)
          c.OutgoingPackets.Write(out)

          c.OutgoingPackets.SetCompression(c.compression_level)
          c.IncomingPackets.SetCompression(c.compression_level)
        }

        out := c.OutgoingPackets.NewPacketID(0x02) // login success
        if c.Version >= packet.V1_16 {
          out.WriteUUID(c.uuid)
        } else {
          out.WriteString(c.uuid.ToDashedString())
        }
        out.WriteString(c.username)
        c.OutgoingPackets.Write(out)

        c.IncomingPackets.SetState(3)
        c.IncomingPackets.EnableAsyncRead()
        // c.OutgoingPackets.EnableAsyncSend()

        return true
      } else {
        log.Warn("Got unknown join state packet id: " + hex.EncodeToString([]byte{byte(id)}))
      }
    } else {
      log.Error("Invalid state for DoHandshake: ", state)
    }
  }
  log.Error("Got to end of handshake without return")
  return false
}

func (c *Connection) updatePackets() bool {
  for {
    incoming_packet := c.IncomingPackets.GetPacket()
    if incoming_packet == nil {
      break
    }

    state := c.IncomingPackets.GetState()

    if (state == 3) { // play state
      out, err := types.IncomingToProto(incoming_packet, c.Version)
      if err != nil {
        log.Error(err)
      } else {
        if out != nil {
          c.ServerPacketStream.Send(out)
        } else {
          log.Error("Packet was nil, but no error was produced")
        }
      }
    } else {
      log.Error("Invalid state for RecieveLoop: ", state)
      return true
    }
  }
  return false
}

func (c *Connection) Disconnect(message *util.Chat) {
  // Cannot send disconnect packet during handshake or status state (the packet just doesn't exist)
  if c.IncomingPackets.GetState() == 2 { // Login state
    out := c.OutgoingPackets.NewPacketID(0x00)
    out.WriteString(message.ToJSON())
    c.OutgoingPackets.Write(out)
  } else if c.IncomingPackets.GetState() == 3 {
    // This will find the appropriate packet for the client, given c.Version
    // c.Version will always be set, and will always be valid, if the current state is 3
    // If the version is invalid, they will have already been disconnected during login state
    out := types.DisconnectPacket(c.Version, message)
    c.OutgoingPackets.Write(out)
    // The client will be disconnected before the packet is sent, if we do not flush
    c.OutgoingPackets.Flush()
  }
  c.Close()
}

func (c *Connection) Close() {
  c.OutgoingPackets.Close()
  c.IncomingPackets.Close()
}
