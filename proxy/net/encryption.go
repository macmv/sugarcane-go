package net

import (
  "fmt"
  "strings"
  "crypto/rsa"
  "crypto/rand"
  "crypto/sha1"
  "crypto/x509"
)

var rsa_key *rsa.PrivateKey
var asn1_public_key []byte

func GenerateRSAKey() {
  reader := rand.Reader

  var err error
  rsa_key, err = rsa.GenerateKey(reader, 1024)
  if err != nil {
    panic(err)
  }

  err = rsa_key.Validate()
  if err != nil {
    panic(err)
  }

  asn1_public_key, err = x509.MarshalPKIXPublicKey(&rsa_key.PublicKey)
  if err != nil {
    panic(err)
  }
}

func authDigest(s []byte) string {
  h := sha1.New()
  h.Write(s)
  hash := h.Sum(nil)

  // Check for negative hashes
  negative := (hash[0] & 0x80) == 0x80
  if negative {
    hash = twosComplement(hash)
  }

  // Trim away zeroes
  res := strings.TrimLeft(fmt.Sprintf("%x", hash), "0")
  if negative {
    res = "-" + res
  }

  return res
}

// little endian
func twosComplement(p []byte) []byte {
  carry := true
  for i := len(p) - 1; i >= 0; i-- {
    p[i] = byte(^p[i])
    if carry {
      carry = p[i] == 0xff
      p[i]++
    }
  }
  return p
}
