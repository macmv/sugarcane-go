package packet_test

import (
  "testing"
  "gitlab.com/macmv/sugarcane/proxy/packet"
  "github.com/stretchr/testify/assert"
)

func TestIncomingPacketLength(t *testing.T) {
  // the packet length put here is 2, so it should read 2 more bytes, and give us 3 total length
  p := packet.NewIncomingPacket([]byte{2, 4, 0, 0, 0, 0})
  assert.Equal(t, int32(3), p.GetLength(), "Length should be the length of the packet plus the size of the varint at the start")

  data := make([]byte, 260)
  data[0] = 128
  data[1] = 2
  p = packet.NewIncomingPacket(data)
  // the length varint was two bytes, and is the number 256, so in total the length should be 258
  assert.Equal(t, int32(258), p.GetLength(), "The length is a varint, and should be parsed as such")

  p = packet.NewIncomingPacket([]byte{3, 0})
  assert.NotEqual(t, nil, p.Error(), "If the length is larger then the input slice, the error should be set")
}

func TestIncomingPacketGetID(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{2, 4, 0, 0, 0, 0})
  assert.Equal(t, int32(4), p.GetID(), "The id should be the second varint in the data")

  p = packet.NewIncomingPacket([]byte{2, 128, 2, 0, 0, 0})
  assert.Equal(t, int32(256), p.GetID(), "The id is a varint, and should be parsed as such")

  p = packet.NewIncomingPacket([]byte{5, 255, 255, 255, 255, 255})
  assert.NotEqual(t, nil, p.Error(), "If the id varint is too big, the error should be set")

  // Length is 0, so we will get iob errors while reading the id varint
  p = packet.NewIncomingPacket([]byte{0})
  assert.Equal(t,
    "Error while reading varint: Failed to read byte: Index 1 out of range",
    p.Error().Error(),
    "If the length is larger then the input slice, the error should be set",
  )
}

func TestIncomingPacketReadBool(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{5, 0, 1, 0, 1, 1})
  assert.Equal(t, true, p.ReadBool(), "The bool read should be correct")
  assert.Equal(t, false, p.ReadBool(), "The bool read should be correct")
  assert.Equal(t, true, p.ReadBool(), "The bool read should be correct")
  assert.Equal(t, true, p.ReadBool(), "The bool read should be correct")
  p.ReadBool()
  assert.Equal(t,
    "Error while reading bool: Failed to read byte: Index 6 out of range",
    p.Error().Error(),
    "After reading another bool, the error should be set",
  )
}

func TestIncomingPacketReadByte(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{5, 0, 1, 0, 3, 255})
  assert.Equal(t, byte(1), p.ReadByte(), "The byte read should be correct")
  assert.Equal(t, byte(0), p.ReadByte(), "The byte read should be correct")
  assert.Equal(t, byte(3), p.ReadByte(), "The byte read should be correct")
  assert.Equal(t, byte(255), p.ReadByte(), "The byte read should be correct")
  p.ReadByte()
  assert.Equal(t,
    "Failed to read byte: Index 6 out of range",
    p.Error().Error(),
    "After reading another byte, the error should be set",
  )
}

func TestIncomingPacketReadShort(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{7, 0, 0, 1, 0, 255, 1, 0})
  assert.Equal(t, int16(1), p.ReadShort(), "The short read should be correct")
  assert.Equal(t, int16(255), p.ReadShort(), "The short read should be correct")
  assert.Equal(t, int16(256), p.ReadShort(), "The short read should be correct")
  p.ReadShort()
  assert.Equal(t,
    "Failed to read a short: Current position is 8, and the slice is 8 bytes long",
    p.Error().Error(),
    "After reading another short, the error should be set",
  )
}

func TestIncomingPacketReadInt(t *testing.T) {
  // The 3 bytes at the end are to test the boundry condition of ReadInt
  p := packet.NewIncomingPacket([]byte{12, 0, 0, 0, 0, 1, 0, 0, 1, 0, 5, 5, 5})
  assert.Equal(t, int32(1), p.ReadInt(), "The int read should be correct")
  assert.Equal(t, int32(256), p.ReadInt(), "The int read should be correct")
  p.ReadInt()
  assert.Equal(t,
    "Failed to read an int: Current position is 10, and the slice is 13 bytes long",
    p.Error().Error(),
    "After reading another int, the error should be set",
  )
}

func TestIncomingPacketReadLong(t *testing.T) {
  // The 7 bytes at the end are to test the boundry condition of ReadLong
  p := packet.NewIncomingPacket([]byte{24, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 5, 5, 5, 5, 5, 5, 5})
  assert.Equal(t, uint64(1), p.ReadLong(), "The long read should be correct")
  assert.Equal(t, uint64(256), p.ReadLong(), "The long read should be correct")
  p.ReadLong()
  assert.NotEqual(t, nil, p.Error(), "After reading another long, the error should be set")
  assert.Equal(t,
    "Failed to read a long: Current position is 18, and the slice is 25 bytes long",
    p.Error().Error(),
    "After reading another long, the error should be set",
  )
}

func TestIncomingPacketReadPosition(t *testing.T) {
  // We aren't going to bother with boundary condition here, as ReadPosition calls ReadLong when it reads any bytes
  p := packet.NewIncomingPacket([]byte{17, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 64, 0, 0, 16, 0})
  x, y, z := p.ReadPosition()
  assert.Equal(t, int32(0), x, "The position read should be correct")
  assert.Equal(t, int32(1), y, "The position read should be correct")
  assert.Equal(t, int32(0), z, "The position read should be correct")
  x, y, z = p.ReadPosition()
  assert.Equal(t, int32(1), x, "The position read should be correct")
  assert.Equal(t, int32(0), y, "The position read should be correct")
  assert.Equal(t, int32(1), z, "The position read should be correct")
  p.ReadPosition()
  assert.Equal(t,
    "Error while reading position: Failed to read a long: Current position is 18, and the slice is 18 bytes long",
    p.Error().Error(),
    "After reading another position, the error should be set",
  )
}

func TestIncomingPacketReadFloat(t *testing.T) {
  // Float encoding is hard, so we aren't going to check any values here. We are mostly just checking the iob error
  p := packet.NewIncomingPacket([]byte{8, 0, 0, 0, 0, 0, 5, 5, 5})
  assert.Equal(t, float32(0), p.ReadFloat(), "The float read should be correct")
  p.ReadFloat()
  assert.NotEqual(t, nil, p.Error(), "After reading another float, the error should be set")
  assert.Equal(t,
    "Failed to read a float: Current position is 6, and the slice is 9 bytes long",
    p.Error().Error(),
    "After reading another float, the error should be set",
  )
}

func TestIncomingPacketReadDouble(t *testing.T) {
  // Same thing with floats. We are just checking the iob error here
  p := packet.NewIncomingPacket([]byte{16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5})
  assert.Equal(t, float64(0), p.ReadDouble(), "The double read should be correct")
  p.ReadDouble()
  assert.Equal(t,
    "Failed to read a double: Current position is 10, and the slice is 17 bytes long",
    p.Error().Error(),
    "After reading another double, the error should be set",
  )
}

func TestIncomingPacketReadProtocolString(t *testing.T) {
  // The 10 is to check iob errors
  p := packet.NewIncomingPacket([]byte{8, 0, 5, 'H', 'e', 'l', 'l', 'o', 10})
  assert.Equal(t, "Hello", p.ReadProtocolString(), "The string read should be correct")
  p.ReadProtocolString()
  assert.Equal(t,
    "Failed to read a protocol string: Expected to read to index 19, but slice is only 9 bytes long",
    p.Error().Error(),
    "After reading another protocol string, the error should be set",
  )

  // The length varint is invalid, this should be a different error than an incomplete string
  p = packet.NewIncomingPacket([]byte{1, 0})
  p.ReadProtocolString()
  assert.Equal(t,
    "Error while reading protocol string: Error while reading varint: Failed to read byte: Index 2 out of range",
    p.Error().Error(),
    "After reading another protocol string, the error should be set",
  )
}

func TestIncomingPacketReadNullString(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{8, 0, 'H', 'e', 'l', 'l', 'o', 0, 'H'})
  assert.Equal(t, "Hello", p.ReadNullString(), "The string read should be correct")
  p.ReadNullString()
  assert.Equal(t,
    "Error while reading null string: Failed to read byte: Index 9 out of range",
    p.Error().Error(),
    "After reading another null string, the error should be set",
  )
}

func TestIncomingPacketReadVarInt(t *testing.T) {
  p := packet.NewIncomingPacket([]byte{19, 0,
  0,                             // 0
  1,                             // 1
  127,                           // 127
  128, 1,                        // 128
  255, 1,                        // 255
  255, 255, 255, 255, 15,        // -1
  255, 255, 255, 255, 255, 255}) // invalid
  assert.Equal(t, int32(0), p.ReadVarInt(), "The varint read should be correct")
  assert.Equal(t, int32(1), p.ReadVarInt(), "The varint read should be correct")
  assert.Equal(t, int32(127), p.ReadVarInt(), "The varint read should be correct")
  assert.Equal(t, int32(128), p.ReadVarInt(), "The varint read should be correct")
  assert.Equal(t, int32(255), p.ReadVarInt(), "The varint read should be correct")
  assert.Equal(t, int32(-1), p.ReadVarInt(), "The varint read should be correct")

  assert.Equal(t, int32(0), p.ReadVarInt(), "The varint read should be invalid, and it should return 0")
  assert.Equal(t,
    "Failed to read varint: Byte at 20 made the varint too big",
    p.Error().Error(),
    "After reading the invalid varint, the error should be set",
  )

  p = packet.NewIncomingPacket([]byte{1, 255})
  assert.Equal(t, int32(0), p.ReadVarInt(), "The varint read should be invalid, and it should return 0")
  assert.Equal(t,
    "Error while reading varint: Failed to read byte: Index 2 out of range",
    p.Error().Error(),
    "After reading the invalid varint, the error should be set",
  )
}

