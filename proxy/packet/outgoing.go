package packet

import (
  "bytes"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  "github.com/klauspost/compress/zlib"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type OutgoingPacket struct {
  buf *util.Buffer
  id packet.Clientbound
  old_id int32
  GarbageBuffer []byte

  // Used for writing different data on older versions. For example,
  // the slot format has changed multiple times.
  version packet.Version
}

// func HasVersion(version int32) bool {
//   _, ok := outgoing_packet_types[version]
//   return ok
// }

func NewOutgoingPacketFromSliceID(buffer []byte, id packet.Clientbound, old_id int32) *OutgoingPacket {
  p := NewOutgoingPacketFromSlice(buffer)
  p.id = id
  p.old_id = old_id
  p.buf.WriteVarInt(old_id)
  return p
}

func NewOutgoingPacketFromSlice(buffer []byte) *OutgoingPacket {
  p := OutgoingPacket{}
  p.id = packet.Clientbound_None
  p.old_id = -1
  p.buf, _ = util.NewBufferFromSlice(buffer, 10)
  return &p
}

// This returns the packets id. NOTE: Will not always be set. Will be Clientbound_None if not set.
func (p *OutgoingPacket) ID() packet.Clientbound {
  return p.id
}

// Sets the packet's version. This slightly changes how certain values
// are written; see WritePosition and WriteItem.
func (p *OutgoingPacket) SetVersion(v packet.Version) {
  p.version = v
}

// This returns the packets id. NOTE: Will not always be set. Will be -1 if not set.
func (p *OutgoingPacket) OldID() int32 {
  return p.old_id
}

func (p *OutgoingPacket) GetData(zlib_writer *zlib.Writer, compression int32) ([]byte, error) {
  // -10 is because we leave 10 zero bytes at the start, for the length varint
  packet_length := p.buf.Length() - 10
  // If compression is not set, this means that the varint for length should be written such that it ends at byte 10
  length_end_byte := 10
  // We set this here, so that the compressed packet format can write to it
  packet_data := p.buf.GetData()

  if compression > 0 {
    if packet_length > compression {
      data_length := packet_length

      var compressed bytes.Buffer
      zlib_writer.Reset(&compressed)
      zlib_writer.Write(p.buf.GetData()[10:])
      err := zlib_writer.Flush()
      if err != nil {
        return nil, err
      }
      // We have just read all that we need to from the packet buffer
      // So it is now safe to write back to that slice, with all of the compressed packet data
      // We start writing at byte 5, as the compressed packet format has a length varint at the start
      out, err := util.NewBufferFromSlice(p.buf.GetRawData(), 5)
      // We allocate a buffer that is ten bytes long at the start, so this should never happen
      if err != nil {
        panic(err)
      }
      out.WriteVarInt(data_length)
      // Avoiding this copy would be very difficult, as we would just need to copy it again later on
      out.WriteByteArray(compressed.Bytes())

      // This is so that the code after this block just needes to access one buffer
      packet_data = out.GetData()
      // This is the length of everything in the packet, including the uncompressed length
      packet_length = out.Length() - 5
      // This is the index where the total packet length varint should end
      length_end_byte = 5
    } else {
      // In this situation, the packet data starts ten bytes in.
      // And the compressed packet format says that the uncompressed length will be zero, if we are not compressing
      // This sets that zero byte
      packet_data[9] = 0
      // So we set the offset to 9, which means that we have one 0 byte in there
      length_end_byte = 9
      // To include the zero byte
      packet_length += 1
    }
  }
  // At this point, p.Buf is the packet data, and length is set to the packet length (not the buffer length, the entire packet length)
  // length_end_byte is set such that the varint will match up with the packet

  // Creating a very short buffer, and writing the varint into it is the easiest way to make a varint
  length_buffer := util.NewBuffer()
  length_buffer.WriteVarInt(packet_length)

  var_int_length := length_buffer.Length()
  // index is the index where the length varint will start
  index := length_end_byte - int(var_int_length)

  // We do not care about the bytes before the index
  data := packet_data[index:]
  // This sets the length varint (less than a 5 byte copy)
  copy(data, length_buffer.GetData())
  // Data is as short as it can be, as we cut off everything before index above
  return data, nil
}

func (p *OutgoingPacket) WriteBool(v bool)        { p.buf.WriteBool(v) }
func (p *OutgoingPacket) WriteByte(v byte)        { p.buf.WriteByte(v) }
func (p *OutgoingPacket) WriteByteArray(v []byte) { p.buf.WriteByteArray(v) }
func (p *OutgoingPacket) WriteShort(v uint16)     { p.buf.WriteShort(v) }
func (p *OutgoingPacket) WriteInt(v int32)        { p.buf.WriteInt(v) }
func (p *OutgoingPacket) WriteLong(v uint64)      { p.buf.WriteLong(v) }
func (p *OutgoingPacket) WriteUUID(v util.UUID)    { p.buf.WriteUUID(v) }
func (p *OutgoingPacket) WriteFloat(v float32)     { p.buf.WriteFloat(v) }
func (p *OutgoingPacket) WriteDouble(v float64)    { p.buf.WriteDouble(v) }
func (p *OutgoingPacket) WriteString(v string)     { p.buf.WriteString(v) }
func (p *OutgoingPacket) WriteNullString(v string) { p.buf.WriteNullString(v) }
func (p *OutgoingPacket) WriteVarInt(v int32)      { p.buf.WriteVarInt(v) }

// This is a fixed point float. It takes the double, multiplies it by
// 32, and rounds it to an int. It then writes that int into the packet,
// encoded as an int (not varint). This is only used in 1.8, but in 1.8,
// it is used any time an entity position is reference.
func (p *OutgoingPacket) WriteFixedFloat(v float64) {
  p.buf.WriteInt(int32(v * 32))
}

// This parses a postition from a grpc packet (always new format),
// and then writes the long back into the buffer, with either
// the new or old format.
func (p *OutgoingPacket) WritePosition(v uint64) {
  // We parse pos here, to make sure the uint64 is valid.
  pos := block.PosFromLong(v)
  if p.version < packet.V1_14 {
    p.buf.WriteOldPosition(pos)
  } else {
    p.buf.WritePosition(pos)
  }
}

// This writes a minecraft item.
// For versions 1.13.2 and up, it starts with a bool, for whether it exists.
// If it does exist, then it is followed byte the item id.
// That is followed by the number of items in the stack.
// That is then followed by an NBT tag, or a null byte if there is none.
// For versions 1.13 and 1.13.1, there is no bool. The item id
// is set to -1, and no data follows. Otherwise, it there is an
// item, the same data as above follows.
// For versions 1.12.2 and below, there is an extra field: right
// after the item count, there is a short, which is the item's
// data value.
func (p *OutgoingPacket) WriteItem(value *pb.Item) {
  if p.version < packet.V1_13_2 {
    if value.Present {
      p.WriteShort(uint16(value.ID))
      p.WriteByte(byte(value.Count))
      if p.version <= packet.V1_12_2 {
        // Item damage
        p.WriteShort(0)
      }
      if len(value.NBT) == 0 {
        p.WriteByte(0)
      } else {
        p.WriteByteArray(value.NBT)
      }
    } else {
      // -1 in uint16 form
      p.WriteShort(^uint16(0))
    }
  } else {
    p.WriteBool(value.Present)
    if value.Present {
      p.WriteVarInt(value.ID)
      p.WriteByte(byte(value.Count))
      if len(value.NBT) == 0 {
        p.WriteByte(0)
      } else {
        p.WriteByteArray(value.NBT)
      }
    }
  }
}
