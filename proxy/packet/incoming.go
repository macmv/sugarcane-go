package packet

import (
  "fmt"
  "math"
  "errors"
  "encoding/binary"

  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"

  pb "gitlab.com/macmv/sugarcane/proto"

  "github.com/golang/protobuf/ptypes"
)

type IncomingPacket struct {
  // Protobuf that this packet represents
  Proto *pb.Packet
  // Will be set if anything goes wrong
  Err error

  // This is an old packet id, so it should not be a packet.Serverbound
  id int32
  // This is the new packet id. It will only be set using SetID()
  new_id packet.Serverbound
  data []byte
  location int32

  // Version this packet was read from. Used for things like
  // parsing positions or items, which have changed between versions.
  // If this packet is being used during login, the value
  // of this is undefined.
  version packet.Version
}

// This takes in a packet, without the length varint. It is assumed that data is the entire
// packet, as this function does not know the length of the packet. Data should include the
// varint for the id at the start. This means that this function should be used for both
// compressed and uncompressed packets; other code should deal with decompression.
func NewIncomingPacket(data []byte) *IncomingPacket {
  p := IncomingPacket{}
  p.data = data
  p.location = 0
  p.id = p.ReadVarInt()
  p.Proto = &pb.Packet{}
  p.Err = nil
  return &p
}

// Returns old packet id. Should be converted using a packet.Spec.
func (p *IncomingPacket) ID() int32 {
  return p.id
}

// Returns the grpc packet id. Will return 0 if SetID has not been called.
func (p *IncomingPacket) NewID() packet.Serverbound {
  return p.new_id
}

// Should be used rarely. This will change the packet id that the
// server recieves. This should only be used when a packet can
// be parsed as seperate packets. For example, in 1.8, a block
// place packet can be a use item packet, depending on the fields.
// This is the only situation this should be used. Other than that,
// you should rely on the registry to set the packet ids.
func (p *IncomingPacket) SetID(id packet.Serverbound) {
  p.new_id = id
}

// Changes the way some values are parsed. Specifically, positions and
// items have changed between versions.
func (p *IncomingPacket) SetVersion(version packet.Version) {
  p.version = version
}

//func (p *IncomingPacket) GetLength() int32 {
//  if p.Err != nil { return -1 }
//  length, numRead := util.ParseVarInt(p.data)
//  return length + numRead
//}

func (p *IncomingPacket) ReadBool() bool {
  if p.Err != nil { return false }
  value := p.ReadByte()
  if p.Err != nil {
    p.Err = errors.New("Error while reading bool: " + p.Err.Error())
    return false
  }
  if value == 0 {
    return false
  } else {
    return true
  }
}

func (p *IncomingPacket) ReadByte() byte {
  if p.Err != nil { return 0 }
  if p.location >= int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf("Failed to read byte: Index %d out of range", p.location))
    return 0
  }
  value := p.data[p.location]
  p.location += 1
  return value
}

func (p *IncomingPacket) ReadBytes(num int32) []byte {
  if p.Err != nil { return nil }
  value := p.data[p.location:p.location + num]
  p.location += num
  return value
}

func (p *IncomingPacket) ReadRemainingBytes() []byte {
  if p.Err != nil { return nil }
  value := p.data[p.location:]
  p.location += int32(len(value))
  return value
}

func (p *IncomingPacket) ReadShort() int16 {
  if p.Err != nil { return 0 }
  if p.location + 2 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a short: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return 0
  }
  value := int16(binary.BigEndian.Uint16(p.data[p.location:]))
  p.location += 2
  return value
}

func (p *IncomingPacket) ReadInt() int32 {
  if p.Err != nil { return 0 }
  if p.location + 4 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read an int: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return 0
  }
  value := int32(binary.BigEndian.Uint32(p.data[p.location:]))
  p.location += 4
  return value
}

func (p *IncomingPacket) ReadLong() uint64 {
  if p.Err != nil { return 0 }
  if p.location + 8 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a long: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return 0
  }
  value := binary.BigEndian.Uint64(p.data[p.location:])
  p.location += 8
  return value
}

func (p *IncomingPacket) ReadUUID() util.UUID {
  if p.Err != nil { return util.NewUUID() }
  if p.location + 16 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a uuid: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return util.NewUUID()
  }
  var a [16]byte
  copy(a[:], p.data[p.location:])
  value := util.NewUUIDFromBytes(a)
  p.location += 8
  return value
}

func (p *IncomingPacket) ReadPosition() (int32, int32, int32) {
  if p.Err != nil { return 0, 0, 0 }
  value := p.ReadLong()
  if p.Err != nil {
    p.Err = errors.New("Error while reading position: " + p.Err.Error())
    return 0, 0, 0
  }
  var x, y, z int32
  if p.version < packet.V1_14 {
    // x, y, z order
    x = int32(value >> 38)
    z = int32(value << 38 >> 38)
    y = int32((value >> 26) & 0xFFF)
  } else {
    // x, z, y order
    x = int32(value >> 38)
    y = int32(value & 0xFFF)
    z = int32((value << 26 >> 38))
  }
  // Fix the sign bit
  if x >= 2 << 24 { x -= 2 << 25 }
  if y >= 2 << 10 { y -= 2 << 11 }
  if z >= 2 << 24 { z -= 2 << 25 }
  return x, y, z
}

func (p *IncomingPacket) ReadFloat() float32 {
  if p.Err != nil { return 0 }
  if p.location + 4 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a float: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return 0
  }
  value := math.Float32frombits(binary.BigEndian.Uint32(p.data[p.location:]))
  p.location += 4
  return value
}

func (p *IncomingPacket) ReadDouble() float64 {
  if p.Err != nil { return 0 }
  if p.location + 8 > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a double: Current position is %d, and the slice is %d bytes long",
      p.location,
      len(p.data),
    ))
    return 0
  }
  value := math.Float64frombits(binary.BigEndian.Uint64(p.data[p.location:]))
  p.location += 8
  return value
}

func (p *IncomingPacket) ReadProtocolString() string {
  if p.Err != nil { return "" }
  length := p.ReadVarInt()
  // If the length varint is invalid
  if p.Err != nil {
    p.Err = errors.New("Error while reading protocol string: " + p.Err.Error())
    return ""
  }
  if length < 0 || p.location + length > int32(len(p.data)) {
    p.Err = errors.New(fmt.Sprintf(
      "Failed to read a protocol string: Expected to read to index %d, but slice is only %d bytes long",
      p.location + length,
      len(p.data),
    ))
    return ""
  }
  value := string(p.data[p.location:p.location + length])
  p.location += length
  return value
}

func (p *IncomingPacket) ReadNullString() string {
  if p.Err != nil { return "" }
  var data []byte
  // If err gets set on any of these ReadByte calls, it will return 0, so it will exit the loop
  val := p.ReadByte()
  for val != 0 {
    data = append(data, val)
    val = p.ReadByte()
  }
  if p.Err != nil {
    // Error must have just been set while reading this string, so we append a message saying so
    p.Err = errors.New("Error while reading null string: " + p.Err.Error())
    return ""
  }
  value := string(data)
  return value
}

func (p *IncomingPacket) ReadVarInt() int32 {
  if p.Err != nil { return 0 }
  result := uint32(0)
  var read byte
  index := uint32(0)
  for {
    read = p.ReadByte()
    if p.Err != nil {
      // We want better error messages, just to make it clear what happened
      p.Err = errors.New("Error while reading varint: " + p.Err.Error())
      return 0
    }
    value := (uint32) (read & 0b01111111)
    result |= (value << (7 * index))

    index++

    // Invalid varint
    if (index > 5) {
      p.Err = errors.New(fmt.Sprintf(
        "Failed to read varint: Byte at %d made the varint too big",
        p.location,
      ))
      return 0
    }
    if (read & 0b10000000) == 0 {
      break
    }
  }

  value := int32(result)
  return value
}

func (p *IncomingPacket) ReadNBT() *nbt.Tag {
  if p.Err != nil { return nil }
  tag := nbt.ReadData(p.data[p.location:])
  p.location += tag.Size()
  return tag
}

func (p *IncomingPacket) ReadItem() *pb.Item {
  if p.Err != nil { return nil }
  item := &pb.Item{}
  if p.version < packet.V1_13_2 {
    id := p.ReadShort()
    if id == -1 {
      item.Present = false
    } else {
      item.Present = true
      item.ID = int32(id)
      count := p.ReadByte()
      item.Count = int32(count)
      if p.version < packet.V1_13 {
        // is damage value, only present in 1.8-1.12
        p.ReadShort()
      }
      tag := p.ReadNBT()
      item.NBT = tag.Serialize()
    }
  } else {
    item.Present = p.ReadBool()
    if item.Present {
      item.ID = p.ReadVarInt()
      item.Count = int32(p.ReadByte())
      item.NBT = p.ReadNBT().Serialize()
    }
  }
  return item
}

func (p *IncomingPacket) CopyBool() bool {
  value := p.ReadBool()
  p.WriteBool(value)
  return value
}

func (p *IncomingPacket) CopyByte() byte {
  value := p.ReadByte()
  p.WriteByte(value)
  return value
}

func (p *IncomingPacket) CopyRemainingBytes() []byte {
  value := p.ReadRemainingBytes()
  p.WriteBytes(value)
  return value
}

func (p *IncomingPacket) CopyShort() int16 {
  value := p.ReadShort()
  p.WriteShort(value)
  return value
}

func (p *IncomingPacket) CopyInt() int32 {
  value := p.ReadInt()
  p.WriteInt(value)
  return value
}

func (p *IncomingPacket) CopyLong() uint64 {
  value := p.ReadLong()
  p.WriteLong(value)
  return value
}

func (p *IncomingPacket) CopyUUID() util.UUID {
  value := p.ReadUUID()
  p.WriteUUID(value)
  return value
}

func (p *IncomingPacket) CopyPosition() (int32, int32, int32) {
  x, y, z := p.ReadPosition()
  p.WritePosition(x, y, z)
  return x, y, z
}

func (p *IncomingPacket) CopyDouble() float64 {
  value := p.ReadDouble()
  p.WriteDouble(value)
  return value
}

func (p *IncomingPacket) CopyFloat() float32 {
  value := p.ReadFloat()
  p.WriteFloat(value)
  return value
}

func (p *IncomingPacket) CopyProtocolString() string {
  value := p.ReadProtocolString()
  p.WriteProtocolString(value)
  return value
}

func (p *IncomingPacket) CopyNullString() string {
  value := p.ReadNullString()
  p.WriteNullString(value)
  return value
}

func (p *IncomingPacket) CopyVarInt() int32 {
  value := p.ReadVarInt()
  p.WriteInt(value)
  return value
}

func (p *IncomingPacket) CopyItem() *pb.Item {
  item := p.ReadItem()
  p.WriteItem(item)
  return item
}

func (p *IncomingPacket) WriteBool(value bool) {
  p.Proto.Bools = append(p.Proto.Bools, value)
}

func (p *IncomingPacket) WriteByte(value byte) {
  p.Proto.Bytes = append(p.Proto.Bytes, value)
}

func (p *IncomingPacket) WriteBytes(value []byte) {
  p.Proto.Bytes = append(p.Proto.Bytes, value...)
}

func (p *IncomingPacket) WriteShort(value int16) {
  p.Proto.Shorts = append(p.Proto.Shorts, int32(value))
}

func (p *IncomingPacket) WriteInt(value int32) {
  p.Proto.Ints = append(p.Proto.Ints, value)
}

func (p *IncomingPacket) WriteLong(value uint64) {
  p.Proto.Longs = append(p.Proto.Longs, value)
}

func (p *IncomingPacket) WriteUUID(value util.UUID) {
  p.Proto.Uuids = append(p.Proto.Uuids, value.ToProto())
}

func (p *IncomingPacket) WritePosition(x, y, z int32) {
  value := ((uint64(x) & 0x3FFFFFF) << 38) | ((uint64(z) & 0x3FFFFFF) << 12) | (uint64(y) & 0xFFF)
  p.Proto.Positions = append(p.Proto.Positions, value)
}

func (p *IncomingPacket) WriteDouble(value float64) {
  p.Proto.Doubles = append(p.Proto.Doubles, value)
}

func (p *IncomingPacket) WriteFloat(value float32) {
  p.Proto.Floats = append(p.Proto.Floats, value)
}

func (p *IncomingPacket) WriteProtocolString(value string) {
  p.Proto.Strings = append(p.Proto.Strings, value)
}

func (p *IncomingPacket) WriteNullString(value string) {
  p.Proto.Strings = append(p.Proto.Strings, value)
}

func (p *IncomingPacket) WriteItem(item *pb.Item) {
  val, err := ptypes.MarshalAny(item)
  if err != nil {
    panic(err)
  }
  p.Proto.Other = append(p.Proto.Other, val)
}
