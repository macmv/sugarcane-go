# Usage:
# ./bench.sh
#   <package>
#   <pprof flag (-cpuprofile, -blockprofile, etc)>
#   <output file (pprof/cpu.out)>

mkdir -p pprof

go test $1 -bench . "${@:2}" && \
  go tool pprof -http 0.0.0.0:6060 $3

