package main

import (
  "flag"
  "errors"
  "net/http"
  _ "net/http/pprof"
  "gitlab.com/macmv/sugarcane/proxy"
  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/ecs"
)

var (
  flag_port = flag.String("port", ":25565", "The port that the proxy should listen for minecraft connections on")
  flag_cluster = flag.String("cluster", "cubiness", "The cluster that the proxy should find servers to connect to")
  flag_group = flag.String("group", "", "The group that started this task (service:dev-server, for example)")
  flag_server_port = flag.String("server_port", ":8483", "The port that the grpc connection is running on")
  flag_local = flag.Bool("local", false, "If set, then the server does not use any aws apis")
  flag_no_compression = flag.Bool("nocompression", false, "If set, then the proxy disables compression")
  flag_no_encryption = flag.Bool("noencryption", false, "If set, then the proxy disables encryption")
  flag_no_mojang = flag.Bool("nomojang", false, "If set, then the proxy does not use any mojang apis, but might still encrypt the connection")
)

var svc *ecs.ECS

func main() {
  go func() {
    http.ListenAndServe(":6061", nil)
  }()

  flag.Parse()

  session := session.Must(session.NewSession())
  svc = ecs.New(session, aws.NewConfig().WithRegion("us-west-2"))

  if *flag_local {
    proxy := proxy.NewProxy(!*flag_no_compression, !*flag_no_encryption, !*flag_no_mojang)
    proxy.Listen(*flag_port, func() (string, error) { return *flag_server_port, nil })
  } else {
    // encryption and mojang apis will always be set on production
    proxy := proxy.NewProxy(!*flag_no_compression, true, true)
    proxy.Listen(*flag_port, getHubServer)
  }
}

func getHubServer() (string, error) {
  list_in := &ecs.ListTasksInput{}
  list_in.SetCluster(*flag_cluster)
  list_out, err := svc.ListTasks(list_in)
  if err != nil {
    return "", err
  }

  desc_in := &ecs.DescribeTasksInput{}
  desc_in.SetCluster(*flag_cluster)
  desc_in.SetTasks(list_out.TaskArns)
  desc_out, err := svc.DescribeTasks(desc_in)
  if err != nil {
    return "", err
  }

  for _, task := range desc_out.Tasks {
    if *task.Group == *flag_group {
      return *task.Containers[0].NetworkInterfaces[0].PrivateIpv4Address + *flag_server_port, nil
    }
  }
  return "", errors.New("Could not find any task in the group \"" + *flag_group + "\" (specify with -group)")
}
