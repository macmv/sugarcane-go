package minecraft

import (
  "sync"
  "time"
  "errors"
  "reflect"
  go_net "net"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/event"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/inventory"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  vanilla_block "gitlab.com/macmv/sugarcane/minecraft/vanilla/block"
  vanilla_item "gitlab.com/macmv/sugarcane/minecraft/vanilla/item"

  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/aws"
)

// This is the main minecraft type. Create one of these, and use it to start the server itself
type Minecraft struct {
  WorldManager *world.WorldManager
  plugins      *PluginManager
  Events       *event.EventManager
  sess         *session.Session
  max_players  int

  load_stage   int
}

// Constructor for Minecraft type.
// Set needs-session to true if it should create an AWS session.
// The max players of the server is used when a proxy or lobby server sends a ReservePlayersRequest.
// If needs_reserve is set to true, it will deny all players sent to this server that have not been 'reserved'.
// If needs_reserve is set to false, then it will accept all incoming connections.
// Basically, if needs_reserve is true, then this is a server that is part of a network.
// If a player wants to join, they will need to be connected from
// another server. If it is false, then players can connect directly to this server.
// The default_world is a string name to the default world. This is where players
// are sent when they first connect.
func New(needs_session bool, max_players int, needs_reserve bool, default_world string) *Minecraft {
  m := Minecraft{}
  m.max_players = max_players
  if needs_session {
    var err error
    m.sess, err = session.NewSession(&aws.Config{
      Region: aws.String("us-west-2"),
    })
    if err != nil {
      panic(err)
    }
  }
  if needs_reserve {
    m.WorldManager = world.NewWorldManagerWithSlots(m.sess, "./worlds", default_world, 5 * time.Second, 12)
  } else {
    m.WorldManager = world.NewWorldManager(m.sess, "./worlds", default_world)
  }
  m.plugins = NewPluginManager(&m)
  m.Events = event.NewEventManager(m.WorldManager)
  return &m
}

// This loads all basic event types and vanilla blocks.
// You must call this before registering anything.
func (m *Minecraft) Init() {
  if m.load_stage != 0 {
    panic("Please call Init() before any other load functions!")
  }
  log.Info("Initializing...")
  // m.plugins.LoadPlugins()
  // m.Events.AddEventType("command")
  m.Events.AddEventType("ready")
  m.Events.AddEventType("player-join")
  m.Events.AddEventType("player-leave")
  m.Events.AddEventType("block-change")
  m.Events.AddEventType("right-click")
  m.Events.AddEventType("left-click")
  m.Events.AddEventType("click-window")
  // m.plugins.EnablePlugins()
  m.Events.EnableBasicHandlers()
  // Need to register all blocks before calling block.Load()
  vanilla_block.RegisterBlocks()
  m.load_stage = 1
}

// This finalizes all blocks, and allows items to be registered.
func (m *Minecraft) FinalizeBlocks() {
  if m.load_stage != 1 {
    panic("Please call Init() before FinalizeBlocks()!")
  }
  // For reference, here are the block ids:
  // https://raw.githubusercontent.com/PrismarineJS/minecraft-data/master/data/pc/1.8/blocks.json
  // https://raw.githubusercontent.com/PrismarineJS/minecraft-data/master/data/pc/1.15.2/blocks.json

  block.LoadBlocks()

  // Must be called before adding custom items
  vanilla_item.RegisterItems()
  m.load_stage = 2
}

// This is the last stage of loading. It will finalize items,
// and load worlds from disk.
func (m *Minecraft) FinishLoad() {
  if m.load_stage != 2 {
    panic("Please call FinalizeBlocks() before FinishLoad()!")
  }

  // Should be in load phase 2
  inventory.SetupWindowTypes()
  player.SetupToolTypes()
  player.SetupBlockTypes()
  m.WorldManager.Load()
  m.load_stage = 3
}

// This is a blocking call, which starts the grpc server.
// The port should look like this:
//   :8483
func (m *Minecraft) StartUpdateLoop(port string) {
  if m.load_stage != 3 {
    panic("Please finish all loading (call FinishLoad()) before starting the server!")
  }
  waitGroup := sync.WaitGroup{}
  log.Info("Starting server...")
  m.WorldManager.StartUpdateLoop(&waitGroup)
  // Open grpc port
  ln, err := go_net.Listen("tcp", port)
  if err != nil {
    log.Error("Could not open tcp port for listening!")
    return
  }
  // This should probably be run when the world_manager update loop
  // starts, but here is fine for now.
  m.Events.HandleEvent("ready")
  // Blocking call, this starts the server (setup_connection() returns a *grpc.Server)
  setup_connection(port, m).Serve(ln)
  // After the connect is closed, we wait for the worlds to finish saving.
  waitGroup.Wait()
}

// Gets the current aws session.
// Returns nil if false was passed into the constructor for the needs-session value.
func (m *Minecraft) AwsSession() *session.Session {
  return m.sess
}

// Use this to register events.
// The string name of the event should be one from the README.
// The event itself can take any number of parameters, and return either a bool or nothing.
// If it returns a bool, then it is treated as an "is_cancelled" flag.
// The arguments must match the ones listed in the README, otherwise you will get errors when the event is called.
// This will cause the server to panic, but that is for testing, and will be disabled soon.
func (m *Minecraft) On(name string, handle interface{}) error {
  rhandle := reflect.ValueOf(handle)
  if rhandle.Kind() == reflect.Func {
    m.Events.AddEventHandler(name, rhandle)
  } else {
    return errors.New("Invalid handle! Please pass a function pointer!")
  }
  return nil
}

// This returns the maximum number of players allowed to connect at any time.
func (m *Minecraft) MaxPlayers() int {
  return m.WorldManager.MaxPlayers()
}

