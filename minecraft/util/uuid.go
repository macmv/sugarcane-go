package util

import (
  "io"
  "math/rand"
  "encoding/hex"
  pb "gitlab.com/macmv/sugarcane/proto"
)

// This is a UUID, used for player account ids.
// This is passed around by value in most situations.
// Internally, this is a big endian array of 16 bytes.
type UUID [16]byte

// Creates a new UUID from a byte array.
func NewUUIDFromBytes(value [16]byte) UUID {
  u := UUID{}
  copy(u[:], value[:])
  return u
}

// Creates a UUID by reading 16 bytes from the given reader.
func NewUUIDFromReader(reader io.Reader) UUID {
  u := UUID{}
  reader.Read(u[:])
  return u
}

// Creates a UUID by reading 16 bytes from math/rand.
// Use NewUUIDFromReader if you need crypto/rand.
func NewUUIDFromRand() UUID {
  u := UUID{}
  rand.Read(u[:])
  return u
}

// Creates a new UUID from a UUID protobuf.
// Used in the server when decoding the UUID sent from the proxy.
func NewUUIDFromProto(value *pb.UUID) UUID {
  u := UUID{}
  copy(u[:], value.BigEndianData)
  return u
}

// Creates a UUID from a hex string.
// Will panic if the string is not 32 characters. (Does not work with dashed UUIDS)
func NewUUIDFromString(value string) UUID {
  u := UUID{}
  if len(value) != 32 {
    panic("Invalid string to convert to UUID: " + value)
  }
  data, err := hex.DecodeString(value)
  if err != nil {
    panic(err)
  }
  copy(u[:], data)
  return u
}

// Creates a UUID with the value 0.
func NewUUID() UUID {
  u := UUID{}
  return u
}

// Returns the hex string of the UUID, without dashes.
func (u UUID) ToString() string {
  hex_string := hex.EncodeToString(u[:])
  return hex_string
}

// Returns the hex string of the UUID, with dashes inserted.
func (u UUID) ToDashedString() string {
  //  0  1  2  3  4  5  6  7     8  9  10 11    12 13 14 15    16 17 18 19 20    21 22 23 24 25 26 27 28 29 30 31
  //  a  0  e  b  b  c  8  d  -  e  0  b  0  -  4  c  2  3  -  a  9  6  5  e  -  f  b  a  6  1  f  f  0  a  e  8
  hex_string := hex.EncodeToString(u[:])
  dashed_string := ""
  dashed_string += hex_string[0:8] + "-"
  dashed_string += hex_string[8:12] + "-"
  dashed_string += hex_string[12:16] + "-"
  dashed_string += hex_string[16:21] + "-"
  dashed_string += hex_string[21:32]
  return dashed_string
}

// Used for printing. Simply calls ToDashedString
func (u UUID) String() string {
  return u.ToDashedString()
}

func (u UUID) ToProto() *pb.UUID {
  p := pb.UUID{}
  p.BigEndianData = u[:]
  return &p
}

// Gets the big endian bytes of the UUID.
func (u UUID) GetBytes() []byte {
  return u[:]
}
