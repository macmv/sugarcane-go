package util_test

import (
  "math"
  "testing"
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/util"
)

// I hit my keyboard, and got these numbers.
const SEED = 0

func TestFastRand(t *testing.T) {
  test_rand(t,  util.NewFastRand(SEED))
}

func TestMathRand(t *testing.T) {
  // rand.NewSource() will give a Source64, so this is safe.
  // This is not true according to the spec, so this may
  // change in the future. But for now, working with Uint64()
  // is easiest.
  test_rand(t, rand.NewSource(SEED).(rand.Source64))
}

func test_rand(t *testing.T, r rand.Source64) {
  // The entropy test makes sure that the rand.Source
  // has at lease 3.5 shannon entropy. I know I shouldn't
  // put rules on what a random number generator is, but
  // this generator should be used for terrain generation,
  // so I want it to give a lot of random data.
  //
  // After running this test with various seeds, I consistently
  // got ~3.99 for both math.Rand and util.FastRand.
  t.Run("Entropy", func(t *testing.T) {
    // This test uses nibbles as the character
    // There are 16 possible nibbles, so this
    // slice is 16 elements long.
    amounts := make([]int, 16)
    // We are using 1024 longs. This is also used
    // later to find the number of nibbles used.
    TEST_SIZE := 1024
    for i := 0; i < TEST_SIZE; i++ {
      long := r.Uint64()
      // t.Log(long)
      for j := 0; j < 64; j += 4 {
        amounts[long >> j & 0xf]++
      }
    }
    // Shannon entropy
    entropy := float64(0)
    for _, amount := range amounts {
      // 16 nibbles in a long
      portion := float64(amount) / float64(TEST_SIZE * 16)
      entropy += portion * math.Log2(portion)
    }
    entropy *= -1
    t.Log(entropy)
    if entropy < 3.99 {
      t.Fatal("Entropy is too low! Expected something larger than 3.99, but got:", entropy)
    }
    // Uncomment to show entropy
    // t.Fail()
  })
  // This counts the number of long lines in the resulting bit
  // stream. Then, based on the fact that there should be 25%
  // 1 lines, 12.5% 2 lines, 6.25% 3 lines, etc, we make sure
  // that nothing is super far off from expected.
  t.Run("Lines", func(t *testing.T) {
    // This is a list of line lengths, to amounts.
    // We expect line_lengths[0] ~= 25% the total bits,
    // line_lengths[1] ~= 12.5%, line_lengths[2] ~= 6.25%,
    // etc.
    line_lengths := make(map[int]int)
    // We are using 1024 longs. This is also used
    // later to find the number of bits used.
    TEST_SIZE := 1024
    // Used to find lines over multiple longs.
    prev_bit := uint64(0)
    line_length := 0
    for i := 0; i < TEST_SIZE; i++ {
      long := r.Uint64()
      // For each bit
      for j := 0; j < 64; j++ {
        bit := long >> j & 1
        if bit == prev_bit {
          line_length++
        } else {
          line_lengths[line_length]++
          line_length = 0
        }
        prev_bit = bit
      }
    }
    portions := []float64{}
    for index, amount := range line_lengths {
      i := index
      for i >= len(portions) {
        portions = append(portions, 0)
      }
      // 64 bits per long
      portions[index] = float64(amount) / float64(TEST_SIZE * 64)
    }
    for _, portion := range portions {
      t.Logf("%0.6f", portion)
    }
    // Uncomment to show the lines
    // t.Fail()
  })
}

func BenchmarkMathRand(b *testing.B) {
  r := rand.NewSource(SEED)
  for i := 0; i < b.N; i++ {
    r.Int63()
  }
}

func BenchmarkFastRand(b *testing.B) {
  r := util.NewFastRand(SEED)
  for i := 0; i < b.N; i++ {
    r.Int63()
  }
}
