package util

import (
  "testing"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func TestBufferLength(t *testing.T) {
  b := NewBuffer()
  b.WriteByteArray([]byte{3, 4, 5})
  assert.Equal(t, int32(3), b.Length(), "Should return the length of the buffer")

  b = NewBuffer()
  b.WriteByteArray([]byte{3, 4, 5, 255})
  assert.Equal(t, int32(4), b.Length(), "Should return the length of the buffer")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0, 0}, 2)
  b.WriteByteArray([]byte{1, 2, 3, 4})
  assert.Equal(t, int32(6), b.Length(), "Should return the length of the buffer")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0, 0}, 2)
  assert.Equal(t, int32(2), b.Length(), "Should return the length of the buffer")
}

func TestNewBufferFromSlice(t *testing.T) {
  _, err := NewBufferFromSlice([]byte{0, 0, 0, 0}, 2)
  assert.Equal(t, nil, err, "Should not return an error when calling new")

  _, err = NewBufferFromSlice([]byte{0, 0, 0, 0}, 4)
  assert.Equal(t, nil, err, "Should not return an error when calling new")

  _, err = NewBufferFromSlice([]byte{0, 0, 0, 0}, 5)
  assert.NotEqual(t, nil, err, "Should return an error, because start_pos is too big")

  _, err = NewBufferFromSlice([]byte{}, 1)
  assert.NotEqual(t, nil, err, "Should return an error, because start_pos is too big")
}

func TestBufferWriteByte(t *testing.T) {
  b := NewBuffer()
  b.WriteByte(0)
  assert.Equal(t, []byte{0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteByte(1)
  assert.Equal(t, []byte{1}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteByte(1)
  b.WriteByte(1)
  assert.Equal(t, []byte{1, 1}, b.GetData(), "Should write the correct data to the buffer")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 0)
  b.WriteByte(1)
  b.WriteByte(1)
  assert.Equal(t, []byte{1, 1}, b.GetData(), "Should return the correct length of data")
  assert.Equal(t, []byte{1, 1, 0}, b.GetRawData(), "Should not remove data from internal buffer")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 2)
  b.WriteByte(1)
  assert.Equal(t, []byte{0, 0, 1}, b.GetData(), "Should start writing at the right place")
  assert.Equal(t, []byte{0, 0, 1}, b.GetRawData(), "Should not allocate any more data")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 2)
  b.WriteByte(1)
  b.WriteByte(1)
  assert.Equal(t, []byte{0, 0, 1, 1}, b.GetData(), "Should append data when needed")
  assert.Equal(t, []byte{0, 0, 1, 1}, b.GetRawData(), "Should not allocate any more data")
}

func TestBufferWriteByteArray(t *testing.T) {
  b := NewBuffer()
  b.WriteByteArray([]byte{1, 2, 3})
  assert.Equal(t, []byte{1, 2, 3}, b.GetData(), "Should write the correct data to the buffer")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 0)
  b.WriteByteArray([]byte{1, 2})
  assert.Equal(t, []byte{1, 2}, b.GetData(), "Should return the correct length of data")
  assert.Equal(t, []byte{1, 2, 0}, b.GetRawData(), "Should not allocate any more data")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 1)
  b.WriteByteArray([]byte{1, 2})
  assert.Equal(t, []byte{0, 1, 2}, b.GetData(), "Should start writing at the correct location")
  assert.Equal(t, []byte{0, 1, 2}, b.GetRawData(), "Should not allocate any more data")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 1)
  b.WriteByteArray([]byte{1, 2, 3, 4})
  assert.Equal(t, []byte{0, 1, 2, 3, 4}, b.GetData(), "Should append when needed")
  assert.Equal(t, []byte{0, 1, 2, 3, 4}, b.GetRawData(), "Should allocate the correct length of data")

  b, _ = NewBufferFromSlice([]byte{0, 0, 0}, 3)
  b.WriteByteArray([]byte{1, 2, 3, 4})
  assert.Equal(t, []byte{0, 0, 0, 1, 2, 3, 4}, b.GetData(), "Should append when needed")
  assert.Equal(t, []byte{0, 0, 0, 1, 2, 3, 4}, b.GetRawData(), "Should allocate the correct length of data")
}


func TestBufferWriteBool(t *testing.T) {
  b := NewBuffer()
  b.WriteBool(false)
  assert.Equal(t, []byte{0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteBool(true)
  assert.Equal(t, []byte{1}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteShort(t *testing.T) {
  b := NewBuffer()
  b.WriteShort(0)
  assert.Equal(t, []byte{0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteShort(1)
  assert.Equal(t, []byte{0, 1}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteShort(255)
  assert.Equal(t, []byte{0, 255}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteShort(256)
  b.WriteShort(1)
  assert.Equal(t, []byte{1, 0, 0, 1}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteInt(t *testing.T) {
  b := NewBuffer()
  b.WriteInt(0)
  assert.Equal(t, []byte{0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteInt(1)
  assert.Equal(t, []byte{0, 0, 0, 1}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteInt(255)
  assert.Equal(t, []byte{0, 0, 0, 255}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteInt(256)
  b.WriteInt(1)
  assert.Equal(t, []byte{0, 0, 1, 0, 0, 0, 0, 1}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteLong(t *testing.T) {
  b := NewBuffer()
  b.WriteLong(0)
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteLong(1)
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 1}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteLong(255)
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 255}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteLong(256)
  b.WriteLong(1)
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWritePosition(t *testing.T) {
  b := NewBuffer()
  b.WritePosition(block.Pos{0, 0, 0})
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WritePosition(block.Pos{1, 0, 0})
  assert.Equal(t, []byte{0, 0, 0, 64, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WritePosition(block.Pos{1, 0, 1})
  assert.Equal(t, []byte{0, 0, 0, 64, 0, 0, 16, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WritePosition(block.Pos{1, 1, 1})
  b.WritePosition(block.Pos{0, 0, 0})
  assert.Equal(t, []byte{0, 0, 0, 64, 0, 0, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteUUID(t *testing.T) {
  b := NewBuffer()
  b.WriteUUID(NewUUIDFromString("00000000000000000000000000000000"))
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteUUID(NewUUIDFromString("01000000000000000000000000000000"))
  assert.Equal(t, []byte{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteFloat(t *testing.T) {
  b := NewBuffer()
  b.WriteFloat(0)
  assert.Equal(t, []byte{0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteDouble(t *testing.T) {
  b := NewBuffer()
  b.WriteDouble(0)
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 0}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteIntArray(t *testing.T) {
  b := NewBuffer()
  b.WriteIntArray([]int32{1, 2, 3})
  assert.Equal(t, []byte{0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 3}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteLongArray(t *testing.T) {
  b := NewBuffer()
  b.WriteLongArray([]uint64{1, 2})
  assert.Equal(t, []byte{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteString(t *testing.T) {
  b := NewBuffer()
  b.WriteString("Hello")
  assert.Equal(t, []byte{5, 'H', 'e', 'l', 'l', 'o'}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteNullString(t *testing.T) {
  b := NewBuffer()
  b.WriteNullString("Hello")
  assert.Equal(t, []byte{'H', 'e', 'l', 'l', 'o', 0}, b.GetData(), "Should write the correct data to the buffer")
}

func TestBufferWriteVarInt(t *testing.T) {
  b := NewBuffer()
  b.WriteVarInt(0)
  assert.Equal(t, []byte{0x00}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteVarInt(1)
  assert.Equal(t, []byte{0x01}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteVarInt(127)
  assert.Equal(t, []byte{0x7f}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteVarInt(128)
  assert.Equal(t, []byte{0x80, 0x01}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteVarInt(255)
  assert.Equal(t, []byte{0xff, 0x01}, b.GetData(), "Should write the correct data to the buffer")

  b = NewBuffer()
  b.WriteVarInt(-1)
  assert.Equal(t, []byte{0xff, 0xff, 0xff, 0xff, 0x0f}, b.GetData(), "Should write the correct data to the buffer")
}

