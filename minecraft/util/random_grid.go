package util

import (
  "math"
  "math/rand"
)

type RandomGrid struct {
  // This is the width/height of every square in the grid.
  dist int
  // This is the width/height of the whole grid.
  size int
  // This is a minimum distance, in absolute coordinates. For example,
  // if dist was 4, then a good value for min_dist would be 1 or 2.
  min_dist float32
  // This is the actual grid of points, which is never edited
  // after the grid is created.
  points [][]point
}

// This is a point in absolute coordinates
type RandomGridPoint struct {
  X int
  Y int
}

// This is a point within the random grid,
// and should never be returned from any function.
type point struct {
  // This is a value from 0 to dist
  x float32
  y float32
}

func (p point) valid() bool {
  return p.x >= 0
}

// This creates a new random point grid. A random point grid is a grid
// where every cell (usually) has a point in it. Dist is the size of each
// grid cell, and min_dist is the minimum required distance between points.
// If min_dist is too large, then more cells will no longer have points.
// Each time a cell needs a point, it will try to generate one 10 times,
// before giving up and leaving the point empty.
func NewRandomGrid(source rand.Source, dist int, min_dist float32) *RandomGrid {
  size := 16
  tries := 10
  r := rand.New(source)

  grid := &RandomGrid{}
  grid.size = size
  grid.dist = dist
  grid.min_dist = min_dist
  grid.points = make([][]point, size)
  for y := 0; y < size; y++ {
    grid.points[y] = make([]point, size)
    for x := 0; x < size; x++ {
      set := false
      for i := 0; i < tries; i++ {
        p := point{}
        p.x = r.Float32() * float32(dist)
        p.y = r.Float32() * float32(dist)
        grid.points[y][x] = p
        if grid.valid(x, y) {
          set = true
          break
        }
      }
      if !set {
        grid.points[y][x].x = -1
        grid.points[y][x].y = -1
      }
    }
  }
  return grid
}

func (r *RandomGrid) valid(x, y int) bool {
  // This is the point we are checking
  // If you called this function, then this is set.
  // We want a panic if it is not set
  checking := r.points[y][x]
  if !checking.valid() {
    return false
  }
  for dy := -1; dy <= 1; dy++ {
    for dx := -1; dx <= 1; dx++ {
      if dx == 0 && dy == 0 {
        // Current point, want to skip
        continue
      }
      // Temp x and y
      tx := x + dx
      ty := y + dy
      // If the distance is to small, the point is invalid
      if r.distance(x, y, tx, ty) < r.min_dist {
        return false
      }
    }
  }
  return true
}

// Gets the distance between two points within the internal grid.
// This takes in r.points indices, and will return a distance relative
// to r.dist.
func (r *RandomGrid) distance(x1, y1, x2, y2 int) float32 {
  // Temporary values, used to access r.points
  tx1 := x1
  ty1 := y1
  tx2 := x2
  ty2 := y2
  // Wrap to be within the grid
  tx1 = (tx1 % r.size + r.size) % r.size
  ty1 = (ty1 % r.size + r.size) % r.size
  tx2 = (tx2 % r.size + r.size) % r.size
  ty2 = (ty2 % r.size + r.size) % r.size
  // If it is outside of points, then points is not complete
  if ty1 >= len(r.points)      { return float32(r.dist) }
  if tx1 >= len(r.points[ty1]) { return float32(r.dist) }
  if ty2 >= len(r.points)      { return float32(r.dist) }
  if tx2 >= len(r.points[ty2]) { return float32(r.dist) }
  a := r.points[ty1][tx1]
  b := r.points[ty2][tx2]
  if !a.valid() || !b.valid() {
    return float32(r.dist)
  }
  // We use the real x and y values here, as we don't
  // want the wrapped tx and ty values to affect the distance
  ax := a.x + float32(x1) * float32(r.dist)
  ay := a.y + float32(y1) * float32(r.dist)
  bx := b.x + float32(x2) * float32(r.dist)
  by := b.y + float32(y2) * float32(r.dist)
  return float32(math.Sqrt(float64((ax - bx) * (ax - bx) + (ay - by) * (ay - by))))
}

// x and y are absolute coordinates, which corrospond to a point in the grid.
// If the grid dist is 4, then x and y should be divided by 4 to find the actual
// square that they are searching. This returns true if the point in that square
// lies within the smaller square coordinates that were passed in.
//
// This is used in terrain generation, by passing in absolute block coordinates.
// If the function returns true, a tree is placed at that position.
func (r *RandomGrid) ContainsPoint(x, y int) bool {
  _, p, _, _ := r.get_point(r.square_space(x, y))
  if !p.valid() {
    return false
  }
  // Wrapped x and y
  wx := (x % r.dist + r.dist) % r.dist
  wy := (y % r.dist + r.dist) % r.dist
  return int(p.x) == wx && int(p.y) == wy
}

// This returns a list of absolute coordinates, which are all points that lie
// within the given rectangle. The points are in the same coordinate space
// as the coordinates passed into ContainsPoint
func (r *RandomGrid) PointsInArea(x1, y1, x2, y2 int) []RandomGridPoint {
  // Find the min and max corners
  // sx and sy are still in absolute coordinates, they haven't been divided yet
  minx := int(math.Min(float64(x1), float64(x2)))
  maxx := int(math.Max(float64(x1), float64(x2)))
  miny := int(math.Min(float64(y1), float64(y2)))
  maxy := int(math.Max(float64(y1), float64(y2)))
  // Find square-space coordinates
  minsx, minsy := r.square_space(minx, miny)
  maxsx, maxsy := r.square_space(maxx, maxy)
  // Iterate through all squares, and add to a list of points
  points := []RandomGridPoint{}
  for sx := minsx; sx <= maxsx; sx++ {
    for sy := minsy; sy <= maxsy; sy++ {
      a, p, _, _ := r.get_point(sx, sy)
      if !p.valid() { continue }
      if a.X >= minx && a.X < maxx && a.Y >= miny && a.Y < maxy {
        points = append(points, a)
      }
    }
  }
  return points
}

func (r *RandomGrid) ClosestPoint(x, y int) (ret RandomGridPoint) {
  // Convert to square space coordinates
  sx := int(math.Floor(float64(x) / float64(r.dist)))
  sy := int(math.Floor(float64(y) / float64(r.dist)))
  min_dist := float32(r.dist) * 3
  for offset_x := -1; offset_x <= 1; offset_x++ {
    for offset_y := -1; offset_y <= 1; offset_y++ {
      tx1 := ((sx + offset_x) % r.size + r.size) % r.size
      ty1 := ((sy + offset_y) % r.size + r.size) % r.size
      o := r.points[ty1][tx1]
      ax := float32(x)
      ay := float32(y)
      bx := o.x + float32(sx + offset_x) * float32(r.dist)
      by := o.y + float32(sy + offset_y) * float32(r.dist)
      dist := float32(math.Sqrt(float64((ax - bx) * (ax - bx) + (ay - by) * (ay - by))))
      if dist < min_dist {
        min_dist = dist
        ret.X = int(bx)
        ret.Y = int(by)
      }
    }
  }
  return
}

// This takes absolute coordinates, and divides them into square space coordinates.
func (r *RandomGrid) square_space(x, y int) (int, int) {
  // Convert to square space coordinates
  sx := int(math.Floor(float64(x) / float64(r.dist)))
  sy := int(math.Floor(float64(y) / float64(r.dist)))
  return sx, sy
}

// This takes square space coordinates, then wraps
// those new coords to be within the grid.
// This will return an absolute point, the point within r.points,
// and the wrapped square space coordinates.
func (r *RandomGrid) get_point(sx, sy int) (RandomGridPoint, point, int, int) {
  // Wrapped coords
  wx := (sx % r.size + r.size) % r.size
  wy := (sy % r.size + r.size) % r.size
  // Local point
  p := r.points[wy][wx]
  // Absolute point
  a := RandomGridPoint{
    int(math.Floor(float64(p.x) + float64(sx) * float64(r.dist))),
    int(math.Floor(float64(p.y) + float64(sy) * float64(r.dist))),
  }
  return a, p, wx, wy
}
