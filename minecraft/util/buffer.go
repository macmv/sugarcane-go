package util

import (
  "math"
  "errors"
  "encoding/binary"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

// This is a custom buffer, optimized for minecraft packets.
// This is used whenever the proxy sends a packet.
type Buffer struct {
  data []byte
  pos int
}

// This creates a new buffer, containing no data.
func NewBuffer() *Buffer {
  b := Buffer{}
  b.data = []byte{}
  b.pos = 0
  return &b
}

// This creates a new buffer, which points to the given slice.
// If the slice is big enough, the buffer will not allocate any more memory.
// It will just start writing into the slice at the given index.
func NewBufferFromSlice(data []byte, start_pos int) (*Buffer, error) {
  if start_pos > len(data) {
    return nil, errors.New("Start pos must be <= len(data)")
  }
  b := Buffer{}
  b.data = data
  b.pos = start_pos
  return &b, nil
}

// Returns the length of the buffer.
// If the buffer started being written at index 10, and you wrote 5 bytes, this would return 15.
func (b *Buffer) Length() int32 {
  return int32(b.pos)
}

// This returns a slice which goes from the very start of the buffer passed in, to the most recently written byte.
// So if you started writing at index 10, and wrote 5 bytes, this would return a slice that is 15 bytes long.
func (b *Buffer) GetData() []byte {
  return b.data[0:b.pos]
}

// This just gives you the buffer that was originally passed into NewBufferFromSlice,
// or the buffer that it created in NewBuffer().
func (b *Buffer) GetRawData() []byte {
  return b.data
}

// Writes a bool into the buffer.
// Will write 0x01 if the bool is true, and 0x00 if it was false.
func (b *Buffer) WriteBool(value bool) {
  if (value) {
    b.WriteByte(0x01)
  } else {
    b.WriteByte(0x00)
  }
}

// This writes a single byte into the buffer.
// All other functions that write a single byte call this one.
// This will either overwrite a single byte in the internal slice, or append a single byte.
func (b *Buffer) WriteByte(value byte) {
  if b.pos < len(b.data) {
    b.data[b.pos] = value
  } else {
    b.data = append(b.data, value)
  }
  b.pos++
}

// This writes a byte array into the buffer.
// All other functions that write multiple bytes call this one.
// If there is enough space in the internal buffer, then it will just copy the values passed in.
// If there is some space, but not enough for the whole array, it will copy the first section, then append the second section.
// Otherwise, it will just append the slice passed in.
// You can see examples of this in buffer_test.go.
func (b *Buffer) WriteByteArray(value []byte) {
  // We check these multiple times. Calling len is slow
  length := len(value)
  data_length := len(b.data)
  num_wrote := 0
  if b.pos < data_length {
    num_wrote = copy(b.data[b.pos:], value)
  }
  // This checks if the end of the slice is outside of b.data
  if b.pos + length > data_length {
    // If it is, we append the part of the slice that was not written
    // If none of the slice was written, num_wrote will be zero, thus appending the whole slice
    b.data = append(b.data, value[num_wrote:]...)
  }
  // We will always write the full length of value, even if we are appending half of it
  // So this should always be increased by length
  b.pos += length
}

// This writes a short, encoded in big endian.
func (b *Buffer) WriteShort(value uint16) {
  tmp := make([]byte, 2)
  binary.BigEndian.PutUint16(tmp, value)
  b.WriteByteArray(tmp)
}

// This writes a short, encoded in little endian.
func (b *Buffer) WriteLittleEndianShort(value uint16) {
  tmp := make([]byte, 2)
  binary.LittleEndian.PutUint16(tmp, value)
  b.WriteByteArray(tmp)
}

// This writes an int, encoded in big endian.
func (b *Buffer) WriteInt(value int32) {
  tmp := make([]byte, 4)
  binary.BigEndian.PutUint32(tmp, uint32(value))
  b.WriteByteArray(tmp)
}

// This writes a long, encoded in big endian.
func (b *Buffer) WriteLong(value uint64) {
  tmp := make([]byte, 8)
  binary.BigEndian.PutUint64(tmp, value)
  b.WriteByteArray(tmp)
}

// This writes a minecraft block position.
// This is a long, where 3 values are encoded.
// It starts with the x value, encoded as a 26 bit integer,
// followed by the z value, which is a 26 bit integer.
// followed by the y value, which is a 12 bit integer,
func (b *Buffer) WritePosition(pos block.Pos) {
  b.WriteLong(pos.AsLong())
}

// This writes a minecraft block position, for versions before 1.14.
// This is a long, where 3 values are encoded.
// It starts with the x value, encoded as a 26 bit integer,
// followed by the y value, which is a 12 bit integer,
// followed by the z value, which is a 26 bit integer.
func (b *Buffer) WriteOldPosition(pos block.Pos) {
  b.WriteLong(pos.AsOldLong())
}

// This writes a minecraft UUID, as 16 bytes.
func (b *Buffer) WriteUUID(value UUID) {
  b.WriteByteArray(value[:])
}

// This writes a float, encoded in IEEE 754 binary format.
func (b *Buffer) WriteFloat(value float32) {
  tmp := make([]byte, 4)
  binary.BigEndian.PutUint32(tmp, math.Float32bits(value))
  b.WriteByteArray(tmp)
}

// This writes a double, encoded in IEEE 754 binary format.
func (b *Buffer) WriteDouble(value float64) {
  tmp := make([]byte, 8)
  binary.BigEndian.PutUint64(tmp, math.Float64bits(value))
  b.WriteByteArray(tmp)
}

// This writes an array of ints.
func (b *Buffer) WriteIntArray(value []int32) {
  for i := 0; i < len(value); i++ {
    b.WriteInt(value[i])
  }
}

// This writes an array of longs.
func (b *Buffer) WriteLongArray(value []uint64) {
  for i := 0; i < len(value); i++ {
    b.WriteLong(value[i])
  }
}

// This writes a length prefixed string.
func (b *Buffer) WriteString(value string) {
  b.WriteVarInt(int32(len(value)))
  b.WriteByteArray([]byte(value))
}

// This writes a null terminated string.
func (b *Buffer) WriteNullString(value string) {
  b.WriteByteArray([]byte(value))
  b.WriteByte(0x00)
}

// This writes a varint, according to minecraft's format.
// This means that the number of bytes it writes can change, and it can be anywhere between 1 and 5 bytes.
// All negative numbers are encoded in 5 bytes, so it is still useful to be able to write 4 byte ints.
func (b *Buffer) WriteVarInt(value int32) {
  // needs to be uint, so that >> will shift the sign bit
  number := uint32(value)
  for {
    tmp := byte(number & 0b01111111)
    number >>= 7
    if number == 0 {
      b.WriteByte(tmp)
      break
    } else {
      tmp |= 0b10000000
      b.WriteByte(tmp)
    }
  }
}
