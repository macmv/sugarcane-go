package util_test

import (
  "testing"

  "gitlab.com/macmv/sugarcane/minecraft/util"
)

func BenchmarkNoise(b *testing.B) {
  b.Run("New", func(b *testing.B) {
    for i := 0; i < b.N; i++ {
      util.NewNoise(util.NewFastRand(0))
    }
  })
  b.Run("Sample", func(b *testing.B) {
    n := util.NewNoise(util.NewFastRand(0))
    for i := 0; i < b.N; i++ {
      n.Sample(0.3, 0.6)
    }
  })
}
