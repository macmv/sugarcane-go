package util

import (
  "testing"
  "math/rand"

  "github.com/stretchr/testify/assert"
)

func TestRandomGridPointsInArea(t *testing.T) {
  rand := rand.New(rand.NewSource(0))
  r := NewRandomGrid(rand, 8, 4)

  // This should just return one point.
  points := r.PointsInArea(0, 0, 8, 8)
  assert.Equal(t, 1, len(points))
  for _, p := range points {
    assert.True(t, p.X >= 0 && p.X <= 8 && p.Y >= 0 && p.Y <= 8, "Point should be within bounds")
  }

  // This tests an area larger than one.
  points = r.PointsInArea(8, 8, 32, 32)
  assert.Equal(t, 9, len(points))
  for _, p := range points {
    assert.True(t, p.X >= 8 && p.X <= 32 && p.Y >= 8 && p.Y <= 32, "Point should be within bounds")
  }

  // This tests the fact that min/max can be passed in any order,
  // as well as wrapping values properly.
  points = r.PointsInArea(0, 0, -32, -32)
  assert.Equal(t, 16, len(points))
  for _, p := range points {
    assert.True(t, p.X >= -32 && p.X <= 0 && p.Y >= -32 && p.Y <= 0, "Point should be within bounds")
  }
}

func TestRandomGridContainsPoint(t *testing.T) {
  rand := rand.New(rand.NewSource(0))
  r := NewRandomGrid(rand, 8, 4)

  points := r.PointsInArea(0, 0, 32, 32)
  for _, p := range points {
    assert.True(t, r.ContainsPoint(p.X, p.Y), "ContainsPoint should be true for all points returned from PointsInArea")
  }

  points = r.PointsInArea(0, 0, -32, -32)
  for _, p := range points {
    assert.True(t, r.ContainsPoint(p.X, p.Y), "ContainsPoint should be true for all points returned from PointsInArea")
  }
}

func TestRandomGridClosestPoint(t *testing.T) {
  rand := rand.New(rand.NewSource(0))
  r := NewRandomGrid(rand, 8, 0.5)

  points := r.PointsInArea(0, 0, 16, 16)
  p := r.ClosestPoint(8, 8)
  assert.Contains(t, points, p, "The closest point should be within the result of PointsInArea")
  // Random numbers. Will change if the seed changes
  assert.Equal(t, RandomGridPoint{10, 10}, p)

  //points = r.PointsInArea(0, 0, -16, -16)
  //p = r.ClosestPoint(-8, -8)
  //assert.Contains(t, points, p, "The closest point should be within the result of PointsInArea")
}
