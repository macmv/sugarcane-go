package util

import (
  "math/rand"
)

// This is a small layer ontop of a RandomGrid.
type Voronoi struct {
  // Point grid for the voronoi map.
  *RandomGrid
}

// Creates an empty voronoi map.
func NewVoronoi(source rand.Source, scale int) *Voronoi {
  return &Voronoi{
    RandomGrid: NewRandomGrid(source, scale, float32(scale) / 4),
  }
}

// This returns a unique id for each voronoi section.
func (v *Voronoi) Get(x, y int) int {
  p := v.ClosestPoint(x, y)
  return p.X ^ p.Y
}

// This adds two noise maps to a voronoi map, which distort
// it in the x and y axis.
type WarpedVoronoi struct {
  // Underlying voronoi map
  *Voronoi

  // Offsets
  x *LayeredNoise
  y *LayeredNoise

  // Noise strength
  strength float64
}

// Creates an empty voronoi map.
func NewWarpedVoronoi(source rand.Source, scale int, noise_strength float64) *WarpedVoronoi {
  return &WarpedVoronoi{
    Voronoi: NewVoronoi(source, scale),
    x: NewLayeredNoise(source, 5),
    y: NewLayeredNoise(source, 5),
    strength: noise_strength,
  }
}

// This returns a unique id for each voronoi section.
func (v *WarpedVoronoi) Get(x, y int) int {
  // Sample x and y
  sx := float64(x) / float64(v.dist)
  sy := float64(y) / float64(v.dist)
  tx := x + int(v.x.Sample(sx, sy) * v.strength * float64(v.dist))
  ty := y + int(v.y.Sample(sx, sy) * v.strength * float64(v.dist))
  return v.Voronoi.Get(tx, ty)
}
