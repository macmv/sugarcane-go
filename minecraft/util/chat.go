package util

import (
  "encoding/json"
  "reflect"
)

type Chat struct {
  sections []map[string]interface{}
}

func NewChat() *Chat {
  c := Chat{}
  return &c
}

func NewChatFromString(message string) *Chat {
  c := NewChat()
  c.AddSection(message)
  return c
}

func NewChatFromStringColor(message string, color string) *Chat {
  c := NewChat()
  c.AddSectionColor(message, color)
  return c
}

func (c *Chat) AddSection(text string) {
  if len(c.sections) > 0 {
    prev := c.sections[len(c.sections) - 1]
    _, ok := prev["color"]
    if ok {
      // resets the color to white, if the previous section had a color
      c.AddSectionColor(text, "white")
      return
    }
  }
  c.sections = append(c.sections, map[string]interface{}{
    "text": text,
  })
}

func (c *Chat) AddSectionColor(text string, color string) {
  if len(c.sections) > 0 {
    prev := c.sections[len(c.sections) - 1]
    _, bold_ok := prev["bold"]
    _, italic_ok := prev["italic"]
    _, underlined_ok := prev["underlined"]
    if bold_ok || italic_ok || underlined_ok {
      // resets formatting of section, if the previous section had formatting
      c.AddSectionFormatted(text, color, false, false, false)
      return
    }
  }
  c.sections = append(c.sections, map[string]interface{}{
    "text": text,
    "color": color,
  })
}

func (c *Chat) AddSectionFormatted(text, color string, bold, italic, underlined bool) {
  c.sections = append(c.sections, map[string]interface{}{
    "text": text,
    "color": color,
    "bold": bold,
    "italic": italic,
    "underlined": underlined,
  })
}

func (c *Chat) AddLink(text, address string) {
  c.sections = append(c.sections, map[string]interface{}{
    "text": text,
    "underlined": true,
    "clickEvent": map[string]string{
      "action": "open_url",
      "value": address,
    },
  })
}

func (c *Chat) AddLinkColor(text, color, address string) {
  c.sections = append(c.sections, map[string]interface{}{
    "text": text,
    "color": color,
    "underlined": true,
    "clickEvent": map[string]string{
      "action": "open_url",
      "value": address,
    },
  })
}

// This returns the chat message, formatted in json.
func (c *Chat) ToJSON() string {
  data, _ := json.Marshal(c.sections)
  return string(data)
}

// This returns an object that can be passed into
// json.Marshal, to produce a chat message.
func (c *Chat) JSONObject() interface{} {
  return c.sections
}

// This returns the chat messages, formatted using the
// legacy color codes. This format is more compact, but
// is deprecated (even though it still works after years).
func (c *Chat) ToFormatCodes() string {
  data := ""
  for _, section := range c.sections {
    data += "§r" // resets previous section formatting
    field, ok := section["color"]
    if ok {
      switch field.(string) {
        case "black":        data += "§0"
        case "dark_blue":    data += "§1"
        case "dark_green":   data += "§2"
        case "dark_aqua":    data += "§3"
        case "dark_red":     data += "§4"
        case "dark_purple":  data += "§5"
        case "gold":         data += "§6"
        case "gray":         data += "§7"
        case "dark_gray":    data += "§8"
        case "blue":         data += "§9"
        case "green":        data += "§a"
        case "aqua":         data += "§b"
        case "red":          data += "§c"
        case "light_purple": data += "§d"
        case "yellow":       data += "§e"
        case "white":        data += "§f"
        // No default case here. If the color is not valid, we just don't put a format code here
      }
    }
    field, ok = section["bold"]
    if ok && field.(bool) {
      data += "§l"
    }
    field, ok = section["italic"]
    if ok && field.(bool) {
      data += "§o"
    }
    field, ok = section["underlined"]
    if ok && field.(bool) {
      data += "§n"
    }
    field, ok = section["text"]
    if !ok {
      panic("No text in section! This should never happen, unless the AddSection function messed up the internal sections")
    }
    text := field.(string)
    data += text
  }
  return data
}

// Converts to chat message to a non colored string
func (c *Chat) String() string {
  data := ""
  for _, section := range c.sections {
    value := reflect.ValueOf(section)
    field := value.FieldByName("Text")
    if !field.IsValid() {
      panic("No text in section! This should never happen, unless the AddSection function messed up the internal sections")
    }
    text := field.Interface().(string)
    data += text
  }
  return data
}
