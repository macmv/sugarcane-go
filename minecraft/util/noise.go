package util

import (
  "math"
  "math/rand"
)

const (
  PERLIN_SIZE = 0x10
  PERLIN_REPEAT_SIZE = PERLIN_SIZE * PERLIN_SIZE
)

// Noise sampler interface.
type NoiseSampler interface {
  // This should sample a noise or layered noise map.
  Sample(x, y float64) float64
}

// This is a perlin noise generator
type Noise struct {
  source *rand.Rand
  a float64
  b float64
  n int

  // Each value is a unit vector in a random direction
  values [PERLIN_REPEAT_SIZE][2]float64
}

func NewNoise(source rand.Source) *Noise {
  n := Noise{}
  n.source = rand.New(source)
  for i := 0; i < PERLIN_REPEAT_SIZE; i++ {
    // Doing this is about twice as fast as using cos(rand)
    // Random vector
    x := n.source.Float64()
    y := n.source.Float64()
    // Normalize it
    length := math.Sqrt(x * x + y * y)
    x /= length
    y /= length
    n.values[i][0] = x
    n.values[i][1] = y
  }
  return &n
}

func (n *Noise) Sample(x, y float64) float64 {
  // Top left corner of the square
  x0 := math.Floor(x)
  y0 := math.Floor(y)
  // Bottom right corner
  x1 := math.Floor(x) + 1
  y1 := math.Floor(y) + 1

  // Weights for each value
  // Could be an s curve or something fancy
  sx := x - x0
  sy := y - y0

  // Dot product of each vector and the offset vector
  val00 := n.interp(x0, y0, x, y)
  val01 := n.interp(x1, y0, x, y)
  val10 := n.interp(x0, y1, x, y)
  val11 := n.interp(x1, y1, x, y)

  a := (val01 - val00) * sx + val00
  b := (val11 - val10) * sx + val10
  c := (b - a) * sy + a

  return c
}

func (n *Noise) interp(x0, y0, x, y float64) float64 {
  p := n.get_value(int(x0), int(y0))

  // Distance from the corner
  dx := x - x0
  dy := y - y0

  // Dot product
  return p[0] * dx + p[1] * dy
}

func (n *Noise) get_value(x, y int) [2]float64 {
  x = x % PERLIN_SIZE
  if x < 0 { x += PERLIN_SIZE }
  y = y % PERLIN_SIZE
  if y < 0 { y += PERLIN_SIZE }
  return n.values[x * PERLIN_SIZE + y]
}

// This is a multi octave noise generator.
type LayeredNoise struct {
  noise *Noise
  octaves int
  offsets []point
}

func NewLayeredNoise(source rand.Source, octaves int) *LayeredNoise {
  r := rand.New(source)
  l := LayeredNoise{}
  l.octaves = octaves
  l.noise = NewNoise(source)
  l.offsets = make([]point, octaves)
  for i := 0; i < octaves; i++ {
    l.offsets[i].x = r.Float32() - 0.5
    l.offsets[i].y = r.Float32() - 0.5
  }
  return &l
}

// Will always return a value between -1 and 1
func (l *LayeredNoise) Sample(x, y float64) float64 {
  var v float64
  for i := 0; i < l.octaves; i++ {
    mult := math.Pow(2, float64(i))
    // Offset the X and Y for each octave, so we don't get artifacts at the 0,0 axis.
    tx := x + float64(l.offsets[i].x)
    ty := y + float64(l.offsets[i].y)
    v += l.noise.Sample(tx * mult, ty * mult) / mult
  }
  return math.Min(math.Max(v, -1), 1)
}
