package util

import (
  "io"
)

// Will parse a varint.
// Returns the number, and the number of bytes that it read.
func ParseVarInt(data []byte) (int32, int32) {
  var result int32 = 0
  var read byte
  var index int32 = 0
  for {
    if index >= int32(len(data)) {
      return -1, -1
    }
    read = data[index]
    var value int32 = int32(read & 0b01111111)
    result |= (value << (7 * index))

    index++

    if (index > 5) {
      return -1, -1
    }
    if (read & 0b10000000) == 0 {
      break
    }
  }
  return result, index
}

// Will parse a varint from the given reader.
// If the reader returns an error, this will return -1, -1.
// Otherwise, this will return the number, and the number of bytes read.
func ReadVarInt(r io.Reader) (int32, int32) {
  var result int32 = 0
  slice := []byte{0}
  var index int32 = 0
  for {
    size, err := r.Read(slice)
    if size < 1 || err != nil {
      return -1, -1
    }
    read := slice[0]
    var value int32 = int32(read & 0b01111111)
    result |= (value << (7 * index))

    index++

    if (index > 5) {
      return -1, -1
    }
    if (read & 0b10000000) == 0 {
      break
    }
  }
  return result, index
}

func ToVarInt(value int32) []byte {
  number := uint32(value)
  bytes := []byte{}
  for {
    tmp := byte(number & 0b01111111)
    number >>= 7
    if number == 0 {
      bytes = append(bytes, tmp)
      break
    } else {
      tmp |= 0b10000000
      bytes = append(bytes, tmp)
    }
  }
  return bytes
}
