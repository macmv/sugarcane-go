package util

import (
  "math/bits"
)

// This is an implementation of wyhash64. It uses
// a uint64 as it's state, and will use math.bits to
// perform 128 bit integer multiplication. After
// benchmarking this, it appears to be ~4 times
// faster than math/rand.
type FastRand struct {
  state uint64
}

// Creates a new FastRand random number generators,
// and seeds it with the given seed.
func NewFastRand(seed int64) *FastRand {
  r := &FastRand{}
  r.Seed(seed)
  return r
}

func (f *FastRand) Seed(seed int64) {
  f.state = uint64(seed)
}

// Returns a pseudo-random positive 64 bit integer.
func (f *FastRand) Int63() int64 {
  return int64(f.Uint64() & (1 << 63 - 1))
}

// Returns a pseudo-random 64 bit integer.
func (f *FastRand) Uint64() uint64 {
  // Original:
  // wyhash64_x += 0x60bee2bee120fc15;
  // __uint128_t tmp;
  // tmp = (__uint128_t) wyhash64_x * 0xa3b195354a39b70d;
  // uint64_t m1 = (tmp >> 64) ^ tmp;
  // tmp = (__uint128_t)m1 * 0x1b03738712fad5c9;
  // uint64_t m2 = (tmp >> 64) ^ tmp;
  // return m2
  f.state += 0x60bee2bee120fc15
  hi, lo := bits.Mul64(f.state, 0xa3b195354a39b70d)
  m1 := hi ^ lo
  hi, lo = bits.Mul64(m1, 0x1b03738712fad5c9)
  return hi ^ lo
}
