package util

import (
  "github.com/stretchr/testify/assert"
  "testing"
  "encoding/hex"
)

func TestChatAddSection(t *testing.T) {
  c := NewChat()
  c.AddSection("Hello")

  assert.Equal(t, []map[string]interface{}{
    {
      "text": "Hello",
    },
  }, c.sections, "AddSection should add the correct structs to the array")
}

func TestChatAddSectionColor(t *testing.T) {
  c := NewChat()
  c.AddSectionColor("Hello", "red")

  assert.Equal(t, []map[string]interface{}{
    {
      "text": "Hello",
      "color": "red",
    },
  }, c.sections, "AddSection should add the correct structs to the array")
}

func TestChatAddSectionFormatted(t *testing.T) {
  c := NewChat()
  c.AddSectionFormatted("Hello", "gold", true, false, false)

  assert.Equal(t, []map[string]interface{}{
    {
      "text": "Hello",
      "color": "gold",
      "bold": true,
      "italic": false,
      "underlined": false,
    },
  }, c.sections, "AddSection should add the correct structs to the array")
}

func TestChatToJSON(t *testing.T) {
  c := NewChat()
  c.AddSection("Hello")
  c.AddSection("Sup")
  assert.Equal(t, `[{"text":"Hello"},{"text":"Sup"}]`, c.ToJSON(), "Chat should be json serialized correctly")

  // cannot test colors, as order of objects in a json object is not defined
}

func TestChatToFormatCodes(t *testing.T) {
  c := NewChat()
  c.AddSection("Hello")
  assert.Equal(t, "§rHello", c.ToFormatCodes(), "Format codes should be inserted correctly")

  c = NewChat()
  c.AddSection("Hello")
  c.AddSectionColor("text", "black")
  c.AddSectionColor("text", "dark_blue")
  c.AddSectionColor("text", "dark_green")
  c.AddSectionColor("text", "dark_aqua")
  c.AddSectionColor("text", "dark_red")
  c.AddSectionColor("text", "dark_purple")
  c.AddSectionColor("text", "gold")
  c.AddSectionColor("text", "gray")
  c.AddSectionColor("text", "dark_gray")
  c.AddSectionColor("text", "blue")
  c.AddSectionColor("text", "green")
  c.AddSectionColor("text", "aqua")
  c.AddSectionColor("text", "red")
  c.AddSectionColor("text", "light_purple")
  c.AddSectionColor("text", "yellow")
  c.AddSectionColor("text", "white")
  c.AddSectionFormatted("text", "white", true, false, true)
  c.AddSectionFormatted("text", "white", true, true, true)
  text := "§rHello"
  for i := 0; i < 16; i++ {
    text += "§r§" + string(hex.EncodeToString([]byte{byte(i)})[1]) + "text"
  }
  text += "§r§f§l§ntext"
  text += "§r§f§l§o§ntext"
  assert.Equal(t, text, c.ToFormatCodes(), "Format codes should be inserted correctly")
}

