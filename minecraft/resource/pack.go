package resource

import (
  "os"
  "bytes"
  "image"
  "image/png"
  _ "image/jpeg"
  "archive/zip"
  "path/filepath"
  "encoding/json"
)

type Pack struct {
  // List of all texture overrides that this pack makes
  textures map[string]image.Image
  // List of json blockstates. Key is the block name.
  block_states map[string]*BlockState
  // List of json models. Key is the model type and name.
  models map[string]*Model

  // Will implement later
  //sounds map[string]sounds
}

// This creates an empty resource pack.
func NewPack() *Pack {
  return &Pack{
    textures: make(map[string]image.Image),
    block_states: make(map[string]*BlockState),
    models: make(map[string]*Model),
  }
}

// Generates a zip file, which is a minecraft resource pack.
// This filesystem is generated every time you call this function,
// so you will always get a unique pointer for each call. It
// is also very slow.
func (p *Pack) Generate() ([]byte, error) {
  buf := new(bytes.Buffer)
  w := zip.NewWriter(buf)

  mcmeta := map[string]interface{}{
    "pack": map[string]interface{}{
      "pack_format": 6,
      "description": "Sugarcane Resource Pack",
    },
  }

  f, err := w.Create("pack.mcmeta")
  if err != nil { return nil, err }

  enc := json.NewEncoder(f)
  err = enc.Encode(mcmeta)
  if err != nil { return nil, err }

  for name, im := range p.textures {
    f, err := w.Create(filepath.Join("assets/minecraft/textures", name) + ".png")
    if err != nil { return nil, err }

    err = png.Encode(f, im)
    if err != nil { return nil, err }
  }

  for name, bs := range p.block_states {
    f, err := w.Create(filepath.Join("assets/minecraft/blockstates", name) + ".json")
    if err != nil { return nil, err }

    enc := json.NewEncoder(f)
    err = enc.Encode(bs)
    if err != nil { return nil, err }
  }

  for name, model := range p.models {
    f, err := w.Create(filepath.Join("assets/minecraft/models", name) + ".json")
    if err != nil { return nil, err }

    enc := json.NewEncoder(f)
    err = enc.Encode(model)
    if err != nil { return nil, err }
  }

  w.Close()

  return buf.Bytes(), nil
}

// This will load an image from path, and add it to the resource pack.
// The name is the path of the image in the resource pack, without
// the namespace. For example, the creeper texture is located here:
// 'assets/minecraft/textures/entity/creeper.png'. If you wanted to
// overwrite the creeper texture, you would pass in 'entity/creeper'
// for the name field.
func (p *Pack) AddTexture(name, path string) error {
  f, err := os.Open(path)
  if err != nil { return err }

  t, _, err := image.Decode(f)
  if err != nil { return err }

  p.AddTextureImage(name, t)

  f.Close()
  return nil
}

// This adds the image as an overwrite in the resource pack. The
// name is in the same format as AddTexture().
func (p *Pack) AddTextureImage(name string, texture image.Image) {
  p.textures[name] = texture
}
