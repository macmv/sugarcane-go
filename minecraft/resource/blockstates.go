package resource

// A blockstate in a resource pack. Example:
//
//  wall_torch.json
//  {
//    "variants": {
//      "facing=east":  [{"model": "block/wall_torch"}],
//      "facing=south": [{"model": "block/wall_torch", "y": 90}],
//      "facing=west":  [{"model": "block/wall_torch", "y": 180}],
//      "facing=north": [{"model": "block/wall_torch", "y": 270}]
//    }
//  }
type BlockState struct {
  // String is a list of properties that should be matched.
  // For example, `snowy=true` would match all variants of grass
  // that are snowy. An empty string matches everything.
  Variants map[string][]*BlockStateVariant `json:"variants"`
}

type BlockStateVariant struct {
  // Path to the json model that should be used
  // for this variant
  Model string `json:"model"`

  // Rotation for the model. Only valid in 90 degree increments.
  X int `json:"x"`
  Y int `json:"y"`

  // If true, then the texture will not rotate with the model.
  UVLock bool `json:"uvlock"`

  // Probabilty that this model will be used. Defaults to 1 (100%).
  // If multiple models are specified, then the one that is
  // chosen will be based on this weight.
  Weight float32 `json:"weight"`
}

// This adds the given blockstate to the resource pack.
// When the resource pack is generated, it will be converted
// to json.
//
// Name is just the name of the block. For example, if you want
// to set the blockstates of 'grass_block', name should be 'grass_block'.
// This will be expanded to the path 'assets/minecraft/blockstates/grass_block.json'.
func (p *Pack) AddBlockState(name string, bs *BlockState) {
  p.block_states[name] = bs
}
