package resource_test

import (
  "io"
  "bytes"
  "testing"
  "archive/zip"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/resource"
)

func TestBlockStateGenerate(t *testing.T) {
  p := resource.NewPack()

  p.AddBlockState("stone", &resource.BlockState{
    Variants: map[string][]*resource.BlockStateVariant{
      "snowy=false": []*resource.BlockStateVariant{
        &resource.BlockStateVariant{
          Model: "block/grass_block",
        },
        &resource.BlockStateVariant{
          Model: "block/grass_block",
          X: 90,
        },
        &resource.BlockStateVariant{
          Model: "block/grass_block",
          X: 180,
        },
        &resource.BlockStateVariant{
          Model: "block/grass_block",
          X: 270,
        },
      },
      "snowy=true": []*resource.BlockStateVariant{
        &resource.BlockStateVariant{
          Model: "block/grass_block_snow",
        },
      },
    },
  })

  d, err := p.Generate()
  assert.Nil(t, err)
  r, err := zip.NewReader(bytes.NewReader(d), int64(len(d)))
  assert.Nil(t, err)

  for _, f := range r.File {
    if f.Name == "pack.mcmeta" {
      rc, err := f.Open()
      assert.Nil(t, err)

      contents, err := io.ReadAll(rc)
      assert.Nil(t, err)

      assert.Equal(t, `{"pack":{"description":"Sugarcane Resource Pack","pack_format":6}}` + "\n", string(contents))

      rc.Close()
    } else if f.Name == "assets/minecraft/blockstates/stone.json" {
      rc, err := f.Open()
      assert.Nil(t, err)

      contents, err := io.ReadAll(rc)
      assert.Nil(t, err)
      assert.NotNil(t, contents) // Contents is too long to check with a nice looking test.

      rc.Close()
    } else {
      t.Fatal("Expected only two files in the zip! Got:", f.Name)
    }
  }
}
