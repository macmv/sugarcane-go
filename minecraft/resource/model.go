package resource

// Json block/entity model. Used in resource packs. Example:
//
//  {
//    "ambientocclusion": false,
//    "textures": {
//      "particle": "#torch"
//    },
//    "elements": [
//      {
//        "from": [ 7, 0, 7 ],
//        "to": [ 9, 10, 9 ],
//        "shade": false,
//        "faces": {
//          "down": { "uv": [ 7, 13, 9, 15 ], "texture": "#torch" },
//          "up":   { "uv": [ 7,  6, 9,  8 ], "texture": "#torch" }
//        }
//      },
//      {
//        "from": [ 7, 0, 0 ],
//        "to": [ 9, 16, 16 ],
//        "shade": false,
//        "faces": {
//          "west": { "uv": [ 0, 0, 16, 16 ], "texture": "#torch" },
//          "east": { "uv": [ 0, 0, 16, 16 ], "texture": "#torch" }
//        }
//      },
//      {
//        "from": [ 0, 0, 7 ],
//        "to": [ 16, 16, 9 ],
//        "shade": false,
//        "faces": {
//          "north": { "uv": [ 0, 0, 16, 16 ], "texture": "#torch" },
//          "south": { "uv": [ 0, 0, 16, 16 ], "texture": "#torch" }
//        }
//      }
//    ]
//  }
type Model struct {
  // Parent model to load.
  Parent string `json:"parent"`

  // Whether or not to enable ambient occlusion. Defaults to true.
  AmbientOcclusion bool `json:"ambientocclusion"`

  // List of models to use.
  Elements []struct {
    // Minimum position within the block. Coordinates are 0-16,
    // where 16 is one block.
    From []float32 `json:"from"`

    // Maximum position within the block. Coordinates are 0-16,
    // where 16 is one block.
    To []float32 `json:"to"`

    // Whether or not to shade this part of the model. Defaults to true.
    Shade bool `json:"shade"`

    // The way in which this element should be rotated.
    Rotation struct {
      // Origin point for rotation
      Origin []int `json:"origin"`

      // One of 'x', 'y', 'z'
      Axis string `json:"axis"`

      // Rotation angle. Can be from 45 to -45, in 22.5 degree increments.
      Angle float32 `json:"angle"`

      // Set to scale the face across the whole block.
      Rescale bool `json:"rescale"`
    }

    // List of faces to render on this model.
    Faces map[string]struct {
      // UV coordinates for this face
      UV []int `json:"uv"`

      // Texture name to use
      Texture string `json:"texture"`

      // Can be one of 'up', 'down', 'north', 'south', 'east', 'west'.
      // If there is a solid block in that direction, then this face will not
      // be rendered.
      Cullface string `json:"cullface"`

      // Rotates the texture in 90 degree increments.
      Rotation int `json:"rotation"`

      // Is unclear what this does. Only affects some blocks.
      TintIndex int `json:"tintindex"`
    } `json:"faces"`
  } `json:"elements"`

  // List of texture variables to texture paths.
  // Defining the variable 'particle' will set the particle
  // texture to use.
  Textures map[string]string
}

// This adds the given blockstate to the resource pack.
// When the resource pack is generated, it will be converted
// to json.
//
// Name is just the name of the model. For example, if you want
// to set the model of grass_block, name should be 'block/grass_block'.
// This will be expanded to the path 'assets/minecraft/models/block/grass_block.json'.
func (p *Pack) AddModel(name string, model *Model) {
  p.models[name] = model
}
