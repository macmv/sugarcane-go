package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type door struct {
  open_by_hand bool
  *block.RegisterBase
}

func new_door(settings block.Settings, open_by_hand bool, v8_id uint32) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &door{}
  kind.open_by_hand = open_by_hand
  kind.RegisterBase = block.NewRegister()
  settings.Versions = block.VersionsMap{
    block.V1_8: {
      // Doors in 1.8 are terrible. The powered and hinge valuesa are entirely based on the top half of the door,
      // while the direction and open values are controlled by the lower half. These leads to a total of 12 blockids.
      // In the f3 menu, through literal magic, they show everything correctly (when you have two halves placed correctly,
      // the 'powered' and 'facing' values appear to be set on both halves of the door, when it is only actually set on
      // one half).
      "facing=east  open=false half=lower": v8_id << 4 | 0,
      "facing=south open=false half=lower": v8_id << 4 | 1,
      "facing=west  open=false half=lower": v8_id << 4 | 2,
      "facing=north open=false half=lower": v8_id << 4 | 3,
      "facing=east  open=true  half=lower": v8_id << 4 | 0 | 4,
      "facing=south open=true  half=lower": v8_id << 4 | 1 | 4,
      "facing=west  open=true  half=lower": v8_id << 4 | 2 | 4,
      "facing=north open=true  half=lower": v8_id << 4 | 3 | 4,
      "hinge=left  powered=false half=upper": v8_id << 4 | 8,
      "hinge=right powered=false half=upper": v8_id << 4 | 9,
      "hinge=left  powered=true  half=upper": v8_id << 4 | 10,
      "hinge=right powered=true  half=upper": v8_id << 4 | 11,
    },
  }
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("open", false))
  kind.AddProperty(block.NewProperty("hinge", "left", "right"))
  kind.AddProperty(block.NewProperty("half", "upper", "lower"))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "east", "west"))
  return kind
}

type trapdoor struct {
  open_by_hand bool
  *block.RegisterBase
}

func new_trapdoor(settings block.Settings, open_by_hand bool) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &trapdoor{}
  kind.open_by_hand = open_by_hand
  kind.RegisterBase = block.NewRegister()
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("open", false))
  kind.AddProperty(block.NewProperty("hinge", "left", "right"))
  kind.AddProperty(block.NewProperty("half", "upper", "lower"))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "east", "west"))
  return kind
}

func add_oak_door(reg *block.Registry) {
  settings := reg.Get(block.V1_13, "minecraft:oak_planks").GetSettings()
  reg.Add(block.V1_13, "minecraft:oak_door", new_door(settings, true, v8_wooden_door))
}

func add_wooden_trapdoors(reg *block.Registry) {
  versions := block.VersionsMap{
    block.V1_8: {
      "facing=north open=false half=lower": v8_trapdoor << 4 | 0,
      "facing=south open=false half=lower": v8_trapdoor << 4 | 1,
      "facing=west  open=false half=lower": v8_trapdoor << 4 | 2,
      "facing=east  open=false half=lower": v8_trapdoor << 4 | 3,
      "facing=north open=true  half=lower": v8_trapdoor << 4 | 0 | 4,
      "facing=south open=true  half=lower": v8_trapdoor << 4 | 1 | 4,
      "facing=west  open=true  half=lower": v8_trapdoor << 4 | 2 | 4,
      "facing=east  open=true  half=lower": v8_trapdoor << 4 | 3 | 4,
      "facing=north open=false half=upper": v8_trapdoor << 4 | 0     | 8,
      "facing=south open=false half=upper": v8_trapdoor << 4 | 1     | 8,
      "facing=west  open=false half=upper": v8_trapdoor << 4 | 2     | 8,
      "facing=east  open=false half=upper": v8_trapdoor << 4 | 3     | 8,
      "facing=north open=true  half=upper": v8_trapdoor << 4 | 0 | 4 | 8,
      "facing=south open=true  half=upper": v8_trapdoor << 4 | 1 | 4 | 8,
      "facing=west  open=true  half=upper": v8_trapdoor << 4 | 2 | 4 | 8,
      "facing=east  open=true  half=upper": v8_trapdoor << 4 | 3 | 4 | 8,
    },
  }

  settings := reg.Get(block.V1_13, "minecraft:oak_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:oak_trapdoor", new_trapdoor(settings, true))

  settings = reg.Get(block.V1_13, "minecraft:spruce_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:spruce_trapdoor", new_trapdoor(settings, true))

  settings = reg.Get(block.V1_13, "minecraft:birch_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:birch_trapdoor", new_trapdoor(settings, true))

  settings = reg.Get(block.V1_13, "minecraft:jungle_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:jungle_trapdoor", new_trapdoor(settings, true))

  settings = reg.Get(block.V1_13, "minecraft:acacia_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:acacia_trapdoor", new_trapdoor(settings, true))

  settings = reg.Get(block.V1_13, "minecraft:dark_oak_planks").GetSettings()
  settings.Versions = versions
  reg.Add(block.V1_13, "minecraft:dark_oak_trapdoor", new_trapdoor(settings, true))
}
