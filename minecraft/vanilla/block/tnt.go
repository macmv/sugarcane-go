package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type tnt struct {
  *block.RegisterBase
}

func new_tnt() block.Register {
  kind := &tnt{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Color: block.Color_TNT,
    Sound: "plant",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_tnt << 4 | 0,
      },
    },
  })
  // NOTE: This property does not exist in 1.13! It was added in 1.13.1.
  // I do not think it is worth my time to implement 1.13, so I'm just going
  // to go with 1.13.1/1.13.2, as that is a more used version. I should
  // add a warning message to 1.13 clients about this, as they will see some
  // incorrect blocks.
  kind.AddProperty(block.NewProperty("unstable", false))
  return kind
}
