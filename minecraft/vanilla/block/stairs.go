package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type stairs struct {
  *block.RegisterBase
}

func new_stairs(b block.Register, v8_id uint32) block.Register {
  settings := b.GetSettings()
  settings.BoundingBox = block.BoundingBox_NONE // overriden by property
  kind := &stairs{}
  kind.RegisterBase = block.NewRegister()

  settings.Versions = block.VersionsMap{
    block.V1_8: {
      // Stairs auto connected in 1.8
      "half=bottom facing=east":  v8_id | 0,
      "half=bottom facing=west":  v8_id | 1,
      "half=bottom facing=south": v8_id | 2,
      "half=bottom facing=north": v8_id | 3,
      "half=top facing=east":     v8_id | 0 | 4,
      "half=top facing=west":     v8_id | 1 | 4,
      "half=top facing=south":    v8_id | 2 | 4,
      "half=top facing=north":    v8_id | 3 | 4,
    },
  }
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("shape", "straight", "inner_left", "inner_right", "outer_left", "outer_right"))
  kind.AddProperty(block.NewProperty("half", "top", "bottom"))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}
