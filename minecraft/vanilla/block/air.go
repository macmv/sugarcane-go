package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func new_air() block.Register {
  kind := block.NewRegister()
  kind.Settings(block.Settings{
    BoundingBox: block.BoundingBox_NONE,
    Transparent: true,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_air << 4 | 0,
      },
    },
  })
  return kind
}
