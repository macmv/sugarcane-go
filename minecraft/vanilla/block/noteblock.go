package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type noteblock struct {
  *block.RegisterBase
}

func new_old_noteblock() block.Register {
  kind := &noteblock{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.8,
    Sound: "wood",
  })
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("note", 25))
  kind.AddProperty(block.NewProperty(
    "instrument",
    "harp",
    "basedrum",
    "snare",
    "hat",
    "bass",
    "flute",
    "bell",
    "guitar",
    "chime",
    "xylophone",
    // These were added in later versions!
    // "iron_xylophone",
    // "cow_bell",
    // "didgeridoo",
    // "bit",
    // "banjo",
    // "pling",
  ))

  return kind
}

func new_noteblock() block.Register {
  kind := &noteblock{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.8,
    Sound: "wood",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_noteblock << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("note", 25))
  kind.AddProperty(block.NewProperty(
    "instrument",
    "harp",
    "basedrum",
    "snare",
    "hat",
    "bass",
    "flute",
    "bell",
    "guitar",
    "chime",
    "xylophone",
    "iron_xylophone",
    "cow_bell",
    "didgeridoo",
    "bit",
    "banjo",
    "pling",
  ))

  return kind
}

type jukebox struct {
  *block.RegisterBase
}

func new_jukebox() block.Register {
  kind := &jukebox{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2,
    Sound: "wood",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_jukebox << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("has_record", false))
  return kind
}
