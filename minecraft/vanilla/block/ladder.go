package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type ladder struct {
  *block.RegisterBase
}

func new_ladder() block.Register {
  kind := &ladder{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.4,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=north": v8_ladder << 4 | 2,
        "facing=south": v8_ladder << 4 | 3,
        "facing=west": v8_ladder << 4 | 4,
        "facing=east": v8_ladder << 4 | 5,
      },
    },
  })
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}
