package vanilla

import (
  "fmt"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type redstone struct {
  *block.RegisterBase
}

func new_redstone() block.Register {
  kind := &redstone{}
  kind.RegisterBase = block.NewRegister()
  settings := block.Settings{
    Sound: "rock",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  for i := uint32(0); i < 16; i++ {
    // Redstone auto-connects in 1.8
    settings.Versions[block.V1_8]["power=" + fmt.Sprint(i)] = v8_redstone_wire << 4 | i
  }
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("east", "up", "side", "none"))
  kind.AddProperty(block.NewProperty("north", "up", "side", "none"))
  kind.AddProperty(block.NewProperty("power", 16))
  kind.AddProperty(block.NewProperty("south", "up", "side", "none"))
  kind.AddProperty(block.NewProperty("west", "up", "side", "none"))
  return kind
}

type lever struct {
  *block.RegisterBase
}

func new_lever() block.Register {
  kind := &lever{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 0.5,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        // Ah yes. Minecraft. Yay 1.13
        "powered=false face=ceiling facing=east":  v8_lever << 4 | 0,
        "powered=false face=ceiling facing=west":  v8_lever << 4 | 0,
        "powered=false face=wall facing=east":     v8_lever << 4 | 1,
        "powered=false face=wall facing=west":     v8_lever << 4 | 2,
        "powered=false face=wall facing=south":    v8_lever << 4 | 3,
        "powered=false face=wall facing=north":    v8_lever << 4 | 4,
        "powered=false face=floor facing=north":   v8_lever << 4 | 5,
        "powered=false face=floor facing=south":   v8_lever << 4 | 5,
        "powered=false face=floor facing=east":    v8_lever << 4 | 6,
        "powered=false face=floor facing=west":    v8_lever << 4 | 6,
        "powered=false face=ceiling facing=north": v8_lever << 4 | 7,
        "powered=false face=ceiling facing=south": v8_lever << 4 | 7,
        "powered=true face=ceiling facing=east":   v8_lever << 4 | 0 | 8,
        "powered=true face=ceiling facing=west":   v8_lever << 4 | 0 | 8,
        "powered=true face=wall facing=east":      v8_lever << 4 | 1 | 8,
        "powered=true face=wall facing=west":      v8_lever << 4 | 2 | 8,
        "powered=true face=wall facing=south":     v8_lever << 4 | 3 | 8,
        "powered=true face=wall facing=north":     v8_lever << 4 | 4 | 8,
        "powered=true face=floor facing=north":    v8_lever << 4 | 5 | 8,
        "powered=true face=floor facing=south":    v8_lever << 4 | 5 | 8,
        "powered=true face=floor facing=east":     v8_lever << 4 | 6 | 8,
        "powered=true face=floor facing=west":     v8_lever << 4 | 6 | 8,
        "powered=true face=ceiling facing=north":  v8_lever << 4 | 7 | 8,
        "powered=true face=ceiling facing=south":  v8_lever << 4 | 7 | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  kind.AddProperty(block.NewProperty("face", "floor", "wall", "ceiling"))
  return kind
}

type redstone_torch struct {
  *block.RegisterBase
}

func new_redstone_torch() block.Register {
  kind := &redstone_torch{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_redstone_torch << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("lit", false))
  return kind
}

func new_redstone_wall_torch() block.Register {
  kind := &redstone_torch{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=east": v8_redstone_torch << 4 | 1,
        "facing=west": v8_redstone_torch << 4 | 2,
        "facing=south": v8_redstone_torch << 4 | 3,
        "facing=north": v8_redstone_torch << 4 | 4,
      },
    },
  })
  kind.AddProperty(block.NewProperty("lit", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

type button struct {
  *block.RegisterBase
}

func new_button(settings block.Settings) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &button{}
  kind.RegisterBase = block.NewRegister()
  id := settings.Versions[block.V1_8][""]
  // Ceiling and floor buttons did not have a direction in 1.8
  settings.Versions[block.V1_8] = make(map[string]uint32)
  settings.Versions[block.V1_8]["powered=false face=ceiling"]           = id | 0
  settings.Versions[block.V1_8]["powered=false face=wall facing=east"]  = id | 1
  settings.Versions[block.V1_8]["powered=false face=wall facing=west"]  = id | 2
  settings.Versions[block.V1_8]["powered=false face=wall facing=south"] = id | 3
  settings.Versions[block.V1_8]["powered=false face=wall facing=north"] = id | 4
  settings.Versions[block.V1_8]["powered=false face=floor"]             = id | 5
  settings.Versions[block.V1_8]["powered=true face=ceiling"]            = id | 0 | 8
  settings.Versions[block.V1_8]["powered=true face=wall facing=east"]   = id | 1 | 8
  settings.Versions[block.V1_8]["powered=true face=wall facing=west"]   = id | 2 | 8
  settings.Versions[block.V1_8]["powered=true face=wall facing=south"]  = id | 3 | 8
  settings.Versions[block.V1_8]["powered=true face=wall facing=north"]  = id | 4 | 8
  settings.Versions[block.V1_8]["powered=true face=floor"]              = id | 5 | 8

  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  kind.AddProperty(block.NewProperty("face", "floor", "wall", "ceiling"))
  return kind
}

type repeater struct {
  *block.RegisterBase
}

func new_repeater() block.Register {
  kind := &repeater{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "wood",
    BoundingBox: block.BoundingBox{
      0, 0, 0,
      1, 2/16, 1,
    },
    Versions: block.VersionsMap{
      block.V1_8: {
        // Repeaters lock/unlock automatically in 1.8
        "powered=false facing=south delay=1": v8_unpowered_repeater << 4 | 0,
        "powered=false facing=west  delay=1": v8_unpowered_repeater << 4 | 1,
        "powered=false facing=north delay=1": v8_unpowered_repeater << 4 | 2,
        "powered=false facing=east  delay=1": v8_unpowered_repeater << 4 | 3,
        "powered=false facing=south delay=2": v8_unpowered_repeater << 4 | 0 | 4,
        "powered=false facing=west  delay=2": v8_unpowered_repeater << 4 | 1 | 4,
        "powered=false facing=north delay=2": v8_unpowered_repeater << 4 | 2 | 4,
        "powered=false facing=east  delay=2": v8_unpowered_repeater << 4 | 3 | 4,
        "powered=false facing=south delay=3": v8_unpowered_repeater << 4 | 0 | 8,
        "powered=false facing=west  delay=3": v8_unpowered_repeater << 4 | 1 | 8,
        "powered=false facing=north delay=3": v8_unpowered_repeater << 4 | 2 | 8,
        "powered=false facing=east  delay=3": v8_unpowered_repeater << 4 | 3 | 8,
        "powered=false facing=south delay=4": v8_unpowered_repeater << 4 | 0 | 12,
        "powered=false facing=west  delay=4": v8_unpowered_repeater << 4 | 1 | 12,
        "powered=false facing=north delay=4": v8_unpowered_repeater << 4 | 2 | 12,
        "powered=false facing=east  delay=4": v8_unpowered_repeater << 4 | 3 | 12,
        "powered=true  facing=south delay=1": v8_powered_repeater << 4 | 0,
        "powered=true  facing=west  delay=1": v8_powered_repeater << 4 | 1,
        "powered=true  facing=north delay=1": v8_powered_repeater << 4 | 2,
        "powered=true  facing=east  delay=1": v8_powered_repeater << 4 | 3,
        "powered=true  facing=south delay=2": v8_powered_repeater << 4 | 0 | 4,
        "powered=true  facing=west  delay=2": v8_powered_repeater << 4 | 1 | 4,
        "powered=true  facing=north delay=2": v8_powered_repeater << 4 | 2 | 4,
        "powered=true  facing=east  delay=2": v8_powered_repeater << 4 | 3 | 4,
        "powered=true  facing=south delay=3": v8_powered_repeater << 4 | 0 | 8,
        "powered=true  facing=west  delay=3": v8_powered_repeater << 4 | 1 | 8,
        "powered=true  facing=north delay=3": v8_powered_repeater << 4 | 2 | 8,
        "powered=true  facing=east  delay=3": v8_powered_repeater << 4 | 3 | 8,
        "powered=true  facing=south delay=4": v8_powered_repeater << 4 | 0 | 12,
        "powered=true  facing=west  delay=4": v8_powered_repeater << 4 | 1 | 12,
        "powered=true  facing=north delay=4": v8_powered_repeater << 4 | 2 | 12,
        "powered=true  facing=east  delay=4": v8_powered_repeater << 4 | 3 | 12,
      },
    },
  })
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("locked", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  kind.AddProperty(block.NewProperty("delay", "1", "2", "3", "4"))
  return kind
}

type redstone_lamp struct {
  *block.RegisterBase
}

func new_redstone_lamp() block.Register {
  kind := &redstone_lamp{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 0.3,
    Sound: "glass",
    Versions: block.VersionsMap{
      block.V1_8: {
        "lit=false": v8_redstone_lamp << 4 | 0,
        "lit=true": v8_lit_redstone_lamp << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("lit", false))
  return kind
}

type pressure_plate struct {
  *block.RegisterBase
}

func new_pressure_plate(settings block.Settings) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &pressure_plate{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("powered", false))
  return kind
}

func add_wooden_pressure_plates(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.5,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "powered=false": v8_wooden_pressure_plate << 4 | 0,
        "powered=true": v8_wooden_pressure_plate << 4 | 1,
      },
    },
  }
  reg.Add(block.V1_13, "minecraft:oak_pressure_plate",      new_pressure_plate(settings))
  reg.Add(block.V1_13, "minecraft:spruce_pressure_plate",   new_pressure_plate(settings))
  reg.Add(block.V1_13, "minecraft:birch_pressure_plate",    new_pressure_plate(settings))
  reg.Add(block.V1_13, "minecraft:jungle_pressure_plate",   new_pressure_plate(settings))
  reg.Add(block.V1_13, "minecraft:acacia_pressure_plate",   new_pressure_plate(settings))
  reg.Add(block.V1_13, "minecraft:dark_oak_pressure_plate", new_pressure_plate(settings))
}

func add_basic_redstone(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:redstone_ore", new_ore(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Color: block.Color_STONE,
    Sound: "stone",
  }, "minecraft:redstone", 4)) // ore.go
  reg.Add(block.V1_13, "minecraft:redstone_torch",      new_redstone_torch())
  reg.Add(block.V1_13, "minecraft:redstone_wall_torch", new_redstone_wall_torch())
  reg.Add(block.V1_13, "minecraft:stone_button",        new_button(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.5,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        // new_button handles the states
        "": v8_stone_button << 4,
      },
    },
  }))
}

func add_redstone_lamp(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:redstone_lamp", new_redstone_lamp())
}
