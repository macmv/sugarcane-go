package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type enchanting_table struct {
  *block.RegisterBase
}

func new_enchanting_table() block.Register {
  kind := &enchanting_table{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 5,
    Resistance: 1200,
    Color: block.Color_RED,
    BoundingBox: block.BoundingBox{
      0, 0, 0,
      1, 12/16, 1,
    },
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_enchanting_table << 4 | 0,
      },
    },
  })
  return kind
}
