package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type infested_stone struct {
  *block.RegisterBase
}

func new_infested_stone(v8_id uint32) block.Register {
  kind := &infested_stone{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0, // I know, it's weird. But its what's in the source code, so idk
    Resistance: 0.75,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_id,
      },
    },
  })
  return kind
}

func add_stone(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 1.5,
    Resistance: 6,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_stone << 4 | 0,
      },
    },
  }
  reg.Add(block.V1_13, "minecraft:stone", new_basic(settings))

  settings.Color = block.Color_DIRT
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 1
  reg.Add(block.V1_13, "minecraft:granite", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 2
  reg.Add(block.V1_13, "minecraft:polished_granite", new_basic(settings))

  settings.Color = block.Color_QUARTZ
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 3
  reg.Add(block.V1_13, "minecraft:diorite", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 4
  reg.Add(block.V1_13, "minecraft:polished_diorite", new_basic(settings))

  settings.Color = block.Color_STONE
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 5
  reg.Add(block.V1_13, "minecraft:andesite", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stone << 4 | 6
  reg.Add(block.V1_13, "minecraft:polished_andesite", new_basic(settings))
}

func add_cobblestone(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:cobblestone", new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 2,
    Resistance: 6,
    Color: block.Color_STONE,
    Sound: "cobblestone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_cobblestone << 4,
      },
    },
  }))
}

func add_sandstone(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.8,
    Color: block.Color_SAND,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_sandstone << 4,
      },
    },
  }
  reg.Add(block.V1_13, "minecraft:sandstone", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_sandstone << 4 | 1
  reg.Add(block.V1_13, "minecraft:chiseled_sandstone", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_sandstone << 4 | 2
  reg.Add(block.V1_13, "minecraft:cut_sandstone", new_basic(settings))
}

func add_stone_bricks(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 1.5,
    Resistance: 6,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_stonebrick << 4,
      },
    },
  }
  reg.Add(block.V1_13, "minecraft:stone_bricks", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stonebrick << 4 | 1
  reg.Add(block.V1_13, "minecraft:mossy_stone_bricks", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stonebrick << 4 | 2
  reg.Add(block.V1_13, "minecraft:cracked_stone_bricks", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_stonebrick << 4 | 3
  reg.Add(block.V1_13, "minecraft:chiseled_stone_bricks", new_basic(settings))
}

func add_infested_stone(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:infested_stone",                 new_infested_stone(v8_monster_egg << 4 | 0))
  reg.Add(block.V1_13, "minecraft:infested_cobblestone",           new_infested_stone(v8_monster_egg << 4 | 1))
  reg.Add(block.V1_13, "minecraft:infested_stone_bricks",          new_infested_stone(v8_monster_egg << 4 | 2))
  reg.Add(block.V1_13, "minecraft:infested_mossy_stone_bricks",    new_infested_stone(v8_monster_egg << 4 | 3))
  reg.Add(block.V1_13, "minecraft:infested_cracked_stone_bricks",  new_infested_stone(v8_monster_egg << 4 | 4))
  reg.Add(block.V1_13, "minecraft:infested_chiseled_stone_bricks", new_infested_stone(v8_monster_egg << 4 | 5))
}
