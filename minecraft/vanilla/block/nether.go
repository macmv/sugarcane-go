package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type nether_portal struct {
  *block.RegisterBase
}

func new_nether_portal() block.Register {
  kind := &nether_portal{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: -1,
    Sound: "glass",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "axis=x": v8_portal << 4 | 0,
        "axis=z": v8_portal << 4 | 2, // why
      },
    },
  })
  kind.AddProperty(block.NewProperty("axis", "x", "z"))
  return kind
}

type basalt struct {
  *block.RegisterBase
}

func new_basalt() block.Register {
  kind := &basalt{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 1.25,
    Resistance : 4.2,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_stone << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("axis", "x", "y", "z"))
  kind.SetDefaultProperty("axis", "y")
  return kind
}

func add_nether(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:netherrack", new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.4,
    Sound: "netherrack",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_netherrack << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_13, "minecraft:soul_sand", new_basic(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.5,
    Sound: "sand",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_soul_sand << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_16, "minecraft:soul_soil", new_basic(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.5,
    Resistance: 0.5,
    Color: block.Color_BROWN,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_soul_sand << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_16, "minecraft:basalt", new_basalt())
  reg.Add(block.V1_16, "minecraft:polished_basalt", new_basalt())
  reg.Add(block.V1_13, "minecraft:glowstone", new_basic(block.Settings{
    Hardness: 0.3,
    Sound: "glass",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_glowstone << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_13, "minecraft:nether_portal", new_nether_portal())
}

func add_nether_bricks(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 2,
    Resistance: 6,
    Color: block.Color_NETHERRACK,
    Sound: "nether_brick",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_nether_brick << 4 | 0,
      },
    },
  }
  reg.Add(block.V1_13, "minecraft:nether_bricks", new_basic(settings))
  settings.Versions = block.VersionsMap{
    block.V1_8: {
      "": v8_nether_brick_fence << 4 | 0,
    },
  }
  reg.Add(block.V1_13, "minecraft:nether_brick_fence", new_fence(settings))
  reg.Add(block.V1_13, "minecraft:nether_brick_stairs", new_stairs(reg.Get(block.V1_13, "minecraft:nether_bricks"), v8_nether_brick_stairs << 4))
  reg.Add(block.V1_13, "minecraft:nether_wart", new_crop(4, block.Color_RED, v8_nether_wart << 4))
}
