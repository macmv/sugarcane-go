package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type sand struct {
  *block.RegisterBase
}

type gravel struct {
  *block.RegisterBase
}

func new_sand(col block.Color) block.Register {
  kind := &sand{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.6,
    Color: col,
    Sound: "sand",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_sand << 4 | 0,
      },
    },
  })
  return kind
}

func new_gravel() block.Register {
  kind := &gravel{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.6,
    Color: block.Color_STONE,
    Sound: "gravel",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_gravel << 4 | 0,
      },
    },
  })
  return kind
}

func (b *gravel) Drops(world block.World, pos block.Pos) (string, int) {
  return "minecraft:flint", 2
}

func add_falling_blocks(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:sand", new_sand(block.Color_SAND))
  reg.Add(block.V1_13, "minecraft:red_sand", new_sand(block.Color_ORANGE))
  reg.Add(block.V1_13, "minecraft:gravel", new_gravel())
}

