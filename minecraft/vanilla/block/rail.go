package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type rail struct {
  *block.RegisterBase
}

func new_rail(turns bool, v8_id uint32) block.Register {
  kind := &rail{}
  kind.RegisterBase = block.NewRegister()
  settings := block.Settings{
    Hardness: 0.7,
    Sound: "metal",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  if turns {
    settings.Versions[block.V1_8]["shape=north_south"]     = v8_id | 0
    settings.Versions[block.V1_8]["shape=east_west"]       = v8_id | 1
    settings.Versions[block.V1_8]["shape=ascending_east"]  = v8_id | 2
    settings.Versions[block.V1_8]["shape=ascending_west"]  = v8_id | 3
    settings.Versions[block.V1_8]["shape=ascending_north"] = v8_id | 4
    settings.Versions[block.V1_8]["shape=ascending_south"] = v8_id | 5
    settings.Versions[block.V1_8]["shape=south_east"]      = v8_id | 6
    settings.Versions[block.V1_8]["shape=south_west"]      = v8_id | 7
    settings.Versions[block.V1_8]["shape=north_east"]      = v8_id | 8
    settings.Versions[block.V1_8]["shape=north_west"]      = v8_id | 9
  } else {
    // Means it can be powered
    settings.Versions[block.V1_8]["powered=false shape=north_south"]     = v8_id | 0
    settings.Versions[block.V1_8]["powered=false shape=east_west"]       = v8_id | 1
    settings.Versions[block.V1_8]["powered=false shape=ascending_east"]  = v8_id | 2
    settings.Versions[block.V1_8]["powered=false shape=ascending_west"]  = v8_id | 3
    settings.Versions[block.V1_8]["powered=false shape=ascending_north"] = v8_id | 4
    settings.Versions[block.V1_8]["powered=false shape=ascending_south"] = v8_id | 5
    settings.Versions[block.V1_8]["powered=true  shape=north_south"]     = v8_id | 0 | 8
    settings.Versions[block.V1_8]["powered=true  shape=east_west"]       = v8_id | 1 | 8
    settings.Versions[block.V1_8]["powered=true  shape=ascending_east"]  = v8_id | 2 | 8
    settings.Versions[block.V1_8]["powered=true  shape=ascending_west"]  = v8_id | 3 | 8
    settings.Versions[block.V1_8]["powered=true  shape=ascending_north"] = v8_id | 4 | 8
    settings.Versions[block.V1_8]["powered=true  shape=ascending_south"] = v8_id | 5 | 8
  }
  kind.Settings(settings)
  if turns {
    kind.AddProperty(block.NewProperty(
      "shape",        // name of property
      "north_south",
      "east_west",
      "ascending_east",
      "ascending_west",
      "ascending_north",
      "ascending_south",
      "south_east",
      "south_west",
      "north_east",
      "north_west",
    ))
  } else {
    kind.AddProperty(block.NewProperty(
      "shape",        // name of property
      "north_south",
      "east_west",
      "ascending_east",
      "ascending_west",
      "ascending_north",
      "ascending_south",
    ))
    kind.AddProperty(block.NewProperty("powered", false))
  }
  return kind
}
