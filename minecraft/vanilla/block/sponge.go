package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type sponge struct {
  *block.RegisterBase
}

func new_sponge() block.Register {
  kind := &sponge{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 0.6,
    Color: block.Color_YELLOW,
    Sound: "plant",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_sponge << 4 | 0,
      },
    },
  })
  return kind
}
