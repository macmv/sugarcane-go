package vanilla

import (
  "fmt"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type sapling struct {
  *block.RegisterBase
}

func new_sapling(t string, v8_meta uint32) block.Register {
  kind := &sapling{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "plant",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "stage=0": v8_sapling << 4 | v8_meta,
        "stage=1": v8_sapling << 4 | v8_meta | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("stage", 2))
  return kind
}

type plant struct {
  growable bool
  *block.RegisterBase
}

func new_plant(v8_id uint32) block.Register {
  kind := &plant{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "plant",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_id,
      },
    },
  })
  kind.growable = false
  return kind
}

// This is for things like sugarcane and cactus. For crops, see crop.go
func new_growable_plant(settings block.Settings) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &plant{}
  kind.RegisterBase = block.NewRegister()
  // Assume that settings.Versions has one field in it, which is the v8 id << 4
  id := settings.Versions[block.V1_8][""]
  settings.Versions[block.V1_8] = make(map[string]uint32)
  for i := uint32(0); i < 16; i++ {
    settings.Versions[block.V1_8]["age=" + fmt.Sprint(i)] = id | i
  }
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("age", 16))
  kind.growable = true
  return kind
}

type carved_pumpkin struct {
  *block.RegisterBase
}

func new_carved_pumpkin() block.Register {
  kind := &carved_pumpkin{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Sound: "wool",
    Versions: block.VersionsMap{
      block.V1_8: {
        // Totally different from chests. Thank you, minecraft.
        "facing=south": v8_pumpkin << 4 | 0,
        "facing=west":  v8_pumpkin << 4 | 1,
        "facing=north": v8_pumpkin << 4 | 2,
        "facing=east":  v8_pumpkin << 4 | 3,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

// Pumpkin/Melon stem
type stem struct {
  *block.RegisterBase
}

func new_attached_stem(v8_id uint32) block.Register {
  kind := &stem{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 1,
    Sound: "wool",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      // Stems auto attached, so we just make this a fully grown stem
      block.V1_8: {
        "": v8_id | 7,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

func new_stem(v8_id uint32) block.Register {
  kind := &stem{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 1,
    Sound: "wool",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "age=0": v8_id | 0,
        "age=1": v8_id | 1,
        "age=2": v8_id | 2,
        "age=3": v8_id | 3,
        "age=4": v8_id | 4,
        "age=5": v8_id | 5,
        "age=6": v8_id | 6,
        "age=7": v8_id | 7,
      },
    },
  })
  kind.AddProperty(block.NewProperty("age", 8))
  return kind
}

type large_mushroom struct {
  *block.RegisterBase
}

func new_large_mushroom() block.Register {
  kind := &large_mushroom{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.2,
    Sound: "wood",
  })
  kind.AddProperty(block.NewProperty("west", false))
  kind.AddProperty(block.NewProperty("up", false))
  kind.AddProperty(block.NewProperty("south", false))
  kind.AddProperty(block.NewProperty("north", false))
  kind.AddProperty(block.NewProperty("east", false))
  kind.AddProperty(block.NewProperty("down", false))
  return kind
}

type vine struct {
  *block.RegisterBase
}

func new_vine() block.Register {
  kind := &stem{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Sound: "wool",
    BoundingBox: block.BoundingBox_NONE,
  })
  kind.AddProperty(block.NewProperty("west", false))
  kind.AddProperty(block.NewProperty("up", false))
  kind.AddProperty(block.NewProperty("south", false))
  kind.AddProperty(block.NewProperty("north", false))
  kind.AddProperty(block.NewProperty("east", false))
  return kind
}

type double_plant struct {
  *block.RegisterBase
}

func new_double_plant(v8_id uint32) block.Register {
  kind := &double_plant{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "plant",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "half=upper": v8_id | 0,
        "half=lower": v8_id | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("half", "upper", "lower"))
  return kind
}

func add_saplings(reg *block.Registry) {
  // The numbers at the end are the block metadata for 1.8. Check this in creative
  // with /setblock <x> <y> <z> minecraft:sapling <number>.
  reg.Add(block.V1_13, "minecraft:oak_sapling",      new_sapling("oak", 0))
  reg.Add(block.V1_13, "minecraft:spruce_sapling",   new_sapling("spurce", 1))
  reg.Add(block.V1_13, "minecraft:birch_sapling",    new_sapling("birch", 2))
  reg.Add(block.V1_13, "minecraft:jungle_sapling",   new_sapling("jungle", 3))
  reg.Add(block.V1_13, "minecraft:acacia_sapling",   new_sapling("acacia", 4))
  reg.Add(block.V1_13, "minecraft:dark_oak_sapling", new_sapling("dark_oak", 5))
}

func add_flowers(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:dandelion",          new_plant(v8_yellow_flower << 4))
  reg.Add(block.V1_13, "minecraft:poppy",              new_plant(v8_red_flower << 4 | 0))
  reg.Add(block.V1_13, "minecraft:blue_orchid",        new_plant(v8_red_flower << 4 | 1))
  reg.Add(block.V1_13, "minecraft:allium",             new_plant(v8_red_flower << 4 | 2))
  reg.Add(block.V1_13, "minecraft:azure_bluet",        new_plant(v8_red_flower << 4 | 3))
  reg.Add(block.V1_13, "minecraft:red_tulip",          new_plant(v8_red_flower << 4 | 4))
  reg.Add(block.V1_13, "minecraft:orange_tulip",       new_plant(v8_red_flower << 4 | 5))
  reg.Add(block.V1_13, "minecraft:white_tulip",        new_plant(v8_red_flower << 4 | 6))
  reg.Add(block.V1_13, "minecraft:pink_tulip",         new_plant(v8_red_flower << 4 | 7))
  reg.Add(block.V1_13, "minecraft:oxeye_daisy",        new_plant(v8_red_flower << 4 | 8))
  reg.Add(block.V1_14, "minecraft:cornflower",         new_plant(v8_red_flower << 4 | 6)) // Most similar
  reg.Add(block.V1_14, "minecraft:wither_rose",        new_plant(v8_red_flower << 4 | 1))
  reg.Add(block.V1_14, "minecraft:lily_of_the_valley", new_plant(v8_red_flower << 4 | 0)) // Nothing is similar to wither_rose
  reg.Add(block.V1_13, "minecraft:brown_mushroom",     new_plant(v8_brown_mushroom << 4))
  reg.Add(block.V1_13, "minecraft:red_mushroom",       new_plant(v8_red_mushroom << 4))
}

func add_pumpkin(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:carved_pumpkin", new_carved_pumpkin())
  reg.Add(block.V1_13, "minecraft:jack_o_lantern", new_carved_pumpkin())
}

func add_mushroom_blocks(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:brown_mushroom_block", new_large_mushroom())
  reg.Add(block.V1_13, "minecraft:red_mushroom_block",   new_large_mushroom())
  reg.Add(block.V1_13, "minecraft:mushroom_stem",        new_large_mushroom())
}

func add_stems(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:attached_melon_stem",   new_attached_stem(v8_melon_stem << 4))
  reg.Add(block.V1_13, "minecraft:attached_pumpkin_stem", new_attached_stem(v8_pumpkin_stem << 4))
  reg.Add(block.V1_13, "minecraft:melon_stem",            new_stem(v8_melon_stem << 4))
  reg.Add(block.V1_13, "minecraft:pumpkin_stem",          new_stem(v8_pumpkin_stem << 4))
}
