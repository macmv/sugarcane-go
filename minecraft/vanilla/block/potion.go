package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type brewing_stand struct {
  *block.RegisterBase
}

func new_brewing_stand() block.Register {
  kind := &snow{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.5,
    Color: block.Color_IRON,
    Sound: "",
    BoundingBox: block.BoundingBox_NONE, // TODO: multiple hitboxes!
    Versions: block.VersionsMap{
      block.V1_8: {
        // Note: 1.8 will auto update this (on the client) to the correct value based on the
        // tile entity. So, if you directly place a block with bottles, and the tile entity
        // has not bottles, then 1.8 clients will reset it to have no bottles.
        "has_bottle_0=false has_bottle_1=false has_bottle_2=false": v8_brewing_stand << 4 | 0,
        "has_bottle_0=true  has_bottle_1=false has_bottle_2=false": v8_brewing_stand << 4 | 1,
        "has_bottle_0=false has_bottle_1=true  has_bottle_2=false": v8_brewing_stand << 4 | 2,
        "has_bottle_0=true  has_bottle_1=true  has_bottle_2=false": v8_brewing_stand << 4 | 3,
        "has_bottle_0=false has_bottle_1=false has_bottle_2=true ": v8_brewing_stand << 4 | 4,
        "has_bottle_0=true  has_bottle_1=false has_bottle_2=true ": v8_brewing_stand << 4 | 5,
        "has_bottle_0=false has_bottle_1=true  has_bottle_2=true ": v8_brewing_stand << 4 | 6,
        "has_bottle_0=true  has_bottle_1=true  has_bottle_2=true ": v8_brewing_stand << 4 | 7,
      },
    },
  })
  kind.AddProperty(block.NewProperty("has_bottle_2", false))
  kind.AddProperty(block.NewProperty("has_bottle_1", false))
  kind.AddProperty(block.NewProperty("has_bottle_0", false))
  return kind
}

type cauldron struct {
  *block.RegisterBase
}

func new_cauldron() block.Register {
  kind := &ice{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 2.0,
    Color: block.Color_STONE,
    Sound: "iron",
    BoundingBox: block.BoundingBox_NONE, // TODO: multiple hitboxes!
    Versions: block.VersionsMap{
      block.V1_8: {
        "level=0": v8_cauldron << 4 | 0,
        "level=1": v8_cauldron << 4 | 1,
        "level=2": v8_cauldron << 4 | 2,
        "level=3": v8_cauldron << 4 | 3,
      },
    },
  })
  kind.AddProperty(block.NewProperty("level", 4))
  return kind
}

func add_potion_things(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:brewing_stand", new_brewing_stand())
  reg.Add(block.V1_13, "minecraft:cauldron", new_cauldron())
}
