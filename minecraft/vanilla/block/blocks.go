package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func RegisterBlocks() {
  reg := block.CreateRegistry()
  reg.Add(block.V1_13, "minecraft:air", new_air())

  // stone.go
  add_stone(reg)
  // grass.go
  add_grass(reg)
  // stone.go
  add_cobblestone(reg)
  // wood.go
  add_planks(reg)
  // plant.go
  add_saplings(reg)

  reg.Add(block.V1_13, "minecraft:bedrock", new_basic(block.Settings{
    Hardness: -1,
    Resistance: -1,
    Color: block.Color_OBSIDIAN,
    Sound: "stone",
  }))

  // fluid.go
  add_fluids(reg)

  // This is sand and gravel
  // falling.go
  add_falling_blocks(reg)

  // These ores are seperate from the rest for some reason
  // ore.go
  add_gold_iron_coal(reg)

  // This is all logs, stripped logs, wood, stripped wood, and leaves
  // wood.go
  add_wood(reg)

  reg.Add(block.V1_13, "minecraft:sponge",     new_sponge())
  reg.Add(block.V1_13, "minecraft:wet_sponge", new_basic(block.Settings{
    Hardness: 0.6,
    Color: block.Color_YELLOW,
    Sound: "plant",
  }))

  // glass.go
  reg.Add(block.V1_13, "minecraft:glass", new_glass())

  // This is lapis ore and lapis block
  // ore.go
  add_lapis(reg)

  // redstone.go
  reg.Add(block.V1_13, "minecraft:dispenser", new_dispenser())

  // stone.go
  add_sandstone(reg)

  // noteblock.go
  reg.Add(block.V1_13, "minecraft:note_block", new_old_noteblock())
  reg.Update(block.V1_14, "minecraft:note_block", new_noteblock())

  // wool.go
  add_beds(reg)

  reg.Add(block.V1_13, "minecraft:powered_rail",  new_rail(false, v8_golden_rail << 4))
  reg.Add(block.V1_13, "minecraft:detector_rail", new_rail(false, v8_detector_rail << 4))

  reg.Add(block.V1_13, "minecraft:sticky_piston", new_piston())

  reg.Add(block.V1_13, "minecraft:cobweb", new_web())

  reg.Add(block.V1_13, "minecraft:grass",         new_plant(v8_tallgrass << 4 | 1))
  reg.Add(block.V1_13, "minecraft:fern",          new_plant(v8_tallgrass << 4 | 2))
  reg.Add(block.V1_13, "minecraft:dead_bush",     new_plant(v8_tallgrass << 4 | 0))
  // 1.8 closest is just tall grass
  reg.Add(block.V1_13, "minecraft:seagrass",      new_plant(v8_tallgrass << 4 | 1))
  reg.Add(block.V1_13, "minecraft:tall_seagrass", new_double_plant(v8_double_plant << 4 | 2))

  reg.Add(block.V1_13, "minecraft:piston",      new_piston())
  reg.Add(block.V1_13, "minecraft:piston_head", new_piston_head())

  // wool.go
  add_wool(reg)

  reg.Add(block.V1_13, "minecraft:moving_piston", new_moving_piston())

  // plant.go
  add_flowers(reg)

  // Gold and iron blocks
  // ore.go
  add_gold_iron_block(reg)

  reg.Add(block.V1_13, "minecraft:bricks", new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 2,
    Resistance: 6,
    Color: block.Color_RED,
    Sound: "stone",
  }))
  reg.Add(block.V1_13, "minecraft:tnt", new_tnt())
  reg.Add(block.V1_13, "minecraft:bookshelf", new_basic(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1.5,
    Color: block.Color_WOOD,
    Sound: "wood",
  }))
  reg.Add(block.V1_13, "minecraft:mossy_cobblestone", new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 2,
    Resistance: 6,
    Color: block.Color_STONE,
    Sound: "cobblestone",
  }))
  reg.Add(block.V1_13, "minecraft:obsidian", new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 50,
    Resistance: 1200,
    Color: block.Color_BLACK,
    Sound: "stone",
  }))

  reg.Add(block.V1_13, "minecraft:torch",      new_torch())
  reg.Add(block.V1_13, "minecraft:wall_torch", new_wall_torch())
  reg.Add(block.V1_13, "minecraft:fire",       new_fire())
  reg.Add(block.V1_16, "minecraft:soul_fire",  new_basic(block.Settings{
    Hardness: -1,
    Color: block.Color_ORANGE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_fire << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_13, "minecraft:spawner",    new_basic(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 5,
    Resistance: 0,
    Color: block.Color_BLACK,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_mob_spawner << 4 | 0,
      },
    },
    Sound: "stone",
  }))

  reg.Add(block.V1_13, "minecraft:oak_stairs",    new_stairs(reg.Get(block.V1_13, "minecraft:oak_planks"), v8_oak_stairs << 4))
  reg.Add(block.V1_13, "minecraft:chest",         new_chest())
  reg.Add(block.V1_13, "minecraft:redstone_wire", new_redstone())

  // This is diamond ore and diamond block
  // ore.go
  add_diamond(reg)

  // wood.go
  add_crafting_table(reg)

  reg.Add(block.V1_13, "minecraft:wheat",    new_crop(8, block.Color_FOLIAGE, v8_wheat << 4))
  reg.Add(block.V1_13, "minecraft:farmland", new_farmland())

  reg.Add(block.V1_13, "minecraft:furnace",  new_furnace())

  // wood.go
  add_signs(reg)

  // door.go
  add_oak_door(reg)

  reg.Add(block.V1_13, "minecraft:ladder", new_ladder())
  reg.Add(block.V1_13, "minecraft:rail", new_rail(true, v8_rail << 4))

  reg.Add(block.V1_13, "minecraft:cobblestone_stairs", new_stairs(reg.Get(block.V1_13, "minecraft:cobblestone"), v8_stone_stairs << 4))

  // wood.go
  add_wall_signs(reg)

  reg.Add(block.V1_13, "minecraft:lever", new_lever())
  reg.Add(block.V1_13, "minecraft:stone_pressure_plate", new_pressure_plate(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.5,
    Color: block.Color_STONE,
    Sound: "stone",
  }))
  reg.Add(block.V1_13, "minecraft:iron_door", new_door(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 5,
    Color: block.Color_IRON,
    Sound: "metal",
  }, false, v8_iron_door << 4))

  // redstone.go
  add_wooden_pressure_plates(reg)
  add_basic_redstone(reg)

  // snow.go
  add_snow_ice(reg)

  // plant.go
  reg.Add(block.V1_13, "minecraft:cactus", new_growable_plant(block.Settings{
    Hardness: 0.4,
    Sound: "wool",
    Versions: block.VersionsMap{
      block.V1_8: {
        // new_growable_plant will add the age fields to the 1.8 version
        "": v8_cactus << 4,
      },
    },
  }))
  reg.Add(block.V1_13, "minecraft:clay", new_basic(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.6,
    Sound: "dirt",
    Color: block.Color_CLAY,
  }))
  reg.Add(block.V1_13, "minecraft:sugar_cane", new_growable_plant(block.Settings{
    Sound: "plant",
    Versions: block.VersionsMap{
      block.V1_8: {
        // new_growable_plant will add the age fields to the 1.8 version
        "": v8_reeds << 4,
      },
    },
  }))

  // noteblock.go
  reg.Add(block.V1_13, "minecraft:juke_box", new_jukebox())
  reg.Add(block.V1_13, "minecraft:oak_fence", new_fence(reg.Get(block.V1_13, "minecraft:oak_planks").GetSettings()))

  reg.Add(block.V1_13, "minecraft:pumpkin", new_basic(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Sound: "wood",
  }))

  // portal.go
  add_nether(reg)

  // This is carved pumpkin and jack o lantern
  // plant.go
  add_pumpkin(reg)

  // cake.go
  reg.Add(block.V1_13, "minecraft:cake", new_cake())

  // redstone.go
  reg.Add(block.V1_13, "minecraft:repeater", new_repeater())

  // glass.go
  add_stained_glass(reg)

  // door.go
  add_wooden_trapdoors(reg)

  // This includes norma, mossy, cracked and chiseled stone bricks.
  // stone.go
  add_stone_bricks(reg)

  // This is all infested stone (stone, cobblestone, stone_bricks, mossy_stone_bricks, etc)
  // stone.go
  add_infested_stone(reg)

  // Is the large mushroom blocks
  // plant.go
  add_mushroom_blocks(reg)

  reg.Add(block.V1_13, "minecraft:iron_bars", new_fence(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 5,
    Resistance: 6,
    Sound: "metal",
  }))
  reg.Add(block.V1_13, "minecraft:glass_pane", new_glass_pane(reg.Get(block.V1_13, "minecraft:glass")))

  reg.Add(block.V1_13, "minecraft:melon", new_basic(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Color: block.Color_LIME,
    Sound: "wood",
  }))

  // These are pumpkin and melon stems, in both growing and attached form
  // plant.go
  add_stems(reg)

  // plant.go
  reg.Add(block.V1_13, "minecraft:vine", new_vine())

  reg.Add(block.V1_13, "minecraft:oak_fence_gate", new_fence_gate())

  reg.Add(block.V1_13, "minecraft:brick_stairs",       new_stairs(reg.Get(block.V1_13, "minecraft:bricks"), v8_brick_stairs << 4))
  reg.Add(block.V1_13, "minecraft:stone_brick_stairs", new_stairs(reg.Get(block.V1_13, "minecraft:stone_bricks"), v8_stone_brick_stairs << 4))

  reg.Add(block.V1_13, "minecraft:mycelium", new_grass(block.Color_PURPLE, "plant", v8_mycelium << 4))
  reg.Add(block.V1_13, "minecraft:lily_pad", new_basic(block.Settings{
    Color: block.Color_FOLIAGE,
    Sound: "lily_pads",
    BoundingBox: block.BoundingBox_NONE,
  }))

  // This is nether bricks, fence, stairs, and nether warts
  // nether.go
  add_nether_bricks(reg)

  // misc.go
  reg.Add(block.V1_13, "minecraft:enchanting_table", new_enchanting_table())

  // This is brewing stand and cauldron
  // potion.go
  add_potion_things(reg)

  // End portal and frame, end_stone, and dragon egg
  // end.go
  add_end(reg)

  // redstone.go
  add_redstone_lamp(reg)

  // crop.go
  reg.Add(block.V1_13, "minecraft:cocoa", new_cocoa())

  reg.Add(block.V1_13, "minecraft:sandstone_stairs", new_stairs(reg.Get(block.V1_13, "minecraft:sandstone"), v8_sandstone_stairs << 4))

  reg.Add(block.V1_13, "minecraft:emerald_ore", new_ore(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_emerald_ore << 4 | 0,
      },
    },
  }, "minecraft:emerald", 1))

  // chest.go
  reg.Add(block.V1_13, "minecraft:ender_chest", new_ender_chest())

  // reg.Add(block.V1_13, "minecraft:tripwire_hook",          new TripWireHookBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement()))
  // reg.Add(block.V1_13, "minecraft:tripwire",               new TripWireBlock((TripWireHookBlock)TRIPWIRE_HOOK, AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement()))
  // reg.Add(block.V1_13, "minecraft:emerald_block",          new Block(AbstractBlock.Properties.create(Material.IRON, MaterialColor.EMERALD).setRequiresTool().hardnessAndResistance(5.0, 6.0).sound(SoundType.METAL)))
  // reg.Add(block.V1_13, "minecraft:spruce_stairs",          new StairsBlock(SPRUCE_PLANKS.getDefaultState(), AbstractBlock.Properties.from(SPRUCE_PLANKS)))
  // reg.Add(block.V1_13, "minecraft:birch_stairs",           new StairsBlock(BIRCH_PLANKS.getDefaultState(), AbstractBlock.Properties.from(BIRCH_PLANKS)))
  // reg.Add(block.V1_13, "minecraft:jungle_stairs",          new StairsBlock(JUNGLE_PLANKS.getDefaultState(), AbstractBlock.Properties.from(JUNGLE_PLANKS)))
  // reg.Add(block.V1_13, "minecraft:command_block",          new CommandBlockBlock(AbstractBlock.Properties.create(Material.IRON, MaterialColor.BROWN).setRequiresTool().hardnessAndResistance(-1.0, 3600000.0).noDrops()))
  // reg.Add(block.V1_13, "minecraft:beacon",                 new BeaconBlock(AbstractBlock.Properties.create(Material.GLASS, MaterialColor.DIAMOND).hardnessAndResistance(3.0).setLightLevel((p_235456_0_) -> {
  // reg.Add(block.V1_13, "minecraft:cobblestone_wall",       new WallBlock(AbstractBlock.Properties.from(COBBLESTONE)))
  // reg.Add(block.V1_13, "minecraft:mossy_cobblestone_wall", new WallBlock(AbstractBlock.Properties.from(COBBLESTONE)))
  //
  // reg.Add(block.V1_13, "minecraft:flower_pot",                new_flower_pot(block.GetKind("minecraft:air"),                AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_oak_sapling",        new_flower_pot(block.GetKind("minecraft:oak_sapling"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_spruce_sapling",     new_flower_pot(block.GetKind("minecraft:spruce_sapling"),     AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_birch_sapling",      new_flower_pot(block.GetKind("minecraft:birch_sapling"),      AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_jungle_sapling",     new_flower_pot(block.GetKind("minecraft:jungle_sapling"),     AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_acacia_sapling",     new_flower_pot(block.GetKind("minecraft:acacia_sapling"),     AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_dark_oak_sapling",   new_flower_pot(block.GetKind("minecraft:dark_oak_sapling"),   AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_fern",               new_flower_pot(block.GetKind("minecraft:fern"),               AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_dandelion",          new_flower_pot(block.GetKind("minecraft:dandelion"),          AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_poppy",              new_flower_pot(block.GetKind("minecraft:poppy"),              AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_blue_orchid",        new_flower_pot(block.GetKind("minecraft:blue_orchid"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_allium",             new_flower_pot(block.GetKind("minecraft:allium"),             AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_azure_bluet",        new_flower_pot(block.GetKind("minecraft:azure_bluet"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_red_tulip",          new_flower_pot(block.GetKind("minecraft:red_tulip"),          AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_orange_tulip",       new_flower_pot(block.GetKind("minecraft:orange_tulip"),       AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_white_tulip",        new_flower_pot(block.GetKind("minecraft:white_tulip"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_pink_tulip",         new_flower_pot(block.GetKind("minecraft:pink_tulip"),         AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_oxeye_daisy",        new_flower_pot(block.GetKind("minecraft:oxeye_daisy"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_cornflower",         new_flower_pot(block.GetKind("minecraft:cornflower"),         AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_lily_of_the_valley", new_flower_pot(block.GetKind("minecraft:lily_of_the_valley"), AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_wither_rose",        new_flower_pot(block.GetKind("minecraft:wither_rose"),        AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_red_mushroom",       new_flower_pot(block.GetKind("minecraft:red_mushroom"),       AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_brown_mushroom",     new_flower_pot(block.GetKind("minecraft:brown_mushroom"),     AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_dead_bush",          new_flower_pot(block.GetKind("minecraft:dead_bush"),          AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  // reg.Add(block.V1_13, "minecraft:potted_cactus",             new_flower_pot(block.GetKind("minecraft:cactus"),             AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().notSolid()))
  //
  // reg.Add(block.V1_13, "minecraft:carrots",                new CarrotBlock(AbstractBlock.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().zeroHardnessAndResistance().sound(SoundType.CROP)))
  // reg.Add(block.V1_13, "minecraft:potatoes",               new PotatoBlock(AbstractBlock.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().zeroHardnessAndResistance().sound(SoundType.CROP)))
  // reg.Add(block.V1_13, "minecraft:oak_button",             new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:spruce_button",          new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:birch_button",           new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:jungle_button",          new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:acacia_button",          new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:dark_oak_button",        new WoodButtonBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:skeleton_skull",         new SkullBlock(SkullBlock.Types.SKELETON, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:skeleton_wall_skull",    new WallSkullBlock(SkullBlock.Types.SKELETON, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(SKELETON_SKULL)))
  // reg.Add(block.V1_13, "minecraft:wither_skeleton_skull",  new WitherSkeletonSkullBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:wither_skeleton_wall_skull",          new WitherSkeletonWallSkullBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(WITHER_SKELETON_SKULL)))
  // reg.Add(block.V1_13, "minecraft:zombie_head",                         new SkullBlock(SkullBlock.Types.ZOMBIE, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:zombie_wall_head",                    new WallSkullBlock(SkullBlock.Types.ZOMBIE, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(ZOMBIE_HEAD)))
  // reg.Add(block.V1_13, "minecraft:player_head",                         new SkullPlayerBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:player_wall_head",                    new SkullWallPlayerBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(PLAYER_HEAD)))
  // reg.Add(block.V1_13, "minecraft:creeper_head",                        new SkullBlock(SkullBlock.Types.CREEPER, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:creeper_wall_head",                   new WallSkullBlock(SkullBlock.Types.CREEPER, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(CREEPER_HEAD)))
  // reg.Add(block.V1_13, "minecraft:dragon_head",                         new SkullBlock(SkullBlock.Types.DRAGON, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F)))
  // reg.Add(block.V1_13, "minecraft:dragon_wall_head",                    new WallSkullBlock(SkullBlock.Types.DRAGON, AbstractBlock.Properties.create(Material.MISCELLANEOUS).hardnessAndResistance(1.0F).lootFrom(DRAGON_HEAD)))
  // reg.Add(block.V1_13, "minecraft:anvil",                               new AnvilBlock(AbstractBlock.Properties.create(Material.ANVIL, MaterialColor.IRON).setRequiresTool().hardnessAndResistance(5.0F, 1200.0F).sound(SoundType.ANVIL)))
  // reg.Add(block.V1_13, "minecraft:chipped_anvil",                       new AnvilBlock(AbstractBlock.Properties.create(Material.ANVIL, MaterialColor.IRON).setRequiresTool().hardnessAndResistance(5.0F, 1200.0F).sound(SoundType.ANVIL)))
  // reg.Add(block.V1_13, "minecraft:damaged_anvil",                       new AnvilBlock(AbstractBlock.Properties.create(Material.ANVIL, MaterialColor.IRON).setRequiresTool().hardnessAndResistance(5.0F, 1200.0F).sound(SoundType.ANVIL)))
  // reg.Add(block.V1_13, "minecraft:trapped_chest",                       new TrappedChestBlock(AbstractBlock.Properties.create(Material.WOOD).hardnessAndResistance(2.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:light_weighted_pressure_plate",       new WeightedPressurePlateBlock(15, AbstractBlock.Properties.create(Material.IRON, MaterialColor.GOLD).setRequiresTool().doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:heavy_weighted_pressure_plate",       new WeightedPressurePlateBlock(150, AbstractBlock.Properties.create(Material.IRON).setRequiresTool().doesNotBlockMovement().hardnessAndResistance(0.5F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:comparator",                          new ComparatorBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).zeroHardnessAndResistance().sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:daylight_detector",                   new DaylightDetectorBlock(AbstractBlock.Properties.create(Material.WOOD).hardnessAndResistance(0.2F).sound(SoundType.WOOD)))
  // reg.Add(block.V1_13, "minecraft:redstone_block",                      new RedstoneBlock(AbstractBlock.Properties.create(Material.IRON, MaterialColor.TNT).setRequiresTool().hardnessAndResistance(5.0F, 6.0F).sound(SoundType.METAL).setOpaque(Blocks::isntSolid)))
  // reg.Add(block.V1_13, "minecraft:nether_quartz_ore",                   new OreBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.NETHERRACK).setRequiresTool().hardnessAndResistance(3.0F, 3.0F).sound(SoundType.NETHER_ORE)))
  // reg.Add(block.V1_13, "minecraft:hopper",                              new HopperBlock(AbstractBlock.Properties.create(Material.IRON, MaterialColor.STONE).setRequiresTool().hardnessAndResistance(3.0F, 4.8F).sound(SoundType.METAL).notSolid()))
  // reg.Add(block.V1_13, "minecraft:quartz_block",                        new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.QUARTZ).setRequiresTool().hardnessAndResistance(0.8F)))
  // reg.Add(block.V1_13, "minecraft:chiseled_quartz_block",               new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.QUARTZ).setRequiresTool().hardnessAndResistance(0.8F)))
  // reg.Add(block.V1_13, "minecraft:quartz_pillar",                       new RotatedPillarBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.QUARTZ).setRequiresTool().hardnessAndResistance(0.8F)))
  // reg.Add(block.V1_13, "minecraft:quartz_stairs",                       new StairsBlock(QUARTZ_BLOCK.getDefaultState(), AbstractBlock.Properties.from(QUARTZ_BLOCK)))
  // reg.Add(block.V1_13, "minecraft:activator_rail",                      new PoweredRailBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS).doesNotBlockMovement().hardnessAndResistance(0.7F).sound(SoundType.METAL)))
  // reg.Add(block.V1_13, "minecraft:dropper",                             new DropperBlock(AbstractBlock.Properties.create(Material.ROCK).setRequiresTool().hardnessAndResistance(3.5F)))
  // reg.Add(block.V1_13, "minecraft:white_terracotta",                    new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.WHITE_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:orange_terracotta",                   new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.ORANGE_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:magenta_terracotta",                  new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.MAGENTA_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:light_blue_terracotta",               new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.LIGHT_BLUE_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:yellow_terracotta",                   new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.YELLOW_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:lime_terracotta",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.LIME_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:pink_terracotta",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.PINK_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:gray_terracotta",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.GRAY_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:light_gray_terracotta",               new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.LIGHT_GRAY_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:cyan_terracotta",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.CYAN_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:purple_terracotta",                   new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.PURPLE_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:blue_terracotta",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.BLUE_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:brown_terracotta",                    new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.BROWN_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:green_terracotta",                    new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.GREEN_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:red_terracotta",                      new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.RED_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:black_terracotta",                    new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.BLACK_TERRACOTTA).setRequiresTool().hardnessAndResistance(1.25F, 4.2F)))
  // reg.Add(block.V1_13, "minecraft:white_stained_glass_pane",            new StainedGlassPaneBlock(DyeColor.WHITE, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:orange_stained_glass_pane",           new StainedGlassPaneBlock(DyeColor.ORANGE, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:magenta_stained_glass_pane",          new StainedGlassPaneBlock(DyeColor.MAGENTA, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:light_blue_stained_glass_pane",       new StainedGlassPaneBlock(DyeColor.LIGHT_BLUE, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:yellow_stained_glass_pane",           new StainedGlassPaneBlock(DyeColor.YELLOW, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:lime_stained_glass_pane",             new StainedGlassPaneBlock(DyeColor.LIME, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:pink_stained_glass_pane",             new StainedGlassPaneBlock(DyeColor.PINK, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:gray_stained_glass_pane",             new StainedGlassPaneBlock(DyeColor.GRAY, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:light_gray_stained_glass_pane",       new StainedGlassPaneBlock(DyeColor.LIGHT_GRAY, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:cyan_stained_glass_pane",             new StainedGlassPaneBlock(DyeColor.CYAN, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:purple_stained_glass_pane",           new StainedGlassPaneBlock(DyeColor.PURPLE, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:blue_stained_glass_pane",             new StainedGlassPaneBlock(DyeColor.BLUE, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:brown_stained_glass_pane",            new StainedGlassPaneBlock(DyeColor.BROWN, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:green_stained_glass_pane",            new StainedGlassPaneBlock(DyeColor.GREEN, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:red_stained_glass_pane",              new StainedGlassPaneBlock(DyeColor.RED, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:black_stained_glass_pane",            new StainedGlassPaneBlock(DyeColor.BLACK, AbstractBlock.Properties.create(Material.GLASS).hardnessAndResistance(0.3F).sound(SoundType.GLASS).notSolid()))
  // reg.Add(block.V1_13, "minecraft:acacia_stairs",                       new StairsBlock(ACACIA_PLANKS.getDefaultState(), AbstractBlock.Properties.from(ACACIA_PLANKS)))
  // reg.Add(block.V1_13, "minecraft:dark_oak_stairs",                     new StairsBlock(DARK_OAK_PLANKS.getDefaultState(), AbstractBlock.Properties.from(DARK_OAK_PLANKS)))
  // reg.Add(block.V1_13, "minecraft:slime_block",                         new SlimeBlock(AbstractBlock.Properties.create(Material.CLAY, MaterialColor.GRASS).slipperiness(0.8F).sound(SoundType.SLIME).notSolid()))
  // reg.Add(block.V1_13, "minecraft:barrier",                             new BarrierBlock(AbstractBlock.Properties.create(Material.BARRIER).hardnessAndResistance(-1.0F, 3600000.8F).noDrops().notSolid().setAllowsSpawn(Blocks::neverAllowSpawn)))
  // reg.Add(block.V1_13, "minecraft:iron_trapdoor",                       new TrapDoorBlock(AbstractBlock.Properties.create(Material.IRON).setRequiresTool().hardnessAndResistance(5.0F).sound(SoundType.METAL).notSolid().setAllowsSpawn(Blocks::neverAllowSpawn)))
  // reg.Add(block.V1_13, "minecraft:prismarine",                          new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.CYAN).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:prismarine_bricks",                   new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.DIAMOND).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:dark_prismarine",                     new Block(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.DIAMOND).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:prismarine_stairs",                   new StairsBlock(PRISMARINE.getDefaultState(), AbstractBlock.Properties.from(PRISMARINE)))
  // reg.Add(block.V1_13, "minecraft:prismarine_brick_stairs",             new StairsBlock(PRISMARINE_BRICKS.getDefaultState(), AbstractBlock.Properties.from(PRISMARINE_BRICKS)))
  // reg.Add(block.V1_13, "minecraft:dark_prismarine_stairs",              new StairsBlock(DARK_PRISMARINE.getDefaultState(), AbstractBlock.Properties.from(DARK_PRISMARINE)))
  // reg.Add(block.V1_13, "minecraft:prismarine_slab",                     new SlabBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.CYAN).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:prismarine_brick_slab",               new SlabBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.DIAMOND).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:dark_prismarine_slab",                new SlabBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.DIAMOND).setRequiresTool().hardnessAndResistance(1.5F, 6.0F)))
  // reg.Add(block.V1_13, "minecraft:sea_lantern",                         new Block(AbstractBlock.Properties.create(Material.GLASS, MaterialColor.QUARTZ).hardnessAndResistance(0.3F).sound(SoundType.GLASS).setLightLevel((p_235455_0_) -> {
}
