package vanilla

import (
  "fmt"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type crop struct {
  *block.RegisterBase
}

func new_crop(num_values int, color block.Color, v8_id uint32) block.Register {
  kind := &crop{}
  kind.RegisterBase = block.NewRegister()
  settings := block.Settings{
    Sound: "crop",
    Color: color,
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  for i := 0; i < num_values; i++ {
    settings.Versions[block.V1_8][fmt.Sprintf("age=%d", i)] = v8_id | uint32(i)
  }
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("age", num_values))
  return kind
}

type cocoa struct {
  *block.RegisterBase
}

func new_cocoa() block.Register {
  kind := &crop{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 1,
    Sound: "plant",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=south age=0": v8_cocoa << 4 | 0,
        "facing=west  age=0": v8_cocoa << 4 | 1,
        "facing=north age=0": v8_cocoa << 4 | 2,
        "facing=east  age=0": v8_cocoa << 4 | 3,
        "facing=south age=1": v8_cocoa << 4 | 0 | 4,
        "facing=west  age=1": v8_cocoa << 4 | 1 | 4,
        "facing=north age=1": v8_cocoa << 4 | 2 | 4,
        "facing=east  age=1": v8_cocoa << 4 | 3 | 4,
        "facing=south age=2": v8_cocoa << 4 | 0 | 8,
        "facing=west  age=2": v8_cocoa << 4 | 1 | 8,
        "facing=north age=2": v8_cocoa << 4 | 2 | 8,
        "facing=east  age=2": v8_cocoa << 4 | 3 | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  kind.AddProperty(block.NewProperty("age", 3))
  return kind
}
