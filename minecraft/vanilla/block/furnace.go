package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type furnace struct {
  *block.RegisterBase
}

func new_furnace() block.Register {
  kind := &furnace{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3.5,
    Sound: "stone",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=north": v8_furnace << 4 | 2,
        "facing=south": v8_furnace << 4 | 3,
        "facing=west": v8_furnace << 4 | 4,
        "facing=east": v8_furnace << 4 | 5,
      },
    },
  })
  kind.AddProperty(block.NewProperty("lit", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}
