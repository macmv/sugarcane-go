package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type piston struct {
  *block.RegisterBase
}

func new_piston() block.Register {
  kind := &piston{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 1.5,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=down  extended=false": v8_piston << 4 | 0,
        "facing=up    extended=false": v8_piston << 4 | 1,
        "facing=north extended=false": v8_piston << 4 | 2,
        "facing=south extended=false": v8_piston << 4 | 3,
        "facing=west  extended=false": v8_piston << 4 | 4,
        "facing=east  extended=false": v8_piston << 4 | 5,
        "facing=down  extended=true": v8_piston << 4 | 0 | 8,
        "facing=up    extended=true": v8_piston << 4 | 1 | 8,
        "facing=north extended=true": v8_piston << 4 | 2 | 8,
        "facing=south extended=true": v8_piston << 4 | 3 | 8,
        "facing=west  extended=true": v8_piston << 4 | 4 | 8,
        "facing=east  extended=true": v8_piston << 4 | 5 | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "east", "south", "west", "up", "down"))
  kind.SetDefaultProperty("facing", "up")
  kind.AddProperty(block.NewProperty("extended", false))
  return kind
}

type piston_head struct {
  *block.RegisterBase
}

func new_piston_head() block.Register {
  kind := &piston_head{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 1.5,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=down  type=normal": v8_piston_head << 4 | 0,
        "facing=up    type=normal": v8_piston_head << 4 | 1,
        "facing=north type=normal": v8_piston_head << 4 | 2,
        "facing=south type=normal": v8_piston_head << 4 | 3,
        "facing=west  type=normal": v8_piston_head << 4 | 4,
        "facing=east  type=normal": v8_piston_head << 4 | 5,
        "facing=down  type=sticky": v8_piston_head << 4 | 0 | 8,
        "facing=up    type=sticky": v8_piston_head << 4 | 1 | 8,
        "facing=north type=sticky": v8_piston_head << 4 | 2 | 8,
        "facing=south type=sticky": v8_piston_head << 4 | 3 | 8,
        "facing=west  type=sticky": v8_piston_head << 4 | 4 | 8,
        "facing=east  type=sticky": v8_piston_head << 4 | 5 | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("type", "normal", "sticky"))
  kind.AddProperty(block.NewProperty("short", false))
  kind.AddProperty(block.NewProperty("facing", "north", "east", "south", "west", "up", "down"))
  kind.SetDefaultProperty("facing", "up")
  return kind
}

type moving_piston struct {
  *block.RegisterBase
}

func new_moving_piston() block.Register {
  kind := &moving_piston{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: -1,
    Resistance: -1,
    Sound: "stone",
  })
  kind.AddProperty(block.NewProperty("type", "normal", "sticky"))
  kind.AddProperty(block.NewProperty("facing", "north", "east", "south", "west", "up", "down"))
  kind.SetDefaultProperty("facing", "up")
  return kind
}
