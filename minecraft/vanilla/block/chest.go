package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type chest struct {
  *block.RegisterBase
}

type ender_chest struct {
  *block.RegisterBase
}

func new_chest() block.Register {
  kind := &chest{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2.5,
    Color: block.Color_WOOD,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        // I literally just tried all the ids. 0-2 are all north, while 3,4,5 are south, west east.
        // This pattern just repeates for all the ids. No clue what the correct way to do this is, but this seems to work.
        "facing=north": v8_chest << 4 | 2,
        "facing=south": v8_chest << 4 | 3,
        "facing=west": v8_chest << 4 | 4,
        "facing=east": v8_chest << 4 | 5,
      },
    },
  })
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("type", "single", "left", "right"))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

func new_ender_chest() block.Register {
  kind := &ender_chest{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 22.5,
    Resistance: 600,
    Color: block.Color_BLACK,
    Sound: "stone",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=north": v8_ender_chest << 4 | 2,
        "facing=south": v8_ender_chest << 4 | 3,
        "facing=west": v8_ender_chest << 4 | 4,
        "facing=east": v8_ender_chest << 4 | 5,
      },
    },
  })
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}
