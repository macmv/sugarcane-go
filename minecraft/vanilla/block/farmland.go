package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type farmland struct {
  *block.RegisterBase
}

func new_farmland() block.Register {
  kind := &farmland{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.6,
    Sound: "ground",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "moisture=0": v8_farmland << 4 | 0,
        "moisture=1": v8_farmland << 4 | 1,
        "moisture=2": v8_farmland << 4 | 2,
        "moisture=3": v8_farmland << 4 | 3,
        "moisture=4": v8_farmland << 4 | 4,
        "moisture=5": v8_farmland << 4 | 5,
        "moisture=6": v8_farmland << 4 | 6,
        "moisture=7": v8_farmland << 4 | 7,
      },
    },
  })
  kind.AddProperty(block.NewProperty("moisture", 8))
  return kind
}
