package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type ore struct {
  item string
  multiplier int
  *block.RegisterBase
}

func new_ore(settings block.Settings, item string, multiplier int) block.Register {
  kind := &ore{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(settings)
  kind.item = item
  kind.multiplier = multiplier
  if item == "minecraft:redstone" {
    kind.AddProperty(block.NewProperty("lit", false))
  }
  return kind
}

func (b *ore) Drops(world block.World, pos block.Pos) (string, int) {
  num := 1
  num *= b.multiplier
  return b.item, num
}

func add_gold_iron_coal(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_gold_ore << 4 | 0
  reg.Add(block.V1_13, "minecraft:gold_ore", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_iron_ore << 4 | 0
  reg.Add(block.V1_13, "minecraft:iron_ore", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_coal_ore << 4 | 0
  reg.Add(block.V1_13, "minecraft:coal_ore", new_ore(settings, "minecraft:coal", 1))
  settings.Versions[block.V1_8][""] = v8_gold_ore << 4 | 0
  reg.Add(block.V1_16, "minecraft:nether_gold_ore", new_ore(settings, "minecraft:gold_nugget", 3))
}

func add_lapis(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_lapis_ore << 4 | 0
  reg.Add(block.V1_13, "minecraft:lapis_ore",   new_ore(settings, "minecraft:lapis_lazuli", 3))
  settings.Sound = "iron"
  settings.Color = block.Color_LAPIS
  settings.Versions[block.V1_8][""] = v8_lapis_block << 4 | 0
  reg.Add(block.V1_13, "minecraft:lapis_block", new_basic(settings))
}

func add_gold_iron_block(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Sound: "iron",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_gold_block << 4 | 0
  settings.Color = block.Color_GOLD; reg.Add(block.V1_13, "minecraft:gold_block", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_iron_block << 4 | 0
  settings.Color = block.Color_IRON; reg.Add(block.V1_13, "minecraft:iron_block", new_basic(settings))
}

func add_diamond(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3,
    Resistance: 3,
    Color: block.Color_STONE,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_diamond_ore << 4 | 0
  reg.Add(block.V1_13, "minecraft:diamond_ore",   new_ore(settings, "minecraft:diamond", 1))
  settings.Sound = "iron"
  settings.Color = block.Color_DIAMOND
  settings.Versions[block.V1_8][""] = v8_diamond_block << 4 | 0
  reg.Add(block.V1_13, "minecraft:diamond_block", new_basic(settings))
}
