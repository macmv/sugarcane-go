package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type fluid struct {
  *block.RegisterBase
}

func new_fluid(settings block.Settings, v8_id uint32) block.Register {
  kind := &sapling{}
  settings.Versions = block.VersionsMap{
    block.V1_8: {
      // I know that fluids only has 8 levels. However, levels 9-15 act the same in both versions, and there might be
      // small differences between using id 10 and id 1, for example. So I decided it was best to just copy
      // all the values over.
      "level=0": v8_id << 4 | 0,
      "level=1": v8_id << 4 | 1,
      "level=2": v8_id << 4 | 2,
      "level=3": v8_id << 4 | 3,
      "level=4": v8_id << 4 | 4,
      "level=5": v8_id << 4 | 5,
      "level=6": v8_id << 4 | 6,
      "level=7": v8_id << 4 | 7,
      "level=8": v8_id << 4 | 8,
      "level=9": v8_id << 4 | 9,
      "level=10": v8_id << 4 | 10,
      "level=11": v8_id << 4 | 11,
      "level=12": v8_id << 4 | 12,
      "level=13": v8_id << 4 | 13,
      "level=14": v8_id << 4 | 14,
      "level=15": v8_id << 4 | 15,
    },
  }
  kind.RegisterBase = new_basic(settings).(*block.RegisterBase)
  kind.AddProperty(block.NewProperty("level", 16))
  return kind
}

func add_fluids(reg *block.Registry) {
  settings := block.Settings{
    Hardness: -1,
    Resistance: -1,
    BoundingBox: block.BoundingBox_NONE,
  }
  settings.Sound = "water"
  reg.Add(block.V1_13, "minecraft:water", new_fluid(settings, v8_water))
  settings.Sound = "lava"
  reg.Add(block.V1_13, "minecraft:lava", new_fluid(settings, v8_lava))
}
