package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type glass struct {
  *block.RegisterBase
}

func new_glass() block.Register {
  kind := &glass{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 0.3,
    Sound: "glass",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_glass << 4,
      },
    },
  })
  return kind
}

func new_stained_glass(settings block.Settings, meta uint32) block.Register {
  kind := &glass{}
  kind.RegisterBase = block.NewRegister()
  settings.Versions = block.VersionsMap{
    block.V1_8: {
      "": v8_stained_glass << 4 | meta,
    },
  }
  kind.Settings(settings)
  return kind
}

func (b *glass) Drops(world block.World, pos block.Pos) (string, int) {
  return "", 0
}

type glass_pane struct {
  *fence
}

func new_glass_pane(base block.Register) block.Register {
  kind := &glass_pane{}
  kind.fence = new_fence(base.GetSettings()).(*fence)
  return kind
}

func (b *glass_pane) Drops(world block.World, pos block.Pos) (string, int) {
  return "", 0
}

func add_stained_glass(reg *block.Registry) {
  settings := block.Settings{
    Hardness: 0.3,
    Sound: "glass",
  }
  settings.Color = block.Color_SNOW;       reg.Add(block.V1_13, "minecraft:white_stained_glass",      new_stained_glass(settings, 0))
  settings.Color = block.Color_ORANGE;     reg.Add(block.V1_13, "minecraft:orange_stained_glass",     new_stained_glass(settings, 1))
  settings.Color = block.Color_MAGENTA;    reg.Add(block.V1_13, "minecraft:magenta_stained_glass",    new_stained_glass(settings, 2))
  settings.Color = block.Color_LIGHT_BLUE; reg.Add(block.V1_13, "minecraft:light_blue_stained_glass", new_stained_glass(settings, 3))
  settings.Color = block.Color_YELLOW;     reg.Add(block.V1_13, "minecraft:yellow_stained_glass",     new_stained_glass(settings, 4))
  settings.Color = block.Color_LIME;       reg.Add(block.V1_13, "minecraft:lime_stained_glass",       new_stained_glass(settings, 5))
  settings.Color = block.Color_PINK;       reg.Add(block.V1_13, "minecraft:pink_stained_glass",       new_stained_glass(settings, 6))
  settings.Color = block.Color_GRAY;       reg.Add(block.V1_13, "minecraft:gray_stained_glass",       new_stained_glass(settings, 7))
  settings.Color = block.Color_LIGHT_GRAY; reg.Add(block.V1_13, "minecraft:light_gray_stained_glass", new_stained_glass(settings, 8))
  settings.Color = block.Color_CYAN;       reg.Add(block.V1_13, "minecraft:cyan_stained_glass",       new_stained_glass(settings, 9))
  settings.Color = block.Color_PURPLE;     reg.Add(block.V1_13, "minecraft:purple_stained_glass",     new_stained_glass(settings, 10))
  settings.Color = block.Color_BLUE;       reg.Add(block.V1_13, "minecraft:blue_stained_glass",       new_stained_glass(settings, 11))
  settings.Color = block.Color_BROWN;      reg.Add(block.V1_13, "minecraft:brown_stained_glass",      new_stained_glass(settings, 12))
  settings.Color = block.Color_GREEN;      reg.Add(block.V1_13, "minecraft:green_stained_glass",      new_stained_glass(settings, 13))
  settings.Color = block.Color_RED;        reg.Add(block.V1_13, "minecraft:red_stained_glass",        new_stained_glass(settings, 14))
  settings.Color = block.Color_BLACK;      reg.Add(block.V1_13, "minecraft:black_stained_glass",      new_stained_glass(settings, 15))
}
