package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type web struct {
  *block.RegisterBase
}

func new_web() block.Register {
  kind := &web{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SWORD,
    Hardness: 4,
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_web << 4 | 0,
      },
    },
  })
  return kind
}
