package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type leaves struct {
  *block.RegisterBase
}

func new_leaves(v8_id uint32) block.Register {
  kind := &leaves{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SHEARS,
    Hardness: 0.2,
    Sound: "plant",
    Versions: block.VersionsMap{
      block.V1_8: {
        // 1.8 has two blocks: minecraft:leaves and minecraft:leaves2.
        // The type of leaf is the first 3 bits, while the 4th is 'decay'.
        "persistent=false": v8_id | 0,
        "persistent=true":  v8_id | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("persistent", false))
  kind.AddProperty(block.NewProperty("distance", "1", "2", "3", "4", "5", "6", "7"))
  kind.SetDefaultProperty("distance", "7")
  return kind
}

type log struct {
  top, side block.Color
  *block.RegisterBase
}

func new_log(top block.Color, side block.Color, v8_id uint32) block.Register {
  kind := &log{}
  kind.top = top
  kind.side = side
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2,
    // Color is not set, as we have a custom Color() function for logs
    Sound: "wood",
    Versions: block.VersionsMap{
      block.V1_8: {
        // There are two blocks: minecraft:log and minecraft:log2.
        // First 2 bits are the type of log, and the last two are the direction.
        // Meta values 12-16 are for solid wood.
        "axis=y": v8_id | 0,
        "axis=x": v8_id | 4,
        "axis=z": v8_id | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("axis", "x", "y", "z"))
  kind.SetDefaultProperty("axis", "y")
  return kind
}

func new_wood(col block.Color, v8_id uint32) block.Register {
  kind := &log{}
  kind.top = col
  kind.side = col
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2,
    // Color is not set, as we have a custom Color() function for logs
    Sound: "wood",
    Versions: block.VersionsMap{
      block.V1_8: {
        // These are full blocks of wood, so the last 2 bits are set.
        "": v8_id | 12,
      },
    },
  })
  kind.AddProperty(block.NewProperty("axis", "x", "y", "z"))
  kind.SetDefaultProperty("axis", "y")
  return kind
}

func (b *log) Color(bs *block.State) block.Color {
  if bs.Type().Properties()["axis"] == "y" {
    return b.top
  } else {
    return b.side
  }
}

// All fences, walls and iron bars are included with this (they all connect)
type fence struct {
  *block.RegisterBase
}

func new_fence(settings block.Settings) block.Register {
  settings.BoundingBox = block.BoundingBox_NONE
  kind := &fence{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(settings)
  kind.AddProperty(block.NewProperty("west", false))
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("south", false))
  kind.AddProperty(block.NewProperty("north", false))
  kind.AddProperty(block.NewProperty("east", false))
  return kind
}

type fence_gate struct {
  *block.RegisterBase
}

func new_fence_gate() block.Register {
  kind := &fence_gate{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2,
    Resistance: 3,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
  })
  kind.AddProperty(block.NewProperty("powered", false))
  kind.AddProperty(block.NewProperty("open", false))
  kind.AddProperty(block.NewProperty("in_wall", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

type sign struct {
  *block.RegisterBase
}

func new_sign(col block.Color) block.Register {
  kind := &sign{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Color: col,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
  })
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("rotation", 16))
  return kind
}

type wall_sign struct {
  *block.RegisterBase
}

func new_wall_sign(col block.Color) block.Register {
  kind := &wall_sign{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 1,
    Color: col,
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
  })
  kind.AddProperty(block.NewProperty("waterlogged", false))
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}

func add_planks(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2,
    Resistance: 3,
    Sound: "planks",
  }
  settings.Color = block.Color_WOOD    ; reg.Add(block.V1_13, "minecraft:oak_planks",      new_basic(settings))
  settings.Color = block.Color_OBSIDIAN; reg.Add(block.V1_13, "minecraft:spruce_planks",   new_basic(settings))
  settings.Color = block.Color_SAND    ; reg.Add(block.V1_13, "minecraft:birch_planks",    new_basic(settings))
  settings.Color = block.Color_DIRT    ; reg.Add(block.V1_13, "minecraft:jungle_planks",   new_basic(settings))
  settings.Color = block.Color_ORANGE  ; reg.Add(block.V1_13, "minecraft:acacia_planks",   new_basic(settings))
  settings.Color = block.Color_BROWN   ; reg.Add(block.V1_13, "minecraft:dark_oak_planks", new_basic(settings))
}

func add_wood(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:oak_log",                new_log(block.Color_WOOD,     block.Color_OBSIDIAN, v8_log  << 4 | 0))
  reg.Add(block.V1_13, "minecraft:spruce_log",             new_log(block.Color_OBSIDIAN, block.Color_BROWN,    v8_log  << 4 | 1))
  reg.Add(block.V1_13, "minecraft:birch_log",              new_log(block.Color_SAND,     block.Color_QUARTZ,   v8_log  << 4 | 2))
  reg.Add(block.V1_13, "minecraft:jungle_log",             new_log(block.Color_DIRT,     block.Color_OBSIDIAN, v8_log  << 4 | 3))
  reg.Add(block.V1_13, "minecraft:acacia_log",             new_log(block.Color_ORANGE,   block.Color_STONE,    v8_log2 << 4 | 0))
  reg.Add(block.V1_13, "minecraft:dark_oak_log",           new_log(block.Color_BROWN,    block.Color_BROWN,    v8_log2 << 4 | 1))
  reg.Add(block.V1_13, "minecraft:stripped_spruce_log",    new_log(block.Color_OBSIDIAN, block.Color_OBSIDIAN, v8_log  << 4 | 1))
  reg.Add(block.V1_13, "minecraft:stripped_birch_log",     new_log(block.Color_SAND,     block.Color_SAND,     v8_log  << 4 | 2))
  reg.Add(block.V1_13, "minecraft:stripped_jungle_log",    new_log(block.Color_DIRT,     block.Color_DIRT,     v8_log  << 4 | 3))
  reg.Add(block.V1_13, "minecraft:stripped_acacia_log",    new_log(block.Color_ORANGE,   block.Color_ORANGE,   v8_log2 << 4 | 0))
  reg.Add(block.V1_13, "minecraft:stripped_dark_oak_log",  new_log(block.Color_BROWN,    block.Color_BROWN,    v8_log2 << 4 | 1))
  // stripped oak log is last, for some inexplicable reason
  reg.Add(block.V1_13, "minecraft:stripped_oak_log",       new_log(block.Color_WOOD,     block.Color_WOOD,     v8_log  << 4 | 0))

  reg.Add(block.V1_13, "minecraft:oak_wood",               new_wood(block.Color_WOOD,     v8_log  << 4 | 0))
  reg.Add(block.V1_13, "minecraft:spruce_wood",            new_wood(block.Color_OBSIDIAN, v8_log  << 4 | 1))
  reg.Add(block.V1_13, "minecraft:birch_wood",             new_wood(block.Color_SAND,     v8_log  << 4 | 2))
  reg.Add(block.V1_13, "minecraft:jungle_wood",            new_wood(block.Color_DIRT,     v8_log  << 4 | 3))
  reg.Add(block.V1_13, "minecraft:acacia_wood",            new_wood(block.Color_GRAY,     v8_log2 << 4 | 0))
  reg.Add(block.V1_13, "minecraft:dark_oak_wood",          new_wood(block.Color_BROWN,    v8_log2 << 4 | 1))
  reg.Add(block.V1_13, "minecraft:stripped_oak_wood",      new_wood(block.Color_WOOD,     v8_log  << 4 | 0))
  reg.Add(block.V1_13, "minecraft:stripped_spruce_wood",   new_wood(block.Color_OBSIDIAN, v8_log  << 4 | 1))
  reg.Add(block.V1_13, "minecraft:stripped_birch_wood",    new_wood(block.Color_SAND,     v8_log  << 4 | 2))
  reg.Add(block.V1_13, "minecraft:stripped_jungle_wood",   new_wood(block.Color_DIRT,     v8_log  << 4 | 3))
  reg.Add(block.V1_13, "minecraft:stripped_acacia_wood",   new_wood(block.Color_ORANGE,   v8_log2 << 4 | 0))
  reg.Add(block.V1_13, "minecraft:stripped_dark_oak_wood", new_wood(block.Color_BROWN,    v8_log2 << 4 | 1))

  reg.Add(block.V1_13, "minecraft:oak_leaves",             new_leaves(v8_leaves  << 4 | 0))
  reg.Add(block.V1_13, "minecraft:spruce_leaves",          new_leaves(v8_leaves  << 4 | 1))
  reg.Add(block.V1_13, "minecraft:birch_leaves",           new_leaves(v8_leaves  << 4 | 2))
  reg.Add(block.V1_13, "minecraft:jungle_leaves",          new_leaves(v8_leaves  << 4 | 3))
  reg.Add(block.V1_13, "minecraft:acacia_leaves",          new_leaves(v8_leaves2 << 4 | 0))
  reg.Add(block.V1_13, "minecraft:dark_oak_leaves",        new_leaves(v8_leaves2 << 4 | 1))
}

func add_crafting_table(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:crafting_table", new_basic(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 2.5,
    Sound: "wood",
  }))
}

func add_signs(reg *block.Registry) {
  // We still call this oak_sign, even though 1.8-1.13 calls this sign.
  reg.Add(block.V1_13, "minecraft:oak_sign",      new_sign(reg.Get(block.V1_13, "minecraft:oak_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:spruce_sign",   new_sign(reg.Get(block.V1_13, "minecraft:spruce_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:birch_sign",    new_sign(reg.Get(block.V1_13, "minecraft:birch_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:acacia_sign",   new_sign(reg.Get(block.V1_13, "minecraft:acacia_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:jungle_sign",   new_sign(reg.Get(block.V1_13, "minecraft:jungle_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:dark_oak_sign", new_sign(reg.Get(block.V1_13, "minecraft:dark_oak_planks").Color(nil)))
}

func add_wall_signs(reg *block.Registry) {
  // We still call this oak_wall_sign, even though 1.8-1.13 calls this wall_sign.
  reg.Add(block.V1_13, "minecraft:oak_wall_sign",      new_wall_sign(reg.Get(block.V1_13, "minecraft:oak_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:spruce_wall_sign",   new_wall_sign(reg.Get(block.V1_13, "minecraft:spruce_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:birch_wall_sign",    new_wall_sign(reg.Get(block.V1_13, "minecraft:birch_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:acacia_wall_sign",   new_wall_sign(reg.Get(block.V1_13, "minecraft:acacia_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:jungle_wall_sign",   new_wall_sign(reg.Get(block.V1_13, "minecraft:jungle_planks").Color(nil)))
  reg.Add(block.V1_14, "minecraft:dark_oak_wall_sign", new_wall_sign(reg.Get(block.V1_13, "minecraft:dark_oak_planks").Color(nil)))
}

// These were added in 1.14
func add_new_signs(reg *block.Registry) {
}
