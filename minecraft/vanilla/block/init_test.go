package vanilla_test

import (
  "testing"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/vanilla/block"
)

func BenchmarkLoad(b *testing.B) {
  for i := 0; i < b.N; i++ {
    vanilla.RegisterBlocks()
    block.LoadBlocks()
  }
}
