package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func new_basic(settings block.Settings) block.Register {
  kind := block.NewRegister()
  kind.Settings(settings.Copy())
  return kind
}
