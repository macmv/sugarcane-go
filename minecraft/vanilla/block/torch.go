package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type torch struct {
  *block.RegisterBase
}

func new_torch() block.Register {
  kind := &torch{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_torch << 4 | 1,
      },
    },
  })
  return kind
}

func new_wall_torch() block.Register {
  kind := &torch{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Sound: "wood",
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=east":  v8_torch << 4 | 1,
        "facing=west":  v8_torch << 4 | 2,
        "facing=south": v8_torch << 4 | 3,
        "facing=north": v8_torch << 4 | 4,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  return kind
}
