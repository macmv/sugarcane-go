package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type snow struct {
  *block.RegisterBase
}

func new_snow() block.Register {
  kind := &snow{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.1,
    Color: block.Color_SNOW,
    Sound: "snow",
    BoundingBox: block.BoundingBox_NONE, // overriden by layer height
    Versions: block.VersionsMap{
      block.V1_8: {
        "layers=1": v8_snow_layer << 4 | 0,
        "layers=2": v8_snow_layer << 4 | 1,
        "layers=3": v8_snow_layer << 4 | 2,
        "layers=4": v8_snow_layer << 4 | 3,
        "layers=5": v8_snow_layer << 4 | 4,
        "layers=6": v8_snow_layer << 4 | 5,
        "layers=7": v8_snow_layer << 4 | 6,
        "layers=8": v8_snow_layer << 4 | 7,
      },
    },
  })
  kind.AddProperty(block.NewProperty("layers", "1", "2", "3", "4", "5", "6", "7", "8"))
  return kind
}

func (b *snow) Drops(world block.World, pos block.Pos) (string, int) {
  return "minecraft:snowball", 4
}

type ice struct {
  *block.RegisterBase
}

func new_ice() block.Register {
  kind := &ice{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 0.5,
    Sound: "glass",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_ice << 4 | 0,
      },
    },
  })
  return kind
}

func add_snow_ice(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:snow",       new_snow())
  reg.Add(block.V1_13, "minecraft:ice",        new_ice())
  reg.Add(block.V1_13, "minecraft:snow_block", new_basic(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.2,
    Sound: "snow",
    Color: block.Color_SNOW,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_snow << 4 | 0,
      },
    },
  }))
}
