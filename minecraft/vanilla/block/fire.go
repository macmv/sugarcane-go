package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type fire struct {
  *block.RegisterBase
}

func new_fire() block.Register {
  kind := &fire{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_fire << 4 | 0,
      },
    },
  })
  kind.AddProperty(block.NewProperty("west", false))
  kind.AddProperty(block.NewProperty("up", false))
  kind.AddProperty(block.NewProperty("south", false))
  kind.AddProperty(block.NewProperty("north", false))
  kind.AddProperty(block.NewProperty("east", false))
  kind.AddProperty(block.NewProperty("age", 16))
  return kind
}
