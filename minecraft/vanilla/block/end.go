package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type end_portal struct {
  *block.RegisterBase
}

type end_portal_frame struct {
  *block.RegisterBase
}

func new_end_portal() block.Register {
  kind := &end_portal{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: -1,
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_end_portal << 4 | 0,
      },
    },
  })
  return kind
}

func new_end_portal_frame() block.Register {
  kind := &end_portal_frame{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: -1,
    // TODO: multiple bounding boxes! This is the one for no eye, but there are two bounding boxes when there is an eye.
    BoundingBox: block.BoundingBox{
      0, 0, 0,
      1, 13/16, 1,
    },
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=south eye=false": v8_end_portal_frame << 4 | 0,
        "facing=west  eye=false": v8_end_portal_frame << 4 | 1,
        "facing=north eye=false": v8_end_portal_frame << 4 | 2,
        "facing=east  eye=false": v8_end_portal_frame << 4 | 3,
        "facing=south eye=true ": v8_end_portal_frame << 4 | 0 | 4,
        "facing=west  eye=true ": v8_end_portal_frame << 4 | 1 | 4,
        "facing=north eye=true ": v8_end_portal_frame << 4 | 2 | 4,
        "facing=east  eye=true ": v8_end_portal_frame << 4 | 3 | 4,
      },
    },
  })
  kind.AddProperty(block.NewProperty("facing", "north", "south", "west", "east"))
  kind.AddProperty(block.NewProperty("eye", false))
  return kind
}

func add_end(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:end_portal", new_end_portal())
  reg.Add(block.V1_13, "minecraft:end_portal_frame", new_end_portal_frame())
  reg.Add(block.V1_13, "minecraft:end_stone", new_basic(block.Settings{
    Hardness: 3,
    Resistance: 9,
    Color: block.Color_SAND,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_end_stone << 4 | 0,
      },
    },
  }))
  reg.Add(block.V1_13, "minecraft:dragon_egg", new_basic(block.Settings{
    Hardness: -1,
    BoundingBox: block.BoundingBox_NONE,
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_dragon_egg << 4 | 0,
      },
    },
  }))
}
