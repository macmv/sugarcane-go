package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type grass struct {
  *block.RegisterBase
}

func new_grass(col block.Color, sound string, v8_id uint32) block.Register {
  kind := &grass{}
  kind.RegisterBase = new_basic(block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.6,
    Resistance: 0,
    Color: col,
    Sound: "grass",
    Versions: block.VersionsMap{
      block.V1_8: {
        "": v8_id,
      },
    },
  }).(*block.RegisterBase)
  kind.AddProperty(block.NewProperty("snowy", false))
  return kind
}

func add_grass(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:grass_block", new_grass(block.Color_DIRT, "grass", v8_grass << 4))
  settings := block.Settings{
    Tool: block.Tool_SHOVEL,
    Hardness: 0.5,
    Resistance: 0,
    Color: block.Color_DIRT,
    Sound: "dirt",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_dirt << 4 | 0
  reg.Add(block.V1_13, "minecraft:dirt",        new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_dirt << 4 | 1
  reg.Add(block.V1_13, "minecraft:coarse_dirt", new_basic(settings))
  reg.Add(block.V1_13, "minecraft:podzol",      new_grass(block.Color_OBSIDIAN, "grass", v8_dirt << 4 | 2))
}
