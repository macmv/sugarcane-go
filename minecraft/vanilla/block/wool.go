package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type bed struct {
  *block.RegisterBase
}

func new_bed(col block.Color) block.Register {
  kind := &bed{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_AXE,
    Hardness: 0.2,
    Resistance: 0,
    Color: col,
    Sound: "wood",
    BoundingBox: block.BoundingBox{
      X1: 0, Y1: 0, Z1: 0,
      X2: 1, Y2: 0.5, Z2: 1,
    },
  })
  kind.AddProperty(block.NewProperty("part", "head", "foot"))
  kind.AddProperty(block.NewProperty("occupied", false))
  kind.AddProperty(block.NewProperty("facing", "north", "east", "south", "west"))
  return kind
}

func add_beds(reg *block.Registry) {
  reg.Add(block.V1_13, "minecraft:white_bed",      new_bed(block.Color_SNOW))
  reg.Add(block.V1_13, "minecraft:orange_bed",     new_bed(block.Color_ORANGE))
  reg.Add(block.V1_13, "minecraft:magenta_bed",    new_bed(block.Color_MAGENTA))
  reg.Add(block.V1_13, "minecraft:light_blue_bed", new_bed(block.Color_LIGHT_BLUE))
  reg.Add(block.V1_13, "minecraft:yellow_bed",     new_bed(block.Color_YELLOW))
  reg.Add(block.V1_13, "minecraft:lime_bed",       new_bed(block.Color_LIME))
  reg.Add(block.V1_13, "minecraft:pink_bed",       new_bed(block.Color_PINK))
  reg.Add(block.V1_13, "minecraft:gray_bed",       new_bed(block.Color_GRAY))
  reg.Add(block.V1_13, "minecraft:light_gray_bed", new_bed(block.Color_LIGHT_GRAY))
  reg.Add(block.V1_13, "minecraft:cyan_bed",       new_bed(block.Color_CYAN))
  reg.Add(block.V1_13, "minecraft:purple_bed",     new_bed(block.Color_PURPLE))
  reg.Add(block.V1_13, "minecraft:blue_bed",       new_bed(block.Color_BLUE))
  reg.Add(block.V1_13, "minecraft:brown_bed",      new_bed(block.Color_BROWN))
  reg.Add(block.V1_13, "minecraft:green_bed",      new_bed(block.Color_GREEN))
  reg.Add(block.V1_13, "minecraft:red_bed",        new_bed(block.Color_RED))
  reg.Add(block.V1_13, "minecraft:black_bed",      new_bed(block.Color_BLACK))
}

func add_wool(reg *block.Registry) {
  settings := block.Settings{
    Tool: block.Tool_SHEARS,
    Hardness: 0.8,
    Sound: "wool",
    Versions: block.VersionsMap{
      block.V1_8: {},
    },
  }
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 0
  settings.Color = block.Color_SNOW      ; reg.Add(block.V1_13, "minecraft:white_wool",      new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 1
  settings.Color = block.Color_ORANGE    ; reg.Add(block.V1_13, "minecraft:orange_wool",     new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 2
  settings.Color = block.Color_MAGENTA   ; reg.Add(block.V1_13, "minecraft:magenta_wool",    new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 3
  settings.Color = block.Color_LIGHT_BLUE; reg.Add(block.V1_13, "minecraft:light_blue_wool", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 4
  settings.Color = block.Color_YELLOW    ; reg.Add(block.V1_13, "minecraft:yellow_wool",     new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 5
  settings.Color = block.Color_LIME      ; reg.Add(block.V1_13, "minecraft:lime_wool",       new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 6
  settings.Color = block.Color_PINK      ; reg.Add(block.V1_13, "minecraft:pink_wool",       new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 7
  settings.Color = block.Color_GRAY      ; reg.Add(block.V1_13, "minecraft:gray_wool",       new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 8
  settings.Color = block.Color_LIGHT_GRAY; reg.Add(block.V1_13, "minecraft:light_gray_wool", new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 9
  settings.Color = block.Color_CYAN      ; reg.Add(block.V1_13, "minecraft:cyan_wool",       new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 10
  settings.Color = block.Color_PURPLE    ; reg.Add(block.V1_13, "minecraft:purple_wool",     new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 11
  settings.Color = block.Color_BLUE      ; reg.Add(block.V1_13, "minecraft:blue_wool",       new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 12
  settings.Color = block.Color_BROWN     ; reg.Add(block.V1_13, "minecraft:brown_wool",      new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 13
  settings.Color = block.Color_GREEN     ; reg.Add(block.V1_13, "minecraft:green_wool",      new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 14
  settings.Color = block.Color_RED       ; reg.Add(block.V1_13, "minecraft:red_wool",        new_basic(settings))
  settings.Versions[block.V1_8][""] = v8_wool << 4 | 15
  settings.Color = block.Color_BLACK     ; reg.Add(block.V1_13, "minecraft:black_wool",      new_basic(settings))
}
