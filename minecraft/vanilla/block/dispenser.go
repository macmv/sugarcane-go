package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type dispenser struct {
  *block.RegisterBase
}

func new_dispenser() block.Register {
  kind := &dispenser{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Tool: block.Tool_PICKAXE,
    Hardness: 3.5,
    Sound: "stone",
    Versions: block.VersionsMap{
      block.V1_8: {
        "facing=down  triggered=false": v8_dispenser << 4 | 0,
        "facing=up    triggered=false": v8_dispenser << 4 | 1,
        "facing=north triggered=false": v8_dispenser << 4 | 2,
        "facing=south triggered=false": v8_dispenser << 4 | 3,
        "facing=west  triggered=false": v8_dispenser << 4 | 4,
        "facing=east  triggered=false": v8_dispenser << 4 | 5,
        "facing=down  triggered=true": v8_dispenser << 4 | 0 | 8,
        "facing=up    triggered=true": v8_dispenser << 4 | 1 | 8,
        "facing=north triggered=true": v8_dispenser << 4 | 2 | 8,
        "facing=south triggered=true": v8_dispenser << 4 | 3 | 8,
        "facing=west  triggered=true": v8_dispenser << 4 | 4 | 8,
        "facing=east  triggered=true": v8_dispenser << 4 | 5 | 8,
      },
    },
  })
  kind.AddProperty(block.NewProperty("triggered", false))
  kind.AddProperty(block.NewProperty("facing", "north", "east", "south", "west", "up", "down"))
  return kind
}
