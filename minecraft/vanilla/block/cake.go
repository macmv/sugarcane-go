package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type cake struct {
  *block.RegisterBase
}

func new_cake() block.Register {
  kind := &cake{}
  kind.RegisterBase = block.NewRegister()
  kind.Settings(block.Settings{
    Hardness: 0.5,
    Sound: "wool",
    BoundingBox: block.BoundingBox{
      0, 0, 0,
      1, 0.5, 1,
    },
    Versions: block.VersionsMap{
      block.V1_8: {
        "bites=0": v8_cake << 4 | 0,
        "bites=1": v8_cake << 4 | 1,
        "bites=2": v8_cake << 4 | 2,
        "bites=3": v8_cake << 4 | 3,
        "bites=4": v8_cake << 4 | 4,
        "bites=5": v8_cake << 4 | 5,
        "bites=6": v8_cake << 4 | 6,
      },
    },
  })
  kind.AddProperty(block.NewProperty("bites", 7))
  return kind
}

func (b *cake) Drops(world block.World, pos block.Pos) (string, int) {
  return "", 0
}
