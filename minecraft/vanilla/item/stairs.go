package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type stairs struct {
  *item.BlockItem
}

func new_stairs(name string) *stairs {
  return &stairs{item.NewBlock(name)}
}

func (i *stairs) OnClick(w block.World, p item.Player, pos block.Pos, face block.Face) {
  if i.Block() == nil {
    chat := util.NewChatFromStringColor("The block ", "red")
    chat.AddSectionColor(i.Name(), "white")
    chat.AddSectionColor(" has not been added yet!", "red")
    p.SendChat(chat)
    // Makes sure there are no ghost blocks for the client
    w.SetBlock(pos, block.Air())
    return
  }

  pos = pos.OffsetFace(face)

  var half, facing block.Face
  if face == block.FACE_BOTTOM || face == block.FACE_TOP {
    facing = p.AxisFacing()
    half = face.Invert()
  } else {
    facing = face.Invert()
    half = block.FACE_BOTTOM
  }
  w.SetBlock(pos, i.Block().Type(map[string]string{
    "facing": facing.String(),
    "half": half.String(),
    "shape": "straight",
  }))
  fix_stairs(w, pos.Add(1, 0, 0))
  fix_stairs(w, pos.Add(-1, 0, 0))
  fix_stairs(w, pos.Add(0, 0, 1))
  fix_stairs(w, pos.Add(0, 0, -1))
}

func (i *stairs) OnShiftClick(w block.World, p item.Player, pos block.Pos, face block.Face) {

}

// This just checks block around the given pos, and generates a block state accordingly.
func fix_stairs(w block.World, pos block.Pos) {
  block := w.BlockType(pos)
  if is_stair(block) {
    w.FixDesync(pos)
  }
}

func is_stair(t *block.Type) bool {
  arr := t.PossiblePropertyValues("shape")
  // If the value doesn't exist, the arr will be nil. len(nil) = 0. So this is safe.
  if len(arr) != 5 {
    return false
  }
  if arr[0] != "straight" { return false }
  if arr[1] != "inner_left" { return false }
  if arr[2] != "inner_right" { return false }
  if arr[3] != "outer_left" { return false }
  if arr[4] != "outer_right" { return false }
  return true
}
