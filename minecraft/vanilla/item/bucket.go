package vanilla

import (
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type bucket struct {
  *item.Basic
  block *block.Kind
}

func new_bucket(name, block_name string) *bucket {
  return &bucket{item.NewBasic(name), block.GetKind(block_name)}
}

func (i *bucket) OnClick(w block.World, p item.Player, pos block.Pos, face block.Face) {
  pos = pos.OffsetFace(face)
  old := w.BlockType(pos)
  t := i.block.DefaultType()
  if t == block.GetKind("minecraft:air").DefaultType() {
    // This happens when you are holding an empty bucket
    if old == block.GetKind("minecraft:water").DefaultType() {
      w.SetBlock(pos, i.block.DefaultType())
      p.Inventory().SetHeldItem(item.NewItemStack(item.GetItem("minecraft:water_bucket"), 1, nil))
    } else if old == block.GetKind("minecraft:lava").DefaultType() {
      w.SetBlock(pos, i.block.DefaultType())
      p.Inventory().SetHeldItem(item.NewItemStack(item.GetItem("minecraft:lava_bucket"), 1, nil))
    }
  } else {
    // This happens when you are holding a water or lava bucket
    if old == block.GetKind("minecraft:air").DefaultType() {
      w.SetBlock(pos, t)
      p.Inventory().SetHeldItem(item.NewItemStack(item.GetItem("minecraft:bucket"), 1, nil))
    }
  }
}
