package vanilla

import (
  "fmt"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type ladder struct {
  *item.BlockItem
}

func new_ladder() *ladder {
  return &ladder{item.NewBlock("minecraft:ladder")}
}

func (i *ladder) OnClick(w block.World, p item.Player, pos block.Pos, face block.Face) {
  if face == block.FACE_BOTTOM || face == block.FACE_TOP {
    return
  }
  pos = pos.OffsetFace(face)
  t := i.Block().Type(map[string]string{"facing": face.String()})
  if t == nil {
    fmt.Println("Ladder is nil! (properties are invalid)")
    return
  }
  w.SetBlock(pos, t)
}
