package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"
)

type Living interface {
  Entity
  Damage(amount float32)
  Health() float32
  // This uses the time since last damaged, and checks if you were attacked in one half of a second
  ImmuneToAttacks() bool
  Equipment() map[int]*item.Stack
}

type LivingBase struct {
  Base
  health float32
  // Tick (in entity time) since it was last hit
  last_hit_time int
  equipment map[int]*item.Stack
}

func new_living(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  base := new_base(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*Base)
  l := LivingBase{}
  l.Base = *base
  l.health = 20
  return &l
}

func NewLivingBase(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) *LivingBase {
  living := new_living(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*LivingBase)
  return living
}

func (l *LivingBase) Damage(amount float32) {
  l.SetHealth(l.health - amount)
  l.last_hit_time = l.Time()
}

func (l *LivingBase) Health() float32 {
  return l.health
}

func (l *LivingBase) SetHealth(val float32) {
  l.health = val
  l.metadata.Write(8, metadata.EntityType_Float, val)
}

// This returns true if the entity was attacked within half second
func (l *LivingBase) ImmuneToAttacks() bool {
  return l.Time() - l.last_hit_time <= 10
}

func (l *LivingBase) Equipment() map[int]*item.Stack {
  return l.equipment
}

func (l *LivingBase) Tick() {
  l.Base.Tick()
  // Moving left
  l.vx = 0.1
  l.Base.ApplyVelocity()
}
