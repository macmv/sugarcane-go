package entity

import (
  "math"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"
)

type Base struct {
  etype Type
  eid uint32
  uuid util.UUID
  // Used for deltas and velocity calculation
  new_x, new_y, new_z float64
  // Only set once per tick, and is based on what new_x, new_y and new_z are
  x, y, z float64
  // When this is true, Collide will be called, and position packets will be sent
  needs_pos_update bool
  pitch, yaw, head_pitch byte
  // velocity is in blocks per tick
  vx, vy, vz float64
  needs_vel_update bool
  attr *Attributes
  metadata *metadata.Metadata
  // Number of ticks since they have spawned
  tick int
}

type CollisionInfo struct {
  // Set to true if the hitbox has collided with any face
  DidCollide bool
  // this is the position that the entity should be at, so that they are not inside a block
  NewX, NewY, NewZ float64
  // hit values for all directions. Used to set velocity to 0 if they were moving in that direciton
  // HitNY is essentially if they are on the ground
  HitPX, HitPY, HitPZ, HitNX, HitNY, HitNZ bool
}

func new_base(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  b := Base{}
  b.attr = &attr
  b.etype = etype
  b.eid = eid
  b.uuid = uuid
  b.x = x
  b.y = y
  b.z = z
  b.pitch = pitch
  b.yaw = yaw
  b.head_pitch = head_pitch
  b.vx = float64(vx) / 8000
  b.vy = float64(vy) / 8000
  b.vz = float64(vz) / 8000
  b.metadata = metadata.New()
  return &b
}

// Current position. Only updated after each game tick.
func (b *Base) X() float64 { return b.x }
func (b *Base) Y() float64 { return b.y }
func (b *Base) Z() float64 { return b.z }

// Position that will be set next tick
func (b *Base) NewX() float64 { return b.new_x }
func (b *Base) NewY() float64 { return b.new_y }
func (b *Base) NewZ() float64 { return b.new_z }

// Hitbox size
func (b *Base) Width() float64 { return b.attr.w }
func (b *Base) Height() float64 { return b.attr.h }

func (b *Base) BlockPos() block.Pos {
  block_pos := block.Pos{int32(b.X()), int32(b.Y()), int32(b.Z())}
  if b.X() < 0 { block_pos.X-- }
  if b.Y() < 0 { block_pos.Y-- }
  if b.Z() < 0 { block_pos.Z-- }
  return block_pos
}

func (b *Base) NeedsPosUpdate() bool { return b.needs_pos_update }
func (b *Base) NeedsVelUpdate() bool { return b.needs_vel_update }
func (b *Base) Vx() int16 { return int16(b.vx * 8000) }
func (b *Base) Vy() int16 { return int16(b.vy * 8000) }
func (b *Base) Vz() int16 { return int16(b.vz * 8000) }

func (b *Base) EID() uint32 { return b.eid }

// This sets the new position of the entity to
// be it's current position plus it's velocity.
// This should be called at the end of Tick(),
// if you are making custom entity AI.
//
// Once Tick() has exited, if needs_pos_update
// is true, Collide will be called. If the position
// is valid, then position packets will be sent.
func (b *Base) ApplyVelocity() {
  b.SetNewPosition(b.x + b.vx, b.y + b.vy, b.z + b.vz)
  b.needs_pos_update = true
}
// This will trigger collision detection next tick, and verify this position at that time.
// This should be called if you want to move an entity (not a player, call Teleport for players).
func (b *Base) SetNewPosition(x, y, z float64) {
  b.new_x = x
  b.new_y = y
  b.new_z = z
  b.needs_pos_update = true
}

// This sets the entity's velocity. This does
// not ensure that any packets will be sent,
// as ApplyVelocity() still must be called.
func (b *Base) SetVelocity(x, y, z int16) {
  b.vx = float64(x) / 8000
  b.vy = float64(y) / 8000
  b.vz = float64(z) / 8000
}

// Called when this entity punches other.
func (b *Base) Hit(other Entity) {
  b.SetVelocity(0, 3000, 0)
}

// Used internally once world collision is finished. Should never be called.
func (b *Base) Collide(info *CollisionInfo) {
  if info.DidCollide {
    // Collided with something, we want to clear velocity
    b.SetVelocity(0, 0, 0)
    b.x = info.NewX
    b.y = info.NewY
    b.z = info.NewZ
  } else {
    // Do a check here to see if needs_pos_update should be set
    b.x = b.new_x
    b.y = b.new_y
    b.z = b.new_z
  }
  b.needs_pos_update = true
}

// This does basic things, such as apply
// gravity to the velocity, and set needs_pos_update
// if the velocity is non zero. This should
// be called before anything else happens in a custom
// Tick() function.
func (b *Base) Tick() {
  b.tick++

  b.vy -= 0.08 // gravity
  b.vy *= 0.98 // air resistance (makes the entity have a max velocity)
  b.vx *= 0.6  // friction (ignoring ice)
  b.vz *= 0.6  // friction (ignoring ice)

  if math.Abs(b.vx) < 0.0001 && math.Abs(b.vy) < 0.0001 && math.Abs(b.vz) < 0.0001 {
    b.vx = 0
    b.vy = 0
    b.vz = 0
    b.needs_pos_update = false
  } else {
    b.needs_pos_update = true
  }
}

func (b *Base) GetLiving() (*Living, bool) {
  return nil, false
}

func (b *Base) Metadata() *metadata.Metadata {
  return b.metadata
}

// Returns the current time for the player. This is a counter that starts at 0 when they log in,
// and increases once every tick
func (b *Base) Time() int {
  return b.tick
}
