package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type exp_orb struct {
  Base
  amount int
}

type ender_pearl struct {
  projectile
  owner util.UUID
}

type eye_of_ender struct {
  projectile
  direction float32
}

func new_exp_orb(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  base := new_base(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*Base)
  e := exp_orb{}
  e.Base = *base
  e.amount = attr.Data
  return &e
}

func new_ender_pearl(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  projectile := new_projectile(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*projectile)
  e := eye_of_ender{}
  e.projectile = *projectile
  e.direction = 0
  return &e
}

func new_eye_of_ender(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  projectile := new_projectile(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*projectile)
  e := eye_of_ender{}
  e.projectile = *projectile
  e.direction = 0
  return &e
}
