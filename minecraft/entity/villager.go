package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/item"
)

type villager struct {
  LivingBase
  trades []*trade
}

type trade struct {
  ask []*item.Stack
  bid []*item.Stack
}

func new_villager(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  living := new_living(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*LivingBase)
  v := villager{}
  v.LivingBase = *living
  v.trades = []*trade{}
  return &v
}
