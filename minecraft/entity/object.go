package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type Object struct {
  Base
  data int
}

func new_object(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw byte,
vx, vy, vz int16) *Object {
  base := new_base(attr, etype, eid, uuid, x, y, z, pitch, yaw, 0, vx, vy, vz).(*Base)
  o := Object{}
  o.Base = *base
  o.data = attr.Data
  return &o
}
