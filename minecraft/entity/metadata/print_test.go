package metadata_test

import (
  "testing"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"

  "github.com/stretchr/testify/assert"
)

func add_basic_items() {
  item.ClearItems()
  item.RegisterItem(item.NewBasic("minecraft:air"))
  item.RegisterItem(item.NewBasic("minecraft:stone"))
  item.RegisterItem(item.NewBasic("minecraft:paper"))
}

func TestPrint(t *testing.T) {
  add_basic_items()
  val := metadata.NewEntityMetadata()
  val.Write(0, metadata.EntityType_Byte, byte(0))
  val.Write(1, metadata.EntityType_VarInt, 123)
  val.Write(2, metadata.EntityType_Float, 3.2)
  val.Write(3, metadata.EntityType_String, "Hello!")
  val.Write(4, metadata.EntityType_Chat, `{"text":"I am chat"}`)
  val.Write(5, metadata.EntityType_OptChat, nil)
  val.Write(6, metadata.EntityType_OptChat, "OptChat time")

  stack := item.NewItemStack(item.GetItem("minecraft:stone"), 10, nil)
  val.Write(7, metadata.EntityType_Item, stack)
  stack = item.NewItemStack(item.GetItem("minecraft:paper"), 10, nbt.NewCompoundTag("Testing"))
  val.Write(8, metadata.EntityType_Item, stack)

  val.Write(9, metadata.EntityType_Bool, false)
  val.Write(10, metadata.EntityType_Rotation, metadata.Rotation{5, 6, 180})
  val.Write(11, metadata.EntityType_Position, block.Pos{1, 2, 1234})
  val.Write(12, metadata.EntityType_OptPosition, nil)
  val.Write(13, metadata.EntityType_OptPosition, block.Pos{4, 5, 12356})
  val.Write(14, metadata.EntityType_Direction, metadata.Direction_Up)
  val.Write(15, metadata.EntityType_OptUUID, nil)
  val.Write(16, metadata.EntityType_OptUUID, util.NewUUID())
  val.Write(17, metadata.EntityType_BlockID, 123)

  tag := nbt.NewCompoundTag("Testing")
  tag.CompoundAddChild("string_test", nbt.TAG_String).WriteString("Hello!")
  tag.CompoundAddChild("int_gaming", nbt.TAG_Int).WriteInt(1234)
  val.Write(18, metadata.EntityType_NBT, tag)

  val.Write(19, metadata.EntityType_Particle, metadata.Particle{ID: 5, Data: []byte{1, 2, 3}})
  val.Write(20, metadata.EntityType_VillagerData, metadata.VillagerData{Type: 5, Profession: 3, Level: 9001})
  val.Write(21, metadata.EntityType_OptVarInt, 1234)
  val.Write(22, metadata.EntityType_Pose, metadata.Pose_Swimming)
  assert.Equal(t,
`0: Byte (id: 0) {
  0
},
1: VarInt (id: 1) {
  123
},
2: Float (id: 2) {
  3.2
},
3: String (id: 3) {
  Hello!
},
4: Chat (id: 4) {
  {"text":"I am chat"}
},
5: Optional Chat (Bool + Chat) (id: 5) {
  <nil>
},
6: Optional Chat (Bool + Chat) (id: 5) {
  OptChat time
},
7: Item (slot) (id: 6) {
  Item{name: minecraft:stone, id: 1, count: 10}
},
8: Item (slot) (id: 6) {
  Item{name: minecraft:paper, id: 2, count: 10, nbt:
  TAG_Compound('Testing') {
  }
}
},
9: Bool (id: 7) {
  false
},
10: Rotation (3 floats) (id: 8) {
  5.000000, 6.000000, 180.000000
},
11: Position (3 ints, encoded as a long) (id: 9) {
  1, 2, 1234, 0x40004d2002
},
12: Optional Position (Bool + Position) (id: 10) {
  <nil>
},
13: Optional Position (Bool + Position) (id: 10) {
  4, 5, 12356, 0x10003044005
},
14: Direction (id: 11) {
  Up (0)
},
15: Optional UUID (Bool + UUID) (id: 12) {
  <nil>
},
16: Optional UUID (Bool + UUID) (id: 12) {
  00000000-0000-0000-00000-00000000000
},
17: Block State (0 implies air/nothing, otherwise a value from the global pallete) (id: 13) {
  123
},
18: NBT (id: 14) {
  TAG_Compound('Testing') {
    TAG_String('string_test'): Hello!
    TAG_Int('int_gaming'): 1234
  }
},
19: Particle (id: 15) {
  Particle{ID: 5, Data: 0x010203}
},
20: Villager Data (id: 16) {
  VillagerData{Type: 5, Profession: 3, Level: 9001}
},
21: Optional VarInt (Just a VarInt, 0 implies absent) (id: 17) {
  1234
},
22: Pose (VarInt enum) (id: 18) {
  Swimming (3)
}
`,
    val.String(),
    "Printed value should show bool at the correct index")
}

// I would like to test serialize to packets here, but that is all handled in the proxy.
// Since the data format is so simple, the part that converts this golang map into a tcp packet
// is actually like 5 lines of code. This means I have just repeated it for all versions where
// the EntityMetadata packet is defined. This is the only packet that uses this data format,
// so I feel it is acceptable to just generate the metadata itself in the packet generation code.
