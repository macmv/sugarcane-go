package metadata

import (
  "fmt"
  "sort"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  pb "gitlab.com/macmv/sugarcane/proto"
)

// This is an entitiy metadata map. It matches one version of the game,
// and uses types that are equivalent to the newest version. These types
// are translated as best as possible in the proxy, but you will still run
// into issues. Therefore, you should use a entity.Metadata, which
// understands what type of entity you are talking about, and knows how
// to correctly set the fields for each version.
type Metadata struct {
  // List of all values stored
  values map[byte]metadata_field
  // List of all values changed since the last ToProto call
  changed_values map[byte]metadata_field
  // Used in the world module to detect if an entity metadata packet should be sent.
  dirty bool
}

type metadata_field struct {
  proto *pb.EntityMetadataField
  value interface{}
}

func New() *Metadata {
  m := Metadata{}
  m.values = make(map[byte]metadata_field)
  m.changed_values = make(map[byte]metadata_field)
  return &m
}

// Whenever this is called, it will mark the metadata as dirty.
func (m *Metadata) Write(index byte, mtype int32, value interface{}) {
  field := metadata_field{}
  field.proto = &pb.EntityMetadataField{}
  field.proto.Index = int32(index)
  field.proto.Type = mtype
  field.value = value
  buf := util.NewBuffer()
  switch mtype {
  case EntityType_Byte:
    buf.WriteByte(value.(byte))
  case EntityType_VarInt:
    switch v := value.(type) {
    case int32:
      field.value = v
      buf.WriteVarInt(v)
    case int64:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    case int:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    }
  case EntityType_Float:
    switch v := value.(type) {
    case float32:
      field.value = v
      buf.WriteFloat(v)
    case float64:
      field.value = float32(v)
      buf.WriteFloat(float32(v))
    }
  case EntityType_String:
    buf.WriteString(value.(string))
  case EntityType_Chat:
    buf.WriteString(value.(string))
  case EntityType_OptChat:
    if value == nil {
      buf.WriteBool(false)
    } else {
      buf.WriteBool(true)
      buf.WriteString(value.(string))
    }
  // TODO: Fix entity metadata items. Also, whyyyyyyyyyyyyyyyyyyyyyyyyyyyy minecraft
  // case EntityType_Item:
  //   buf.WriteItem(value.(*item.Stack).ToProto())
  case EntityType_Bool:
    buf.WriteBool(value.(bool))
  case EntityType_Rotation:
    p := value.(Rotation)
    buf.WriteFloat(p.X)
    buf.WriteFloat(p.Y)
    buf.WriteFloat(p.Z)
  case EntityType_Position:
    p := value.(block.Pos)
    buf.WritePosition(p)
  case EntityType_OptPosition:
    if value == nil {
      buf.WriteBool(false)
    } else {
      buf.WriteBool(true)
      buf.WritePosition(value.(block.Pos))
    }
  case EntityType_Direction:
    switch v := value.(type) {
    case int32:
      field.value = direction(v)
      buf.WriteVarInt(v)
    case int64:
      field.value = direction(v)
      buf.WriteVarInt(int32(v))
    case int:
      field.value = direction(v)
      buf.WriteVarInt(int32(v))
    }
  case EntityType_OptUUID:
    if value == nil {
      buf.WriteBool(false)
    } else {
      buf.WriteBool(true)
      buf.WriteUUID(value.(util.UUID))
    }
  case EntityType_BlockID:
    switch v := value.(type) {
    case int32:
      field.value = v
      buf.WriteVarInt(v)
    case int64:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    case int:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    }
  case EntityType_NBT:
    buf.WriteByteArray(value.(*nbt.Tag).Serialize())
  case EntityType_Particle:
    buf.WriteVarInt(value.(Particle).ID)
    buf.WriteByteArray(value.(Particle).Data)
  case EntityType_VillagerData:
    buf.WriteVarInt(value.(VillagerData).Type)
    buf.WriteVarInt(value.(VillagerData).Profession)
    buf.WriteVarInt(value.(VillagerData).Level)
  case EntityType_OptVarInt:
    switch v := value.(type) {
    case int32:
      field.value = v
      buf.WriteVarInt(v)
    case int64:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    case int:
      field.value = int32(v)
      buf.WriteVarInt(int32(v))
    }
  case EntityType_Pose:
    switch v := value.(type) {
    case int32:
      field.value = pose(v)
      buf.WriteVarInt(v)
    case int64:
      field.value = pose(v)
      buf.WriteVarInt(int32(v))
    case int:
      field.value = pose(v)
      buf.WriteVarInt(int32(v))
    }
  }
  field.proto.Data = buf.GetData()
  m.values[index] = field
  m.changed_values[index] = field
  m.dirty = true
}

// Gets the value at a given index.
//
// This will return true if the value is set, and false if it is not set.
// It is up to the user to figure out of casting is safe, as all fields are set with
// calls to Write, and the type of any field can be changed at any time.
// This type will never change the type of values, only the user can change those.
func (m *Metadata) Get(index byte) (interface{}, bool) {
  v, ok := m.values[index]
  if !ok {
    return nil, false
  }
  return v.value, true
}

// Gets the value at a given index. If the value is not set, then it will insert a default
// value into the metadata, with the given type, and return that. If the value is already set,
// no matter what the type is, it will return that other value.
//
// If it writes a value, it will mark the metadata as dirty
func (m *Metadata) GetType(index byte, mtype int32) interface{} {
  _, ok := m.values[index]
  if !ok {
    switch mtype {
    case EntityType_Byte:
      m.Write(index, mtype, byte(0))
    case EntityType_VarInt:
      m.Write(index, mtype, 0)
    case EntityType_Float:
      m.Write(index, mtype, 0)
    case EntityType_String:
      m.Write(index, mtype, "")
    case EntityType_Chat:
      m.Write(index, mtype, "")
    case EntityType_OptChat:
      m.Write(index, mtype, nil)
    case EntityType_Item:
      m.Write(index, mtype, nil)
    case EntityType_Bool:
      m.Write(index, mtype, false)
    case EntityType_Rotation:
      m.Write(index, mtype, Rotation{0, 0, 0})
    case EntityType_Position:
      m.Write(index, mtype, block.Pos{0, 0, 0})
    case EntityType_OptPosition:
      m.Write(index, mtype, nil)
    case EntityType_Direction:
      m.Write(index, mtype, Direction_Down)
    case EntityType_OptUUID:
      m.Write(index, mtype, nil)
    case EntityType_BlockID:
      m.Write(index, mtype, 0)
    case EntityType_NBT:
      m.Write(index, mtype, nil)
    case EntityType_Particle:
      m.Write(index, mtype, Particle{})
    case EntityType_VillagerData:
      m.Write(index, mtype, VillagerData{})
    case EntityType_OptVarInt:
      m.Write(index, mtype, 0)
    case EntityType_Pose:
      m.Write(index, mtype, Pose_Standing)
    }
  }
  val, _ := m.Get(index)
  return val
}

func (m *Metadata) String() string {
  out := ""
  keys := make([]byte, len(m.values))
  i := 0
  for key := range m.values {
    keys[i] = key
    i++
  }
  sort.Slice(keys, func (i, j int) bool { return keys[i] < keys[j] })
  for i, index := range keys {
    val := m.values[index]
    out += fmt.Sprintf("%d: %s (id: %d) {\n", index, string_name(val.proto.Type), val.proto.Type)
    switch val.value.(type) {
    case *nbt.Tag:
      out += val.value.(*nbt.Tag).StringIndent(2)
    case block.Pos:
      p := val.value.(block.Pos)
      out += fmt.Sprintf("  %d, %d, %d, 0x%x\n", p.X, p.Y, p.Z, p.AsLong())
    default:
      out += fmt.Sprintf("  %v\n", val.value)
    }
    out += "}"
    if i != len(keys) - 1 {
      out += ","
    }
    out += "\n"
  }
  return out
}

func (m *Metadata) ToProto() *pb.EntityMetadata {
  proto := pb.EntityMetadata{}
  proto.Values = make(map[int32]*pb.EntityMetadataField)
  for index, value := range m.changed_values {
    proto.Values[int32(index)] = value.proto
  }
  m.changed_values = make(map[byte]metadata_field)
  return &proto
}

// Gets whether or not the metadata is dirty
func (m *Metadata) Dirty() bool {
  return m.dirty
}

// Sets dirty to false, and returns the previous value
func (m *Metadata) ClearDirty() bool {
  prev := m.dirty
  m.dirty = false
  return prev
}
