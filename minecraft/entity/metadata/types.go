package metadata

import (
  "fmt"
)

const (
  // I am not using iota here, as these constants should be exactly what is defined as per the minecraft wiki.
  // If I used iota, it would be less clear what each individual value was.
  EntityType_Byte         = 0
  EntityType_VarInt       = 1
  EntityType_Float        = 2
  EntityType_String       = 3
  EntityType_Chat         = 4
  EntityType_OptChat      = 5
  EntityType_Item         = 6
  EntityType_Bool         = 7
  EntityType_Rotation     = 8 // 3 floats in a row
  EntityType_Position     = 9
  EntityType_OptPosition  = 10
  EntityType_Direction    = 11 // (Down = 0, Up = 1, North = 2, South = 3, West = 4, East = 5)
  EntityType_OptUUID      = 12
  EntityType_BlockID      = 13 // blockstate
  EntityType_NBT          = 14
  EntityType_Particle     = 15
  EntityType_VillagerData = 16 // 3 VarInts: villager type, villager profession, level
  EntityType_OptVarInt    = 17 // used for EID. 0 means absent, 1+ is an actual id
  EntityType_Pose         = 18 // 0: STANDING, 1: FALL_FLYING, 2: SLEEPING, 3: SWIMMING, 4: SPIN_ATTACK, 5: SNEAKING, 6: DYING

  Direction_Down          = 0
  Direction_Up            = 1
  Direction_North         = 2
  Direction_South         = 3
  Direction_West          = 4
  Direction_East          = 5

  Pose_Standing           = 0
  Pose_FallFlying         = 1
  Pose_Sleeping           = 2
  Pose_Swimming           = 3
  Pose_SpinAttack         = 4
  Pose_Sneaking           = 5
  Pose_Dying              = 6
)

func string_name(t int32) string {
  switch t {
    case EntityType_Byte:         return "Byte"
    case EntityType_VarInt:       return "VarInt"
    case EntityType_Float:        return "Float"
    case EntityType_String:       return "String"
    case EntityType_Chat:         return "Chat"
    case EntityType_OptChat:      return "Optional Chat (Bool + Chat)"
    case EntityType_Item:         return "Item (slot)"
    case EntityType_Bool:         return "Bool"
    case EntityType_Rotation:     return "Rotation (3 floats)"
    case EntityType_Position:     return "Position (3 ints, encoded as a long)"
    case EntityType_OptPosition:  return "Optional Position (Bool + Position)"
    case EntityType_Direction:    return "Direction"
    case EntityType_OptUUID:      return "Optional UUID (Bool + UUID)"
    case EntityType_BlockID:      return "Block State (0 implies air/nothing)"
    case EntityType_NBT:          return "NBT"
    case EntityType_Particle:     return "Particle"
    case EntityType_VillagerData: return "Villager Data"
    case EntityType_OptVarInt:    return "Optional VarInt (Just a VarInt, 0 implies absent)"
    case EntityType_Pose:         return "Pose (VarInt enum)"
  }
  return "Unknown type"
}

// func (p Position) AsLong() uint64 {
//   return ((uint64(p.X) & 0x3FFFFFF) << 38) | ((uint64(p.Z) & 0x3FFFFFF) << 12) | (uint64(p.Y) & 0xFFF)
// }

type Rotation struct {
  X, Y, Z float32
}

func (r Rotation) String() string {
  return fmt.Sprintf("%f, %f, %f", r.X, r.Y, r.Z)
}

type Particle struct {
  ID int32
  Data []byte
}

func (p Particle) String() string {
  out := fmt.Sprintf("Particle{ID: %d", p.ID)
  if p.Data != nil {
    out += fmt.Sprintf(", Data: 0x%x", p.Data)
  }
  return out + "}"
}

type direction int32

func (d direction) String() string {
  switch d {
    case Direction_Up:    return "Up (0)"
    case Direction_Down:  return "Down (1)"
    case Direction_North: return "North (2)"
    case Direction_South: return "South (3)"
    case Direction_East:  return "East (4)"
    case Direction_West:  return "West (5)"
  }
  return fmt.Sprintf("Unknown direction (%d)", d)
}

type VillagerData struct {
  Type, Profession, Level int32
}

func (v VillagerData) String() string {
  return fmt.Sprintf("VillagerData{Type: %d, Profession: %d, Level: %d}", v.Type, v.Profession, v.Level)
}

type pose int32

func (p pose) String() string {
  switch p {
    case Pose_Standing:   return "Standing (0)"
    case Pose_FallFlying: return "FallFlying (1)"
    case Pose_Sleeping:   return "Sleeping (2)"
    case Pose_Swimming:   return "Swimming (3)"
    case Pose_SpinAttack: return "SpinAttack (4)"
    case Pose_Sneaking:   return "Sneaking (5)"
    case Pose_Dying:      return "Dying (6)"
  }
  return fmt.Sprintf("Unknown pose (%d)", p)
}
