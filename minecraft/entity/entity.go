package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"
)

type Entity interface {
  // Used internally to see if the entity's position changed last tick
  NeedsPosUpdate() bool
  // Getter for position. Only updated after calling Tick()
  X() float64
  Y() float64
  Z() float64
  // Getter for next position. Calculated on the spot, and is the position + velocity
  NewX() float64
  NewY() float64
  NewZ() float64
  // Block getter. Finds the negative coordinate correctly.
  BlockPos() block.Pos

  // Hitbox size
  Width() float64
  Height() float64

  // Used internally for knockback, and entity velocity in general
  Vx() int16
  Vy() int16
  Vz() int16

  // Entity's id in the world. Is a counter that starts from 1
  EID() uint32

  // Used for knockback. This might also trigger collision next tick, because of the subsequent position change.
  // If you are clearing the velicty, then it will not cause collision to occur.
  SetVelocity(x, y, z int16)
  // Will set the position fields, and trigger collision detection on the next tick.
  // Will also send position packets if the move was valid. Do not call this for players! Call Teleport for players.
  SetNewPosition(x, y, z float64)

  // Called when this entity gets punched by another entity.
  Hit(other Entity)

  // Called whenever the entity hits a block. Can be overriden to make bouncy snowballs, for example.
  Collide(info *CollisionInfo)

  // This will get a pointer to a living struct, if this is a living entity.
  // Used for combat, as all living entities have health/armor
  GetLiving() (*Living, bool)

  // Called internally whenever a game tick occurs. Do not call this function, unless you know what you are doing.
  // The main thing that will happen when you call this is that the position will be updated to whatever the New position is,
  // and velocity will be applied. This will also trigger pathfinding (if there is any).
  // It will basically speed up the entity if you call this. So don't call it
  Tick()

  // This gets a pointer to the metadata for this entity. It can be edited with Write(),
  // and will be marked dirty each time you update it.
  Metadata() *metadata.Metadata

  // Returns the number of ticks since this entity spawned. Used internally for timers, and attack damage
  // calculations. This is safe to call.
  Time() int
}

func New(etype Type,
eid uint32,
uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16,
attr *Attributes) Entity {
  t, ok := entity_types[etype]
  if !ok {
    return nil
  }
  a := (*t.attr)
  if attr != nil {
    a = a.Merge(*attr)
  }
  ent := t.init(a,
  etype, eid, uuid,
  x, y, z,
  pitch, yaw, head_pitch,
  vx, vy, vz)
  return ent
}
