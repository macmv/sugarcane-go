package entity

type Attributes struct {
  CanFly bool
  Hostile bool
  Data int
  w, h float64
}

func (a Attributes) Merge(other Attributes) Attributes {
  if other.CanFly {
    a.CanFly = true
  }
  if other.Hostile {
    a.Hostile = true
  }
  if other.Data != 0 {
    a.Data = other.Data
  }
  return a
}
