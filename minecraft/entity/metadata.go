package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"
)

type StructuredEntityMetadata struct {
  // Versioned metadata. This is what is sent to the proxy.
  v1_8 *metadata.Metadata
  v1_12 *metadata.Metadata
  v1_15 *metadata.Metadata

  // The type that this metadata is referencing
  //t entity.Type
}
