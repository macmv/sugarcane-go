package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type sometimes_hostile struct {
  LivingBase
}

func new_sometimes_hostile(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  living := new_living(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*LivingBase)
  h := sometimes_hostile{}
  h.LivingBase = *living
  return &h
}
