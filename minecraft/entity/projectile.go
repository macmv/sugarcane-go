package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type projectile struct {
  Base
  speed float32
}

func new_projectile(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  base := new_base(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*Base)
  p := projectile{}
  p.Base = *base
  p.speed = 2
  return &p
}
