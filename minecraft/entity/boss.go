package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
)

type boss struct {
  LivingBase
}

func new_boss(attr Attributes,
etype Type, eid uint32, uuid util.UUID,
x, y, z float64,
pitch, yaw, head_pitch byte,
vx, vy, vz int16) Entity {
  living := new_living(attr, etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz).(*LivingBase)
  b := boss{}
  b.LivingBase = *living
  return &b
}
