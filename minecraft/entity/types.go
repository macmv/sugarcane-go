package entity

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type entity_constructor func(
  attr Attributes,
  etype Type, eid uint32, uuid util.UUID,
  x, y, z float64,
  pitch, yaw, head_pitch byte,
  vx, vy, vz int16,
) Entity

type entity_type struct {
  t Type
  init entity_constructor
  attr *Attributes
  // What version it was added in. block.V1_8 means it was added
  // in 1.8 or before.
  version block.Version
  // Used for entity metadata
  extends Type
}

var (
  // Object ids. Not sure what these are used for
  objects map[Type]int
  // Various metadata about each entity
  entity_types map[Type]*entity_type
)

// Entity type. Should be used by value. This understands what type it inherits from,
// and how to format entity metadata. It also knows what version it was added, and
// the versions that it's various fields were added. Whenever you need to reference an
// entity by type, you should use this type.
type Type int32

const (
  Type_AreaEffectCloud Type = iota
  Type_ArmorStand
  Type_Arrow
  Type_Bat
  Type_Bee
  Type_Blaze
  Type_Boat
  Type_Cat
  Type_CaveSpider
  Type_Chicken
  Type_Cod
  Type_Cow
  Type_Creeper
  Type_Donkey
  Type_Dolphin
  Type_DragonFireball
  Type_Drowned
  Type_ElderGuardian
  Type_EndCrystal
  Type_EnderDragon
  Type_Enderman
  Type_Endermite
  Type_EvokerFangs
  Type_Evoker
  Type_ExpOrb
  Type_EyeOfEnder
  Type_FallingBlock
  Type_FireworkRocket
  Type_Fox
  Type_Ghast
  Type_Giant
  Type_Guardian
  Type_Horse
  Type_Husk
  Type_Illusioner
  Type_Item
  Type_ItemFrame
  Type_Fireball
  Type_LeashKnot
  Type_Llama
  Type_LlamaSpit
  Type_MagmaCube
  Type_Minecart
  Type_ChestMinecart
  Type_CommandMinecart
  Type_FurnaceMinecart
  Type_HopperMinecart
  Type_SpawnerMinecart
  Type_TntMinecart
  Type_Mule
  Type_Mooshroom
  Type_Ocelot
  Type_Painting
  Type_Panda
  Type_Parrot
  Type_Pig
  Type_Pufferfish
  Type_ZombiePigman
  Type_PolarBear
  Type_PrimedTNT
  Type_Rabbit
  Type_Salmon
  Type_Sheep
  Type_Shulker
  Type_ShulkerBullet
  Type_Silverfish
  Type_Skeleton
  Type_SkeletonHorse
  Type_Slime
  Type_SmallFireball
  Type_SnowGolem
  Type_Snowball
  Type_SpectralArrow
  Type_Spider
  Type_Squid
  Type_Stray
  Type_TraderLlama
  Type_TropicalFish
  Type_Turtle
  Type_ThrownEgg
  Type_ThrownEnderPearl
  Type_ThrownExpBottle
  Type_ThrownPotion
  Type_Trident
  Type_Vex
  Type_Villager
  Type_IronGolem
  Type_Vindicator
  Type_Pillager
  Type_WanderingTrader
  Type_Witch
  Type_Wither
  Type_WitherSkeleton
  Type_WitherSkull
  Type_Wolf
  Type_Zombie
  Type_ZombieHorse
  Type_ZombieVillager
  Type_Phantom
  Type_Ravager
  Type_LightningBolt
  Type_Player
  Type_FishingBobber
)

func IsObject(etype Type) bool {
  _, ok := objects[etype]
  return ok
}

func get_entity_type(etype Type) (*entity_type, bool) {
  val, ok := entity_types[etype]
  return val, ok
}

func init() {
  objects = make(map[Type]int)
  // Used to check if an entity is an object. Value is never used.
  // Might be used in 1.16, keeping it here so that I don't need to rewrite
  objects[Type_Boat]             = 1
  objects[Type_Item]             = 2
  objects[Type_AreaEffectCloud]  = 3
  objects[Type_Minecart]         = 10
  objects[Type_PrimedTNT]        = 50
  objects[Type_EndCrystal]       = 51
  objects[Type_Arrow]            = 60
  objects[Type_Snowball]         = 61
  objects[Type_ThrownEgg]        = 62
  objects[Type_Fireball]         = 63
  objects[Type_SmallFireball]    = 64
  objects[Type_ThrownEnderPearl] = 65
  objects[Type_WitherSkull]      = 66
  objects[Type_ShulkerBullet]    = 67
  objects[Type_LlamaSpit]        = 68
  objects[Type_FallingBlock]     = 70
  objects[Type_ItemFrame]        = 71
  objects[Type_EyeOfEnder]       = 72
  objects[Type_ThrownPotion]     = 73
  objects[Type_ThrownExpBottle]  = 75
  objects[Type_FireworkRocket]   = 76
  objects[Type_LeashKnot]        = 77
  objects[Type_ArmorStand]       = 78
  objects[Type_EvokerFangs]      = 79
  objects[Type_FishingBobber]    = 90
  objects[Type_SpectralArrow]    = 91
  objects[Type_DragonFireball]   = 93
  objects[Type_Trident]          = 94

  entity_types = make(map[Type]*entity_type)
  c(Type_AreaEffectCloud,  new_base,              &Attributes{w: 0     , h: 0.5   , })
  c(Type_ArmorStand,       new_base,              &Attributes{w: 0.5   , h: 1.975 , })
  c(Type_Arrow,            new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  c(Type_Bat,              new_living,            &Attributes{w: 0.5   , h: 0.9   , CanFly: true})
  c(Type_Bee,              new_living,            &Attributes{w: 0.7   , h: 0.6   , CanFly: true})
  c(Type_Blaze,            new_living,            &Attributes{w: 0.6   , h: 1.8   , CanFly: true, Hostile: true})
  c(Type_Boat,             new_vehicle,           &Attributes{w: 1.375 , h: 0.5625, })
  c(Type_Cat,              new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  c(Type_CaveSpider,       new_living,            &Attributes{w: 0.7   , h: 0.5   , Hostile: true})
  c(Type_Chicken,          new_living,            &Attributes{w: 0.4   , h: 0.7   , })
  c(Type_Cod,              new_living,            &Attributes{w: 0.5   , h: 0.3   , })
  c(Type_Cow,              new_living,            &Attributes{w: 0.9   , h: 1.4   , })
  c(Type_Creeper,          new_living,            &Attributes{w: 0.6   , h: 1.7   , Hostile: true})
  c(Type_Donkey,           new_vehicle,           &Attributes{w: 1.5   , h: 1.3965, })
  c(Type_Dolphin,          new_living,            &Attributes{w: 0.9   , h: 0.6   , })
  c(Type_DragonFireball,   new_projectile,        &Attributes{w: 1.0   , h: 1.0   , })
  c(Type_Drowned,          new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_ElderGuardian,    new_living,            &Attributes{w: 1.9975, h: 1.9975, Hostile: true})
  c(Type_EndCrystal,       new_base,              &Attributes{w: 2.0   , h: 2.0   , })
  c(Type_EnderDragon,      new_boss,              &Attributes{w: 16.0  , h: 8.0   , Hostile: true})
  c(Type_Enderman,         new_sometimes_hostile, &Attributes{w: 0.6   , h: 2.9   , })
  c(Type_Endermite,        new_living,            &Attributes{w: 0.4   , h: 0.3   , Hostile: true})
  c(Type_EvokerFangs,      new_base,              &Attributes{w: 0.5   , h: 0.8   , })
  c(Type_Evoker,           new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_ExpOrb,           new_exp_orb,           &Attributes{w: 0.5   , h: 0.5   , })
  c(Type_EyeOfEnder,       new_eye_of_ender,      &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_FallingBlock,     new_base,              &Attributes{w: 0.98  , h: 0.98  , })
  c(Type_FireworkRocket,   new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_Fox,              new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  c(Type_Ghast,            new_living,            &Attributes{w: 4.0   , h: 4.0   , CanFly: true, Hostile: true})
  c(Type_Giant,            new_living,            &Attributes{w: 3.6   , h: 12.0  , Hostile: true})
  c(Type_Guardian,         new_living,            &Attributes{w: 0.85  , h: 0.85  , Hostile: true})
  // c(Type_Hoglin,           new_living,            &Attributes{w: 1.3965, h: 1.4   , Hostile: true})
  c(Type_Horse,            new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  c(Type_Husk,             new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Illusioner,       new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Item,             new_base,              &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_ItemFrame,        new_base,              &Attributes{w: 0.75  , h: 0.75  , })
  c(Type_Fireball,         new_projectile,        &Attributes{w: 1.0   , h: 1.0   , CanFly: true})
  c(Type_LeashKnot,        new_base,              &Attributes{w: 0.375 , h: 0.5   , })
  c(Type_Llama,            new_living,            &Attributes{w: 0.9   , h: 1.87  , })
  c(Type_LlamaSpit,        new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_MagmaCube,        new_living,            &Attributes{w: 0.51  , h: 0.51  , Hostile: true})
  c(Type_Minecart,         new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_ChestMinecart,    new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_CommandMinecart,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_FurnaceMinecart,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_HopperMinecart,   new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_SpawnerMinecart,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_TntMinecart,      new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  c(Type_Mule,             new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  c(Type_Mooshroom,        new_living,            &Attributes{w: 0.9   , h: 1.4   , })
  c(Type_Ocelot,           new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  c(Type_Painting,         new_base,              &Attributes{w: 0.0   , h: 0.0   , })
  c(Type_Panda,            new_living,            &Attributes{w: 1.3   , h: 1.25  , })
  c(Type_Parrot,           new_living,            &Attributes{w: 0.5   , h: 0.9   , })
  c(Type_Pig,              new_living,            &Attributes{w: 0.9   , h: 0.9   , })
  // c(Type_Piglin,           new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_PiglinBrute,      new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Pufferfish,       new_living,            &Attributes{w: 0.7   , h: 0.7   , })
  // Moved to zombified piglin in 1.16
  c(Type_ZombiePigman,     new_sometimes_hostile, &Attributes{w: 0.6   , h: 1.95  , })
  c(Type_PolarBear,        new_sometimes_hostile, &Attributes{w: 1.4   , h: 1.4   , })
  c(Type_PrimedTNT,        new_base,              &Attributes{w: 0.98  , h: 0.98  , })
  c(Type_Rabbit,           new_living,            &Attributes{w: 0.4   , h: 0.5   , })
  c(Type_Salmon,           new_living,            &Attributes{w: 0.7   , h: 0.4   , })
  c(Type_Sheep,            new_living,            &Attributes{w: 0.9   , h: 1.3   , })
  c(Type_Shulker,          new_living,            &Attributes{w: 1.0   , h: 1.0   , Hostile: true})
  c(Type_ShulkerBullet,    new_projectile,        &Attributes{w: 0.3125, h: 0.3125, })
  c(Type_Silverfish,       new_living,            &Attributes{w: 0.4   , h: 0.3   , Hostile: true})
  c(Type_Skeleton,         new_living,            &Attributes{w: 0.6   , h: 1.99  , Hostile: true})
  c(Type_SkeletonHorse,    new_vehicle,           &Attributes{w: 0.3965, h: 1.6   , })
  c(Type_Slime,            new_living,            &Attributes{w: 0.51  , h: 0.51  , Hostile: true})
  c(Type_SmallFireball,    new_projectile,        &Attributes{w: 0.3125, h: 0.3125, CanFly: true})
  c(Type_SnowGolem,        new_sometimes_hostile, &Attributes{w: 0.7   , h: 1.9   , })
  c(Type_Snowball,         new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_SpectralArrow,    new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  c(Type_Spider,           new_sometimes_hostile, &Attributes{w: 1.4   , h: 1.9   , })
  c(Type_Squid,            new_living,            &Attributes{w: 0.8   , h: 0.8   , })
  c(Type_Stray,            new_living,            &Attributes{w: 0.6   , h: 1.99  , Hostile: true})
  // c(Type_Strider,          new_living,            &Attributes{w: 0.9   , h: 1.7   , })
  c(Type_TraderLlama,      new_living,            &Attributes{w: 0.9   , h: 1.87  , })
  c(Type_TropicalFish,     new_living,            &Attributes{w: 0.5   , h: 0.4   , })
  c(Type_Turtle,           new_living,            &Attributes{w: 1.2   , h: 0.4   , })
  c(Type_ThrownEgg,        new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_ThrownEnderPearl, new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_ThrownExpBottle,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_ThrownPotion,     new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  c(Type_Trident,          new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  c(Type_Vex,              new_living,            &Attributes{w: 0.4   , h: 0.8   , CanFly: true, Hostile: true})
  c(Type_Villager,         new_villager,          &Attributes{w: 0.6   , h: 1.95  , })
  c(Type_IronGolem,        new_sometimes_hostile, &Attributes{w: 1.4   , h: 2.7   , })
  c(Type_Vindicator,       new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Pillager,         new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_WanderingTrader,  new_villager,          &Attributes{w: 0.6   , h: 1.95  , })
  c(Type_Witch,            new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Wither,           new_boss,              &Attributes{w: 0.9   , h: 3.5   , CanFly: true, Hostile: true})
  c(Type_WitherSkeleton,   new_living,            &Attributes{w: 0.7   , h: 2.4   , Hostile: true})
  c(Type_WitherSkull,      new_projectile,        &Attributes{w: 0.3125, h: 0.3125, CanFly: true})
  c(Type_Wolf,             new_sometimes_hostile, &Attributes{w: 0.6   , h: 0.85  , })
  // c(Type_Zoglin,           new_living,            &Attributes{w: 1.3965, h: 1.4   , Hostile: true})
  c(Type_Zombie,           new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_ZombieHorse,      new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  c(Type_ZombieVillager,   new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_ZombifiedPiglin,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  c(Type_Phantom,          new_living,            &Attributes{w: 0.9   , h: 1.5   , Hostile: true})
  c(Type_Ravager,          new_living,            &Attributes{w: 1.95  , h: 2.2   , Hostile: true})
  c(Type_LightningBolt,    new_base,              &Attributes{w: 0.0   , h: 0.0   , })
  c(Type_Player,           new_base,              &Attributes{w: 0.6   , h: 1.8   , })
  c(Type_FishingBobber,    new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_AreaEffectCloud,  Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0     , h: 0.5   , })
  // c(Type_ArmorStand,       Type_Living,           block.V1_8,  new_base,              &Attributes{w: 0.5   , h: 1.975 , })
  // c(Type_Arrow,            Type_AbstractArrow,    block.V1_8,  new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  // c(Type_Bat,              Type_Mob,              block.V1_8,  new_living,            &Attributes{w: 0.5   , h: 0.9   , CanFly: true})
  // c(Type_Bee,              Type_Animal,           block.V1_15, new_living,            &Attributes{w: 0.7   , h: 0.6   , CanFly: true})
  // c(Type_Blaze,            Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.8   , CanFly: true, Hostile: true})
  // c(Type_Boat,             Type_Entity,           block.V1_8,  new_vehicle,           &Attributes{w: 1.375 , h: 0.5625, })
  // c(Type_Cat,              Type_TameableAnimal,   block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  // c(Type_CaveSpider,       Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.7   , h: 0.5   , Hostile: true})
  // c(Type_Chicken,          Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.4   , h: 0.7   , })
  // c(Type_Cod,              Type_Fish,             block.V1_8,  new_living,            &Attributes{w: 0.5   , h: 0.3   , })
  // c(Type_Cow,              Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.4   , })
  // c(Type_Creeper,          Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.7   , Hostile: true})
  // c(Type_Donkey,           Type_ChestHorse,       block.V1_8,  new_vehicle,           &Attributes{w: 1.5   , h: 1.3965, })
  // c(Type_Dolphin,          Type_WaterAnimal,      block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 0.6   , })
  // c(Type_DragonFireball,   Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 1.0   , h: 1.0   , })
  // c(Type_Drowned,          Type_Zombie,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_ElderGuardian,    Type_Guardian,         block.V1_8,  new_living,            &Attributes{w: 1.9975, h: 1.9975, Hostile: true})
  // c(Type_EndCrystal,       Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 2.0   , h: 2.0   , })
  // c(Type_EnderDragon,      Type_Mob,              block.V1_8,  new_boss,              &Attributes{w: 16.0  , h: 8.0   , Hostile: true})
  // c(Type_Enderman,         Type_Monster,          block.V1_8,  new_sometimes_hostile, &Attributes{w: 0.6   , h: 2.9   , })
  // c(Type_Endermite,        Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.4   , h: 0.3   , Hostile: true})
  // c(Type_EvokerFangs,      Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.5   , h: 0.8   , })
  // c(Type_Evoker,           Type_SpellIllager,     block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_ExpOrb,           Type_Entity,           block.V1_8,  new_exp_orb,           &Attributes{w: 0.5   , h: 0.5   , })
  // c(Type_EyeOfEnder,       Type_Entity,           block.V1_8,  new_eye_of_ender,      &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_FallingBlock,     Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.98  , h: 0.98  , })
  // c(Type_FireworkRocket,   Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_Fox,              Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  // c(Type_Ghast,            Type_Flying,           block.V1_8,  new_living,            &Attributes{w: 4.0   , h: 4.0   , CanFly: true, Hostile: true})
  // c(Type_Giant,            Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 3.6   , h: 12.0  , Hostile: true})
  // c(Type_Guardian,         Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.85  , h: 0.85  , Hostile: true})
  // c(Type_Hoglin,           Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 1.3965, h: 1.4   , Hostile: true})
  // c(Type_Horse,            Type_AbstractHorse,    block.V1_8,  new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  // c(Type_Husk,             Type_Zombie,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Illusioner,       Type_SpellIllager,     block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Item,             Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_ItemFrame,        Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.75  , h: 0.75  , })
  // c(Type_Fireball,         Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 1.0   , h: 1.0   , CanFly: true})
  // c(Type_LeashKnot,        Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.375 , h: 0.5   , })
  // c(Type_Llama,            Type_ChestHorse,       block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.87  , })
  // c(Type_LlamaSpit,        Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_MagmaCube,        Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.51  , h: 0.51  , Hostile: true})
  // c(Type_Minecart,         Type_Entity,           block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_ChestMinecart,    Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_CommandMinecart,  Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_FurnaceMinecart,  Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_HopperMinecart,   Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_SpawnerMinecart,  Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_TntMinecart,      Type_Minecart,         block.V1_8,  new_vehicle,           &Attributes{w: 0.98  , h: 0.7   , })
  // c(Type_Mule,             Type_ChestHorse,       block.V1_8,  new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  // c(Type_Mooshroom,        Type_Cow,              block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.4   , })
  // c(Type_Ocelot,           Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 0.7   , })
  // c(Type_Painting,         Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.0   , h: 0.0   , })
  // c(Type_Panda,            Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 1.3   , h: 1.25  , })
  // c(Type_Parrot,           Type_TameableAnimal,   block.V1_8,  new_living,            &Attributes{w: 0.5   , h: 0.9   , })
  // c(Type_Pig,              Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 0.9   , })
  // c(Type_Piglin,           Type_AbstractPiglin,   block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_PiglinBrute,      Type_AbstractPiglin,   block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Pufferfish,       Type_Fish,             block.V1_8,  new_living,            &Attributes{w: 0.7   , h: 0.7   , })
  // // Moved to zombified piglin
  // // c(Type_ZombiePigman,     Type_None,             block.V1_8,  new_sometimes_hostile, &Attributes{w: 0.6   , h: 1.95  , })
  // c(Type_PolarBear,        Type_Animal,           block.V1_8,  new_sometimes_hostile, &Attributes{w: 1.4   , h: 1.4   , })
  // c(Type_PrimedTNT,        Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.98  , h: 0.98  , })
  // c(Type_Rabbit,           Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.4   , h: 0.5   , })
  // c(Type_Salmon,           Type_Fish,             block.V1_8,  new_living,            &Attributes{w: 0.7   , h: 0.4   , })
  // c(Type_Sheep,            Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.3   , })
  // c(Type_Shulker,          Type_Pathfinder,       block.V1_8,  new_living,            &Attributes{w: 1.0   , h: 1.0   , Hostile: true})
  // c(Type_ShulkerBullet,    Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.3125, h: 0.3125, })
  // c(Type_Silverfish,       Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.4   , h: 0.3   , Hostile: true})
  // c(Type_Skeleton,         Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.99  , Hostile: true})
  // c(Type_SkeletonHorse,    Type_AbstractHorse,    block.V1_8,  new_vehicle,           &Attributes{w: 0.3965, h: 1.6   , })
  // c(Type_Slime,            Type_Flying,           block.V1_8,  new_living,            &Attributes{w: 0.51  , h: 0.51  , Hostile: true})
  // c(Type_SmallFireball,    Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.3125, h: 0.3125, CanFly: true})
  // c(Type_SnowGolem,        Type_Pathfinder,       block.V1_8,  new_sometimes_hostile, &Attributes{w: 0.7   , h: 1.9   , })
  // c(Type_Snowball,         Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_SpectralArrow,    Type_AbstractArrow,    block.V1_8,  new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  // c(Type_Spider,           Type_Monster,          block.V1_8,  new_sometimes_hostile, &Attributes{w: 1.4   , h: 1.9   , })
  // c(Type_Squid,            Type_WaterAnimal,      block.V1_8,  new_living,            &Attributes{w: 0.8   , h: 0.8   , })
  // c(Type_Stray,            Type_Skeleton,         block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.99  , Hostile: true})
  // c(Type_Strider,          Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.7   , })
  // c(Type_TraderLlama,      Type_Llama,            block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.87  , })
  // c(Type_TropicalFish,     Type_Fish,             block.V1_8,  new_living,            &Attributes{w: 0.5   , h: 0.4   , })
  // c(Type_Turtle,           Type_Animal,           block.V1_8,  new_living,            &Attributes{w: 1.2   , h: 0.4   , })
  // c(Type_ThrownEgg,        Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_ThrownEnderPearl, Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_ThrownExpBottle,  Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_ThrownPotion,     Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
  // c(Type_Trident,          Type_AbstractArrow,    block.V1_8,  new_projectile,        &Attributes{w: 0.5   , h: 0.5   , })
  // c(Type_Vex,              Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.4   , h: 0.8   , CanFly: true, Hostile: true})
  // c(Type_Villager,         Type_AbstractVillager, block.V1_8,  new_villager,          &Attributes{w: 0.6   , h: 1.95  , })
  // c(Type_IronGolem,        Type_Pathfinder,       block.V1_8,  new_sometimes_hostile, &Attributes{w: 1.4   , h: 2.7   , })
  // c(Type_Vindicator,       Type_Raider,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Pillager,         Type_Raider,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_WanderingTrader,  Type_AbstractVillager, block.V1_8,  new_villager,          &Attributes{w: 0.6   , h: 1.95  , })
  // c(Type_Witch,            Type_Raider,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Wither,           Type_Monster,          block.V1_8,  new_boss,              &Attributes{w: 0.9   , h: 3.5   , CanFly: true, Hostile: true})
  // c(Type_WitherSkeleton,   Type_Skeleton,         block.V1_8,  new_living,            &Attributes{w: 0.7   , h: 2.4   , Hostile: true})
  // c(Type_WitherSkull,      Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.3125, h: 0.3125, CanFly: true})
  // c(Type_Wolf,             Type_TameableAnimal,   block.V1_8,  new_sometimes_hostile, &Attributes{w: 0.6   , h: 0.85  , })
  // c(Type_Zoglin,           Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 1.3965, h: 1.4   , Hostile: true})
  // c(Type_Zombie,           Type_Monster,          block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_ZombieHorse,      Type_AbstractHorse,    block.V1_8,  new_vehicle,           &Attributes{w: 1.3965, h: 1.6   , })
  // c(Type_ZombieVillager,   Type_Zombie,           block.V1_8,  new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_ZombifiedPiglin,  Type_Zombie,           block.V1_16, new_living,            &Attributes{w: 0.6   , h: 1.95  , Hostile: true})
  // c(Type_Phantom,          Type_Flying,           block.V1_8,  new_living,            &Attributes{w: 0.9   , h: 1.5   , Hostile: true})
  // c(Type_Ravager,          Type_Raider,           block.V1_8,  new_living,            &Attributes{w: 1.95  , h: 2.2   , Hostile: true})
  // c(Type_LightningBolt,    Type_Entity,           block.V1_8,  new_base,              &Attributes{w: 0.0   , h: 0.0   , })
  // c(Type_Player,           Type_Living,           block.V1_8,  new_base,              &Attributes{w: 0.6   , h: 1.8   , })
  // c(Type_FishingBobber,    Type_Entity,           block.V1_8,  new_projectile,        &Attributes{w: 0.25  , h: 0.25  , })
}

// This creates an entity type
func c(t Type, constructor entity_constructor, attr *Attributes) *entity_type {
  return &entity_type{
    t: t,
    // extends: extends,
    // version: version,
    init: constructor,
    attr: attr,
  }
}
