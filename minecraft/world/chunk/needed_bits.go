package  chunk

// This is log2(id) + 1, but with a min return value of 4, and a max return
// value of 9 (inclusive). Paletted chunks with 9 or more bits use the global
// palette, so we need to know if that's the case. Any values less that 4 will
// always go to 4, so that's why it's the minimum value.
//
// After benchmarking, this ends up removing around 0.2s off of the initial world
// generation, because we aren't calling math.Log2(). At the time of writing, this
// is a 30% speed increase.
func needed_bits(id uint32) int32 {
  if id < 16 {
    return 4
  } else if id < 32 {
    return 5
  } else if id < 64 {
    return 6
  } else if id < 128 {
    return 7
  } else if id < 256 {
    return 8
  } else {
    return 9
  }
}
