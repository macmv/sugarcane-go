package chunk

import (
  "io/ioutil"

  "google.golang.org/protobuf/proto"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func (c *MultiChunk) SaveToDisk(path string) {
  log.Info("Saving chunk at " + path)
  chunk_proto := c.Serialize(block.VLATEST)
  data, err := proto.Marshal(chunk_proto)
  if err != nil {
    log.Error(err)
  }
  err = ioutil.WriteFile(path, data, 0644)
  if err != nil {
    log.Error(err)
  }
}

func (c *MultiChunk) SaveToS3(bucket, key string, uploader *s3manager.Uploader) {
  log.Info("Saving to S3 not implemented!")
}

// func (c *Chunk) SaveToS3(bucket, key string, uploader *s3manager.Uploader) {
//   data := c.serialize()
//   uploader.Upload(&s3manager.UploadInput{
//     Bucket: &bucket,
//     Key:    &key,
//     Body:   bytes.NewReader(data),
//   })
// }
