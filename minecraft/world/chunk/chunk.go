package chunk

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type Chunk struct {
  // 1.17 why do you make this so hard!!!
  sections [16]Section
  // This creates an empty check section
  constructor func(v block.Version) Section
  // Block version to use
  version block.Version
}

// Creates an empty chunk. Constructor is a function pointer, which should
// create an empty chunk.
func NewChunk(version block.Version, constructor func(v block.Version) Section) *Chunk {
  return &Chunk{
    constructor: constructor,
    version: version,
  }
}

func (c *Chunk) HasChunkSection(y int32) bool {
  if y < 0 || y >= 16 {
    return false
  }
  return c.sections[y] != nil
}

func (c *Chunk) GetChunkSection(y int32) Section {
  if !c.HasChunkSection(y) {
    c.sections[y] = c.constructor(c.version)
  }
  return c.sections[y]
}

func (c *Chunk) GetBlock(pos block.Pos) *block.Type {
  section_y := pos.ChunkY()
  if !c.HasChunkSection(section_y) {
    return block.Air()
  }
  section := c.GetChunkSection(section_y)
  pos.Y = pos.ChunkRelY()
  return section.GetBlock(pos)
}

func (c *Chunk) SetBlock(pos block.Pos, t *block.Type) {
  section_y := pos.ChunkY()
  if !c.HasChunkSection(section_y) && t == block.Air() {
    return
  }
  section := c.GetChunkSection(section_y)
  pos.Y = pos.ChunkRelY()
  section.SetBlock(pos, t)
  if section.IsAll(block.Air()) {
    c.sections[section_y] = nil
  }
}

func (c *Chunk) IsAll(t *block.Type) bool {
  for y := 0; y < 16; y++ {
    if c.sections[y] != nil {
      if !c.sections[y].IsAll(t) {
        return false
      }
    }
  }
  return true
}

func (c *Chunk) Fill(min, max block.Pos, t *block.Type) {
  for y := min.ChunkY(); y <= max.ChunkY(); y++ {
    section := c.GetChunkSection(y)

    smin := min
    if min.ChunkY() == y {
      // If we are on the first section,
      // we want to start from the correct position
      smin.Y = min.ChunkRelY()
    } else {
      // Otherwise, we start from the bottom of the section
      smin.Y = 0
    }
    smax := max
    if max.ChunkY() == y {
      // If we are on the last section,
      // we only want to go up to the top of the fill
      smax.Y = max.ChunkRelY()
    } else {
      // Otherwise, we go to the top of the chunk
      smax.Y = 15
    }

    section.Fill(smin, smax, t)
  }
}

func (c *Chunk) Serialize() *pb.Chunk {
  proto := &pb.Chunk{}
  // we don't set X and Z here, as the chunk doesn't know it's coordinates

  proto.Sections = make(map[int32]*pb.Chunk_Section)
  for y := int32(0); y < 16; y++ {
    if c.HasChunkSection(y) {
      proto.Sections[y] = c.GetChunkSection(y).Serialize()
    }
  }

  heightmap_data := make([]uint64, 36)
  offset := 0
  index := 0
  min_block := 16
  for i := 0; i < 256; i++ {
    heightmap_data[index] |= (uint64(min_block) << offset)
    offset += 9 // 9 bits per entry
    if offset > 64 {
      offset -= 64
      index += 1
      heightmap_data[index] |= (uint64(min_block) << offset)
    }
  }
  proto.Heightmap = heightmap_data

  return proto
}

// Only paletted chunks can be converted from
func (c *Chunk) FromProto(proto *pb.Chunk) bool {
  for y, section_proto := range proto.Sections {
    // Will set it if it is not a chunk
    c.GetChunkSection(y)
    // Need this, as c.GetChunkSection() returns an interface
    paletted, ok := c.sections[y].(*paletted_section)
    if !ok {
      return false
    }
    paletted.deserialize(section_proto)
  }
  return true
}

func (c *Chunk) CopyFrom(o *Chunk) {
  c.sections = [16]Section{}
  for y := int32(0); y < 16; y++ {
    if o.HasChunkSection(y) {
      section := c.sections[y]
      if section == nil {
        section = c.constructor(c.version)
        c.sections[y] = section
      }
      c.sections[y].CopyFrom(o.GetChunkSection(y))
    }
  }
}
