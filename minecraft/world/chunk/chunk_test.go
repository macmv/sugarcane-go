package chunk

import (
  "testing"
  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func init() {
  // Init
  reg := block.CreateRegistry()

  // Block 0
  kind := block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 0 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:block_0", kind)

  // Block 1
  kind = block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 1 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:stone", kind)

  // Block 2
  kind = block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 2 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:dirt", kind)

  // Finalize
  block.LoadBlocks()
}

func TestChunkSetters(t *testing.T) {
  test := func(c *Chunk) {
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, false, c.HasChunkSection(i), "No chunk sections should exist when first created")
    }

    c.SetBlock(block.Pos{0, 0, 0}, block.GetKind("minecraft:block_0").DefaultType())
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, false, c.HasChunkSection(i), "No chunk sections should exist when an empty section is set to air")
    }

    c.SetBlock(block.Pos{0, 0, 0}, block.GetKind("minecraft:stone").DefaultType())
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, i == 0, c.HasChunkSection(i), "The first chunk section should be generated once a block is placed there")
    }

    c.SetBlock(block.Pos{0, 0, 0}, block.GetKind("minecraft:block_0").DefaultType())
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, false, c.HasChunkSection(i), "The first chunk section should be removed after is set to air")
    }
  }

  // Make sure each version is working properly
  test(NewChunk(0, new_fixed_section))
  test(NewChunk(0, new_paletted_chunk))
}

func TestChunkSectionGetters(t *testing.T) {
  test := func(c *Chunk) {
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, false, c.HasChunkSection(i), "No chunk sections should exist when first created")
    }

    assert.Equal(t, block.Air(), c.GetBlock(block.Pos{0, 0, 0}), "Getting blocks in empty sections should return air")
    for i := int32(0); i < 16; i++ {
      assert.Equal(t, false, c.HasChunkSection(i), "No chunk sections should exist when an empty section is set to air")
    }

    c.SetBlock(block.Pos{0, 0, 0}, block.GetKind("minecraft:stone").DefaultType())
    assert.Equal(t, block.GetKind("minecraft:stone").DefaultType(), c.GetBlock(block.Pos{0, 0, 0}), "Getting blocks after they are set should work")
  }

  // Make sure each version is working properly
  test(NewChunk(0, new_fixed_section))
  test(NewChunk(0, new_paletted_chunk))
}
