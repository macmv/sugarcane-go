package chunk

import (
  "encoding/binary"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type fixed_section struct {
  version block.Version

  data []uint16
}

func new_fixed_section(v block.Version) Section {
  s := fixed_section{}
  s.data = make([]uint16, 16 * 16 * 16)
  s.version = v
  return &s
}

// VERY SLOW. Call this on a palleted (1.13+) chunk if possible.
func (s *fixed_section) IsAll(t *block.Type) bool {
  id := uint16(t.StateVersion(s.version))
  for x := int32(0); x < 16; x++ {
    for y := int32(0); y < 16; y++ {
      for z := int32(0); z < 16; z++ {
        if s.get_block_id(block.Pos{x, y, z}) != id {
          return false
        }
      }
    }
  }
  return true
}

func (s *fixed_section) GetBlock(pos block.Pos) *block.Type {
  return block.TypeFromIDVersion(uint32(s.get_block_id(pos)), block.V1_8)
}

func (s *fixed_section) get_block_id(p block.Pos) uint16 {
  return s.data[p.Y * 16 * 16 + p.Z * 16 + p.X]
}

func (s *fixed_section) SetBlock(p block.Pos, t *block.Type) {
  s.data[p.Y * 16 * 16 + p.Z * 16 + p.X] = uint16(t.StateVersion(block.V1_8))
}

func (s *fixed_section) Fill(min, max block.Pos, t *block.Type) {
  for x := min.X; x <= max.X; x++ {
    for y := min.Y; y <= max.Y; y++ {
      for z := min.Z; z <= max.Z; z++ {
        s.data[y * 16 * 16 + z * 16 + x] = uint16(t.StateVersion(block.V1_8))
      }
    }
  }
}

// Caching the chunks is way too memory intensive. Since the chunk data
// is so easy to encode, it is worthwhile to generate it every time.
func (s *fixed_section) Serialize() *pb.Chunk_Section {
  proto := &pb.Chunk_Section{}

  // proto.Data is a byte array, while s.block_data is a uint16 array
  proto.Data = make([]byte, len(s.data) * 2)
  // If this is too slow, we can always use unsafe.Pointer to convert
  // and then copy all of the data.
  for i := 0; i < len(s.data); i++ {
    // little endian becuase 1.8 is weird and inconsistent
    binary.LittleEndian.PutUint16(proto.Data[i*2:], s.data[i])
  }

  return proto
}

func (s *fixed_section) CopyFrom(o Section) {
  for y := int32(0); y < 16; y++ {
    for z := int32(0); z < 16; z++ {
      for x := int32(0); x < 16; x++ {
        pos := block.Pos{x, y, z}
        s.SetBlock(pos, o.GetBlock(pos))
      }
    }
  }
}
