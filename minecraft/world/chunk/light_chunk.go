package chunk

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

// Is a chunk of lighting data.
// If the world has skylight, two
// light chunks will need to be made.
type LightChunk struct {
  // The chunk that this light
  // chunk is within. Used to
  // find walls within the chunk.
  c *Chunk
  // Set to true to make light
  // propogate downwards, instead
  // of from light sources.
  sky bool
  // Light sections. Must not be
  // a fixed length array, because
  // 1.17 exists.
  sections []*LightSection
}

// A 16x16x16 area of light data.
// For worlds with skylight, two
// light sections must be created
// for every chunk section.
type LightSection struct {
  // The chunk this section is in.
  c *Chunk
  // Set to true if this light section
  // should propogate light downwards,
  // instead of from blocks
  sky bool
  // 16x16x16 / 2 bytes,
  // because each light
  // value is 1/2 byte.
  data []byte
}

type LightUpdate struct {
  // Position of the light update. Can
  // be relative or global, depending on
  // where this light update came from.
  Pos block.Pos
  // The light level that pos should be
  // at.
  Amount byte
}

// Creates a new light chunk. This chunk does not
// store block data, only lighting data. If you
// set sky to true, then this chunk will not emit
// light from light sources, instead it will emit
// light from the sky downwards. c should be a latest
// version chunk, which is the chunk that this light
// chunk is on. c will be used to see where walls
// are, in order to block light.
func NewLightChunk(c *Chunk, sky bool) *LightChunk {
  return &LightChunk{
    c: c,
    sky: sky,
  }
}

// Creates a new light section. This is a 16x16x16
// area of light data.
func NewLightSection(c *Chunk, sky bool) *LightSection {
  return &LightSection{
    c: c,
    sky: sky,
  }
}

// Adds a section at the given y position. This is called
// from MultiChunk automatically. Only call this function
// if you know what you are doing. If a light section
// already exists at y, this is a nop.
func (l *LightChunk) AddSection(y int32) {
  if y >= int32(len(l.sections)) {
    for i := int32(len(l.sections)); i < y; i++ {
      l.sections = append(l.sections, nil)
    }
  }
  if l.sections[y] == nil {
    l.sections[y] = NewLightSection(l.c, l.sky)
  }
}

// Gets a light section given then y index. y should be
// a block y position / 16.
func (l *LightChunk) GetSection(y int32) *LightSection {
  l.AddSection(y)
  return l.sections[y]
}

// This is a nop for sky light chunks. Pos should be relative to
// the chunk origin. If pos is outside of the chunk, then light
// will still be propogated, as if the light were in the given
// position relative to the chunk.
func (l *LightChunk) AddLightSource(pos block.Pos, light byte) {
  if l.sky {
    return
  }
  rel := pos.ChunkRelPos()
  edges := l.GetSection(pos.Y / 16).AddLightSource(rel, light)
  for _, update := range edges {
    l.GetSection(update.Pos.Y / 16).AddLightSource(update.Pos, update.Amount)
  }
}

// Adds light coming from pos with amount intensity. This is
// a nop for sky chunk sections. Pos must be within the zero chunk column.
func (l *LightSection) AddLightSource(pos block.Pos, amount byte) []LightUpdate {
  if l.sky {
    return nil
  }
  edges := []LightUpdate{}
  queue := []LightUpdate{
    {
      Pos: pos,
      Amount: amount,
    },
  }
  for len(queue) != 0 {
    // Pop
    item := queue[len(queue) - 1]
    queue = queue[:len(queue) - 1]
    if item.Pos.X > 15 || item.Pos.X < 0 || item.Pos.Z > 15 || item.Pos.Z < 0 {
      edges = append(edges, item)
      continue
    }
    t := l.c.GetBlock(item.Pos)
    if l.light_at(item.Pos) > item.Amount || !t.Transparent() {
      continue
    }
    l.set_light(pos.WithY(pos.ChunkRelY()), item.Amount)
    east  := item.Pos.AddX(1)
    west  := item.Pos.AddX(-1)
    up    := item.Pos.AddY(1)
    down  := item.Pos.AddY(-1)
    south := item.Pos.AddZ(1)
    north := item.Pos.AddZ(-1)
    item.Amount--
    queue = append(queue, LightUpdate{Pos: east,  Amount: item.Amount})
    queue = append(queue, LightUpdate{Pos: west,  Amount: item.Amount})
    queue = append(queue, LightUpdate{Pos: up,    Amount: item.Amount})
    queue = append(queue, LightUpdate{Pos: down,  Amount: item.Amount})
    queue = append(queue, LightUpdate{Pos: south, Amount: item.Amount})
    queue = append(queue, LightUpdate{Pos: north, Amount: item.Amount})
  }
  return edges
}

func (l *LightSection) light_at(pos block.Pos) byte {
  return l.data[pos.X / 2 + pos.Z * 8 + pos.Y * 16 * 8] << (pos.X % 2 * 4) & 0xf
}

func (l *LightSection) set_light(pos block.Pos, amount byte) {
  l.data[pos.X / 2 + pos.Z * 8 + pos.Y * 16 * 8] = amount << (pos.X % 2 * 4) & 0xf
}
