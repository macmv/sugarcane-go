package chunk

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// General representation of a chunk section. Chunks use these internally,
// so that the versioning is all up to the chunk section.
type Section interface {
  // pos.Y is relative to this section (must be within 0-15)
  // This function is not expected to check that.
  GetBlock(pos block.Pos) *block.Type
  // pos.Y is relative to this section (must be within 0-15)
  // This function is not expected to check that.
  SetBlock(pos block.Pos, t *block.Type)
  // Both positions are relative to the chunk (including y). If max is larger than min,
  // you can expect this function to do nothing/fail in undefined ways.
  Fill(min, max block.Pos, t *block.Type)

  // Used most commonly to check if the chunk is all air
  IsAll(t *block.Type) bool

  // Generates a protobuf that stores all the data in this
  // chunk section.
  Serialize() *pb.Chunk_Section

  // Should overwrite all data with the data of other.
  CopyFrom(other Section)
}
