package chunk

import (
  "testing"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func BenchmarkSectionSetID(b *testing.B) {
  b.Run("Add a new block into the chunk", func (b *testing.B) {
    section := new_paletted_chunk(0).(*paletted_section)
    for i := 0; i < b.N; i++ {
      section.set_block_id(block.Pos{0, 0, 0}, uint32(i % 2))
    }
  })
}

func TestNewChunkSection(t *testing.T) {
  section := new_paletted_chunk(0).(*paletted_section)

  assert.Equal(t, int32(4), section.bits_per_block,        "newChunkSection should start 4 bits_per_block")
  assert.Equal(t, 1, len(section.palette_to_global),       "newChunkSection should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0], "newChunkSection should write to the data in the palette to global map")
  assert.Equal(t, 1, len(section.global_to_palette),       "newChunkSection should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0], "newChunkSection should write to the data in the global to palette map")
}

func TestUpdateBitsPerBlock(t *testing.T) {
  section := new_paletted_chunk(0).(*paletted_section)

  // Should set s.bits_per_block to 5, and increase the size of block_data
  section.update_bits_per_block(5)
  assert.Equal(t, int32(5), section.bits_per_block,               "updateBitsPerBlock should set bits_per_block")
  assert.Equal(t, 16 * 16 * 16 * 5 / 64, len(section.block_data), "updateBitsPerBlock should set the correct length of block_data")

  section.update_bits_per_block(4)
  assert.Equal(t, int32(4), section.bits_per_block,               "updateBitsPerBlock should set bits_per_block")
  assert.Equal(t, 16 * 16 * 16 * 4 / 64, len(section.block_data), "updateBitsPerBlock should set the correct length of block_data")

  section.block_data[0] = 1
  section.block_data[16] = 1
  section.update_bits_per_block(5)
  assert.Equal(t, int32(5), section.bits_per_block,               "updateBitsPerBlock should set bits_per_block")
  assert.Equal(t, 16 * 16 * 16 * 5 / 64, len(section.block_data), "updateBitsPerBlock should set the correct length of block_data")
  assert.Equal(t, uint64(1), section.block_data[0],               "updateBitsPerBlock should copy block_data correctly")
  assert.Equal(t, uint64(0), section.block_data[16],              "updateBitsPerBlock should copy block_data correctly")
  assert.Equal(t, uint64(1), section.block_data[20],              "updateBitsPerBlock should copy block_data correctly")
}

func TestSetPaletteID(t *testing.T) {
  section := new_paletted_chunk(0).(*paletted_section)

  // Should set the value in the chunk to 11
  section.set_palette_id(block.Pos{0, 0, 0}, 11)
  assert.Equal(t, uint64(11), section.block_data[0])

  // 20 is outside of 4 bits per block, so this should return an error
  err := section.set_palette_id(block.Pos{0, 0, 0}, 20)
  assert.Equal(t, int32(4), section.bits_per_block, "set_palette_id should not change the bits per block")
  assert.NotEqual(t, nil, err, "set_palette_id should return an error if we pass a value too large")

  // NOT A VALID TEST
  // Set palette id should do nothing except edit the block data.
  // It should not modify the palette or change the bits per block.
  // section.set_palette_id(0, 0, 0, 20)
  // assert.Equal(t, int32(5), section.bits_per_block, "set_palette_id should set the bits_per_block to 5, as 20 does not fit into 4 bits")

  // section.set_palette_id(0, 0, 0, 0)
  // assert.Equal(t, int32(4), section.bits_per_block, "set_palette_id should reduce the bits_per_block to 4, as now all of the blocks fit into 4")
}

func TestSetBlockID(t *testing.T) {
  section := new_paletted_chunk(0).(*paletted_section)

  // Should add an item to the pallete, and give it the id 1
  section.set_block_id(block.Pos{0, 0, 0}, 123)
  assert.Equal(t, uint64(1), section.block_data[0],          "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, 2, len(section.palette_to_global),         "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],   "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(123), section.palette_to_global[1], "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, 2, len(section.global_to_palette),         "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],   "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(1), section.global_to_palette[123], "SetBlockID should write to the data in the global to palette map")
  assert.True(t, section.validate(),                         "SetBlockID should update block amounts correctly")

  // Should remove the item from the palette
  section.set_block_id(block.Pos{0, 0, 0}, 0)
  assert.Equal(t, uint64(0), section.block_data[0],         "SetBlockID should remove the data in the chunk buffer")
  assert.Equal(t, 1, len(section.palette_to_global),        "SetBlockID should remove the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],  "SetBlockID should remove the data in the palette to global map")
  assert.Equal(t, 1, len(section.global_to_palette),        "SetBlockID should remove the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],  "SetBlockID should remove the data in the global to palette map")
  assert.True(t, section.validate(),                        "SetBlockID should update block amounts correctly")

  // Should create and set an item in the palette
  section.set_block_id(block.Pos{0, 0, 0}, 981)
  // Should remove the item 981, and add item 123 to the palette. The block_data should not change at all
  section.set_block_id(block.Pos{0, 0, 0}, 123)
  assert.Equal(t, uint64(1), section.block_data[0],          "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, 2, len(section.palette_to_global),         "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],   "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(123), section.palette_to_global[1], "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, 2, len(section.global_to_palette),         "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],   "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(1), section.global_to_palette[123], "SetBlockID should write to the data in the global to palette map")
  assert.True(t, section.validate(),                         "SetBlockID should update block amounts correctly")

  // Remove old data in the chunk
  section.set_block_id(block.Pos{0, 0, 0}, 0)
  // Should create and set an item in the palette
  section.set_block_id(block.Pos{0, 1, 0}, 981)
  // Should realize that 123 < 981, and insert this item into the palette.
  // It should then recreate the block_data, so that it matches this new palette
  section.set_block_id(block.Pos{0, 0, 0}, 123)
  assert.Equal(t, uint64(1), section.block_data[0],          "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, uint64(2), section.block_data[16],         "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, 3, len(section.palette_to_global),         "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],   "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(123), section.palette_to_global[1], "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(981), section.palette_to_global[2], "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, 3, len(section.global_to_palette),         "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],   "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(1), section.global_to_palette[123], "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(2), section.global_to_palette[981], "SetBlockID should write to the data in the global to palette map")
  assert.True(t, section.validate(),                         "SetBlockID should update block amounts correctly")

  // This should force the section to remove the item in the middle of the palette
  // This will make all of the blocks with an id > 123 be shifted down by one
  section.set_block_id(block.Pos{0, 0, 0}, 0)
  // fmt.Println(section.palette_to_global)
  // fmt.Printf("%x\n", section.block_data)
  assert.Equal(t, uint64(0), section.block_data[0],          "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, uint64(1), section.block_data[16],         "SetBlockID should write to the data in the chunk buffer")
  assert.Equal(t, 2, len(section.palette_to_global),         "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],   "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, uint32(981), section.palette_to_global[1], "SetBlockID should write to the data in the palette to global map")
  assert.Equal(t, 2, len(section.global_to_palette),         "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],   "SetBlockID should write to the data in the global to palette map")
  assert.Equal(t, uint32(1), section.global_to_palette[981], "SetBlockID should write to the data in the global to palette map")
  assert.True(t, section.validate(),                         "SetBlockID should update block amounts correctly")
}

func TestFill(t *testing.T) {
  section := new_paletted_chunk(0).(*paletted_section)

  section.fill_id(block.Pos{0, 0, 0}, block.Pos{2, 2, 2}, 123)

  assert.Equal(t, 2, len(section.palette_to_global),         "Fill should write to the data in the palette to global map")
  assert.Equal(t, uint32(0), section.palette_to_global[0],   "Fill should write to the data in the palette to global map")
  assert.Equal(t, uint32(123), section.palette_to_global[1], "Fill should write to the data in the palette to global map")
  assert.Equal(t, 2, len(section.global_to_palette),         "Fill should write to the data in the global to palette map")
  assert.Equal(t, uint32(0), section.global_to_palette[0],   "Fill should write to the data in the global to palette map")
  assert.Equal(t, uint32(1), section.global_to_palette[123], "Fill should write to the data in the global to palette map")
  assert.True(t, section.validate(),                         "Fill should update block amounts correctly")
}
