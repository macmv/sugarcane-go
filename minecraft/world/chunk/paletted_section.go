package chunk

import (
  "fmt"
  "errors"
  "encoding/binary"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type paletted_section struct {
  // Global ids to pallete ids.
  global_to_palette map[uint32]uint32
  // Ordered list of global ids.
  palette_to_global []uint32
  // Ordered list of amount of each block.
  block_amounts []int32
  // Version that this chunk is being used on
  version block.Version

  bits_per_block int32
  // This is a number which is just s.bits_per_block 1s. Used as a mask in get_palette_id
  bitmask uint32
  // Actual block data within the chunk
  block_data []uint64
  // If the palette is enabled right now
  using_palette bool

  cached_proto *pb.Chunk_Section
  // This is true whenever anything in the chunk section changes. This is used so that we don't
  // regenerate the protobuf every time it needs to be sent (helps with static maps)
  proto_dirty bool
}

func new_paletted_chunk(v block.Version) Section {
  s := paletted_section{}
  s.version = v
  s.global_to_palette = make(map[uint32]uint32)
  s.palette_to_global = []uint32{0}
  s.block_amounts = []int32{16 * 16 * 16} // There are 4096 air blocks
  s.global_to_palette[0] = 0
  s.palette_to_global[0] = 0
  s.bits_per_block = 4
  s.bitmask = uint32(1 << s.bits_per_block - 1)
  s.block_data = make([]uint64, 16 * 16 * 16 * int32(s.bits_per_block) / 64)
  s.using_palette = true
  s.proto_dirty = true
  return &s
}

func (s *paletted_section) CopyFrom(o Section) {
  s.global_to_palette = make(map[uint32]uint32)
  s.global_to_palette[0] = 0
  s.palette_to_global = s.palette_to_global[:1] // Reuse the old slice; prevents garbage collection
  s.palette_to_global[0] = 0
  s.block_amounts = s.block_amounts[:1] // Reuse the old slice; prevents garbage collection
  s.block_amounts[0] = 16 * 16 * 16
  s.bits_per_block = 4
  s.bitmask = uint32(1 << s.bits_per_block - 1)
  // s.bits_per_block is 4, so this will always be the minimum length; no need to check for index out of bounds.
  s.block_data = s.block_data[:16 * 16 * 16 * int32(s.bits_per_block) / 64]
  s.using_palette = true
  s.proto_dirty = true
  for y := int32(0); y < 16; y++ {
    for z := int32(0); z < 16; z++ {
      for x := int32(0); x < 16; x++ {
        pos := block.Pos{x, y, z}
        s.SetBlock(pos, o.GetBlock(pos))
      }
    }
  }
}

// Very fast, because of the palette.
func (s *paletted_section) IsAll(t *block.Type) bool {
  if len(s.palette_to_global) != 1 {
    return false
  }
  return s.palette_to_global[0] == t.State()
}

// This calls get_block_id, and then converts that block id
// into a *block.Type.
func (s *paletted_section) GetBlock(pos block.Pos) *block.Type {
  return block.TypeFromIDVersion(s.get_block_id(pos), s.version)
}

// This calls on get_palette_id, and then reads from the palette.
// This will return a 1.15.2 block id.
// If the id returned by get_palette_id does not exist in the palette,
// this will panic.
func (s *paletted_section) get_block_id(pos block.Pos) uint32 {
  palette_id := s.get_palette_id(pos)
  if int(palette_id) >= len(s.palette_to_global) {
    panic("Palette id is invalid!")
  } else {
    return s.palette_to_global[palette_id]
  }
}

// This gets an id from the data array. This does nothing but bit manipulation,
// and will not access the palette at all.
func (s *paletted_section) get_palette_id(pos block.Pos) uint32 {
  index := ((pos.Y * 16 + pos.Z) * 16 + pos.X) * s.bits_per_block
  start := index / 64
  start_offset := index % 64
  end := (index + s.bits_per_block) / 64

  value := s.block_data[start] >> start_offset
  if start_offset > (64 - s.bits_per_block) {
    value |= s.block_data[end] << (64 - start_offset)
  }

  return uint32(value) & s.bitmask
}

// This fills an area within the chunk. This is a lot faster than
// individual SetBlock calls, since it only has to update the
// palette once.
func (s *paletted_section) Fill(min, max block.Pos, t *block.Type) {
  id := t.StateVersion(s.version)
  s.fill_id(min, max, id)
}

func (s *paletted_section) fill_id(min, max block.Pos, id uint32) {
  // Need paranthases for some reason. Does not compile without them
  if (min == block.Pos{0, 0, 0} && max == block.Pos{15, 15, 15}) {
    // Shortcut for filling entire section
    for k := range s.global_to_palette {
      delete(s.global_to_palette, k)
    }
    if id != 0 {
      // We must add air into the palette, as 1.14+ chunks require air in the palette.
      s.block_amounts = []int32{0, 16 * 16 * 16}
      s.global_to_palette[0] = 0
      s.global_to_palette[id] = 1
      s.palette_to_global = []uint32{0, id}
    } else {
      s.block_amounts = s.block_amounts[:1]
      s.block_amounts[0] = 16 * 16 * 16
      s.global_to_palette[id] = 0
      s.palette_to_global = s.palette_to_global[:1]
      s.palette_to_global[0] = id
    }
    s.block_data = s.block_data[:16 * 16 * 16 * 4 / 64]
    // Make the block data all ones
    for i := range s.block_data {
      s.block_data[i] = 0x1111111111111111
    }
    s.bits_per_block = 4
    s.bitmask = uint32(1 << s.bits_per_block - 1)
    s.proto_dirty = true
  } else {
    if max.X < min.X { return }
    if max.Y < min.Y { return }
    if max.Z < min.Z { return }
    for x := min.X; x <= max.X; x++ {
      for y := min.Y; y <= max.Y; y++ {
        for z := min.Z; z <= max.Z; z++ {
          s.block_amounts[s.get_palette_id(block.Pos{x, y, z})]--
        }
      }
    }
    // Air is required in the chunk, so we never check palette_id 1.
    for palette_id := int32(1); palette_id < int32(len(s.block_amounts)); palette_id++ {
      // new_palette_id, ok := s.global_to_palette[id]
      amount := s.block_amounts[palette_id]
      if amount == 0 {
        // If this is true, then we are removing an item from the palettee during this fill.
        // The reason we check for new_palette_id is so that we don't remove and then re-add the same
        // palette id in this function (adding/removing is slow, as we need to iterate through the whole
        // chunk section).
        s.remove_from_palette(palette_id)
        palette_id--
      }
    }
    palette_id, ok := s.global_to_palette[id]
    if !ok {
      // Even with new_palette_id check above, this could be a brand new palette id, in which case we must add it.
      palette_id = s.add_to_palette(id)
      // s.add_to_palette sets this to 1, and we want it at 0.
      s.block_amounts[palette_id]--
    }
    palette_id = s.global_to_palette[id]
    s.block_amounts[palette_id] += (max.X - min.X + 1) * (max.Y - min.Y + 1) * (max.Z - min.Z + 1)
    for x := min.X; x <= max.X; x++ {
      for y := min.Y; y <= max.Y; y++ {
        for z := min.Z; z <= max.Z; z++ {
          s.set_palette_id(block.Pos{x, y, z}, palette_id)
        }
      }
    }
  }
}

// This sets the position to the given block type.
// Internally, it does a ton of garbage, but at the end of the day,
// the block state will be in the palette and in the chunk. Read
// paletted_section.set_block_id() if you want more details.
func (s *paletted_section) SetBlock(pos block.Pos, t *block.Type) {
  s.set_block_id(pos, t.StateVersion(s.version))
}

// This sets a block in the chunk section. First, it checks if it needs to remove
// the palette entry from the block that it is replacing. If it does, then it shifts
// all block data larger than the palette id down by one. Then, it checks if the new
// block has an entry in the palette. If it does not, it shifts all block data up by one.
// After doing this, it sets the new palette id into the block data.
//
// This is seperate from SetBlock() for the sake of testing. You should not call
// this function, as SetBlock will do what you are looking for.
func (s *paletted_section) set_block_id(pos block.Pos, id uint32) {
  // This will find the block we are replacing
  prev_palette_id := s.get_palette_id(pos)
  prev_id := s.palette_to_global[prev_palette_id]
  if prev_id == id {
    return
  }

  // Easiest to update this here, before we shift s.block_amounts
  s.block_amounts[prev_palette_id]--

  // If this is zero, then we just replaced the last one.
  // If this is negative, then something has gone horribly wrong.
  if s.block_amounts[prev_palette_id] == 0 {
    // This updates the block data and palette
    s.remove_from_palette(int32(prev_palette_id))
  }

  palette_id, ok := s.global_to_palette[id]
  if !ok {
    // This deals with all of the palette stuff.
    s.add_to_palette(id)
  } else {
    // Item is already in the palette, easiest to update this here.
    s.block_amounts[palette_id]++
  }
  palette_id, ok = s.global_to_palette[id]
  if !ok {
    fmt.Println(s.palette_to_global)
    fmt.Println(id)
    panic("The item in the palette should have just been added!")
  }

  err := s.set_palette_id(pos, palette_id)
  // If this error occurred, then s.bits_per_block is too small for palette_id
  // If this happens, we did something wrong.
  if err != nil {
    panic(err)
  }
}

// This simply sets the correct bits in s.block_data. It will always
// set proto_dirty to true. If the passed id is too large to fit in s.bits_per_block,
// then it returns an error.
func (s *paletted_section) set_palette_id(pos block.Pos, id uint32) error {
  s.proto_dirty = true
  needed_bits := needed_bits(id)
  if s.bits_per_block < needed_bits {
    return errors.New("Cannot set palette id, as bits_per_block is too low!")
  }

  index := (pos.Y * 16 + pos.Z) * 16 + pos.X
  start := (index * s.bits_per_block) / 64
  start_offset := (index * s.bits_per_block) % 64
  end := ((index + 1) * s.bits_per_block) / 64

  mask := ^uint64(0) // all 1
  other_mask := uint64(0)
  split := false // flag for if the block is split between two longs
  mask = mask >> (64 - s.bits_per_block) // makes the number only have bits_per_block number of bits set
  if start_offset > (64 - s.bits_per_block) { // Means that the block is split between longs
    // In this case, we need to set two masks, one for each long
    other_mask = mask >> (64 - start_offset)
    mask = mask << start_offset
    split = true
  } else {
    mask = mask << start_offset
  }
  mask = ^mask // inverts mask, so that &= will clear the bits that were set
  other_mask = ^other_mask

  // fmt.Println("Setting block at", x, y, z)
  // fmt.Println("To:", id)
  // fmt.Println("start_offset:", start_offset, "s.bits_per_block", s.bits_per_block)
  // fmt.Printf("Got mask      : %#b\n", mask)
  // fmt.Printf("Got other mask: %#b\n", other_mask)

  // sets the block to 0
  s.block_data[start] &= mask
  // sets the block to the given id
  s.block_data[start] |= uint64(id) << start_offset
  if split {
    // sets the block to 0
    s.block_data[end] &= other_mask
    // sets the block to the given id
    s.block_data[end] |= uint64(id) >> (64 - start_offset)
  }
  s.proto_dirty = true
  return nil
}

// This will copy all of the chunk data into a new long array, and change
// the number of bits used to represent each block
func (s *paletted_section) update_bits_per_block(new_bits_per_block int32) {
  if s.bits_per_block == new_bits_per_block {
    return
  }
  new_data := make([]uint64, 16 * 16 * 16 * int32(new_bits_per_block) / 64)

  for y := int32(0); y < 16; y++ {
    for z := int32(0); z < 16; z++ {
      for x := int32(0); x < 16; x++ {
        old_value := s.get_palette_id(block.Pos{x, y, z})

        index := ((y * 16 + z) * 16 + x) * new_bits_per_block
        start := index / 64
        start_offset := index % 64
        end := (index + new_bits_per_block) / 64

        // sets the block to the given id
        new_data[start] |= uint64(old_value) << start_offset
        if start_offset > (64 - new_bits_per_block) {
          // sets the block to the given id
          new_data[end] |= uint64(old_value) >> (64 - start_offset)
        }
      }
    }
  }

  s.proto_dirty = true
  s.block_data = new_data
  s.bits_per_block = new_bits_per_block
  s.bitmask = uint32(1 << s.bits_per_block - 1) // This is just bits_per_block number of 1s
}

// This is called whenever an id is not in the palette.
// This inserts it, and shifts all the block data. Returns
// the new palette id.
func (s *paletted_section) add_to_palette(id uint32) uint32 {
  // New palette id, must add to palette
  if id > s.palette_to_global[len(s.palette_to_global) - 1] {
    // If this is true, this is very simple. We just want to append the new entry to the palette
    s.global_to_palette[id] = uint32(len(s.palette_to_global))
    s.palette_to_global = append(s.palette_to_global, id)
    s.block_amounts = append(s.block_amounts, 1) // One block that has just been set
    return uint32(len(s.palette_to_global) - 1)
  } else {
    // If this is true, this is very simple. We just want to append the new entry to the palette
    // If this is true, we must shift everything above id up by one, and then insert the new entry
    // We must add one item to the palette, so we always want to append an item here
    s.palette_to_global = append(s.palette_to_global, 0)
    s.block_amounts = append(s.block_amounts, 0)
    palette_id := int32(-1)
    for i := len(s.palette_to_global) - 2; i >= 0; i-- {
      if s.palette_to_global[i] > id {
        // If this is true, we must shift this up by one
        s.global_to_palette[s.palette_to_global[uint32(i)]] = uint32(i)+1
        s.palette_to_global[i+1] = s.palette_to_global[i]
        s.block_amounts[i+1] = s.block_amounts[i]
      } else {
        // If this is true, then i must be less than id. (since we did the ok check above)
        // When this happens, we have already shifted everything above up by one. So,
        // we need to insert the new element here, and then stop the loop
        s.global_to_palette[id] = uint32(i)+1
        s.palette_to_global[i+1] = id
        s.block_amounts[i+1] = 1 // One block that has just been set
        // Everything larger than shift_value should be shifted up by one in the block_data long array
        palette_id = int32(i)
        break
      }
    }
    if palette_id == -1 {
      // If this is true, then the item we are adding to the palette needs to be inserted at the start
      // of the palette. Everything has already been shifted over, so this is easy.
      s.global_to_palette[id] = 0
      s.palette_to_global[0] = id
      s.block_amounts[0] = 1 // One block that has just been set
      palette_id = 0
    }
    // This shifts all values larger than the palette id up by one.
    // If palette_id was never set, it is still -1, which means all
    // values are shifted up by one.
    for y := int32(0); y < 16; y++ {
      for z := int32(0); z < 16; z++ {
        for x := int32(0); x < 16; x++ {
          pos := block.Pos{x, y, z}
          found := s.get_palette_id(pos)
          if int32(found) > palette_id {
            s.set_palette_id(pos, found + 1)
          }
        }
      }
    }
    s.proto_dirty = true
    return uint32(palette_id)
  }
}

// Used when we remove an item in the palette
// Only used when the item is not removed from the end
// id can be negative, if we need to shift everything down
func (s *paletted_section) remove_from_palette(palette_id int32) {
  // In 1.14+, chunks have one caveat: the palette must contain air. Based on all of my tests
  // in 1.14 and 1.15, it appears the chunk is broken for the client if the first element of
  // the chunk section is not air. Therefore, we must have this check.
  // Also my code might just be broken, but I spent like an hour debugging this, so I doubt that.
  if palette_id == 0 {
    return
  }
  // Need to do this list first, so get_palette_id doesn't break.
  // This shifts everything larger than palette_id down by one.
  for y := int32(0); y < 16; y++ {
    for z := int32(0); z < 16; z++ {
      for x := int32(0); x < 16; x++ {
        pos := block.Pos{x, y, z}
        found := s.get_palette_id(pos)
        if int32(found) > palette_id {
          s.set_palette_id(pos, found - 1)
        }
      }
    }
  }

  // Used later in delete()
  global_id := s.palette_to_global[palette_id]

  // This loops through all elements of the palette above prev_id
  for i := palette_id; i < int32(len(s.palette_to_global)) - 1; i++ {
    // This decrements each one of the palette ids
    s.global_to_palette[s.palette_to_global[i+1]]--
    // This shifts each one of the global ids down by one
    s.palette_to_global[i] = s.palette_to_global[i+1]
    s.block_amounts[i] = s.block_amounts[i+1]
  }
  // This removes the (now garbage) entry
  delete(s.global_to_palette, global_id)

  // We just pop off the last item here
  s.palette_to_global = s.palette_to_global[:len(s.palette_to_global)-1]
  s.block_amounts = s.block_amounts[:len(s.block_amounts)-1]
  s.proto_dirty = true
}

func (s *paletted_section) Serialize() *pb.Chunk_Section {
  if !s.proto_dirty {
    return s.cached_proto
  }
  proto := &pb.Chunk_Section{}
  if s.palette_to_global[0] == 0 {
    // There is air in the chunk, so we can use s.block_amounts[0] to see the number of air blocks.
    // This counter is for non air blocks, so we can just take 4096 - air blocks.
    // If palette_to_global[0] != 0, then there can be no air in the chunk (air has id zero, so it
    // will always be at the start if the palette).
    proto.NonAirBlocks = 4096 - s.block_amounts[0]
  }
  proto.BitsPerBlock = s.bits_per_block

  if s.using_palette {
    palette_length := uint32(len(s.palette_to_global))
    for i := uint32(0); i < palette_length; i++ {
      proto.Palette = append(proto.Palette, s.palette_to_global[i])
    }
  }

  // proto.Data is a byte array, while s.block_data is a long array
  proto.Data = make([]byte, len(s.block_data) * 8)
  for i := 0; i < len(s.block_data); i++ {
    binary.BigEndian.PutUint64(proto.Data[i*8:], s.block_data[i])
  }

  s.cached_proto = proto
  s.proto_dirty = false
  return proto
}

func (s *paletted_section) deserialize(proto *pb.Chunk_Section) {
  s.bits_per_block = proto.BitsPerBlock
  s.bitmask = uint32(1 << s.bits_per_block - 1)
  s.block_data = make([]uint64, 16 * 16 * 16 * int32(s.bits_per_block) / 64)

  for i := 0; i < len(s.block_data); i++ {
    v := binary.BigEndian.Uint64(proto.Data[i*8:])
    s.block_data[i] = v
  }

  s.global_to_palette = make(map[uint32]uint32)
  s.palette_to_global = make([]uint32, len(proto.Palette))
  s.block_amounts = make([]int32, len(proto.Palette))
  if s.bits_per_block <= 8 {
    for id, global_id := range proto.Palette {
      s.global_to_palette[global_id] = uint32(id)
      s.palette_to_global[id] = global_id
    }
    for x := int32(0); x < 16; x++ {
      for y := int32(0); y < 16; y++ {
        for z := int32(0); z < 16; z++ {
          val := s.get_palette_id(block.Pos{x, y, z})
          s.block_amounts[val]++
        }
      }
    }
  }
  s.proto_dirty = true
}

func (s *paletted_section) validate() bool {
  valid := true
  total := int32(0)
  for _, v := range s.block_amounts {
    total += v
  }
  if total != 4096 {
    fmt.Println("ERROR: Total block amount is wrong:", total)
    valid = false
  }
  for palette, global := range s.palette_to_global {
    if s.global_to_palette[global] != uint32(palette) {
      fmt.Println("ERROR: Invalid palette:", s.palette_to_global)
      fmt.Println(s.global_to_palette)
      valid = false
      break
    }
  }
  if len(s.palette_to_global) != len(s.global_to_palette) {
    fmt.Println("ERROR: Palettes are not the same length!")
    valid = false
  }
  if len(s.palette_to_global) == 0 {
    fmt.Println("ERROR: Palette is empty!")
    valid = false
  }
  return valid
}
