package chunk

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

// This is a general representation of a chunk, which contains a seperate struct for each
// version supported. As more versions get added, this chunk will get larger, as it needs
// to reference more versions. However, this is the fastest way to do things, as copying
// and regenerating the chunk for every new client would be far too slow.
type MultiChunk struct {
  // Past versions of this same chunk. Used for serializing and writing.
  versions map[block.Version]*Chunk
  // Latest version of the chunk. Used for reading, writing, and serializing.
  latest *Chunk
}

func NewMultiChunk() *MultiChunk {
  c := MultiChunk{}
  c.versions = make(map[block.Version]*Chunk)

  c.versions[block.V1_8] =  NewChunk(block.V1_8, new_fixed_section)

  // Too slow! Temporarily disabling
  // c.versions[block.V1_12] = NewChunk(block.V1_12, new_paletted_chunk)
  // c.versions[block.V1_13] = NewChunk(block.V1_13, new_paletted_chunk)
  // c.versions[block.V1_14] = NewChunk(block.V1_14, new_paletted_chunk)
  c.versions[block.V1_15] = NewChunk(block.V1_15, new_paletted_chunk)

  c.latest = NewChunk(block.VLATEST, new_paletted_chunk)
  return &c
}

func (c *MultiChunk) HasChunkSection(y int32) bool {
  return c.latest.HasChunkSection(y)
}

// This returns a block state, which is a block within the world.
// pos should be in absolute coordinates, as that is the position
// that will be used in the block.State. This will call ChunkRelPos
// to access the blocks in the chunk.
func (c *MultiChunk) GetBlockAt(pos block.Pos) *block.State {
  // This block state can be used by users, and acts like a usable block object.
  // It can be used to get or set the block at that location,
  // and is mostly just a utility class, that wraps the
  // chunk.SetBlockType and chunk.GetBlockType.
  return block.NewState(
    c.SetBlock,
    pos,
    c.GetBlock(pos.ChunkRelPos()),
  )
}

// This gets a block within the chunk. NOTE: pos needs to be relative to
// a chunk. This function will not call pos.ChunkRelPos(), as that is
// slower than relying on the user. If the pos is outside of the chunk,
// then the block position will be wrapped, and it will be very hard to
// predict what will happen.
func (c *MultiChunk) GetBlock(pos block.Pos) *block.Type {
  return c.latest.GetBlock(pos)
}

// This checks if an entire chunk is the given block. It only checks this
// for the latest version, which is a paletted chunk, so this is pretty fast.
func (c *MultiChunk) IsAll(t *block.Type) bool {
  return c.latest.IsAll(t)
}

// This sets a block within the chunk. NOTE: pos needs to be relative to
// a chunk. This function will not call pos.ChunkRelPos(), as that is
// slower than relying on the user. If the pos is outside of the chunk,
// then the block position will be wrapped, and it will be very hard to
// predict what will happen.
func (c *MultiChunk) SetBlock(pos block.Pos, t *block.Type) {
  c.latest.SetBlock(pos, t)
  for _, section := range c.versions {
    section.SetBlock(pos, t)
  }
}

// This sets all blocks within the range min to max within the chunk.
// For palleted chunks (1.12+), this is much faster than SetBlockType.
// For fixed size chunks (1.8), this is about the same speed.
// However, older chunks rely on new chunks to see what is air, so it
// ends up being a speed improvement no matter what. If min is less
// than max, then this call will return immediatley.
func (c *MultiChunk) Fill(min, max block.Pos, t *block.Type) {
  c.latest.Fill(min, max, t)
  for _, chunk := range c.versions {
    chunk.Fill(min, max, t)
  }
}

// This converts the chunk to a protobuf, for the given protocol version
func (c *MultiChunk) Serialize(version block.Version) *pb.Chunk {
  if version == block.VLATEST {
    return c.latest.Serialize()
  }
  chunk, ok := c.versions[version]
  if !ok {
    return nil
  }
  return chunk.Serialize()
}

// This takes a protobuf (from a grpc connection) and loads all of the chunk
// sections accordingly. This is used on the client whenever a chunk packet
// is recieved.
// This must always be used with the latest version!
func (c *MultiChunk) LoadFromProto(proto *pb.Chunk) {
  c.latest.FromProto(proto)
  for _, chunk := range c.versions {
    chunk.CopyFrom(c.latest)
  }
}

