package world

import (
  "math"
  "gitlab.com/macmv/sugarcane/minecraft/entity"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type location struct {
  x, y, z float64
}

type Hitbox interface {
  // feet position (bottom center of hitbox)
  X() float64
  Y() float64
  Z() float64

  // New position the entity is trying to move to
  NewX() float64
  NewY() float64
  NewZ() float64

  // radius on X and Z in blocks
  Width() float64
  // height of the hitbox in blocks
  Height() float64
}

func (w *World) Collide(hitbox Hitbox) *entity.CollisionInfo {
  block_width := int32(math.Ceil(hitbox.Width() / 2))
  block_height := int32(math.Ceil(hitbox.Height()))
  block_x := int32(math.Floor(hitbox.NewX()))
  block_y := int32(math.Floor(hitbox.NewY()))
  block_z := int32(math.Floor(hitbox.NewZ()))
  info := &entity.CollisionInfo{}
  info.NewX = hitbox.NewX()
  info.NewY = hitbox.NewY()
  info.NewZ = hitbox.NewZ()
  dx := hitbox.NewX() - hitbox.X()
  dy := hitbox.NewY() - hitbox.Y()
  dz := hitbox.NewZ() - hitbox.Z()
  for x := block_x - block_width; x <= block_x + block_width; x++ {
    for z := block_z - block_width; z <= block_z + block_width; z++ {
      for y := block_y; y <= block_y + block_height; y++ {
        w.test_collsion(hitbox, info, dx, dy, dz, x, y, z)
      }
    }
  }
  return info
}

func (w *World) test_collsion(hitbox Hitbox, info *entity.CollisionInfo, dx, dy, dz float64, bx, by, bz int32) {
  t := w.BlockType(block.Pos{bx, by, bz})
  if t.BoundingBox() != block.BoundingBox_NONE {
    min := location{
      x: hitbox.X() - hitbox.Width() / 2,
      y: hitbox.Y(),
      z: hitbox.Z() - hitbox.Width() / 2,
    }
    max := location{
      x: hitbox.X() + hitbox.Width() / 2,
      y: hitbox.Y() + hitbox.Height(),
      z: hitbox.Z() + hitbox.Width() / 2,
    }

    // It does not matter that plane is set when dx = 0, because face_in_block will ignore it if dx = 0
    plane := min.x + dx
    if dx > 0 {
      plane = max.x + dx
    }
    // x face
    hit, pos := w.face_in_block(dx, 0, 0, min.y, min.z, max.y, max.z, min.x + dx, bx, by, bz)
    if hit {
      info.NewX = pos
      info.DidCollide = true
      if dx < 0 {
        info.HitNX = true
      } else if dx > 0 {
        info.HitPX = true
      }
    }

    plane = min.y + dy
    if dy > 0 {
      plane = max.y + dy
    }
    // y face (top/bottom)
    hit, pos = w.face_in_block(0, dy, 0, min.x, min.z, max.x, max.z, plane, bx, by, bz)
    if hit {
      info.NewY = pos
      info.DidCollide = true
      if dy < 0 {
        info.HitNY = true
      } else if dy > 0 {
        info.HitPY = true
      }
    }

    plane = min.z + dz
    if dz > 0 {
      plane = max.z + dz
    }
    // z face
    hit, pos = w.face_in_block(0, 0, dz, min.x, min.y, max.x, max.y, plane, bx, by, bz)
    if hit {
      info.NewZ = pos
      info.DidCollide = true
      if dz < 0 {
        info.HitNZ = true
      } else if dy > 0 {
        info.HitPZ = true
      }
    }
  }
}

// This will be called by passing in a plane, represented by min_ and max_ values
// dx, dy, and dz are the direction of the plane. If more than 1 of these is non 0, it will preduce undefined behavior
// min_ and max_ values determine the size/location of the plane
// c is used to define the location of the plane in the axis that is set
// So if dy was 1, then a->x, b->z, and c->y
// The value of dx, dy and dz do not matter. It only matters if they are <0, =0, or >0
func (w *World) face_in_block(dx, dy, dz float64, min_a, min_b, max_a, max_b, c float64, bx, by, bz int32) (bool, float64) {
  b_min_x := math.Floor(float64(bx))
  b_min_y := math.Floor(float64(by))
  b_min_z := math.Floor(float64(bz))
  b_max_x := b_min_x + 1
  b_max_y := b_min_y + 1
  b_max_z := b_min_z + 1
  plane_in_block_a := false
  plane_in_block_b := false
  dir := float64(0)
  if dx != 0 {
    // a->y, b->z, c->x
    // Checks if the x axis is inside the block
    if c > b_min_x && c < b_max_x {
      // The plane can be bigger or smaller than the block
      // So we make sure the plane is somewhere inside the block
      plane_in_block_a = min_a < b_max_y && max_a > b_min_y
      plane_in_block_b = min_b < b_max_z && max_b > b_max_z
    } else {
      return false, 0
    }
    dir = dx
  } else if dy != 0 {
    // a->x, b->z, c->y
    if c > b_min_y && c < b_max_y {
      // The plane can be bigger or smaller than the block
      // So we make sure the plane is somewhere inside the block
      plane_in_block_a = min_a < b_max_x && max_a > b_min_x
      plane_in_block_b = min_b < b_max_z && max_b > b_min_z
    } else {
      return false, 0
    }
    dir = dy
  } else if dz != 0 {
    // a->x, b->y, c->z
    // Checks if the x axis is inside the block
    if c > b_min_z && c < b_max_z {
      // The plane can be bigger or smaller than the block
      // So we make sure the plane is somewhere inside the block
      plane_in_block_a = min_a < b_max_x && max_a > b_min_x
      plane_in_block_b = min_b < b_max_y && max_b > b_min_y
    } else {
      return false, 0
    }
    dir = dz
  } else {
    return false, 0
  }
  if plane_in_block_a && plane_in_block_b {
    new_pos := float64(0)
    if dir > 0 {
      new_pos = math.Floor(c)
    } else {
      new_pos = math.Floor(c) + 1
    }
    return true, new_pos
  } else {
    return false, 0
  }
}
