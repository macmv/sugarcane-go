package world

import (
  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/entity"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

// Gets the entity with the given EID.
//
// It will return (an entity, true) if one was found, and (nil, false) of none was found.
func (w *World) GetEntity(id uint32) (entity.Entity, bool) {
  w.entities_lock.Lock()
  val, ok := w.entities[id]
  w.entities_lock.Unlock()
  return val, ok
}

// This will spawn an entity of the given type in the world.
// This will return nil and do nothing if the given entity type was not an entity.
// If the entity you passed in was an object, it will call SpawnObject, with a data value of 0.
//
// The returned entity is a pointer to the entity that was just spawned.
//
// vx, vy, and vz are velocity values, in 1/8000 of a block per tick.
func (w *World) Spawn(etype entity.Type, x, y, z float64, pitch, yaw, head_pitch byte, vx, vy, vz int16) entity.Entity {
  is_object := entity.IsObject(etype)
  if is_object {
    return w.SpawnObject(etype, x, y, z, pitch, yaw, vx, vy, vz, 0)
  }
  eid := w.NextEID()
  uuid := util.NewUUIDFromRand()
  block_pos := block.Pos{int32(x), int32(y), int32(z)}
  if x < 0 { block_pos.X-- }
  if y < 0 { block_pos.Y-- }
  if z < 0 { block_pos.Z-- }
  chunk_pos := block_pos.ChunkPos()

  ent := entity.New(etype, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz, nil)
  if ent == nil {
    log.Warn("Cannot spawn entity of type ", etype)
    return nil
  }
  w.entities_lock.Lock()
  w.entities[eid] = ent
  w.entities_lock.Unlock()

  out := packet.NewOutgoingPacket(packet.Clientbound_SpawnLivingEntity)
  out.SetInt(0, int32(eid))
  out.SetUUID(0, uuid)
  out.SetInt(1, int32(etype))
  out.SetDouble(0, x)
  out.SetDouble(1, y)
  out.SetDouble(2, z)
  out.SetByte(0, yaw)
  out.SetByte(1, pitch)
  out.SetByte(2, head_pitch)
  out.SetShort(0, vx)
  out.SetShort(1, vy)
  out.SetShort(2, vz)
  w.SendInView(chunk_pos, out)
  return ent
}

// Pass an entity id as the first parameter.
// The will return nil and do nothing if the given type is not an object.
//
// I understand that object types and entity types are different.
// To make your life simpler, I have mapped all objects' entity ids to object ids.
// This means that it is easier to check if an entity id is valid, rather than an object id.
//
// The data field can be set to anything, and that is what will be send in the spawn obejct packet. (There are no checks!)
// For example, if you wanted to spawn a tnt minecart, you would call this:
// world.SpawnObject(entity.Type_Minecart, 3)
//
// vx, vy, and vz are velocity values, in 1/8000 of a block per tick.
//
// For exp orbs, the data field is the amount of exp in the orb (will change the size of it)
//
// For paintings, call world.SpawnPainting(). This function will return nil and do nothing of Type_Painting is passed in.
func (w *World) SpawnObject(etype entity.Type, x, y, z float64, yaw, pitch byte, vx, vy, vz int16, data int32) entity.Entity {
  is_object := entity.IsObject(etype)
  if !is_object {
    return nil
  }
  eid := w.NextEID()
  uuid := util.NewUUIDFromRand()
  block_pos := block.Pos{int32(x), int32(y), int32(z)}
  if x < 0 { block_pos.X-- }
  if y < 0 { block_pos.Y-- }
  if z < 0 { block_pos.Z-- }
  chunk_pos := block_pos.ChunkPos()

  ent := entity.New(etype, eid, uuid, x, y, z, yaw, pitch, 0, vx, vy, vz, &entity.Attributes{Data: int(data)})
  if ent == nil {
    log.Warn("Cannot spawn entity of type ", etype)
    return nil
  }
  w.entities_lock.Lock()
  w.entities[eid] = ent
  w.entities_lock.Unlock()

  switch etype {
  case entity.Type_ExpOrb:
    out := packet.NewOutgoingPacket(packet.Clientbound_SpawnExpOrb)
    out.SetInt(0, int32(eid))
    out.SetDouble(0, x)
    out.SetDouble(1, y)
    out.SetDouble(2, z)
    out.SetShort(0, int16(data))
    w.SendInView(chunk_pos, out)
  case entity.Type_Painting:
    return nil
  default:
    out := packet.NewOutgoingPacket(packet.Clientbound_SpawnEntity)
    out.SetInt(0, int32(eid))
    out.SetUUID(0, uuid)
    out.SetInt(1, int32(etype))
    out.SetDouble(0, x)
    out.SetDouble(1, y)
    out.SetDouble(2, z)
    out.SetByte(0, pitch)
    out.SetByte(1, yaw)
    out.SetInt(2, data)
    out.SetShort(0, vx)
    out.SetShort(1, vy)
    out.SetShort(2, vz)
    w.SendInView(chunk_pos, out)
  }
  return ent
}
