package terrain

import (
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

// This is the default world generator. You can add biomes to this
// generator, until Lock() is called. This uses a voronoi map, combined
// with noise maps to generate the biome regions. It also uses a global
// height, temperature and humidity map.
type VoronoiWorldGenerator struct {
  // Y position that oceans start at
  SeaLevel int32

  seed int64
  voronoi *util.WarpedVoronoi

  biomes []BiomeGenerator
  ocean BiomeGenerator
  locked bool

  height_map      util.NoiseSampler
  temperature_map util.NoiseSampler
  humidity_map    util.NoiseSampler
}

// This returns a new VoronoiBiomeGenerator, given a world seed. This
// generator will have no biomes. Call Init() to add the default biomes,
// and/or AddBiome to add your own biomes into the mix.
func NewVoronoi(seed int64) *VoronoiWorldGenerator {
  g := &VoronoiWorldGenerator{}
  g.SeaLevel = 72
  g.seed = seed
  r := rand.New(util.NewFastRand(seed))
  g.voronoi = util.NewWarpedVoronoi(r, 128, 2)
  g.height_map = util.NewLayeredNoise(r, 6)
  g.temperature_map = util.NewLayeredNoise(r, 5)
  g.humidity_map = util.NewLayeredNoise(r, 5)
  return g
}

// This adds a biome to the world. It will do nothing if Lock()
// has already been called.
func (g *VoronoiWorldGenerator) AddBiome(biome BiomeGenerator) {
  if !g.locked {
    g.biomes = append(g.biomes, biome)
  }
}

// This adds all of the default biomes, which are expected to be
// used with this generator.
// This is not necessary, but is the simplest option if you just want
// to add a new biome to the pre-existing generation.
// If you want to make a new world, with all new terrain, you should
// not call this function.
func (g *VoronoiWorldGenerator) Init() {
  g.ocean = new_ocean_generator(g, g.seed)
  g.AddBiome(new_evergreen_generator(g, g.seed))
  g.AddBiome(new_forest_generator(g, g.seed))
  g.AddBiome(new_desert_generator(g, g.seed))
  g.AddBiome(new_solid_generator(g, block.GetKind("minecraft:black_wool").DefaultType()))
  g.AddBiome(new_solid_generator(g, block.GetKind("minecraft:red_wool").DefaultType()))
  g.AddBiome(new_solid_generator(g, block.GetKind("minecraft:cyan_wool").DefaultType()))
  g.AddBiome(new_solid_generator(g, block.GetKind("minecraft:yellow_wool").DefaultType()))
  g.AddBiome(new_solid_generator(g, block.GetKind("minecraft:lime_wool").DefaultType()))
  g.AddBiome(new_meme_generator(g))
}

// This locks the generator, and prevents any biomes from being added.
// You must call this before generating any terrain.
func (g *VoronoiWorldGenerator) Lock() {
  g.locked = true
}

// This is used internally, to generate chunks. You may call this function,
// if you want to restore a chunk to it's generated state. Since chunks are
// not aware of their position in the world, you can mix and match chunk positions.
// If you want the default look, make sure you pass in the correct chunk_pos
// to this function.
func (g *VoronoiWorldGenerator) FillChunk(chunk_pos block.ChunkPos, chunk *chunk.MultiChunk) {
  if !g.locked {
    panic("Tried to use biome generator before calling Lock()!")
  }
  biomes := make(map[BiomeGenerator]struct{})
  chunk_pos.ForEachColumn(func (pos block.Pos) bool {
    biome := g.BiomeAt(pos)
    biomes[biome] = struct{}{}
    return true
  })
  if len(biomes) == 1 {
    for b := range biomes {
      b.FillChunk(chunk_pos, chunk)
    }
  } else {
    chunk_pos.ForEachColumn(func (pos block.Pos) bool {
      g.BiomeAt(pos).FillColumn(pos, chunk)
      return true
    })
  }
  for b := range biomes {
    b.DetailChunk(chunk_pos, chunk)
  }
}

// This returns the expected height at that point in the world.
// This should be used as a baseline. Things like oceans or mountains
// will add to this value, whereas things like deserts probably
// shouldn't modify this value.
func (g *VoronoiWorldGenerator) HeightAt(pos block.Pos) int32 {
  return int32(g.ExactHeight(pos))
}

// This returns the height as a float. Same as HeightAt, but can be used
// if you need a more precise value.
func (g *VoronoiWorldGenerator) ExactHeight(pos block.Pos) float64 {
  return g.height_map.Sample(float64(pos.X) / 2048, float64(pos.Z) / 2048) * 256 + 80
}

func (g *VoronoiWorldGenerator) MinHeight(chunk_pos block.ChunkPos) int32 {
  min_height := int32(256)
  chunk_pos.ForEachColumn(func(pos block.Pos) bool {
    height := g.HeightAt(pos)
    if height < min_height {
      min_height = height
    }
    return true
  })
  return min_height
}

// This returns the biome at the block. pos.Y is ignored. The biome is
// an index into g.biomes, and will always be within [0, len(g.biomes)-1].
func (g *VoronoiWorldGenerator) BiomeAt(pos block.Pos) BiomeGenerator {
  height := g.HeightAt(pos)
  if height < g.SeaLevel {
    // Ocean
    return g.ocean
  }
  biome := g.voronoi.Get(int(pos.X), int(pos.Z))
  biome = biome % len(g.biomes)
  biome += len(g.biomes)
  biome = biome % len(g.biomes)
  return g.biomes[biome]
}

// Returns the global height map. This should be used as the
// base height of the world, to make biome borders look nice.
func (g *VoronoiWorldGenerator) HeightMap() util.NoiseSampler {
  return g.height_map
}

// Returns the global temperature map. This should not be needed,
// as the generator automatically uses this in FillChunk().
func (g *VoronoiWorldGenerator) TempuratureMap() util.NoiseSampler {
  return g.temperature_map
}

// Returns the global humidity map. This should not be needed,
// as the generator automatically uses this in FillChunk().
func (g *VoronoiWorldGenerator) HumidityMap() util.NoiseSampler {
  return g.humidity_map
}
