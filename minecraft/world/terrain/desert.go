package terrain

import (
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

type DesertGenerator struct {
  w *VoronoiWorldGenerator

  seed int64
  rand *rand.Rand

  cactus_grid *util.RandomGrid
  noise *util.LayeredNoise
}

func new_desert_generator(w *VoronoiWorldGenerator, seed int64) *DesertGenerator {
  g := DesertGenerator{}
  g.w = w
  g.seed = seed
  g.rand = rand.New(rand.NewSource(seed))
  g.cactus_grid = util.NewRandomGrid(g.rand, 20, 0.1)
  g.noise = util.NewLayeredNoise(g.rand, 2)
  return &g
}

func (g *DesertGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height := g.w.MinHeight(chunk_pos)
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height - 5, 15},
    block.GetKind("minecraft:stone").DefaultType(),
  )
  chunk_pos.ForEachColumn(func (pos block.Pos) bool {
    g.fill_column_above(min_height, pos, c)
    return true
  })
  // Cacti
  points := g.cactus_grid.PointsInArea(
    int(chunk_pos.X) * 16      - 0,
    int(chunk_pos.Z) * 16      - 0,
    int(chunk_pos.X) * 16 + 16 + 0,
    int(chunk_pos.Z) * 16 + 16 + 0,
  )
  for _, p := range points {
    g.spawn_cactus(block.Pos{int32(p.X), 0, int32(p.Y)}, c)
  }
}

func (g *DesertGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {}

func (g *DesertGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_above(0, pos, c)
}

func (g *DesertGenerator) fill_column_above(min_height int32, pos block.Pos, c *chunk.MultiChunk) {
  height := g.w.HeightAt(pos)
  p := pos.ChunkRelPos()
  p.Y = height
  c.Fill(p.WithY(min_height), p.AddY(-6), block.GetKind("minecraft:stone").DefaultType())
  c.Fill(p.AddY(-5), p.AddY(-2), block.GetKind("minecraft:sandstone").DefaultType())
  c.Fill(p.AddY(-1), p, block.GetKind("minecraft:sand").DefaultType())
  p.Y++
  if g.cactus_grid.ContainsPoint(int(pos.X), int(pos.Z)) {
    g.spawn_cactus(pos, c)
  }
}

func (g *DesertGenerator) spawn_cactus(pos block.Pos, c *chunk.MultiChunk) {
  g.rand.Seed(g.seed ^ int64(pos.X) ^ int64(pos.Z))
  cactus_height := int32(g.rand.Intn(3) + 2)
  p := pos.ChunkRelPos()
  p.Y = g.w.HeightAt(pos) + 1
  c.Fill(p, p.AddY(cactus_height), block.GetKind("minecraft:cactus").DefaultType())
}
