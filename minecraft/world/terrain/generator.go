package terrain

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

// This is what needs to be passed into the world.
// If you want to add biomes, you should use the default
// generator, and add BiomeGenerators to that.
type WorldGenerator interface {
  // This will fill an entire chunk.
  FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk)
  // This will do things like add default biomes,
  // setup noise maps, etc.
  Init()
  // This will lock the world generator. This is used
  // to make sure no biomes are added after terrain
  // starts generating. In your implementation, you
  // can just leave this blank.
  Lock()
}

// This is an implementation-specific terrain generator.
// You will need this if you want to add custom biomes,
// but if you are making your own terrain from scratch,
// you do not need to use this.
type BiomeGenerator interface {
  // This will fill a single block column.
  // Pos.X and pos.Z are the absolute coordinates of the chunk column.
  // Pos.Y should be ignored.
  FillColumn(pos block.Pos, c *chunk.MultiChunk)

  // This will fill an entire chunk.
  // You can call FillColumn 256 times,
  // but there are some optimizations
  // that can be done for whole chunks.
  // This will be called every time the
  // biome occupies an entire chunk.
  FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk)

  // This is a second pass over a chunk. This
  // should do things like add trees, and add
  // other details. This is called for every biome
  // the a chunk is in, so this call be called
  // multiple times for each chunk. This should allow
  // you to make trees that overlap biome borders.
  DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk)
}
