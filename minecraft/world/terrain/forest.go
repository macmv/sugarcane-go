package terrain

import (
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

type ForestGenerator struct {
  w *VoronoiWorldGenerator

  seed int64
  rand *rand.Rand

  tree_grid *util.RandomGrid
  bush_grid *util.RandomGrid
}

type EvergreenGenerator struct {
  w *VoronoiWorldGenerator

  seed int64
  rand *rand.Rand

  tree_grid *util.RandomGrid
}

func new_forest_generator(w *VoronoiWorldGenerator, seed int64) *ForestGenerator {
  g := ForestGenerator{}
  g.w = w
  g.seed = seed
  g.rand = rand.New(util.NewFastRand(seed))
  g.tree_grid = util.NewRandomGrid(g.rand, 4, 2)
  g.bush_grid = util.NewRandomGrid(g.rand, 10, 1)
  // g.noise = util.NewLayeredNoise(g.rand, 3)
  return &g
}

func (g *ForestGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height := g.w.MinHeight(chunk_pos)
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height - 5, 15},
    block.GetKind("minecraft:stone").DefaultType(),
  )
  chunk_pos.ForEachColumn(func(pos block.Pos) bool {
    g.fill_column_above(min_height, pos, c)
    return true
  })
  // Bushes
  points := g.bush_grid.PointsInArea(
    int(chunk_pos.X) * 16      - 2,
    int(chunk_pos.Z) * 16      - 2,
    int(chunk_pos.X) * 16 + 16 + 2,
    int(chunk_pos.Z) * 16 + 16 + 2,
  )
  for _, p := range points {
    g.rand.Seed(g.seed ^ int64(p.X) ^ int64(p.Y))
    bush_height := int32(g.rand.Intn(2) + 1)
    for x1 := p.X - 1; x1 <= p.X + 1; x1++ {
      for z1 := p.Y - 1; z1 <= p.Y + 1; z1++ {
        pos := block.Pos{int32(x1), 0, int32(z1)}
        height := g.w.HeightAt(pos)
        for y := height; y < bush_height + height; y++ {
          pos.Y = y
          if pos.InChunk(chunk_pos) {
            if y == bush_height + height - 1 {
              if g.rand.Intn(2) < 1 {
                c.SetBlock(pos.ChunkRelPos(), block.GetKind("minecraft:oak_leaves").DefaultType())
              }
            } else {
              // Bulk center of leaves
              c.SetBlock(pos.ChunkRelPos(), block.GetKind("minecraft:oak_leaves").DefaultType())
            }
          }
        }
      }
    }
  }
}

func (g *ForestGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  // Trees
  points := g.tree_grid.PointsInArea(
    int(chunk_pos.X) * 16      - 3,
    int(chunk_pos.Z) * 16      - 3,
    int(chunk_pos.X) * 16 + 16 + 3,
    int(chunk_pos.Z) * 16 + 16 + 3,
  )
  for _, p := range points {
    pos := block.Pos{int32(p.X), 0, int32(p.Y)}
    // Make sure the trunk is in this biome
    if g.w.BiomeAt(pos) != g {
      continue
    }
    pos.Y = g.w.HeightAt(pos)
    g.rand.Seed(g.seed ^ int64(p.X) ^ int64(p.Y))
    tree_height := int32(g.rand.Intn(3) + 4)
    leaf_height := int32(g.rand.Intn(2) + 1)
    SpawnOakTree(pos, tree_height, leaf_height, chunk_pos, c)
  }
}

func (g *ForestGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_above(0, pos, c)
}

func (g *ForestGenerator) fill_column_above(min_height int32, pos block.Pos, c *chunk.MultiChunk) {
  g.rand.Seed(g.seed ^ int64(pos.X) ^ int64(pos.Y))
  height := g.w.HeightAt(pos)
  p := pos.ChunkRelPos()
  p.Y = height
  c.Fill(
    p.WithY(min_height),
    p.WithY(height - 5),
    block.GetKind("minecraft:stone").DefaultType(),
  )
  c.Fill(
    p.WithY(height - 4),
    p.WithY(height - 1),
    block.GetKind("minecraft:dirt").DefaultType(),
  )
  c.SetBlock(p.WithY(height), block.GetKind("minecraft:grass_block").DefaultType())
  p.Y++
  if g.rand.Intn(10) < 1 {
    c.SetBlock(p, block.GetKind("minecraft:fern").DefaultType())
  } else if g.rand.Intn(5) < 1 {
    c.SetBlock(p, block.GetKind("minecraft:grass").DefaultType())
  } else if g.rand.Intn(100) < 1 {
    c.SetBlock(p, block.GetKind("minecraft:poppy").DefaultType())
  }
  if height < 16 {
    for y := height; y < 16; y++ {
      p.Y = y
      c.SetBlock(p, block.GetKind("minecraft:water").DefaultType())
    }
  }
}

func new_evergreen_generator(w *VoronoiWorldGenerator, seed int64) *EvergreenGenerator {
  g := EvergreenGenerator{}
  g.w = w
  g.seed = seed
  g.rand = rand.New(util.NewFastRand(seed))
  g.tree_grid = util.NewRandomGrid(g.rand, 6, 3)
  return &g
}

func (g *EvergreenGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height := g.w.MinHeight(chunk_pos)
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height - 5, 15},
    block.GetKind("minecraft:stone").DefaultType(),
  )
  chunk_pos.ForEachColumn(func(pos block.Pos) bool {
    g.fill_column_above(min_height, pos, c)
    return true
  })
}

func (g *EvergreenGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  // Trees
  points := g.tree_grid.PointsInArea(
    int(chunk_pos.X) * 16      - 5,
    int(chunk_pos.Z) * 16      - 5,
    int(chunk_pos.X) * 16 + 16 + 5,
    int(chunk_pos.Z) * 16 + 16 + 5,
  )
  for _, p := range points {
    pos := block.Pos{int32(p.X), 0, int32(p.Y)}
    // Make sure the trunk is in this biome
    if g.w.BiomeAt(pos) != g {
      continue
    }
    pos.Y = g.w.HeightAt(pos)
    g.rand.Seed(g.seed ^ int64(p.X) ^ int64(p.Y))
    tree_height := int32(g.rand.Intn(5) + 18)
    leaf_start := int32(g.rand.Intn(5) + 2)
    SpawnEvergreenTree(g.rand, pos, tree_height, leaf_start, chunk_pos, c)
  }
}

func (g *EvergreenGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_above(0, pos, c)
}

func (g *EvergreenGenerator) fill_column_above(min_height int32, pos block.Pos, c *chunk.MultiChunk) {
  height := g.w.HeightAt(pos)
  p := pos.ChunkRelPos()
  p.Y = height
  c.Fill(
    p.WithY(min_height),
    p.WithY(height - 5),
    block.GetKind("minecraft:stone").DefaultType(),
  )
  c.Fill(
    p.WithY(height - 4),
    p.WithY(height - 1),
    block.GetKind("minecraft:dirt").DefaultType(),
  )
  c.SetBlock(p.WithY(height), block.GetKind("minecraft:grass_block").DefaultType())
  p.Y++
  if g.rand.Intn(10) < 1 {
    c.SetBlock(p, block.GetKind("minecraft:fern").DefaultType())
  } else if g.rand.Intn(5) < 1 {
    c.SetBlock(p, block.GetKind("minecraft:grass").DefaultType())
  }
  if height < 16 {
    for y := height; y < 16; y++ {
      p.Y = y
      c.SetBlock(p, block.GetKind("minecraft:water").DefaultType())
    }
  }
}
