package terrain

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

type SolidGenerator struct {
  w *VoronoiWorldGenerator
  t *block.Type
}

func new_solid_generator(w *VoronoiWorldGenerator, t *block.Type) *SolidGenerator {
  g := SolidGenerator{}
  g.w = w
  g.t = t
  return &g
}

func (g *SolidGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height := g.w.MinHeight(chunk_pos)
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height, 15},
    g.t,
  )
  chunk_pos.ForEachColumn(func (pos block.Pos) bool {
    g.fill_column_above(min_height, pos, c)
    return true
  })
}

func (g *SolidGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {}

func (g *SolidGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_above(0, pos, c)
}

func (g *SolidGenerator) fill_column_above(min_height int32, pos block.Pos, c *chunk.MultiChunk) {
  height := g.w.HeightAt(pos)
  p := pos.ChunkRelPos()
  c.Fill(p.WithY(min_height), p.WithY(height), g.t)
}

type MemeGenerator struct {
  w *VoronoiWorldGenerator
  letters map[rune][][]*block.Type
}

func new_meme_generator(w *VoronoiWorldGenerator) *MemeGenerator {
  g := MemeGenerator{}
  g.w = w
  x := block.GetKind("minecraft:purple_wool").DefaultType()
  M := block.GetKind("minecraft:gold_block").DefaultType()
  g.letters = map[rune][][]*block.Type{
    'P': {
      { M, M, M, x },
      { M, x, M, x },
      { M, M, M, x },
      { M, x, x, x },
      { M, x, x, x },
      { x, x, x, x },
    },
    'O': {
      { M, M, M, x },
      { M, x, M, x },
      { M, x, M, x },
      { M, x, M, x },
      { M, M, M, x },
      { x, x, x, x },
    },
    'G': {
      { M, M, M, x },
      { M, x, x, x },
      { M, x, x, x },
      { M, x, M, x },
      { M, M, M, x },
      { x, x, x, x },
    },
  }
  return &g
}

func (g *MemeGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height := g.w.MinHeight(chunk_pos)
  min_height -= 1
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height, 15},
    block.GetKind("minecraft:stone").DefaultType(),
  )
  chunk_pos.ForEachColumn(func (pos block.Pos) bool {
    g.fill_column_above(min_height, pos, c)
    return true
  })
}

func (g *MemeGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {}

func (g *MemeGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_above(0, pos, c)
}

func (g *MemeGenerator) fill_column_above(min_height int32, pos block.Pos, c *chunk.MultiChunk) {
  height := g.w.HeightAt(pos)
  p := pos.ChunkRelPos()
  var t *block.Type
  x := (pos.X % 4 + 4) % 4
  z := (pos.Z % 6 + 6) % 6
  switch pos.X / 4 % 3 {
    case 0: t = g.letters['P'][z][x]
    case 1: t = g.letters['O'][z][x]
    case 2: t = g.letters['G'][z][x]
  }
  c.Fill(p.WithY(min_height), p.WithY(height), t)
}
