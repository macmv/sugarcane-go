package terrain

import (
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

func SpawnOakTree(pos block.Pos, tree_height, leaf_height int32, chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  height := pos.Y
  if pos.InChunk(chunk_pos) {
    p := pos.ChunkRelPos()
    for y := height; y < height + tree_height; y++ {
      // The trunk
      p.Y = y
      c.SetBlock(p, block.GetKind("minecraft:oak_log").DefaultType())
    }
  }
  // Leaves
  for y := tree_height + height - leaf_height; y < tree_height + height + 2; y++ {
    for x1 := pos.X - 2; x1 <= pos.X + 2; x1++ {
      for z1 := pos.Z - 2; z1 <= pos.Z + 2; z1++ {
        p := block.Pos{x1, y, z1}
        if p.InChunk(chunk_pos) {
          // Will pass this to c.SetBlock
          r := p.ChunkRelPos()
          if p.X == pos.X && p.Z == pos.Z {
            // Center column of leaves
            if y >= tree_height + height {
              c.SetBlock(r, block.GetKind("minecraft:oak_leaves").DefaultType())
            }
          } else if y == tree_height + height + 1 {
            // Top rectangle of the leaves
            if p.X >= pos.X - 1 && p.X <= pos.X + 1 && p.Z >= pos.Z - 1 && p.Z <= pos.Z + 1 &&
            (p.X != pos.X - 1 || p.Z != pos.Z - 1) &&
            (p.X != pos.X + 1 || p.Z != pos.Z - 1) &&
            (p.X != pos.X - 1 || p.Z != pos.Z + 1) &&
            (p.X != pos.X + 1 || p.Z != pos.Z + 1) {
              // TODO: Proper leaf check here
              if c.GetBlock(r) == block.GetKind("minecraft:spruce_leaves").DefaultType() {
                if rand.Intn(2) == 0 {
                  c.SetBlock(r, block.GetKind("minecraft:oak_leaves").DefaultType())
                }
              } else {
                c.SetBlock(r, block.GetKind("minecraft:oak_leaves").DefaultType())
              }
            }
          } else {
            // Bulk center of leaves
            if (p.X != pos.X - 2 || p.Z != pos.Z - 2) &&
            (p.X != pos.X + 2 || p.Z != pos.Z - 2) &&
            (p.X != pos.X - 2 || p.Z != pos.Z + 2) &&
            (p.X != pos.X + 2 || p.Z != pos.Z + 2) {
              // TODO: Proper leaf check here
              if c.GetBlock(r).Kind() == block.GetKind("minecraft:spruce_leaves") {
                if rand.Intn(2) == 0 {
                  c.SetBlock(r, block.GetKind("minecraft:oak_leaves").DefaultType())
                }
              } else {
                c.SetBlock(r, block.GetKind("minecraft:oak_leaves").DefaultType())
              }
            }
          }
        }
      }
    }
  }
}

func SpawnEvergreenTree(rand *rand.Rand, pos block.Pos, tree_height, leaves_start int32, chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  LOG := block.GetKind("minecraft:spruce_log").DefaultType()
  LEAVES := block.GetKind("minecraft:spruce_leaves").DefaultType()
  // Leaves. We fill each leaf clumn one by one, so we iterate through
  // all of the possible columns first.
  for x := pos.X - 6; x <= pos.X + 6; x++ {
    for z := pos.Z - 6; z <= pos.Z + 6; z++ {
      p := block.Pos{x, pos.Y + 1, z}
      if !p.InChunk(chunk_pos) {
        continue
      }
      if p.X == pos.X && p.Z == pos.Z {
        p = p.ChunkRelPos()
        // Trunk
        c.Fill(p, p.AddY(tree_height - 4), LOG)
        // Center column of leaves
        c.Fill(p.AddY(tree_height - 3), p.AddY(tree_height), LEAVES)
      } else {
        dist_to_center := p.Distance(pos)
        // Slope is the slope of the leaves. Randomness is added
        // to make the tree look more natural.
        // Changing 0.2 will effect the randomness of the slope.
        // Changing 3 will effect the base slope value.
        slope := 3 + (rand.Float32() - 0.5) * 0.2
        leaves_height := float32(tree_height) - dist_to_center * slope + 2
        leaves_height += rand.Float32() - 0.5
        p = p.ChunkRelPos()
        c.Fill(p.AddY(leaves_start), p.AddY(int32(leaves_height)), LEAVES)
      }
    }
  }
}
