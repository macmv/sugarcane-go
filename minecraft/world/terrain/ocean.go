package terrain

import (
  "math/rand"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

const OCEAN_DEPTH_MULTIPLIER = 2

type OceanGenerator struct {
  w *VoronoiWorldGenerator

  seed int64
  rand *rand.Rand

  seagrass_noise util.NoiseSampler
  sand_noise util.NoiseSampler
  clay_noise util.NoiseSampler
  dirt_noise util.NoiseSampler
}

func new_ocean_generator(w *VoronoiWorldGenerator, seed int64) *OceanGenerator {
  g := OceanGenerator{}
  g.w = w
  g.seed = seed
  source := util.NewFastRand(seed)
  g.rand = rand.New(source)
  g.seagrass_noise = util.NewNoise(source)
  g.sand_noise = util.NewNoise(source)
  g.clay_noise = util.NewNoise(source)
  g.dirt_noise = util.NewNoise(source)
  return &g
}

func (g *OceanGenerator) FillChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {
  min_height, max_height := g.min_max_height(chunk_pos)
  // The sand goes 5 blocks down from the minimum level
  min_height -= 5
  c.Fill(
    block.Pos{0, 0, 0},
    block.Pos{15, min_height, 15},
    block.GetKind("minecraft:stone").DefaultType(),
  )
  c.Fill(
    block.Pos{0, max_height, 0},
    block.Pos{15, g.w.SeaLevel, 15},
    block.GetKind("minecraft:water").DefaultType(),
  )
  chunk_pos.ForEachColumn(func(pos block.Pos) bool {
    g.fill_column_range(min_height, max_height, pos, c)
    return true
  })
}

func (g *OceanGenerator) min_max_height(chunk_pos block.ChunkPos) (int32, int32) {
  min_height := int32(256)
  max_height := int32(0)
  chunk_pos.ForEachColumn(func(pos block.Pos) bool {
    height := int32((g.w.ExactHeight(pos) - float64(g.w.SeaLevel)) * OCEAN_DEPTH_MULTIPLIER) + g.w.SeaLevel
    if height < min_height {
      min_height = height
    }
    if height > max_height {
      max_height = height
    }
    return true
  })
  return min_height, max_height
}

func (g *OceanGenerator) DetailChunk(chunk_pos block.ChunkPos, c *chunk.MultiChunk) {}

func (g *OceanGenerator) FillColumn(pos block.Pos, c *chunk.MultiChunk) {
  g.fill_column_range(0, g.w.SeaLevel, pos, c)
}

func (g *OceanGenerator) fill_column_range(min_y, max_y int32, pos block.Pos, c *chunk.MultiChunk) {
  y := int32((g.w.ExactHeight(pos) - float64(g.w.SeaLevel)) * OCEAN_DEPTH_MULTIPLIER) + g.w.SeaLevel
  if min_y < 0 {
    min_y = 0
  }
  if y < 5 {
    y = 5
  }
  p := pos.ChunkRelPos().WithY(y)

  // Underneath the sea floor
  c.Fill(p.WithY(min_y), p.AddY(-5), block.GetKind("minecraft:stone").DefaultType())

  // Sea floor
  floor := block.GetKind("minecraft:gravel").DefaultType()
  seagrass := true
  if g.sand_noise.Sample(float64(pos.X) / 8, float64(pos.Z) / 8) > 0.08 {
    floor = block.GetKind("minecraft:sand").DefaultType()
  } else if g.clay_noise.Sample(float64(pos.X) / 8, float64(pos.Z) / 8) > 0.08 {
    floor = block.GetKind("minecraft:clay").DefaultType()
    seagrass = false
  } else if g.dirt_noise.Sample(float64(pos.X) / 32, float64(pos.Z) / 32) > 0.1 {
    floor = block.GetKind("minecraft:dirt").DefaultType()
    seagrass = false
  }
  c.Fill(p.AddY(-4), p.AddY(-1), floor)

  // Seagrass/kelp
  g.rand.Seed(g.seed ^ (int64(pos.X) * int64(pos.Z) * 997))
  if seagrass && g.rand.Intn(3) < 1 && g.seagrass_noise.Sample(float64(pos.X) / 16, float64(pos.Z) / 16) > 0 {
    c.SetBlock(p, block.GetKind("minecraft:seagrass").DefaultType())
  } else {
    c.SetBlock(p, block.GetKind("minecraft:water").DefaultType())
  }

  // The rest of the water
  c.Fill(p.AddY(1), p.WithY(max_y), block.GetKind("minecraft:water").DefaultType())
}
