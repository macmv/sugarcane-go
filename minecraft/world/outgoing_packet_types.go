package world

import (
  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
)

func (w *World) GenerateNewChunkPacket(chunk *chunk.MultiChunk, pos block.ChunkPos, version block.Version) *packet.OutgoingPacket {
  proto := chunk.Serialize(version)
  // version was invalid
  if proto == nil {
    log.Warn("Tried to generate chunk data with invalid version: ", version)
    return nil
  }
  proto.X = pos.X
  proto.Z = pos.Z

  out := packet.NewOutgoingPacket(packet.Clientbound_ChunkData)
  out.SetOther(0, proto)
  return out
}

func (w *World) GenerateUpdateLightPacket(chunk *chunk.MultiChunk, pos block.ChunkPos, version block.Version) *packet.OutgoingPacket {
  packet := packet.NewOutgoingPacket(packet.Clientbound_UpdateLight)
  packet.SetInt(0, pos.X)
  packet.SetInt(1, pos.Z)
  mask := int32(0)
  for i := 0; i < 16; i++ {
    if chunk.HasChunkSection(int32(i)) {
      mask |= (1 << (i+1))
    }
  }
  // If there are chunk sections on the bottom or top, we will always send the lighting data
  // for the chunk below/above that, even if it isn't needed. Checking to see if the lighting
  // data is needed would be much slower.
  if chunk.HasChunkSection(0) {
    mask |= 1
  }
  if chunk.HasChunkSection(15) {
    mask |= (1 << 17)
  }
  // 14 is 32-18, and we want to only use the first 18 bits of each number
  packet.SetInt(2, mask    << 14 >> 14) // all sections that are needed are included for the sky light array
  packet.SetInt(3, 0       << 14 >> 14) // no sections are included for the block light array
  packet.SetInt(4, (^mask) << 14 >> 14) // all sections that are not needed have 0 sky light
  packet.SetInt(5, (^0)    << 14 >> 14) // all sections have 0 block light
  // The proxy assumes that everything is full bright, so it reads the mask above, and uses it to generate the light arrays.
  // This means that all sky light values are always 15 (in the needed chunk sections), and block light values are always 0.
  return packet
}

func GeneratePlayerInfoPacket(players []*player.Player, action int32) *packet.OutgoingPacket {
  packet := packet.NewOutgoingPacket(packet.Clientbound_PlayerInfo)
  packet.SetInt(0, action)
  packet.SetInt(1, int32(len(players)))
  buf := util.NewBuffer()
  for _, player := range players {
    buf.WriteUUID(player.UUID)
    if action == 0 { // adding players to list
      buf.WriteString(player.Name())
      buf.WriteVarInt(0) // number of properties (textures and stuff. textures kind of work if you just leave this blank)
      buf.WriteVarInt(0) // gamemode
      buf.WriteVarInt(0) // ping
      buf.WriteBool(false) // has display name
    } else if action == 1 { // update gamemode
      buf.WriteVarInt(0) // gamemode
    } else if action == 2 { // update latency
      buf.WriteVarInt(0) // ping
    } else if action == 3 { // update display name
      buf.WriteBool(false) // has display name
    } else if action == 4 { // remove player
      // no fields
    } else {
      panic("Invalid action!")
    }
  }
  packet.SetByteArray(0, buf.GetData())
  return packet
}

