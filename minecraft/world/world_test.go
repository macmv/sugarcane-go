package world_test

import (
  "os"
  "testing"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/vanilla/block"
)

func BenchmarkGeneration(b *testing.B) {
  vanilla.RegisterBlocks()
  block.LoadBlocks()
  for i := 0; i < b.N; i++ {
    log := util.NewLog(os.Stdout)
    w := world.NewWorldManager(log, nil, "", "world")
    w.AddWorld("world")
    w.DefaultWorld().LockSettings()
  }
}
