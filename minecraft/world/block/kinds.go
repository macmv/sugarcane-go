package block

import (
  "fmt"
  "sort"
  "strings"
)

// singleton struct for each block type (not each block state)
type Kind struct {
  // The register used with this kind. It is what
  // gives all the functionality to the block.
  reg Register
  // Keys are the possible proerties, values are
  // a map that just contains the keys of the
  // possible values you can use for that value
  properties map[string]map[string]struct{}
  // This is a map of property keys, to possible
  // property values, in order.
  ordered_properties map[string][]string
  // List of all types, in the order defined by ordered_properties.
  ordered_types []*Type
  // A map of possible block states for this block.
  // Each key is the block state, and the value is
  // a block type. The properties of that block should
  // keyed with a key of properties, and the values
  // should match the properties.
  states map[uint32]*Type
  // Map of property string to id
  reverse_states map[string]uint32
  // The state listed as "default" by the json file.
  // This is the type that is chosen when a block is
  // created with /setblock kind.
  default_state uint32
}

// Map of names to block kinds. Names
// should be formatted namespace:block.
var block_kinds map[string]*Kind
// Ordered list of block states. Never
// changed after the game has loaded.
// Unless the clients have a modded
// client, this shouldn't ever change.
var block_types []*Type
// List of past versions, and their block
// states. Never changed after the game
// has loaded. Unless the clients have a
// modded client, this shouldn't ever change.
var block_versions map[Version][]*Type
// Used as the default block. Should always
// reference the block with state 0.
var air *Type

// The default block in the game. The type
// returned can be compared against any block
// type to check if it's air.
func Air() *Type {
  return air
}

// This loads all blocks in the game, using
// the registry from CreateRegistry(). Once
// called, you cannot change the blocks anymore.
func LoadBlocks() {
  // block_versions = make(map[Version]map[uint32]*Type)
  // block_versions[V1_8] = make(map[uint32]uint32)
  apply_registry()
  // print_prop_types()
  Validate(VLATEST)
  apply_old_versions()
  // print_versions()
  // load_blocks_from_file(base_dir, base_url, "1.15.2/blocks.json", 152) // 152 is 1.15.2
  // add_version_from_file(base_dir, base_url, "1.8/blocks.json", 8)      // 8 is 1.8.* (all 1.8 subversions are identical)
}

func print_prop_types() {
  // for name := range prop_types {
  //   // I was testing out ladders, as they have a facing and waterlogged property.
  //   check := "minecraft:ladder"
  //   if len(name) > len(check) && name[:len(check)] == check {
  //     fmt.Println("Prop type:", name)
  //   }
  // }
}

func print_kinds() {
  fmt.Println("Kinds           ------------")
  for id := 0; id < len(block_types); id++ {
    if id > 2000 {
      break
    }
    t := block_types[uint32(id)]
    if t.properties == nil {
      fmt.Println(id, "->", t.kind.Name())
    } else {
      fmt.Println(id, "->", t.kind.Name(), t.properties)
    }
  }
  fmt.Println("End of kinds    ------------")
}

func print_versions() {
  fmt.Println("Versions        ------------")
  for o, t := range block_versions[V1_13] {
    if t != nil {
      fmt.Println("BS:", t, "->", "V1_13:", o, "(", t.kind.Name(), ")")
    }
  }
  fmt.Println("End of versions ------------")
}

// Returns a kind from a name. Will return
// nil if the block does not exist. Will
// panic if LoadBlocks() has not been called
// yet. If you need the info about another
// block during load, call registry.Get().
func GetKind(name string) *Kind {
  if block_types == nil {
    panic("Please call LoadBlocks first!")
  }
  k, _ := block_kinds[name]
  return k
}

// Returns the name of the kind. This will
// call the registry's Name() function, which
// should always return the same value.
func (k *Kind) Name() string {
  return k.reg.Name()
}

// This returns a map of all state ids to types.
// This will copy the internal states map, so
// it is safe to edit. However, this function
// is decently slow, especially for blocks like
// redstone, which can have thousands of states.
func (k *Kind) Types() map[uint32]*Type {
  types := make(map[uint32]*Type)
  for id, t := range k.states {
    types[id] = t
  }
  return types
}

// This returns the default type of the kind.
// If the kind is nil, this will return air.
func (k *Kind) DefaultType() *Type {
  if k == nil { return Air() }
  return k.states[k.default_state]
}

// This returns the default properties for the kind.
// This will copy all the properties, but because
// properties is usually a pretty short list, this
// is still fast.
func (k *Kind) DefaultProperties() map[string]string {
  if k.states == nil {
    return nil
  }
  new_map := make(map[string]string)
  for key, val := range k.states[k.default_state].properties {
    new_map[key] = val
  }
  return new_map
}

// This returns the block type based on a list of properties
// If the passed map is incomplete, it will use default values
// for the remaining fields. If there is only one state, it will
// return that. This will never return nil.
func (k *Kind) Type(properties map[string]string) *Type {
  if k.states == nil {
    return k.states[k.default_state]
  }
  def := k.states[k.default_state]
  // This is a map of all properties that the type will have
  prop_map := make(map[string]string)
  for key, _ := range k.properties {
    val, ok := properties[key]
    if !ok {
      // If not specified, use the default value
      val = def.Property(key)
    }
    prop_map[key] = val
  }

  keys := make([]string, len(prop_map))
  i := 0
  for k, _ := range prop_map {
    keys[i] = k
    i++
  }
  sort.Strings(keys)
  // Now that we have a sorted list of keys, we generate the key string
  str := ""
  for _, k := range keys {
    str += " " + k + "=" + prop_map[k]
  }

  return TypeFromID(k.reverse_states[str])
}

// This returns all of the possible properties for the type.
// This does not return a copy, so editing this map will break
// things in terrible and unexpected ways.
func (k *Kind) AllProperties() map[string]map[string]struct{} {
  return k.properties
}

// This returns a list of new kinds mapped to old kinds.
// Used for generating the Versions map within each type.
// The old type should have either the same properties, or
// a shorter set of properties. This will panic if there
// is a state in old_kind that does not exist in k.
// The reason this is so complicated is because of things
// like noteblocks: in 1.14, they added a bunch of instruments,
// which inserted a bunch of new states in between all of the
// old ones.
func (k *Kind) find_common_states(new_kind *Kind) []struct{new_type, old_type *Type} {
  // Map of prop strings to new types
  types := make([]struct{new_type, old_type *Type}, len(new_kind.states))
  old_index := 0
  for i := 0; i < len(new_kind.states); i++ {
    new_type := new_kind.ordered_types[i]
    old_type := k.ordered_types[old_index]
    if new_type.properties_match(old_type) {
      old_index++
      if old_index >= len(k.states) {
        old_index = len(k.states) - 1
      }
    } else {
      old_type = k.DefaultType()
    }
    types[i].new_type = new_type
    types[i].old_type = old_type
  }
  return types
}

// This applies all of the versions that this type defines to
// the block_versions map.
func (k *Kind) apply_versions() {
  for version, m := range k.reg.Versions() {
    // Means there is only 1 id for this entire kind
    id, ok := m[""]
    if ok {
      for _, t := range k.Types() {
        t.add_version(version, id)
      }
    } else {
      for prop_string, id := range m {
        props := make(map[string]string)
        sections := strings.Split(prop_string, " ")
        for _, sec := range sections {
          // This is so that we can line up the values in the source code, and make it look nice
          if sec == "" { continue }
          parts := strings.Split(sec, "=")
          // If len(parts) < 2, we want this to fail
          props[parts[0]] = parts[1]
        }
        for _, t := range k.states {
          matches := true
          for key, val := range t.properties {
            for k, v := range props {
              if k == key && v != val {
                matches = false
                break
              }
            }
            if !matches {
              break
            }
          }
          if matches {
            // This will panic if we call it multiple times, so we don't need to bother verifying the data ourselves.
            t.add_version(version, id)
          }
        }
      }
    }
  }
}

// Will panic if there is a missing value for one of the versions.
// Used when adding blocks, to make sure nothing was missed.
func (k *Kind) verify_version(version Version) {
  // if it has at least one of that version
  has_one_version := false
  for _, t := range k.Types() {
    // Means r.versions[version] has been set.
    // If it is air, then there should only be one state.
    if t.versions[version] != 0 {
      has_one_version = true
    } else {
      if has_one_version {
        if t.properties != nil {
          panic(fmt.Sprintf("%s with properties %v does not have an id for version %d", k.Name(), t.properties, version))
        } else {
          panic(fmt.Sprintf("%s does not have an id for version %d", k.Name(), version))
        }
      }
    }
  }
}
