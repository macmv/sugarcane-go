package block

type Face int

const (
  FACE_BOTTOM Face = iota
  FACE_TOP
  FACE_NORTH
  FACE_SOUTH
  FACE_WEST
  FACE_EAST
)

// This gets the block face according to a string.
// Will return FACE_BOTTOM if the string is invalid.
func FaceFromString(s string) Face {
  switch s {
  case "bottom":
    return FACE_BOTTOM
  case "top":
    return FACE_TOP
  case "north":
    return FACE_NORTH
  case "south":
    return FACE_SOUTH
  case "west":
    return FACE_WEST
  case "east":
    return FACE_EAST
  default:
    return FACE_BOTTOM
  }
}

// This returns the string name of the face.
// This will return an empty string if invalid.
func (f Face) String() string {
  switch f {
  case FACE_BOTTOM:
    return "bottom"
  case FACE_TOP:
    return "top"
  case FACE_NORTH:
    return "north"
  case FACE_SOUTH:
    return "south"
  case FACE_WEST:
    return "west"
  case FACE_EAST:
    return "east"
  default:
    return ""
  }
}

// This returns the block offset of the face.
// For example, the bottom face returns 0, -1, 0.
// This returns 0, 0, 0 if the face is invalid.
func (f Face) Offset() (int32, int32, int32) {
  switch f {
  case FACE_BOTTOM:
    return 0, -1, 0
  case FACE_TOP:
    return 0, 1, 0
  case FACE_NORTH:
    return 0, 0, -1
  case FACE_SOUTH:
    return 0, 0, 1
  case FACE_WEST:
    return -1, 0, 0
  case FACE_EAST:
    return 1, 0, 0
  default:
    return 0, 0, 0
  }
}

// The returns the opposite of the face.
// For example, bottom returns top.
// This returns the bottom face if the face is invalid.
func (f Face) Invert() Face {
  switch f {
  case FACE_BOTTOM:
    return FACE_TOP
  case FACE_TOP:
    return FACE_BOTTOM
  case FACE_NORTH:
    return FACE_SOUTH
  case FACE_SOUTH:
    return FACE_NORTH
  case FACE_WEST:
    return FACE_EAST
  case FACE_EAST:
    return FACE_WEST
  default:
    return FACE_BOTTOM
  }
}

// This gets the face that is one to the left of the given face.
// For example, north returns west.
// Bottom and top return themselves.
func (f Face) Left() Face {
  switch f {
  case FACE_NORTH:
    return FACE_WEST
  case FACE_WEST:
    return FACE_SOUTH
  case FACE_SOUTH:
    return FACE_EAST
  case FACE_EAST:
    return FACE_NORTH
  default:
    return f
  }
}
// This gets the face that is one to the right of the given face.
// For example, north returns east.
// Bottom and top return themselves.
func (f Face) Right() Face {
  switch f {
  case FACE_NORTH:
    return FACE_EAST
  case FACE_EAST:
    return FACE_SOUTH
  case FACE_SOUTH:
    return FACE_WEST
  case FACE_WEST:
    return FACE_NORTH
  default:
    return f
  }
}

func (f Face) Valid() bool {
  return f >= 0 && f < 6
}
