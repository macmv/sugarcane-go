package block

import (
  "fmt"
  "errors"
  "strings"
)

// Block pos needs to be relative to a chunk.
type set_type_func func(pos Pos, t *Type)

// one for each block in the world
type State struct {
  set_type set_type_func
  pos Pos
  block_type *Type
}

func NewState(set_type set_type_func, pos Pos, block_type *Type) *State {
  if block_type == nil {
    panic("Passed nil block_type into NewState!")
    return nil
  }
  s := State{}
  s.set_type = set_type
  s.pos = pos
  s.block_type = block_type
  return &s
}

func NewStateFromID(set_type set_type_func, pos Pos, id uint32) *State {
  s := State{}
  s.set_type = set_type
  s.pos = pos
  s.block_type = TypeFromID(id)
  return &s
}

func (s *State) X() int32 {
  return s.pos.X
}
func (s *State) Y() int32 {
  return s.pos.Y
}
func (s *State) Z() int32 {
  return s.pos.Z
}
func (s *State) Pos() Pos {
  return s.pos
}

func (s *State) SetType(new_type *Type) error {
  if new_type == nil {
    return errors.New("Nil block type!")
  }
  s.block_type = new_type
  s.update_id()
  return nil
}

func (s *State) SetProperty(key string, val string) error {
  s.block_type = s.block_type.get_other_type_from_property(key, val)
  s.update_id()
  return nil
}

func (s *State) SetProperties(new_props map[string]string) error {
  fmt.Println("Prop map: ", new_props)
  fmt.Println("Previous block type: ", s.block_type)
  s.block_type = s.block_type.kind.Type(new_props)
  fmt.Println("New block type: ", s.block_type)
  s.update_id()
  return nil
}


func (s *State) Type() *Type {
  return s.block_type
}

func (s *State) Properties() map[string]string {
  return s.block_type.properties
}

func (s *State) AllProperties() map[string]map[string]struct{} {
  return s.block_type.kind.AllProperties()
}

func (s *State) State() uint32 {
  return s.block_type.State()
}

func (s *State) StateVersion(version Version) uint32 {
  return s.block_type.StateVersion(version)
}

// the format minecraft:chest[facing=west]
func (s *State) SetFromCommandString(name string) {
  sections := strings.Split(name, "[")
  s.block_type = GetKind(sections[0]).DefaultType()
  s.update_id()
}

func (s *State) update_id() {
  // The set_id function takes chunk coordinates, so we need to call ChunkRelPos().
  s.set_type(s.Pos().ChunkRelPos(), s.block_type)
}

