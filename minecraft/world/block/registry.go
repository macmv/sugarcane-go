package block

import (
  "fmt"
  "strings"
)

// This is a global block registry. It is a small wrapper over VersionRegistry,
// which allows a registry to be copied to be used for a certain version.
type Registry struct {
  // All versions of the block data.
  versions map[Version]*VersionRegistry
}

var block_registry *Registry

// This creates a new block registry.
func NewRegistry() *Registry {
  r := &Registry{
    versions: make(map[Version]*VersionRegistry),
  }
  for version := V1_13; version <= VLATEST; version++ {
    r.versions[version] = NewVersionRegistryFrom(r.versions[version - 1])
  }
  return r
}

type VersionRegistry struct {
  // List of registries to copy every to.
  clone_to []*VersionRegistry
  // List of registered items
  items []*register
  // Map of names to item indices
  names map[string]int32
}

type register struct {
  // Registered item
  item Register
  // Name of item
  name string
}

// This creates a registry for a single version. If other is non-nil,
// then any actions made to other will also be applied to this version.
// So, if you have a 1.13 registry, then you can create a NewVersionRegistryFrom(v1.13)
// to create a 1.14 registry. Now, you can register things to the 1.13
// registry, and they will be added to the 1.14 registry as well.
// Also, multiple registries may be based off of one version.
func NewVersionRegistryFrom(other *VersionRegistry) *VersionRegistry {
  r := &VersionRegistry{
    items: make([]*register, 0),
    names: make(map[string]int32),
  }
  if other != nil {
    other.clone_to = append(other.clone_to, r)
  }
  return r
}

// This registers a new item. id should be a constant, defined in user-space
// code (probably with iota). id is not used for sorting, the order of items
// depends on the order that Add is called. So, for packets, ID would be the
// grpc packet id, which doesn't need to be the same as any minecraft packet
// id. If id is -1, then this is treated as a placeholder, and nil will be
// inserted into the internal array.
func (r *VersionRegistry) Add(name string, item Register) {
  for _, o := range r.clone_to {
    o.Add(name, item)
  }
  // If this is true, then this is a placeholder packet.
  // So, we don't want to add it to packet_map
  if name == "" {
    r.items = append(r.items, nil)
    return
  }
  item.SetName(name)
  // Must stay in this order!
  r.names[name] = int32(len(r.items))
  r.items = append(r.items, &register{item, name})
}

// This inserts a new item at index, shifting all packets with
// a greater index down by one. Useful if a new version adds an item
// near the beginning, and you need to shift all the other items
// down by one.
//
// Index should be the old item id (say, and old block id), and id
// should be the constant defined in user space code (a custom constant refering to that block).
// The old ids greater than index will all be shifted down by one, and
// then this new item will be inserted. The new ids will not be modified.
func (r *VersionRegistry) insert(index int32, name string, item Register) {
  for _, o := range r.clone_to {
    o.insert(index, name, item)
  }
  // Increase length by one.
  r.items = append(r.items, nil)
  // Shift all the packets down by one.
  copy(r.items[index+1:], r.items[index:])
  for name, i := range r.names {
    if i >= index {
      // Increase all the indecies to match up with r.items
      r.names[name]++
    }
  }
  // If this is true, then we are inserting a placeholder packet.
  // This means we do not want to add it to packet_map.
  if name == "" {
    r.items[index] = nil
    return
  }
  item.SetName(name)
  // Insert the new packet.
  r.names[name] = index
  r.items[index] = &register{item, name}
}

// This inserts item after name in the list. The item's name will be new_name.
func (r *VersionRegistry) InsertAfter(name, new_name string, item Register) {
  index, ok := r.names[name]
  if !ok {
    return
  }
  // After means + 1
  r.insert(index + 1, new_name, item)
}

// Will remove an item from the registry. This will decrease all items that
// have a greater index down by one.
func (r *VersionRegistry) Remove(name string) {
  for _, o := range r.clone_to {
    o.Remove(name)
  }
  index := r.names[name]
  // Shift all the packets down by one.
  copy(r.items[index:], r.items[index+1:])
  for name, i := range r.names {
    if i > index {
      // Decrease all the indecies to match up with r.items
      r.names[name]--
    }
  }
  // Remove the element
  delete(r.names, name)
  r.items = r.items[:len(r.items)-1]
}

// This moves an item around within the registry. Useful for when
// a packet changes id, or an entity changes name. The first argument
// is the user defined id that you want to access. The second argument
// is the new index you would like to move that item to. This does the
// same thing as Remove() then Insert(), but it is more efficient.
func (r *VersionRegistry) move(name string, new_index int32) {
  for _, o := range r.clone_to {
    o.move(name, new_index)
  }
  current_index := r.names[name]
  item := r.items[current_index]
  if new_index > current_index {
    // We are moving the item downwards in the list, so we want to
    // shift everything between current and new up by one.
    copy(r.items[current_index:], r.items[current_index+1:new_index+1])
    for name, index := range r.names {
      if index > current_index && index <= new_index {
        r.names[name]--
      }
    }
    // Insert the item
    r.items[new_index] = item
    r.names[item.name] = new_index
  } else if new_index < current_index {
    // We are moving the item upwards in the list, so we want to
    // shift everything between new and current down by one.
    copy(r.items[new_index+1:], r.items[new_index:current_index])
    for name, index := range r.names {
      if index >= new_index && index < current_index {
        r.names[name]++
      }
    }
    // Insert the item
    r.items[new_index] = item
    r.names[item.name] = new_index
  }
  // If new_index and current_index were the same, this is a nop.
}

// This moves the item at name after new_name in the list.
func (r *VersionRegistry) MoveAfter(name, new_name string) {
  new_index, ok := r.names[new_name]
  if !ok { return }
  prev_index, ok := r.names[name]
  if !ok { return }
  if new_index > prev_index {
    r.move(name, new_index)
  } else {
    r.move(name, new_index + 1)
  }
}

// This overrides an item. Useful for if a packet slightly changes between versions.
//
// The old id is infered, since this should only be used to override packets.
// If you call this function, and id is not in the registry, it will panic.
// To add a new item, use Add or Insert instead.
func (r *VersionRegistry) Set(name string, item Register) {
  for _, o := range r.clone_to {
    o.Set(name, item)
  }
  index := r.names[name]
  if name == "" {
    delete(r.names, name)
    r.items[index] = nil
    return
  }
  item.SetName(name)
  // Sets the new item.
  r.items[index] = &register{item, name}
}

// This gets the item at the id, and panics if the id is not within the registry.
func (r *VersionRegistry) Get(name string) (item Register) {
  v, ok := r.GetOk(name)
  if !ok {
    panic("The name " + name + " is not within this registry!")
  }
  return v
}

// This gets the item at the id, and returns false if the id is not within the registry.
// This does not return the internal index, as that value has no meaning.
func (r *VersionRegistry) GetOk(name string) (item Register, ok bool) {
  index, ok := r.names[name]
  if !ok {
    return nil, false
  }
  return r.items[index].item, ok
}

// This lists all elements added to the registry, in the order that they were addded.
func (r *VersionRegistry) List() []Register {
  list := make([]Register, len(r.items))
  for i, item := range r.items {
    if item == nil {
      list[i] = nil
    } else {
      list[i] = item.item
    }
  }
  return list
}

func (r *VersionRegistry) String() string {
  var b strings.Builder
  fmt.Fprintf(&b, "Registry {\n")
  for index, item := range r.items {
    if item == nil {
      fmt.Fprintf(&b, "  Index: 0x%x: Placeholder <nil>\n", index)
    } else {
      fmt.Fprintf(&b, "  Index: 0x%x: Name: %s, Value: %T\n", index, item.name, item.item)
    }
  }
  fmt.Fprintf(&b, "}\n")
  return b.String()
}

func (r *VersionRegistry) Validate() error {
  for index, item := range r.items {
    if item == nil {
      continue
    }
    if r.names[item.name] != int32(index) {
      return fmt.Errorf(
        "ID map does not match r.items! id 0x%x should have index 0x%x, but id map shows 0x%x",
        item.name,
        index,
        r.names[item.name],
      )
    }
  }
  return nil
}

func (r *VersionRegistry) Generate() (map[string]*Kind, []*Type) {
  kinds := make(map[string]*Kind)
  types := []*Type{}
  // prop_types := make(map[string]*Type)
  state_id := uint32(0)

  for _, item := range r.items {
    if item == nil {
      continue
    }
    k := &Kind{}
    reg := item.item
    k.reg = reg
    props := reg.Properties()
    k.states = make(map[uint32]*Type)
    k.ordered_types = []*Type{}

    if len(props) == 0 {
      k.properties = nil
      k.default_state = state_id
      t := &Type{kind: k, properties: nil, state: state_id}
      types = append(types, t)
      // prop_types[t.PropString()] = t
      k.states[state_id] = t
      k.ordered_types = append(k.ordered_types, t)
      state_id++
    } else {
      k.properties = make(map[string]map[string]struct{})
      k.ordered_properties = make(map[string][]string)

      for _, p := range props {
        this_prop := make(map[string]struct{})
        arr := make([]string, len(p.Values()))
        for i, v := range p.Values() {
          this_prop[v] = struct{}{}
          arr[i] = v
        }
        k.properties[p.Name()] = this_prop
        k.ordered_properties[p.Name()] = arr
      }

      // each element in possible_states is an index into the states
      // possible states is a list of all pooossible combinations of indices
      possible_states := make([]int, len(props))
      completed := true
      default_props := reg.DefaultProperties()
      for completed {
        state_map := make(map[string]string)
        matches := true
        for key_index, val_index := range possible_states {
          p := props[key_index]
          val := p.Values()[val_index]
          state_map[p.Name()] = val
          if matches && val != default_props[p.Name()] {
            matches = false
          }
        }
        if matches {
          k.default_state = state_id
        }
        t := &Type{kind: k, properties: state_map, state: state_id}
        types = append(types, t)
        // prop_types[t.PropString()] = t
        k.states[state_id] = t
        k.ordered_types = append(k.ordered_types, t)
        state_id++
        completed = false
        for i, p := range props {
          possible_states[i]++
          if possible_states[i] >= p.Size() {
            possible_states[i] = 0
          } else {
            completed = true
            break
          }
        }
      }
    }
    kinds[k.Name()] = k
  }
  return kinds, types
}

// This adds a new block to the registry. The version should be a version
// >= 1.13. The order of Add calls only matters for the order of the block
// data. If you add a block to 1.14, then another to 1.13, the one from
// 1.13 will carry over to 1.14.
func (r *Registry) Add(version Version, name string, item Register) {
  r.versions[version].Add(name, item)
}

// This gets an item from the registry. The version can be any valid version, and
// this will act the same for all versions, with one caveat: if you call Update(),
// then this will return different items for different versions. Other than that,
// the version does not matter for this call.
func (r *Registry) Get(version Version, name string) Register {
  return r.versions[version].Get(name)
}

// This updates a block. For example, noteblocks got some more notes in 1.14. So,
// you might call Add(1.13, "noteblock", old_data), and then Update(1.14, "noteblock", new_data).
func (r *Registry) Update(version Version, name string, item Register) {
  r.versions[version].Set(name, item)
}

// Generates a kinds and types map. This is what is used to convert all registered
// blocks into a usable blockstate map.
func (r *Registry) Generate() (map[string]*Kind, []*Type, map[Version][]*Type) {
  new_kinds, new_types := r.versions[VLATEST].Generate()

  versions := make(map[Version][]*Type)
  for version, reg := range r.versions {
    // Skip the latest version, as we don't need
    // to map that.
    if version == VLATEST {
      continue
    }
    old_kinds, old_types := reg.Generate()
    version_list := []*Type{}
    for name, old_kind := range old_kinds {
      // Blocks are almost never removed; the ones that
      // are removed are not implemented.
      new_kind, ok := new_kinds[name]
      if !ok {
        panic("Unable to find new kind with name " + name)
      }
      // Means there is only one state
      if new_kind.states == nil {
        new_type := new_types[new_kind.default_state]
        old_type := old_types[old_kind.default_state]
        if new_type.versions == nil {
          // The length can be VLATEST, as VLATEST will never
          // be in the versions list.
          new_type.versions = make([]uint32, VLATEST)
        }
        new_type.versions[version] = old_type.state
        if old_type.state >= uint32(len(version_list)) {
          for i := uint32(len(version_list)); i <= old_type.state; i++ {
            version_list = append(version_list, nil)
          }
        }
        version_list[old_type.state] = new_type
        continue
      }
      for _, types := range old_kind.find_common_states(new_kind) {
        new_type := types.new_type
        old_type := types.old_type
        if new_type.versions == nil {
          // The length can be VLATEST, as VLATEST will never
          // be in the versions list.
          new_type.versions = make([]uint32, VLATEST)
        }
        new_type.versions[version] = old_type.state
        if old_type.state >= uint32(len(version_list)) {
          for i := uint32(len(version_list)); i <= old_type.state; i++ {
            version_list = append(version_list, nil)
          }
        }
        version_list[old_type.state] = new_type
      }
    }
    versions[version] = version_list
  }

  return new_kinds, new_types, versions
}

// From here on in the file is all of the static stuff.
// These functions handle singletons, such as the main block registry.
// However, you can also create your own registry (for whatever reason)
// with the functions above.

// This sets the main block registry to a new registry. If there is
// already a registry, it will be overriden.
func CreateRegistry() *Registry {
  // if block_registry != nil {
  //   panic("A block registry already exists!")
  // }
  block_registry = NewRegistry()
  return block_registry
}
