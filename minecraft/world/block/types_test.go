package block_test

import (
  "fmt"
  "testing"
  "math/rand"
  "encoding/hex"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  "github.com/stretchr/testify/assert"
)

// On my slow-ish laptop, this test ends up resulting in times of ~750 nano seconds. This is
// acceptable, and is very similar to what would be happening in a reverse type look up.
// With reverse type look ups, we need to have access to a map keyed by a property map. Go
// maps cannot be used, as they are pointers. So this test is to see if generating a string
// each time is worth it. It looks like this will be more than fast enough, as generating strings
// is not too slow. This test is for both map look up speed and string generation speed.
// If I wanted this to be faster, I would use some exponent math on the number of possible
// values of each property, but that is unnecessary optimization. Making a large table of all
// properties (in string form) to all types seems to be the best way to do this (not fastest,
// but by far fast enough).
func BenchmarkLookup(b *testing.B) {
  // "minecraft:redstone south=false west=true" is 40 characters long. This is a good test of
  // the property map, as that is the average length of one of these strings.
  length := 40
  fmt.Println(length)
  b.Run("Map string lookup", func(b *testing.B) {
    m := make(map[string]int)
    for i := 0; i < 10000; i++ {
      str := make([]byte, length/2)
      rand.Read(str)
      // Random length character strings
      m[hex.EncodeToString(str)] = i
    }
    b.ResetTimer()
    for i := 0; i < b.N; i++ {
      str := ""
      for j := 0; j < (length/2)/4; j++ {
        str += hex.EncodeToString([]byte{
          byte(rand.Int()),
          byte(rand.Int()),
          byte(rand.Int()),
          byte(rand.Int()),
        })
      }
      v, ok := m[str]
      if ok { fmt.Println(v) }
    }
  })
}

func init() {
  // Init
  reg := block.CreateRegistry()

  // Block 0
  kind := block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 0 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:block_0", kind)

  // Block 1
  kind = block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 1 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:stone", kind)

  // Block 2
  kind = block.NewRegister()
  kind.Settings(block.Settings{
    Versions: block.VersionsMap{
      block.V1_8: {
        "": 2 << 4 | 0,
      },
    },
  })
  reg.Add("minecraft:dirt", kind)

  // Finalize
  block.LoadBlocks()
}

func TestVersions(t *testing.T) {
  air := block.Air()

  assert.Equal(t, air, block.GetKind("minecraft:block_0").DefaultType(), "block.Air() should be block 0")

  stone := block.GetKind("minecraft:stone").DefaultType()
  assert.Equal(t, stone.State(), uint32(1), "It should give stone the correct state")
  assert.Equal(t, stone, block.TypeFromID(1), "TypeFromID should get the correct type")
  assert.Equal(t, stone, block.TypeFromIDVersion(1, block.V1_15_2), "TypeFromIDVersion (with 1.15.2) should get the correct type")
  assert.Equal(t, stone, block.TypeFromIDVersion(1 << 4, block.V1_8), "TypeFromIDVersion (with 1.8) should get the correct type")
}
