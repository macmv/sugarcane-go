package block

import (
  "os"
  "fmt"
  "encoding/json"
)

type BurgerData struct {
  // List of all blocks in the game
  Blocks struct {
    // Alphebetical list of blocks. Most useful option, as it gives all blockstate ids.
    BlockList map[string]struct {
      // Display name
      DisplayName string `json:"display_name"`
      // Block name
      Name string `json:"text_id"`
      // Hardness
      Hardness float32 `json:"hardness"`
      // Resistance
      Resistance float32 `json:"resistance"`
      // Min blockstate id
      MinState int32 `json:"min_state_id"`
      // Max blockstate id
      MaxState int32 `json:"max_state_id"`
      // max - min + 1
      NumStates int32 `json:"num_states"`
      // List of properties
      Properties []struct {
        // Property name
        Name string `json:"name"`
        // Property type. If it is int, then the
        // possible values are just 0-num_values.
        // If it is bool, then the possible values
        // are true, false. If it is anything else,
        // then the possible values are listed under
        // Values.
        Type string `json:"type"`
        // Number of possible values this property can have.
        NumValues int32 `json:"num_values"`
        // List of possible values. May be empty,
        // depending on Type.
        Values []string `json:"values"`
      } `json:"states"`
    } `json:"block"`
  } `json:"blocks"`
}

// This reads from data/{v}.json, and then validates that against the registered blocks.
// Useful for implementing new versions. This migh panic, if the json file does not exist.
// If it can load the file, then it will print a lot of stuff to stdout.
func Validate(v Version) {
  data := load_burger(v)
  if data == nil { return }
  // List of expected block names at each state. Does not include property data.
  expected_states := []string{}
  for _, b := range data.Blocks.BlockList {
    for b.MaxState >= int32(len(expected_states)) {
      expected_states = append(expected_states, "")
    }
    for i := b.MinState; i <= b.MaxState; i++ {
      if expected_states[i] != "" {
        panic(fmt.Sprint("Block is already at id", i))
      }
      expected_states[i] = "minecraft:" + b.Name
    }
  }
  for i, name := range expected_states {
    if i > 5000 {
      break
    }
    got := TypeFromID(uint32(i)).Kind().Name()
    if got != name {
      before := TypeFromID(uint32(i) - 1).Kind().Name()
      fmt.Println("Name at index", i, "is incorrect! Expected:", name, "got:", got, "before:", before)
    }
  }
}

func load_burger(v Version) *BurgerData {
  f, err := os.Open("data/" + v.String() + ".json")
  if err != nil { fmt.Println(err); return nil }
  defer f.Close()

  data := []*BurgerData{}
  err = json.NewDecoder(f).Decode(&data)
  if err != nil { panic(err) }
  return data[0]
}
