package block

import (
  "fmt"
)

type VersionsMap map[Version]map[string]uint32

// This is a block versions. This is probably
// just an iota for every major version in the game.
// This allows older versions of blocks to be packed
// into an array, instead of a map to protocol versions.
type Version int32

const (
  V1_8    Version = iota
  // Will probably never be implemented.
  // V1_9
  // V1_10
  // V1_11
  V1_12
  V1_13
  V1_14
  V1_15
  V1_16

  VINVALID Version = -1

  // Latest block version to use. Should always map to a valid version.
  VLATEST Version = V1_16
)

func apply_old_versions() {
  for _, kind := range block_kinds {
    kind.apply_versions()
  }
  for _, kind := range block_kinds {
    // This first checks that at least one of it's type has a version for 1.8.
    // If it does, then it will panic if any of the rest of the types do not have an id for 1.8.
    // If it does not have any ids for 1.8, then it does nothing
    kind.verify_version(V1_8)
    kind.verify_version(V1_12)
  }
}

func (v Version) String() string {
  switch v {
    case V1_8   : return "1.8"
    case V1_12  : return "1.12"
    case V1_13  : return "1.13"
    case V1_14  : return "1.14"
    case V1_15  : return "1.15"
    case V1_16  : return "1.16"
    default:      return fmt.Sprint("Invalid block version ", int32(v))
  }
}
