package block

import (
  "fmt"
  "sort"
  "strings"
)

// One for each possible block state
type Type struct {
  kind *Kind
  properties map[string]string
  state uint32
  // A list where the index is a block version,
  // and the values are blockstate ids. Does
  // not include VLATEST.
  versions []uint32
}

func TypeFromID(id uint32) *Type {
  if id > uint32(len(block_types)) {
    return nil
  }
  return block_types[id]
}

func TypeFromIDVersion(id uint32, version Version) *Type {
  if version == VLATEST {
    return TypeFromID(id)
  }
  if id >= uint32(len(block_versions[version])) {
    return nil
  }
  return block_versions[version][id]
}

// Used by block states when they need to change a single property.
// What this does, is gets the current property map, then updates one field.
// With that new map, it then uses it's block kind to look up the new type.
func (t *Type) get_other_type_from_property(key, val string) *Type {
  new_properties := make(map[string]string)
  for k, v := range t.properties {
    new_properties[k] = v
  }
  new_properties[key] = val
  return t.kind.Type(new_properties)
}

func (t *Type) Kind() *Kind {
  if t == nil { return Air().Kind() }
  return t.kind
}

func (t *Type) State() uint32 {
  if t == nil { return 0 }
  return t.state
}

func (t *Type) StateVersion(version Version) uint32 {
  if t == nil { return 0 }
  if version == VLATEST {
    return t.state
  }
  return t.versions[version]
}

func (t *Type) Name() string {
  return t.kind.Name()
}
func (t *Type) CommandString() string {
  str := t.kind.Name()
  if t.properties == nil {
    return str
  }
  properties := make([]string, len(t.properties))
  i := 0
  for key, val := range t.properties {
    properties[i] = key + "=" + val
    i++
  }
  sort.Strings(properties)
  return str + "[" + strings.Join(properties, ",") + "]"
}

// Returns the bounding box of the block. If this block
// does not have a bounding box, this will return
// BoundingBox_NONE.
func (t *Type) BoundingBox() BoundingBox {
  // Act like air for nil types
  if t == nil { return BoundingBox_NONE }
  return t.kind.reg.BoundingBox(t)
}

// Returns true if this block is transparent to block light.
func (t *Type) Transparent() bool {
  // Act like air for nil types
  if t == nil { return true }
  return t.kind.reg.Transparent(t)
}

// This copies all properties from type into a new map. This is slow, and
// probably shouldn't be used very much.
func (t *Type) Properties() map[string]string {
  if t.properties == nil {
    return nil
  }
  new_map := make(map[string]string)
  for key, val := range t.properties {
    new_map[key] = val
  }
  return new_map
}

// This returns true if the properties of t and other are identical.
// This is slow, as it just iterates through a map and compares a lot
// of strings.
func (t *Type) properties_match(other *Type) bool {
  if len(t.properties) != len(other.properties) {
    return false
  }
  for k, v := range t.properties {
    other_val, ok := other.properties[k]
    if !ok || other_val != v {
      return false
    }
  }
  return true
}

// This gets the property for the given name. If the property does not exist,
// it will return an empty string.
func (t *Type) Property(name string) string {
  val, ok := t.properties[name]
  if !ok {
    return ""
  }
  return val
}

// This returns true if the property exists, and false if otherwise.
func (t *Type) HasProperty(name string) bool {
  _, ok := t.properties[name]
  return ok
}

// This generates a string in the format "minecraft:log axis=x".
// Properties will always be in alphabetical order.
// Used internally for property to type lookups.
func (t *Type) PropString() string {
  str := t.kind.Name()
  keys := make([]string, len(t.properties))
  i := 0
  for k, _ := range t.properties {
    keys[i] = k
    i++
  }
  // Always generate it in order
  sort.Strings(keys)
  for _, k := range keys {
    str += " " + k + "=" + t.properties[k]
  }
  return str
}

// This returns a list of all possible values for one property key.
func (t *Type) PossiblePropertyValues(name string) []string {
  if !t.HasProperty(name) {
    return nil
  }
  base := t.Kind().ordered_properties[name]
  arr := make([]string, len(base))
  for i, v := range base {
    arr[i] = v
  }
  return arr
}

func (t *Type) add_version(version Version, id uint32) {
  if t.versions == nil {
    t.versions = make([]uint32, VLATEST)
  }
  if block_versions[version] == nil {
    block_versions[version] = []*Type{}
    // panic(fmt.Sprintf("Version %d does not exist!\n(Failed while setting version for type of kind %s)", version, t.kind.Name()))
  }
  old := t.versions[version]
  if old != 0 {
    fmt.Printf("Error while adding state %v (version %v) to %v:\n", id, version, t)
    panic(fmt.Sprintf("This type already has version %v set to %v", version, old))
  }
  t.versions[version] = id
  if id >= uint32(len(block_versions[version])) {
    // Fill the list will some nil entries,
    for i := uint32(len(block_versions[version])); i < id; i++ {
      block_versions[version] = append(block_versions[version], nil)
    }
    // and then append the item.
    block_versions[version] = append(block_versions[version], t)
  } else {
    prev := block_versions[version][id]
    // We only want to overwrite a value if it doesn't exist.
    if prev == nil {
      block_versions[version][id] = t
    }
  }
}

func (t *Type) String() string {
  return fmt.Sprintf("Type { %s %d }", t.CommandString(), t.state)
}
