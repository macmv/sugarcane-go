package block

type World interface {
  Block(pos Pos) *State
  BlockType(pos Pos) *Type

  SetBlock(pos Pos, t *Type)
  SetBlockProperty(pos Pos, key, val string)
  SetBlockProperties(pos Pos, props map[string]string)

  FixDesync(pos Pos)
}
