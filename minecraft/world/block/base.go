package block

import (
)

// Users create these, and pass them into Register()
// Once all blocks have been registered, these are all converted into *Kinds
type RegisterBase struct {
  name string
  properties []*Property
  default_prop map[string]string
  settings *Settings

  sound string
  bounding_box string
  V1_8_ID int
}

type Register interface {
  Name() string
  SetName(name string)
  Properties() []*Property
  DefaultProperties() map[string]string

  Settings(set Settings)
  GetSettings() Settings
  Color(bs *State) Color
  Sound(bs *State) string
  BoundingBox(t *Type) BoundingBox
  Transparent(t *Type) bool

  Versions() map[Version]map[string]uint32

  Drops(w World, pos Pos) (string, int)
}

func NewRegister() *RegisterBase {
  r := RegisterBase{}
  r.default_prop = make(map[string]string)
  return &r
}

func (r *RegisterBase) Name() string                         { return r.name }
func (r *RegisterBase) SetName(name string)                  { r.name = name }
func (r *RegisterBase) Properties() []*Property              { return r.properties }
func (r *RegisterBase) DefaultProperties() map[string]string { return r.default_prop }

// Sets the settings for a block. Will panic if called twice.
func (r *RegisterBase) Settings(set Settings) {
  if r.settings == nil {
    r.settings = &set
  } else {
    panic("Settings already set!")
  }
}

// DO NOT USE THIS.
// This should only when you need to do something like copy all of the
// settings for a block (example: stairs copying all settings from planks).
// Otherwise, this should not be used, as the other specific methods can
// be overriten by other blocks. For example, a log changes color when it is
// place at a different angle. The settings is never updated, but the result from
// Color() is updated.
func (r *RegisterBase) GetSettings() Settings                    { return *r.settings }

func (r *RegisterBase) Color(bs *State) Color                    { return r.settings.Color }
func (r *RegisterBase) Sound(bs *State) string                   { return r.settings.Sound }
func (r *RegisterBase) BoundingBox(t *Type) BoundingBox          { return r.settings.BoundingBox }
func (r *RegisterBase) Transparent(t *Type) bool                 { return r.settings.Transparent }
func (r *RegisterBase) Versions() map[Version]map[string]uint32  { return r.settings.Versions }

func (r *RegisterBase) AddProperty(p *Property) {
  r.properties = append(r.properties, p)
  r.default_prop[p.Name()] = p.Default()
}

func (r *RegisterBase) SetDefaultProperty(key, val string) {
  r.default_prop[key] = val
}

func (r *RegisterBase) Drops(w World, pos Pos) (string, int) {
  return r.Name(), 1
}

