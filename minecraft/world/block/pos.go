package block

import (
  "math"
)

// Block position. This should be used any time a block
// position in a world is needed. This only stores X, Y, Z,
// so you will need something else if you want to store the
// world along with a position.
type Pos struct {
  X, Y, Z int32
}

// This is a chunk position. It is used to refer to chunk columns
// within a world. It is also used as a key for chunks within a
// world
type ChunkPos struct {
  X, Z int32
}

// This returns the chunk that this block position is in.
// It just divides X by 16, but rounds down.
func (p Pos) ChunkX() int32 {
  if p.X < 0 {
    return (p.X - 15) / 16
  } else {
    return p.X / 16
  }
}
// This returns the chunk that this block position is in.
// It just divides Y by 16, but rounds down. Since chunks
// are usually accessed in columns, this function probably
// won't be very useful. ChunkPos() does not call this function,
// and you probably shouldn't either. It is only used internally
// within world/chunk.Chunk, when accessing chunk sections.
func (p Pos) ChunkY() int32 {
  if p.Y < 0 {
    return (p.Y - 15) / 16
  } else {
    return p.Y / 16
  }
}
// This returns the chunk that this block position is in.
// It just divides Z by 16, but rounds down.
func (p Pos) ChunkZ() int32 {
  if p.Z < 0 {
    return (p.Z - 15) / 16
  } else {
    return p.Z / 16
  }
}
// This returns the chunk postition that pos is in.
func (p Pos) ChunkPos() ChunkPos {
  return ChunkPos{
    X: p.ChunkX(),
    Z: p.ChunkZ(),
  }
}

// This returns the block position relative to the chunk
// it's in. This will always be between 0 and 15.
func (p Pos) ChunkRelX() int32 {
  v := p.X % 16
  if v < 0 {
    v += 16
  }
  return v
}
// This returns the block position relative to the chunk
// it's in. This will always be between 0 and 15. NOTE:
// This function probably isn't very useful. It is only
// used internally when accessing individual chunk sections,
// which is all wrapped within world/chunk.Chunk. This
// function is not used in ChunkRelPos(), as typically
// this is not what you want.
func (p Pos) ChunkRelY() int32 {
  v := p.Y % 16
  if v < 0 {
    v += 16
  }
  return v
}
// This returns the block position relative to the chunk
// it's in. This will always be between 0 and 15.
func (p Pos) ChunkRelZ() int32 {
  v := p.Z % 16
  if v < 0 {
    v += 16
  }
  return v
}
// This returns a new position, where the X and Z are relative
// to the chunk (between 0 and 15). Y is unchanged.
func (p Pos) ChunkRelPos() Pos {
  return Pos{
    X: p.ChunkRelX(),
    Y: p.Y,
    Z: p.ChunkRelZ(),
  }
}

// This returns true if the position is within the chunk
// column.
func (p Pos) InChunk(c ChunkPos) bool {
  return p.ChunkX() == c.X && p.ChunkZ() == c.Z
}

// This returns a new block position, which is offset
// one block in the direction of face.
func (p Pos) OffsetFace(face Face) Pos {
  return p.Add(face.Offset())
}

// This adds the coorsponding x, y and z values to
// pos.X, pos.Y, and pos.Z. This new value is returned,
// and the original is not changed.
func (p Pos) Add(x, y, z int32) Pos {
  return Pos{
    p.X + x,
    p.Y + y,
    p.Z + z,
  }
}

// Adds returns the current position with pos.X increased
// by x. Does not modify the original pos.
func (p Pos) AddX(amount int32) Pos {
  p.X += amount
  return p
}

// Adds returns the current position with pos.Y increased
// by y. Does not modify the original pos.
func (p Pos) AddY(amount int32) Pos {
  p.Y += amount
  return p
}

// Adds returns the current position with pos.Z increased
// by z. Does not modify the original pos.
func (p Pos) AddZ(amount int32) Pos {
  p.Z += amount
  return p
}

// This copies p, sets X to x, then returns the copy.
// If you want to change only the X of a position, you
// can do this in the arguments of a function.
func (p Pos) WithX(x int32) Pos {
  p.X = x
  return p
}

// This copies p, sets Y to y, then returns the copy.
// If you want to change only the Y of a position, you
// can do this in the arguments of a function.
func (p Pos) WithY(y int32) Pos {
  p.Y = y
  return p
}

// This copies p, sets Z to z, then returns the copy.
// If you want to change only the Z of a position, you
// can do this in the arguments of a function.
func (p Pos) WithZ(z int32) Pos {
  p.Z = z
  return p
}

// Returns the distance between the two block positions.
func (p Pos) Distance(o Pos) float32 {
  a := float32(p.X - o.X)
  a *= a
  b := float32(p.Y - o.Y)
  b *= b
  c := float32(p.Z - o.Z)
  c *= c
  return float32(math.Sqrt(float64(a + b + c)))
}

// Converts a long into 3 ints, according to minecraft's position type.
// This is in the order X, Z, then Y. For versions 1.14+.
func PosFromLong(value uint64) Pos {
  x := int32(value >> 38)
  y := int32(value & 0xFFF)
  z := int32((value << 26 >> 38))
  if x >= int32(math.Pow(2, 25)) { x -= int32(math.Pow(2, 26)) }
  if y >= int32(math.Pow(2, 11)) { y -= int32(math.Pow(2, 12)) }
  if z >= int32(math.Pow(2, 25)) { z -= int32(math.Pow(2, 26)) }
  return Pos{x, y, z}
}

// Converts a long into 3 ints, according to minecraft's old position type.
// This is in the order X, Y, then Z. For versions 1.8 - 1.13.
// NOTE: During server packet handling, all longs will be in the new format.
// The proxy translates them correctly. So, this function should only be used
// on the proxy.
func PosFromOldLong(value uint64) Pos {
  x := int32(value >> 38)
  z := int32(value << 38 >> 38)
  y := int32((value >> 26) & 0xFFF)
  if x >= int32(math.Pow(2, 25)) { x -= int32(math.Pow(2, 26)) }
  if y >= int32(math.Pow(2, 11)) { y -= int32(math.Pow(2, 12)) }
  if z >= int32(math.Pow(2, 25)) { z -= int32(math.Pow(2, 26)) }
  return Pos{x, y, z}
}

// This returns a long which is used in packets, for versions 1.14+
func (p Pos) AsLong() uint64 {
  return ((uint64(p.X) & 0x3FFFFFF) << 38) | ((uint64(p.Z) & 0x3FFFFFF) << 12) | (uint64(p.Y) & 0xFFF)
}

// This returns a long which is used in packets, for versions 1.8-1.13
func (p Pos) AsOldLong() uint64 {
  return ((uint64(p.X) & 0x3FFFFFF) << 38) | ((uint64(p.Y) & 0xFFF) << 26) | (uint64(p.Z) & 0x3FFFFFF)
}

// This calls f for every column within the chunk. The position passed
// to f has X and Z set, but Y is 0. If f returns false, then the loop
// stops.
func (c ChunkPos) ForEachColumn(f func(pos Pos) bool) {
  done := false
  for x := c.X * 16; x < c.X * 16 + 16; x++ {
    for z := c.Z * 16; z < c.Z * 16 + 16; z++ {
      if !f(Pos{x, 0, z}) {
        done = true
        break
      }
    }
    if done {
      break
    }
  }
}
