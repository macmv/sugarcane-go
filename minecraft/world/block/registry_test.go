package block_test

import (
  "testing"

  "github.com/stretchr/testify/assert"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

func TestRegistryAdd(t *testing.T) {
  r := block.NewVersionRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)

  item, ok := r.GetOk("1")
  assert.Equal(t, reg1, item)
  assert.Equal(t, true, ok)

  item, ok = r.GetOk("2")
  assert.Equal(t, reg2, item)
  assert.Equal(t, true, ok)

  item, ok = r.GetOk("3")
  assert.Equal(t, reg3, item)
  assert.Equal(t, true, ok)
  assert.Nil(t, r.Validate())
}

func TestRegistryList(t *testing.T) {
  r := block.NewVersionRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)

  assert.Equal(t, []block.Register{
    reg1,
    reg2,
    reg3,
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryInsert(t *testing.T) {
  r := block.NewVersionRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  reg4 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)
  r.InsertAfter("1", "new name for reg4", reg4)

  assert.Equal(t, []block.Register{
    reg1,
    reg4,
    reg2,
    reg3,
  }, r.List())
  assert.Nil(t, r.Validate())
}

func TestRegistryRemove(t *testing.T) {
  r := block.NewVersionRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  reg4 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)
  r.Add("4", reg4)
  r.Remove("3")

  assert.Equal(t, []block.Register{
    reg1,
    reg2,
    reg4,
  }, r.List())
  t.Log(r)
  assert.Nil(t, r.Validate())
}

func TestRegistryMove(t *testing.T) {
  r := block.NewVersionRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  reg4 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)
  r.Add("4", reg4)
  // Moves item upwards, shifts other items down
  r.MoveAfter("3", "1")

  assert.Equal(t, []block.Register{
    reg1,
    reg3,
    reg2,
    reg4,
  }, r.List())
  t.Log(r)
  assert.Nil(t, r.Validate())

  r = block.NewVersionRegistry()
  reg1 = block.NewRegister()
  reg2 = block.NewRegister()
  reg3 = block.NewRegister()
  reg4 = block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("3", reg3)
  r.Add("4", reg4)
  // Moves item downwards, shifts other items up
  r.MoveAfter("2", "4")

  assert.Equal(t, []block.Register{
    reg1,
    reg3,
    reg4,
    reg2,
  }, r.List())
  t.Log(r)
  assert.Nil(t, r.Validate())
}

func TestRegistryUpdate(t *testing.T) {
  r := block.NewRegistry()
  reg1 := block.NewRegister()
  reg2 := block.NewRegister()
  reg3 := block.NewRegister()
  reg4 := block.NewRegister()
  r.Add("1", reg1)
  r.Add("2", reg2)
  r.Add("4", reg4)

  assert.Equal(t, []block.Register{
    reg1,
    reg2,
    reg4,
  }, r.List())

  r.Update(block.V1_14)

  r.InsertAfter("2", "3", reg3)

  assert.Equal(t, []block.Register{
    reg1,
    reg2,
    reg3,
    reg4,
  }, r.List())

  kinds, types, versions := r.Generate()

  for name, k := range kinds {
    if !(name == "1" || name == "2" || name == "3" || name == "4") {
      t.Log(kinds)
      t.Fatal("Name is incorrect:", name)
    }
    t.Log(k)
  }

  for id, ty := range types {
    if !(id == 0 || id == 1 || id == 2 || id == 3) {
      t.Fatal("ID is incorrect:", id)
    }
    t.Log(ty)
  }

  for version, m := range versions {
    if version != block.V1_13 {
      t.Fatal("Version is incorrect:", version)
    }
    for id, _ := range m {
      if !(id == 0 || id == 1 || id == 2) {
        t.Fatal("ID is incorrect:", id)
      }
    }
    t.Log(m)
  }
}
