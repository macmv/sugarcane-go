package block

import (
  "path"
  "path/filepath"

  "os"
  // "strconv"
  "net/url"
  "net/http"
  "io/ioutil"
  // "encoding/json"
)

func apply_registry() {
  // if block_kinds != nil {
  //   panic("Cannot apply registry when block kinds is already set!")
  // }
  block_kinds, block_types, block_versions = block_registry.Generate()
  air = block_types[0]
}

func load_blocks_from_file(base_dir, base_url, path string, version int) {
  // data := download_or_read(base_dir, base_url, path)
  // load_blocks_from_json(data, version)
}

func add_version_from_file(base_dir, base_url, path string, version int) {
  // data := download_or_read(base_dir, base_url, path)
  // add_version_from_json(data, version)
}

func download_or_read(base_dir, base_url, location string) []byte {
  filename := filepath.Join(base_dir, location)
  data, err := ioutil.ReadFile(filename)
  if err != nil {
    // download_file takes a url, so we want to use the path module
    u, err := url.Parse(base_url)
    if err != nil { panic(err) }
    u.Path = path.Join(u.Path, location)
    data = download_file(u.String())

    err = os.MkdirAll(filepath.Dir(filename), 0755)
    if err != nil { panic(err) }
    err = ioutil.WriteFile(filename, data, 0644)
    if err != nil { panic(err) }
  }
  return data
}

func download_file(url string) []byte {
  res, err := http.Get(url)
  if err != nil { panic(err) }
  defer res.Body.Close()

  data, err := ioutil.ReadAll(res.Body)
  if err != nil { panic(err) }
  return data
}

func load_blocks_from_json(string_data []byte, version int) {
  if block_types != nil {
    panic("Already loaded block states!")
  }
  // block_kinds, block_types = load_types_from_json(string_data, version)
}

// func load_types_from_json(string_data []byte, version int) (map[string]*Kind, map[uint32]*Type) {
//   kinds := make(map[string]*Kind)
//   types := make(map[uint32]*Type)
// 
//   var data []map[string]interface{}
//   err := json.Unmarshal(string_data, &data)
//   if err != nil {
//     panic(err)
//   }
//   for _, block_data := range data {
//     _, ok := block_data["states"].([]interface{})
//     if ok { // 1.13+ data
//       kind, new_types := load_states_data(block_data)
//       for id, t := range new_types {
//         types[id] = t
//       }
//       kinds[kind.Name()] = kind
//     } else { // 1.12- data
//       kind, new_types := load_old_data(block_data)
//       for id, t := range new_types {
//         types[id] = t
//       }
//       kinds[kind.Name()] = kind
//     }
//   }
//   return kinds, types
// }

// func load_states_data(block_data map[string]interface{}) (*Kind, map[uint32]*Type) {
//   name := block_data["name"].(string)
//   states := block_data["states"].([]interface{})
//   min_state_id := uint32(block_data["minStateId"].(float64))
//   default_state_id := uint32(block_data["defaultState"].(float64))
// 
//   kind := Kind{name: name}
//   kind.default_state = default_state_id
//   types := make(map[uint32]*Type)
// 
//   if len(states) > 0 {
//     kind.states = make(map[uint32]map[string]string)
//     kind.properties = make(map[string]map[string]struct{})
// 
//     ordered_props := []string{}
//     ordered_value_map := make(map[string][]string)
//     // Convert all properties into a list of possible properties
//     for _, state := range states {
//       s := state.(map[string]interface{})
//       name := s["name"].(string)
//       prop_type := s["type"].(string)
// 
//       props := make(map[string]struct{})
//       ordered_values := []string{}
//       if prop_type == "enum" {
//         for _, val := range s["values"].([]interface{}) {
//           props[val.(string)] = struct{}{}
//           ordered_values = append(ordered_values, val.(string))
//         }
//       } else if prop_type == "bool" {
//         props["true"] = struct{}{}
//         props["false"] = struct{}{}
//         ordered_values = append(ordered_values, "true", "false")
//       } else if prop_type == "int" {
//         num_values := int(s["num_values"].(float64))
//         for i := 0; i < num_values; i++ {
//           str := strconv.Itoa(i)
//           props[str] = struct{}{}
//           ordered_values = append(ordered_values, str)
//         }
//       } else {
//         panic("Invalid property type: " + prop_type)
//       }
//       kind.properties[name] = props
//       ordered_value_map[name] = ordered_values
//       ordered_props = append(ordered_props, name)
//     }
//     id := min_state_id
//     indices := make([]int, len(ordered_props))
//     possible_properties := make(map[uint32][]int)
//     // Convert all possible properties into a map of possible combinations of properties (states)
//     for {
//       possible_properties[id] = make([]int, len(indices))
//       copy(possible_properties[id], indices)
// 
//       indices[0]++
//       complete := false
//       for i := 0; i < len(indices); i++ {
//         index := indices[i]
//         val_map := ordered_value_map[ordered_props[i]]
//         if index >= len(val_map) {
//           indices[i] = 0
//           if i+1 >= len(indices) {
//             complete = true
//             break
//           }
//           indices[i+1]++
//         } else {
//           break
//         }
//       }
//       if complete {
//         break
//       }
//       id++
//     }
//     // Convert each one of these combinations into a block.Type
//     for id, prop_indices := range possible_properties {
//       possible_map := make(map[string]string)
//       for key, index := range prop_indices {
//         name := ordered_props[key]
//         value := ordered_value_map[name][index]
//         possible_map[name] = value
//       }
//       kind.states[id] = possible_map
//       types[id] = &Type{kind: &kind, properties: possible_map, state: id}
//     }
//   } else {
//     kind.states = nil
//     kind.properties = nil
//     types[min_state_id] = &Type{kind: &kind, properties: nil, state: min_state_id}
//   }
//   return &kind, types
// }
// 
// func load_old_data(block_data map[string]interface{}) (*Kind, map[uint32]*Type) {
//   name := block_data["name"].(string)
//   // We << 4, because block metadata (damage value, color of wool) is the first 4 bits
//   id := uint32(block_data["id"].(float64)) << 4
// 
//   kind := Kind{name: name}
//   kind.default_state = id
//   // kinds do not have properties in 1.8
//   kind.properties = nil
//   // states is tricky. It depends on properties, so it can't be set properly. But it also cannot be nil
//   // (for something like wool), as the correct block.Type cannot be retrieved.
//   types := make(map[uint32]*Type)
// 
//   variations, ok := block_data["variations"].([]interface{})
//   if ok {
//     kind.states = make(map[uint32]map[string]string)
//     for _, val := range variations {
//       values := val.(map[string]interface{})
//       damage := uint32(values["metadata"].(float64))
//       if damage > 15 {
//         panic("Invalid block metadata value")
//       }
//       props := make(map[string]string)
//       props["meta"] = strconv.Itoa(int(damage))
// 
//       new_id := id | damage
//       types[new_id] = &Type{kind: &kind, properties: props, state: new_id}
//       kind.states[new_id] = props
//     }
//   } else {
//     kind.states = nil
//     types[id] = &Type{kind: &kind, properties: nil, state: id}
//   }
// 
//   return &kind, types
// }
