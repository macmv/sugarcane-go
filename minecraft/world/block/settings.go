package block

// This is basically a large struct which is used to declare what settings a block has.
// These settings cover all attributes that all blocks have: Hitbox, Properties, Sound, Color, Tool, etc.
type Settings struct {
  Tool Tool
  Hardness, Resistance float64
  Color Color
  Sound string
  Versions VersionsMap

  BoundingBox BoundingBox
  Transparent bool
}

// This is a deep copy. It is mostly used to copy the versions map
func (s Settings) Copy() Settings {
  n := s
  n.Versions = make(VersionsMap)
  for version, m := range s.Versions {
    new_map := make(map[string]uint32)
    for name, id := range m {
      new_map[name] = id
    }
    n.Versions[version] = new_map
  }
  return n
}

// This is the bounding box of the block. If all the values are 0,
// then this is a full block (same as 0, 0, 0, 1, 1, 1). If all values are -1,
// then this is an empty box. If any values are > 1 or < 0, and it's
// not an empty box, this is an invalid box.
type BoundingBox struct {
  X1, Y1, Z1 float64
  X2, Y2, Z2 float64
}

// This is a shorthand for BoundBox{-1, -1, -1, -1, -1, -1} (an empty
// bounding box).
var BoundingBox_NONE = BoundingBox{
  -1, -1, -1,
  -1, -1, -1,
}
