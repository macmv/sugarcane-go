package block

const (
  Color_AIR Color = iota
  Color_GRASS
  Color_SAND
  Color_WOOL
  Color_TNT
  Color_ICE
  Color_IRON
  Color_FOLIAGE
  Color_SNOW
  Color_CLAY
  Color_DIRT
  Color_STONE
  Color_WATER
  Color_WOOD
  Color_QUARTZ
  Color_ORANGE
  Color_MAGENTA
  Color_LIGHT_BLUE
  Color_YELLOW
  Color_LIME
  Color_PINK
  Color_GRAY
  Color_LIGHT_GRAY
  Color_CYAN
  Color_PURPLE
  Color_BLUE
  Color_BROWN
  Color_GREEN
  Color_RED
  Color_BLACK
  Color_GOLD
  Color_DIAMOND
  Color_LAPIS
  Color_EMERALD
  Color_OBSIDIAN
  Color_NETHERRACK
  Color_WHITE_TERRACOTTA
  Color_ORANGE_TERRACOTTA
  Color_MAGENTA_TERRACOTTA
  Color_LIGHT_BLUE_TERRACOTTA
  Color_YELLOW_TERRACOTTA
  Color_LIME_TERRACOTTA
  Color_PINK_TERRACOTTA
  Color_GRAY_TERRACOTTA
  Color_LIGHT_GRAY_TERRACOTTA
  Color_CYAN_TERRACOTTA
  Color_PURPLE_TERRACOTTA
  Color_BLUE_TERRACOTTA
  Color_BROWN_TERRACOTTA
  Color_GREEN_TERRACOTTA
  Color_RED_TERRACOTTA
  Color_BLACK_TERRACOTTA
  Color_CRIMSON_NYLIUM
  Color_CRIMSON_STEM
  Color_CRIMSON_HYPHAE
  Color_WARPED_NYLIUM
  Color_WARPED_STEM
  Color_WARPED_HYPHAE
  Color_WARPED_WART
)

// Resets iota
const (
  Tool_PICKAXE Tool = iota
  Tool_AXE
  Tool_SHOVEL
  Tool_HOE
  Tool_SHEARS
  Tool_SWORD
)

func init() {
  colors = make([]uint32, Color_WARPED_WART + 1)
  colors[Color_AIR]                   = 0x000000
  colors[Color_GRASS]                 = 0x7fb238
  colors[Color_SAND]                  = 0xf7e9a3
  colors[Color_WOOL]                  = 0xc7c7c7
  colors[Color_TNT]                   = 0xff0000
  colors[Color_ICE]                   = 0xa0a0ff
  colors[Color_IRON]                  = 0xa7a7a7
  colors[Color_FOLIAGE]               = 0x007c00
  colors[Color_SNOW]                  = 0xffffff
  colors[Color_CLAY]                  = 0xa4a8b8
  colors[Color_DIRT]                  = 0x976d4d
  colors[Color_STONE]                 = 0x707070
  colors[Color_WATER]                 = 0x4040ff
  colors[Color_WOOD]                  = 0x8f7748
  colors[Color_QUARTZ]                = 0xfffcf5
  colors[Color_ORANGE]                = 0xd87f33
  colors[Color_MAGENTA]               = 0xb24cd8
  colors[Color_LIGHT_BLUE]            = 0x6699d8
  colors[Color_YELLOW]                = 0xe5e533
  colors[Color_LIME]                  = 0x7fcc19
  colors[Color_PINK]                  = 0xf27fa5
  colors[Color_GRAY]                  = 0x4c4c4c
  colors[Color_LIGHT_GRAY]            = 0x999999
  colors[Color_CYAN]                  = 0x4c7f99
  colors[Color_PURPLE]                = 0x7f3fb2
  colors[Color_BLUE]                  = 0x334cb2
  colors[Color_BROWN]                 = 0x664c33
  colors[Color_GREEN]                 = 0x667f33
  colors[Color_RED]                   = 0x993333
  colors[Color_BLACK]                 = 0x191919
  colors[Color_GOLD]                  = 0xfaee4d
  colors[Color_DIAMOND]               = 0x5cdbd5
  colors[Color_LAPIS]                 = 0x4a80ff
  colors[Color_EMERALD]               = 0x00d93a
  colors[Color_OBSIDIAN]              = 0x815631
  colors[Color_NETHERRACK]            = 0x700200
  colors[Color_WHITE_TERRACOTTA]      = 0xd1b1a1
  colors[Color_ORANGE_TERRACOTTA]     = 0x9f5224
  colors[Color_MAGENTA_TERRACOTTA]    = 0x95576c
  colors[Color_LIGHT_BLUE_TERRACOTTA] = 0x706c8a
  colors[Color_YELLOW_TERRACOTTA]     = 0xba8524
  colors[Color_LIME_TERRACOTTA]       = 0x677535
  colors[Color_PINK_TERRACOTTA]       = 0xa04d4e
  colors[Color_GRAY_TERRACOTTA]       = 0x392923
  colors[Color_LIGHT_GRAY_TERRACOTTA] = 0x876b62
  colors[Color_CYAN_TERRACOTTA]       = 0x575c5c
  colors[Color_PURPLE_TERRACOTTA]     = 0x7a4958
  colors[Color_BLUE_TERRACOTTA]       = 0x4c3e5c
  colors[Color_BROWN_TERRACOTTA]      = 0x4c3223
  colors[Color_GREEN_TERRACOTTA]      = 0x4c522a
  colors[Color_RED_TERRACOTTA]        = 0x8e3c2e
  colors[Color_BLACK_TERRACOTTA]      = 0x251610
  colors[Color_CRIMSON_NYLIUM]        = 0xbd3031
  colors[Color_CRIMSON_STEM]          = 0x943f61
  colors[Color_CRIMSON_HYPHAE]        = 0x5c191d
  colors[Color_WARPED_NYLIUM]         = 0x167e86
  colors[Color_WARPED_STEM]           = 0x3a8e8c
  colors[Color_WARPED_HYPHAE]         = 0x562c3e
  colors[Color_WARPED_WART]           = 0x14b485
}

type Color int
type Tool int

var colors []uint32

