package block

import (
  "testing"

  "github.com/stretchr/testify/assert"
)

func TestBlockPos(t *testing.T) {
  for i := int32(0); i < int32(16); i++ {
    assert.Equal(t, int32(-2), Pos{X: i - 32}.ChunkX())
    assert.Equal(t, int32(-1), Pos{X: i - 16}.ChunkX())
    assert.Equal(t, int32(0),  Pos{X: i +  0}.ChunkX())
    assert.Equal(t, int32(1),  Pos{X: i + 16}.ChunkX())
    assert.Equal(t, int32(i),  Pos{X: i - 32}.ChunkRelX())
    assert.Equal(t, int32(i),  Pos{X: i - 16}.ChunkRelX())
    assert.Equal(t, int32(i),  Pos{X: i +  0}.ChunkRelX())
    assert.Equal(t, int32(i),  Pos{X: i + 16}.ChunkRelX())

    assert.Equal(t, int32(-2), Pos{Z: i - 32}.ChunkZ())
    assert.Equal(t, int32(-1), Pos{Z: i - 16}.ChunkZ())
    assert.Equal(t, int32(0),  Pos{Z: i +  0}.ChunkZ())
    assert.Equal(t, int32(1),  Pos{Z: i + 16}.ChunkZ())
    assert.Equal(t, int32(i),  Pos{Z: i - 32}.ChunkRelZ())
    assert.Equal(t, int32(i),  Pos{Z: i - 16}.ChunkRelZ())
    assert.Equal(t, int32(i),  Pos{Z: i +  0}.ChunkRelZ())
    assert.Equal(t, int32(i),  Pos{Z: i + 16}.ChunkRelZ())

    assert.Equal(t, ChunkPos{-2, -2}, Pos{X: i - 32, Z: i - 32}.ChunkPos())
    assert.Equal(t, ChunkPos{-1, -1}, Pos{X: i - 16, Z: i - 16}.ChunkPos())
    assert.Equal(t, ChunkPos{0,   0}, Pos{X: i +  0, Z: i +  0}.ChunkPos())
    assert.Equal(t, ChunkPos{1,   1}, Pos{X: i + 16, Z: i + 16}.ChunkPos())
    assert.Equal(t, Pos{i, 5, i}, Pos{i - 32, 5, i - 32}.ChunkRelPos())
    assert.Equal(t, Pos{i, 5, i}, Pos{i - 16, 5, i - 16}.ChunkRelPos())
    assert.Equal(t, Pos{i, 5, i}, Pos{i +  0, 5, i +  0}.ChunkRelPos())
    assert.Equal(t, Pos{i, 5, i}, Pos{i + 16, 5, i + 16}.ChunkRelPos())
  }
}
