package block

import (
  "strconv"
)

// This is a property. Each blockstate is a possible combination of properties.
// So when all of the blocks are compiled into a blockstate map, all possible combinations
// of properties are given a unique id.
//
// Each property can have one of three types: enum, int or bool. Read more about this in the
// NewProperty function.
type Property struct {
  name string
  values []string
  def string
}

func (p *Property) Size() int {
  return len(p.values)
}

func (p *Property) Values() []string {
  return p.values
}

func (p *Property) Name() string {
  return p.name
}

func (p *Property) Default() string {
  return p.def
}

// This creates a new property. The first paramater must be a unique name for this property.
// For the next paramaters, the type infers which kind of property you would like.
//
// In order to make this an enum type, just pass in two or more strings. The default value
// will be the first string passed.
//
// In order to make this an int, just pass in an int type, which is the number of ints you want.
// For example, the redstone block uses an int for it's 'power' property. To create that, you
// would pass in 16. The default will be 0.
//
// In order to create a bool type, pass in true or false. The value you pass in will be the default.
func NewProperty(name string, values ...interface{}) *Property {
  p := Property{}
  p.name = name
  // If there is only 1 value, it will always be a bool or int, as 1 enum value doesn't make sense
  if len(values) == 1 {
    // bool type
    if values[0] == true || values[0] == false {
      p.values = []string{"true", "false"}
      if values[0] == true {
        p.def = "true"
      } else {
        p.def = "false"
      }
    } else {
      num, ok := values[0].(int)
      if !ok {
        panic("Invalid type passed to NewProperty")
      }
      for i := 0; i < num; i++ {
        p.values = append(p.values, strconv.Itoa(i))
      }
      p.def = "0"
    }
  } else {
    for _, val := range values {
      str, ok := val.(string)
      if !ok {
        panic("Invalid type passed to NewProperty")
      }
      p.values = append(p.values, str)
    }
    p.def = p.values[0]
  }
  return &p
}
