package world

import (
  "os"
  "fmt"
  "sync"
  "time"
  "strconv"
  "strings"
  "io/ioutil"
  "path/filepath"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/entity"
  "gitlab.com/macmv/sugarcane/minecraft/schedule"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/terrain"

  pb "gitlab.com/macmv/sugarcane/proto"

  "google.golang.org/protobuf/proto"

  "github.com/aws/aws-sdk-go/service/s3"
)

// Removed, in favor of block.ChunkPos
// type ChunkLocation struct {
//   X, Z int32
// }

type entities_map map[uint32]entity.Entity
type players_map map[util.UUID]*player.Player

type World struct {
  // I ran a lot of tests. This is most likely the fastest option, in
  // terms of thread safe maps. Any other option runs into the fact that
  // interfaces are slow. With generics, sync.Map might be faster. For
  // now though, this is the best way to do things.
  players map[util.UUID]*player.Player
  players_lock *sync.Mutex

  entities map[uint32]entity.Entity
  entities_lock *sync.Mutex

  chunks map[block.ChunkPos]*chunk.MultiChunk
  chunks_lock *sync.Mutex

  name string
  next_eid uint32
  view_distance int32
  default_chunk *chunk.MultiChunk
  Scheduler *schedule.Scheduler
  manager *WorldManager
  wg *sync.WaitGroup

  // terrain.WorldGenerator is an interface,
  // and will generate all chunks in the world.
  generator terrain.WorldGenerator

  // Miliseconds per tick on all of the player update loops
  mspt float64

  on_chunk_set func(loc block.ChunkPos)

  // User space code to setup the world.
  Settings settings
  // Should be used by interal code to access settings,
  // and is never changed after LockSettings() is called.
  locked_settings settings
  locked_settings_set bool
}

type settings struct {
  GenerateTerrain bool
  Seed int64
}

// This creates a new world, given a world manager and a name.
// Things like default chunk and world seed can be set using
// world.Settings.
func NewWorld(manager *WorldManager, name string) *World {
  w := World{}
  w.name = name
  w.view_distance = 10
  w.Scheduler = schedule.NewScheduler()

  w.players = make(map[util.UUID]*player.Player)
  w.players_lock = &sync.Mutex{}
  w.entities = make(map[uint32]entity.Entity)
  w.entities_lock = &sync.Mutex{}
  w.chunks = make(map[block.ChunkPos]*chunk.MultiChunk)
  w.chunks_lock = &sync.Mutex{}

  w.manager = manager
  w.locked_settings_set = false
  return &w
}

// This finalizes the world settings. This must be called before
// the world is usable. No terrain will generate before this is
// called.
func (w *World) LockSettings() {
  if w.locked_settings_set {
    return
  }
  w.locked_settings_set = true
  w.locked_settings = w.Settings
  if !w.locked_settings.GenerateTerrain {
    w.default_chunk = chunk.NewMultiChunk()
  }

  total_chunks := (w.view_distance * 2 + 1) * (w.view_distance * 2 + 1)
  num_chunks := int32(0)
  for x := -w.view_distance; x <= w.view_distance; x++ {
    for z := -w.view_distance; z <= w.view_distance; z++ {
      w.Chunk(block.ChunkPos{x, z})
      num_chunks++

      percent := float32(num_chunks) / float32(total_chunks) * 100
      prev_percent := float32(num_chunks - 1) / float32(total_chunks) * 100
      // We want to print a message for every 10% finished
      if int32(prev_percent / 10) != int32(percent / 10) {
        log.Info(fmt.Sprintf("Generating world... %.2f%% done", percent))
      }
    }
  }
}

// This returns a copy of the world's chunks. Since this is a copy,
// it is slow to generate.
func (w *World) Chunks() map[block.ChunkPos]*chunk.MultiChunk {
  w.chunks_lock.Lock()
  chunks := make(map[block.ChunkPos]*chunk.MultiChunk, len(w.chunks))
  for pos, c := range w.chunks {
    chunks[pos] = c
  }
  w.chunks_lock.Unlock()
  return chunks
}

// This loads the world from disk. It does not send any packets,
// so you should only call this before the server is started.
func (w *World) Load() {
  // log.Info("Chunk loading (from disk) is temporarily disabled!")
  chunks_dir := getChunkDir(w.name)
  os.MkdirAll(chunks_dir, 0755)
  files, err := ioutil.ReadDir(chunks_dir)
  if err != nil {
    log.Error("Cannot read from chunks dir, err: ", err)
    return
  }
  log.Info("Loading world '", w.name, "' from disk")
  w.chunks_lock.Lock()
  for _, file := range files {
    sections := strings.Split(file.Name(), ".")
    x, err := strconv.Atoi(sections[0])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    z, err := strconv.Atoi(sections[1])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    chunk := chunk.NewMultiChunk()
    path := filepath.Join(getChunkDir(w.name), file.Name())
    chunk_data, err := ioutil.ReadFile(path)
    if err != nil {
      log.Error("Error while loading chunk: ", err)
      continue
    }
    chunk_proto := &pb.Chunk{}
    err = proto.Unmarshal(chunk_data, chunk_proto)
    if err != nil {
      log.Error("Error while loading chunk: ", err)
      continue
    }
    chunk.LoadFromProto(chunk_proto)
    w.chunks[block.ChunkPos{int32(x), int32(z)}] = chunk
  }
  w.chunks_lock.Unlock()
}

// This returns a copy of all of the players in the world.
// This is slow, as it must allocate a new map, and add all
// of the players to it. For things like fining a player by
// name, or iterating through players, you should use the
// utility functions to do that (PlayerByName and ForAllPlayers).
func (w *World) Players() map[util.UUID]*player.Player {
  w.players_lock.Lock()
  players := make(map[util.UUID]*player.Player, len(w.players))
  for id, p := range w.players {
    players[id] = p
  }
  w.players_lock.Unlock()
  return players
}

// This searches for a player with the given username. If no
// player is found, it will return nil.
func (w *World) PlayerByName(name string) *player.Player {
  w.players_lock.Lock()
  for _, p := range w.players {
    if p.Name() == name {
      w.players_lock.Unlock()
      return p
    }
  }
  w.players_lock.Unlock()
  return nil
}

// Gets a player from a UUID. Returns nil if the player is not present.
func (w *World) Player(id util.UUID) *player.Player {
  w.players_lock.Lock()
  p, _ := w.players[id]
  w.players_lock.Unlock()
  return p
}

// This returns the world's name.
func (w *World) Name() string {
  return w.name
}

// Runs f for all players in the world. If f returns false, the loop exists.
func (w *World) RunForAll(f func(p *player.Player) bool) {
  w.players_lock.Lock()
  for _, p := range w.players {
    if !f(p) {
      break
    }
  }
  w.players_lock.Unlock()
}

// Runs a function for all players within view of the given chunk.
// If f returns false, the loop will exit.
func (w *World) RunInView(pos block.ChunkPos, f func(p *player.Player) bool) {
  w.RunForAll(func(p *player.Player) bool {
    if p.ChunkX() - w.view_distance < pos.X && p.ChunkX() + w.view_distance > pos.X &&
       p.ChunkZ() - w.view_distance < pos.Z && p.ChunkZ() + w.view_distance > pos.Z {
      if !f(p) {
        return false
      }
    }
    return true
  })
}

func (w *World) SendToAll(out... *packet.OutgoingPacket) {
  w.RunForAll(func(p *player.Player) bool {
    for _, packet := range out {
      packet.Send(p.PacketStream)
    }
    return true
  })
}

// Sends the packet to all players within render distance of the given chunk.
func (w *World) SendInView(pos block.ChunkPos, out... *packet.OutgoingPacket) {
  w.RunInView(pos, func(p *player.Player) bool {
    for _, packet := range out {
      packet.Send(p.PacketStream)
    }
    return true
  })
}

// Sends the packet to all players, who do not have the given id, within render distance of the given chunk.
func (w *World) SendInViewNotID(pos block.ChunkPos, id util.UUID, out... *packet.OutgoingPacket) {
  w.RunInView(pos, func(p *player.Player) bool {
    if p.UUID != id {
      for _, packet := range out {
        packet.Send(p.PacketStream)
      }
    }
    return true
  })
}

// This checks if the world has the given player.
func (w *World) HasPlayer(p *player.Player) bool {
  p = w.Player(p.UUID)
  return p != nil
}

// This starts the update loop for the world.
// Should never be called, as mc.StartUpdateLoop()
// will call this for you. This is a non-blocking call,
// as it spawns the update loop as a new go routine.
func (w *World) StartUpdateLoop(wg *sync.WaitGroup) {
  w.wg = wg
  go w.run_update_loop()
}

// This runs the entity update loop. Is a blocking
// call.
func (w *World) run_update_loop() {
  w.wg.Add(1)
  ticker := time.NewTicker(time.Second / 20)
  tick := 0
  for range ticker.C {
    if tick % 20 == 0 {
      mspt := w.mspt / 20
      w.mspt = 0

      out := packet.NewOutgoingPacket(packet.Clientbound_PlayerListHeader)
      out.SetString(0, w.get_playerlist_header().ToJSON())
      out.SetString(1, w.get_playerlist_footer(mspt).ToJSON())
      w.SendToAll(out)
    }
    w.Scheduler.Update()
    w.entities_lock.Lock()
    for _, ent := range w.entities {
      // if ent.NeedsVelUpdate() {
      //   out := packet.NewOutgoingPacket(packet.Clientbound_EntityVelocity)
      //   out.SetInt(0, int32(ent.EID()))
      //   out.SetShort(0, 0)
      //   out.SetShort(1, 3000)
      //   out.SetShort(2, 0)
      //   w.SendInView(out, ent.ChunkX(), ent.ChunkZ())
      // }
      _, ok := ent.(*player.Player)
      // If the entity is a player, all of this will be handled in run_player_update_loop
      if !ok {
        ent.Tick()
        if ent.NeedsPosUpdate() {
          info := w.Collide(ent)
          ent.Collide(info)
          // ent.Collide may set NeedsPosUpdate() to false
          if ent.NeedsPosUpdate() {
            out := packet.NewOutgoingPacket(packet.Clientbound_EntityTeleport)
            out.SetInt(0, int32(ent.EID()))
            out.SetDouble(0, ent.X())
            out.SetDouble(1, ent.Y())
            out.SetDouble(2, ent.Z())
            out.SetBool(0, true)
            w.SendInView(ent.BlockPos().ChunkPos(), out)
          }
        }
        if ent.Metadata().Dirty() {
          // Crashes everything except newest clients.
          // TODO: Rewrite entity metadata to support all versions.
          // log.Info("Entity ", ent.EID(), " metadata:\n", ent.Metadata())
          // out := packet.NewOutgoingPacket(packet.Clientbound_EntityMetadata)
          // out.SetInt(0, int32(ent.EID()))
          // out.SetEntityMetadata(ent.Metadata())
          // w.SendInView(ent.ChunkX(), ent.ChunkZ(), out)
          ent.Metadata().ClearDirty()
        }
      }
    }
    w.entities_lock.Unlock()
    tick++
  }
  w.wg.Done()
}

// This runs an update loop for the given player. This is
// a blocking call, which will exit once the player has
// left the world.
func (w *World) run_player_update_loop(p *player.Player) {
  w.wg.Add(1)
  ticker := time.NewTicker(time.Second / 20)
  for range ticker.C {
    start := time.Now()
    // If the player has disconnected, we want to stop this update loop
    if !w.HasPlayer(p) {
      break
    }
    // Player teleport packets are sent here, to avoid desyncs
    if p.Time() % 20 == 0 { // run this once per second
      out := packet.NewOutgoingPacket(packet.Clientbound_EntityTeleport)
      out.SetInt(0, int32(p.EID()))
      out.SetBool(0, p.OnGround())
      out.SetByte(0, p.YawByte())
      out.SetByte(1, p.PitchByte())
      out.SetDouble(0, p.X())
      out.SetDouble(1, p.Y())
      out.SetDouble(2, p.Z())
      w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)

      out = packet.NewOutgoingPacket(packet.Clientbound_EntityHeadLook)
      out.SetInt(0, int32(p.EID()))
      out.SetByte(0, p.YawByte())
      w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
    }

    // Player metadata updates are handled here
    if p.Metadata().Dirty() {
      // log.Info("Player eid ", p.EID(), " metadata:\n", p.Metadata())
      // out := packet.NewOutgoingPacket(packet.Clientbound_EntityMetadata)
      // out.SetInt(0, int32(p.EID()))
      // out.SetEntityMetadata(p.Metadata())
      // w.SendInView(p.ChunkX(), p.ChunkZ(), out)
      p.Metadata().ClearDirty()
    }

    // Player position updates are handled here
    if p.NeedsPosUpdate() {
      info := w.Collide(p)
      p.Collide(info)
      p.Tick()
      prev_chunk_x := p.PrevChunkX()
      prev_chunk_z := p.PrevChunkZ()
      chunk_x := p.ChunkX()
      chunk_z := p.ChunkZ()
      chunks_to_unload := []block.ChunkPos{}
      // New chunk/Unload chunk packets are handled here
      if (chunk_x != prev_chunk_x || chunk_z != prev_chunk_z) { // player moved between chunks
        // Player moved between chunks packet. Needed for client to render the correct chunks
        out := packet.NewOutgoingPacket(packet.Clientbound_UpdateViewPosition)
        out.SetInt(0, p.ChunkX())
        out.SetInt(1, p.ChunkZ())
        out.Send(p.PacketStream)
        // Previous chunk edges. Used to detect what chunks are
        // already loaded.
        prev_min_chunk_x := prev_chunk_x - w.view_distance
        prev_min_chunk_z := prev_chunk_z - w.view_distance
        prev_max_chunk_x := prev_chunk_x + w.view_distance
        prev_max_chunk_z := prev_chunk_z + w.view_distance
        // New chunk edges. Used to detect which chunks should
        // be unloaded, and which new chunks should be loaded.
        min_chunk_x := chunk_x - w.view_distance
        min_chunk_z := chunk_z - w.view_distance
        max_chunk_x := chunk_x + w.view_distance
        max_chunk_z := chunk_z + w.view_distance
        // Iterates through all visible chunks, and previously visible chunks,
        // and finds the one that are going out of view, or coming into view
        for vd_x := -w.view_distance; vd_x <= w.view_distance; vd_x++ {
          for vd_z := -w.view_distance; vd_z <= w.view_distance; vd_z++ {
            prev_x := vd_x + prev_chunk_x
            prev_z := vd_z + prev_chunk_z
            x := vd_x + chunk_x
            z := vd_z + chunk_z
            outside_new_chunk := prev_x < min_chunk_x ||
            prev_x > max_chunk_x ||
            prev_z < min_chunk_z ||
            prev_z > max_chunk_z
            outside_old_chunk := x < prev_min_chunk_x ||
            x > prev_max_chunk_x ||
            z < prev_min_chunk_z ||
            z > prev_max_chunk_z
            // This might unload extra chunks, but the client accepts as many unload chunk
            // packets as possible. Better to avoid memory leaks on the clients.
            if outside_new_chunk {
              // If this is true, we need to unload the chunk at prev_x, prev_z
              if p.Version() == packet.V1_8 {
                // This is how we unload for 1.8 clients. It's annoying,
                // but the easiest solution. The other solution would have
                // the proxy convert two types of grpc packets into one minecraft
                // packet, which it cannot do.
                out := packet.NewOutgoingPacket(packet.Clientbound_ChunkData)
                proto := &pb.Chunk{}
                proto.X = prev_x
                proto.Z = prev_z
                out.SetOther(0, proto)
                out.Send(p.PacketStream)
              } else {
                // Seperate unload chunk packet for all other versions
                out := packet.NewOutgoingPacket(packet.Clientbound_UnloadChunk)
                out.SetInt(0, prev_x)
                out.SetInt(1, prev_z)
                out.Send(p.PacketStream)
              }
              // Make sure to unload this chunk later
              chunks_to_unload = append(chunks_to_unload, block.ChunkPos{prev_x, prev_z})
            }
            // This makes sure a chunk is within the new chunk borders,
            // and it was not in the previous borders. If this is true,
            // we need to load a new chunk.
            if outside_old_chunk {
              // Means that we must load a new chunk, at x,z
              pos := block.ChunkPos{x, z}
              chunk, _ := w.Chunk(pos)
              w.GenerateNewChunkPacket(chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
              w.GenerateUpdateLightPacket(chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
            }
          }
        }
      }
      if p.NeedsPositionSet() {
        out := packet.NewOutgoingPacket(packet.Clientbound_EntityTeleport)
        out.SetInt(0, int32(p.EID()))
        out.SetBool(0, p.OnGround())
        out.SetByte(0, p.YawByte())
        out.SetByte(1, p.PitchByte())
        out.SetDouble(0, p.PrevX())
        out.SetDouble(1, p.PrevY())
        out.SetDouble(2, p.PrevZ())
        w.SendInView(p.BlockPos().ChunkPos(), out)
      } else if p.NeedsTeleport() {
        out := packet.NewOutgoingPacket(packet.Clientbound_EntityTeleport)
        out.SetInt(0, int32(p.EID()))
        out.SetBool(0, p.OnGround())
        out.SetByte(0, p.YawByte())
        out.SetByte(1, p.PitchByte())
        out.SetDouble(0, p.X())
        out.SetDouble(1, p.Y())
        out.SetDouble(2, p.Z())
        w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
        p.SetPrevPosition()
      } else {
        if p.NeedsRotUpdate() {
          out := packet.NewOutgoingPacket(packet.Clientbound_EntityHeadLook)
          out.SetInt(0, int32(p.EID()))
          out.SetByte(0, p.YawByte())
          w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)

          if p.NeedsPosUpdate() {
            out = packet.NewOutgoingPacket(packet.Clientbound_EntityPositionAndRotation)
            out.SetInt(0, int32(p.EID()))
            out.SetShort(0, p.DX())
            out.SetShort(1, p.DY())
            out.SetShort(2, p.DZ())
            out.SetByte(0, p.YawByte())
            out.SetByte(1, p.PitchByte())
            out.SetBool(0, p.OnGround())
            w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
          } else {
            out = packet.NewOutgoingPacket(packet.Clientbound_EntityRotation)
            out.SetInt(0, int32(p.EID()))
            out.SetByte(0, p.YawByte())
            out.SetByte(1, p.PitchByte())
            out.SetBool(0, p.OnGround())
            w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
          }
        } else {
          if p.NeedsPosUpdate() {
            out := packet.NewOutgoingPacket(packet.Clientbound_EntityPosition)
            out.SetInt(0, int32(p.EID()))
            out.SetShort(0, p.DX())
            out.SetShort(1, p.DY())
            out.SetShort(2, p.DZ())
            out.SetBool(0, p.OnGround())
            w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
          }
        }
        if p.NeedsVelUpdate() {
          out := packet.NewOutgoingPacket(packet.Clientbound_EntityVelocity)
          out.SetInt(0, int32(p.EID()))
          out.SetShort(0, p.Vx())
          out.SetShort(1, p.Vy())
          out.SetShort(2, p.Vz())
          w.SendInViewNotID(p.BlockPos().ChunkPos(), p.UUID, out)
        }
        p.SetPrevPosition()
      }
      // Now that p.SetPrevPosition() has been called, we can unload these chunks.
      for _, pos := range chunks_to_unload {
        w.TryUnload(pos)
      }
      p.ClearFlags()
      if p.NeedsEquipmentChange() {
        e := p.Equipment()
        out := packet.NewOutgoingPacket(packet.Clientbound_EntityEquipment)
        out.SetInt(0, int32(p.EID()))
        out.SetInt(1, 0)
        out.SetItem(0, e[0])
        w.SendInView(p.BlockPos().ChunkPos(), out)
        p.ClearEquipmentChange()
      }
    } else {
      // We always want to call Tick, so that time works, but we want to call it after we call Collide,
      // if that is even needed.
      p.Tick()
    }
    w.mspt += float64(time.Since(start)) / float64(time.Millisecond)
  }
  w.wg.Done()
}

// This returns a unique entity ID, which
// can be used with a new entity.
func (w *World) NextEID() uint32 {
  w.next_eid++
  return w.next_eid
}

// This adds a player to the given world. It should
// not be called, as that player will not be removed
// from it's previous world. Instead, you should call
// WorldManager.MovePlayerToWorld(name string). This
// will send all neccessary packets to allow the player
// to spawn in.
func (w *World) AddPlayer(p *player.Player) {
  // send chunk data
  for x := -w.view_distance; x <= w.view_distance; x++ {
    for z := -w.view_distance; z <= w.view_distance; z++ {
      pos := block.ChunkPos{x, z}
      chunk, _ := w.Chunk(pos)
      w.GenerateNewChunkPacket(chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
      w.GenerateUpdateLightPacket(chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
    }
  }

  // send spawn position (is where the compass points)
  out := packet.NewOutgoingPacket(packet.Clientbound_SpawnPosition)
  out.SetPosition(0, block.Pos{0, 0, 0})
  out.Send(p.PacketStream)

  // send player position and look
  out = packet.NewOutgoingPacket(packet.Clientbound_PlayerPositionAndLook)
  out.SetByte(0, 0)
  out.SetInt(0, 0)
  out.SetFloat(0, 0)
  out.SetFloat(1, 0)
  out.SetDouble(0, p.X())
  out.SetDouble(1, p.Y())
  out.SetDouble(2, p.Z())
  out.Send(p.PacketStream)

  w.players_lock.Lock()
  w.players[p.UUID] = p

  w.entities_lock.Lock()
  w.entities[p.EID()] = p
  w.entities_lock.Unlock()

  player_list := []*player.Player{}
  for _, p := range w.players {
    player_list = append(player_list, p)
  }
  w.players_lock.Unlock()

  // This locks players_lock!
  w.SendToAll(GeneratePlayerInfoPacket(player_list, 0))

  spawn_player_packet := packet.NewOutgoingPacket(packet.Clientbound_SpawnPlayer)
  spawn_player_packet.SetInt(0, int32(p.EID()))
  spawn_player_packet.SetUUID(0, p.UUID)
  spawn_player_packet.SetDouble(0, p.X())
  spawn_player_packet.SetDouble(1, p.Y())
  spawn_player_packet.SetDouble(2, p.Z())
  spawn_player_packet.SetByte(0, p.YawByte())
  spawn_player_packet.SetByte(1, p.PitchByte())

  join_chat := util.NewChat()
  join_chat.AddSection(p.Name())
  join_chat.AddSectionColor(" has joined the game", "yellow")
  w.players_lock.Lock()
  for id, other_player := range w.players {
    // send everyone the chat message about a new player joining
    other_player.SendChat(join_chat)
    if id != p.UUID {
      // show the new player to the already existing player
      spawn_player_packet.Send(other_player.PacketStream)
      // show one of the already online players to the new player
      spawn_other_player_packet := packet.NewOutgoingPacket(packet.Clientbound_SpawnPlayer)
      spawn_other_player_packet.SetInt(0, int32(other_player.EID()))
      spawn_other_player_packet.SetUUID(0, other_player.UUID)
      spawn_other_player_packet.SetDouble(0, other_player.X())
      spawn_other_player_packet.SetDouble(1, other_player.Y())
      spawn_other_player_packet.SetDouble(2, other_player.Z())
      spawn_other_player_packet.SetByte(0, other_player.YawByte())
      spawn_other_player_packet.SetByte(1, other_player.PitchByte())
      spawn_other_player_packet.Send(p.PacketStream)
    }
  }
  w.players_lock.Unlock()

  p.UpdateAbilities()

  go w.run_player_update_loop(p)
}

// This removes the player from the server.
func (w *World) DisconnectPlayer(p *player.Player) {
  w.players_lock.Lock()
  _, ok := w.players[p.UUID]
  if !ok {
    w.players_lock.Unlock()
    return
  }
  delete(w.players, p.UUID)
  w.players_lock.Unlock()

  player_info_packet := GeneratePlayerInfoPacket([]*player.Player{p}, 4)

  destroy_entity_packet := packet.NewOutgoingPacket(packet.Clientbound_DestroyEntity)
  destroy_entity_packet.SetIntArray(0, []int32{int32(p.EID())})

  leave_chat := util.NewChat()
  leave_chat.AddSection(p.Name())
  leave_chat.AddSectionColor(" has left the game", "yellow")

  for _, p := range w.players {
    player_info_packet.Send(p.PacketStream)
    destroy_entity_packet.Send(p.PacketStream)
    p.SendChat(leave_chat)
  }
}

// This removes the player from the server, and gives them a disconnect reason.
// This is shown on a dirt screen in the vanilla client.
func (w *World) KickPlayer(p *player.Player, reason *util.Chat) {
  w.players_lock.Lock()
  _, ok := w.players[p.UUID]
  w.players_lock.Unlock()
  if !ok {
    return
  }

  disconnect_packet := packet.NewOutgoingPacket(packet.Clientbound_Disconnect)
  disconnect_packet.SetString(0, reason.ToJSON())
  disconnect_packet.Send(p.PacketStream)

  w.DisconnectPlayer(p)
}

// This gets the chunk at the given location.
// If the chunk already exists, it returns that chunk, and false.
// If terrain generation is enabled, then it generates a new chunk, at returns that (and false).
// Otherwise, it will return the default chunk, and true.
func (w *World) Chunk(pos block.ChunkPos) (*chunk.MultiChunk, bool) {
  w.chunks_lock.Lock()
  chunk, ok := w.chunks[pos]
  w.chunks_lock.Unlock()
  if !ok {
    if !w.locked_settings.GenerateTerrain {
      return w.default_chunk, true
    } else {
      w.generate_chunk(pos)
      w.chunks_lock.Lock()
      chunk, _ := w.chunks[pos]
      w.chunks_lock.Unlock()
      return chunk, false
    }
  }
  return chunk, false
}

// This is the same as Chunk(), but if Chunk() would return
// the default chunk, this function generates a new empty chunk.
func (w *World) ChunkNotDefault(pos block.ChunkPos) *chunk.MultiChunk {
  w.chunks_lock.Lock()
  chunk, ok := w.chunks[pos]
  w.chunks_lock.Unlock()
  if !ok {
    return w.generate_chunk(pos)
  }
  return chunk
}

// This returns the chunk at the given block.
// This returns four values: The chunk (can be the default chunk),
// the chunk's position, and a bool for if this is the default chunk.
func (w *World) ChunkAtBlock(pos block.Pos) (*chunk.MultiChunk, block.ChunkPos, bool) {
  chunk_pos := pos.ChunkPos()
  c, def := w.Chunk(chunk_pos)
  return c, chunk_pos, def
}

// This gets a pointer to a block.State.
// Block states know everything about their current block,
// as they contain pointers to the world and chunk they are in.
// If you only want to set a block, you should call w.SetBlock(),
// as that does not create a new struct. This function
// will always create a new block.State, which can be
// expensive if called a lot.
func (w *World) Block(pos block.Pos) *block.State {
  if pos.Y > 255 || pos.Y < 0 {
    return nil
  }

  chunk, chunk_pos, is_default := w.ChunkAtBlock(pos)
  if is_default {
    chunk = w.generate_chunk(chunk_pos)
  }
  return chunk.GetBlockAt(pos)
}

// This returns the block type at the given position.
// This does not allocate any memory, so it's a lot faster
// than calling w.Block(pos).Type()
func (w *World) BlockType(pos block.Pos) *block.Type {
  if pos.Y > 255 {
    return block.GetKind("minecraft:air").DefaultType()
  }

  chunk, _, _ := w.ChunkAtBlock(pos)
  return chunk.GetBlock(pos.ChunkRelPos())
}

// This returns the block id at the given position.
// It is shorthand for world.BlockType(pos).State().
func (w *World) BlockState(pos block.Pos) uint32 {
  return w.BlockType(pos).State()
}

// This sets the block at the given position to the given type.
// This is faster than world.Block(pos).SetType(t), since this
// does not allocate any memory on the heap.
func (w *World) SetBlock(pos block.Pos, t *block.Type) {
  if pos.Y > 255 {
    return
  }

  chunk, chunk_pos, is_default := w.ChunkAtBlock(pos)
  if is_default {
    chunk = w.generate_chunk(chunk_pos)
  }
  chunk.SetBlock(pos.ChunkRelPos(), t)

  w.send_block_update(pos)
}

// This sets the block property at the given position.
// See block.State.SetProperty() for more information.
func (w *World) SetBlockProperty(pos block.Pos, category, name string) {
  block := w.Block(pos)
  block.SetProperty(category, name)

  w.send_block_update(pos)
}

// This sets multiple block properties at the given postition.
// See block.State.SetProperties() for more information.
func (w *World) SetBlockProperties(pos block.Pos, properties map[string]string) {
  block := w.Block(pos)
  block.SetProperties(properties)

  w.send_block_update(pos)
}

// This sends a block update to everyone in render distance.
// As the name implies, this is most commonly used to fix
// 'ghost' (client-side only) blocks
func (w *World) FixDesync(pos block.Pos) {
  w.send_block_update(pos)
}

// This is called whenever a player tries to break a block.
// This will spawn block breaking particles,
// and set the block to air.
func (w *World) BreakBlock(p *player.Player, pos block.Pos) {
  if !w.PlaceBlock(p, pos, block.Air()) {
    // Means they cannot reach the block
    return
  }

  old_block_state := w.BlockType(pos).State()
  block_break_particle_packet := packet.NewOutgoingPacket(packet.Clientbound_Effect)
  block_break_particle_packet.SetInt(0, 2001)
  block_break_particle_packet.SetInt(1, int32(old_block_state))
  block_break_particle_packet.SetPosition(0, pos)
  block_break_particle_packet.SetBool(0, false)

  w.SendInViewNotID(pos.ChunkPos(), p.UUID, block_break_particle_packet)
}

// This checks if the player can place the block.
//
// TODO: Actually implement this function. Currently,
// it always returns true.
func (w *World) PlayerCanPlaceBlock(player *player.Player, pos block.Pos) bool {
  return true
}

// This is called whenever a player tries to place a block.
// It first calls PlayerCanPlaceBlock, and then either places the block,
// or sends a packet to cancel the block place on the client. This returns
// true of the block place was successful.
func (w *World) PlaceBlock(p *player.Player, pos block.Pos, t *block.Type) bool {
  if pos.Y > 255 {
    return false
  }

  if w.PlayerCanPlaceBlock(p, pos) {
    w.SetBlock(pos, t)
    return true
  } else {
    w.send_block_update(pos)
    return false
  }
}

// This will send a block update to all players in render distance of the given block.
// It also handles translation of block ids between versions.
func (w *World) send_block_update(pos block.Pos) {
  t := w.BlockType(pos)
  w.RunInView(pos.ChunkPos(), func (p *player.Player) bool {
    out := packet.NewOutgoingPacket(packet.Clientbound_BlockChange)
    out.SetPosition(0, pos)
    out.SetInt(0, int32(t.StateVersion(p.Version().BlockVersion())))
    out.Send(p.PacketStream)
    return true
  })
}

// This will try to unload the chunk at the given position. It
// will return true if the chunk was unloaded. If the world is
// using default chunks, this will always return false. If any
// players are within view distance of the given chunk, it
// will return false. Otherwise, it will call Unload(pos).
func (w *World) TryUnload(pos block.ChunkPos) bool {
  if w.default_chunk != nil {
    return false
  }
  visible := false
  w.RunInView(pos, func(p *player.Player) bool {
    visible = true
    return false
  })
  if visible {
    return false
  }
  w.Unload(pos)
  return true
}

// This sets the chunk at the given chunk coordinates to
// a default chunk, or an empty chunk if default chunk is
// not enabled. If the previous chunk was not a default
// chunk, or if it was a modified terrain chunk, then it
// will be saved to disk.
func (w *World) Unload(pos block.ChunkPos) {
  w.chunks_lock.Lock()
  _, ok := w.chunks[pos]
  if !ok {
    log.Error("Cannot clear chunk at ", pos, " as it does not exist")
    w.chunks_lock.Unlock()
    return
  }
  delete(w.chunks, pos)
  w.chunks_lock.Unlock()

  // We can't use SendInView here, as the packet is generated differently for each player
  w.RunInView(pos, func(p *player.Player) bool {
    w.GenerateNewChunkPacket(w.default_chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
    w.GenerateUpdateLightPacket(w.default_chunk, pos, p.Version().BlockVersion()).Send(p.PacketStream)
    return true
  })
}

// This will loop through all loaded chunks, and reset them to
// default chunks, if they are void (all air).
func (w *World) ClearVoid() {
  for p, c := range w.Chunks() {
    if c.IsAll(block.Air()) {
      w.Unload(p)
    }
  }
}

// This saves the chunk at the given coordinate to disk,
// and puts it within the given directory.
func (w *World) SaveChunkToDiskDir(pos block.ChunkPos, dir string) {
  w.chunks_lock.Lock()
  chunk, ok := w.chunks[pos]
  w.chunks_lock.Unlock()
  if !ok {
    return
  }
  chunk.SaveToDisk(filepath.Join(dir, strconv.Itoa(int(pos.X)) + "." + strconv.Itoa(int(pos.Z)) + ".dat"))
}

// This saves the chunk at the given coordinate to S3,
// given the bucket and prefix.
func (w *World) SaveChunkToS3Dir(pos block.ChunkPos, bucket, key string) {
  w.chunks_lock.Lock()
  chunk, ok := w.chunks[pos]
  w.chunks_lock.Unlock()
  if !ok {
    return
  }
  chunk.SaveToS3(bucket, filepath.Join(key, strconv.Itoa(int(pos.X)) + "." + strconv.Itoa(int(pos.Z)) + ".dat"), w.manager.s3uploader)
}

// This saves the given chunk to disk,
// at the default directory.
func (w *World) SaveChunkToDisk(pos block.ChunkPos) {
  w.SaveChunkToDiskDir(pos, getChunkDir(w.name))
}

// This saves the chunk to S3,
// at the default location.
func (w *World) SaveChunkToS3(pos block.ChunkPos, bucket string) {
  w.SaveChunkToS3Dir(pos, bucket, w.name)
}

// This saves all chunk to disk, at the given directory.
func (w *World) SaveToDiskDir(dir string) {
  w.ClearVoid()
  chunks_dir := getChunkDir(dir)
  log.Info("Saving world to directory ", chunks_dir)
  os.MkdirAll(chunks_dir, 0755)
  files, err := ioutil.ReadDir(chunks_dir)
  if err != nil {
    panic("Cannot read from chunks dir, err: " + err.Error())
  }
  w.chunks_lock.Lock()
  for _, file := range files {
    sections := strings.Split(file.Name(), ".")
    x, err := strconv.Atoi(sections[0])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    z, err := strconv.Atoi(sections[1])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    _, ok := w.chunks[block.ChunkPos{int32(x), int32(z)}]
    if !ok {
      os.Remove(filepath.Join(chunks_dir, file.Name()))
    }
  }
  w.chunks_lock.Unlock()
  for p, _ := range w.chunks {
    w.SaveChunkToDiskDir(p, chunks_dir)
  }
}

// This saves all chunks to S3, given the bucket name and prefix
func (w *World) SaveToS3Dir(bucket, dir string) {
  chunks_dir := filepath.Join(dir, "chunks")
  log.Info("Saving world to s3 at ", chunks_dir)
  out, err := w.manager.s3.ListObjectsV2(&s3.ListObjectsV2Input{
    Bucket: &bucket,
    Prefix: &chunks_dir,
  })
  if err != nil {
    panic("Cannot read from chunks in s3, err: " + err.Error())
  }
  w.chunks_lock.Lock()
  for _, file := range out.Contents {
    log.Info("Got file on s3: ", *file.Key)
    path := strings.Split(*file.Key, "/")
    name := path[len(path) - 1]
    sections := strings.Split(name, ".")
    x, err := strconv.Atoi(sections[0])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    z, err := strconv.Atoi(sections[1])
    if err != nil {
      panic("Invalid filename: " + err.Error())
    }
    _, ok := w.chunks[block.ChunkPos{int32(x), int32(z)}]
    if !ok {
      log.Info("Deleting file from s3: ", *file.Key)
      _, err := w.manager.s3.DeleteObject(&s3.DeleteObjectInput{
        Bucket: &bucket,
        Key: file.Key,
      })
      if err != nil {
        panic("Cannot delete object from s3, err: " + err.Error())
      }
    }
  }
  w.chunks_lock.Unlock()
  for p, _ := range w.chunks {
    w.SaveChunkToS3Dir(p, bucket, chunks_dir)
  }
}

// This saves all chunks to the default directory.
func (w *World) SaveToDisk() {
  w.SaveToDiskDir(w.name)
}

// This saves all chunks to the default location.
func (w *World) SaveToS3() {
  w.SaveToS3Dir("cubiness-data", w.name)
}

// This gets the header of the tab-list. This will
// be able to be overwritten by user code in the future.
func (w *World) get_playerlist_header() *util.Chat {
  c := util.NewChat()
  c.AddSectionColor("Sugarcane\n", "green")
  return c
}

// This gets the footer of the tab-list. This will
// be able to be overwritten by user code in the future.
func (w *World) get_playerlist_footer(mspt float64) *util.Chat {
  c := util.NewChatFromString("\n")
  color := "green"
  if mspt > 50 {
    color = "red"
  } else if mspt > 30 {
    color = "gold"
  } else if mspt > 10 {
    color = "yellow"
  }
  c.AddSectionColor(fmt.Sprintf("MSPT: %0.4f", mspt), color)
  return c
}

func getChunkDir(dir string) string {
  return filepath.Join("worlds", dir, "chunks")
}
