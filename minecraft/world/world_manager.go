package world

import (
  "sync"
  "time"
  "io/ioutil"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"

  "github.com/aws/aws-sdk-go/service/s3"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type WorldManager struct {
  // Map of world name -> world
  worlds map[string]*World
  // Map of player -> world name (the world they are in)
  players map[*player.Player]string
  // Map of player UUIDs -> time they reserved the slot

  reserved_slots map[util.UUID]time.Time
  // If a player reserves a slot, and joins later reserve_time + reserved_slot_timeout,
  // they will not be allowed to join.
  reserved_slot_timeout time.Duration
  // Maximum number of players online.
  max_players int
  // The name of the default world
  default_world_name string
  // Resource pack url
  resource_pack string

  // The path to all worlds on disk.
  path string

  // Used to upload worlds to s3
  s3uploader *s3manager.Uploader
  s3 *s3.S3
}

func NewWorldManagerWithSlots(sess *session.Session, path, default_world_name string, timeout time.Duration, max_players int) *WorldManager {
  w := NewWorldManager(sess, path, default_world_name)
  w.reserved_slots = make(map[util.UUID]time.Time)
  w.reserved_slot_timeout = timeout
  w.max_players = max_players
  return w
}

func NewWorldManager(sess *session.Session, path, default_world_name string) *WorldManager {
  w := WorldManager{}
  w.worlds = make(map[string]*World)
  w.players = make(map[*player.Player]string)
  w.path = path
  w.default_world_name = default_world_name
  if sess != nil {
    w.s3uploader = s3manager.NewUploader(sess)
    w.s3 = s3.New(sess)
  }
  return &w
}

// Sets the resource pack url. If url is "", then the resource pack
// is disabled.
func (w *WorldManager) SetResourcePack(url string) {
  w.resource_pack = url
}

func (w *WorldManager) StartUpdateLoop(waitGroup *sync.WaitGroup) {
  for _, world := range w.worlds {
    world.StartUpdateLoop(waitGroup)
  }
}

// This returns the map of worlds. It is a reference to the actual
// worlds map, so modifying it can break things.
func (w *WorldManager) Worlds() map[string]*World {
  return w.worlds
}

// This returns the default world. This will always return a valid
// world pointer.
func (w *WorldManager) DefaultWorld() *World {
  return w.worlds[w.default_world_name]
}

// This adds a new world
func (w *WorldManager) AddWorld(name string) {
  _, ok := w.worlds[name]
  if ok {
    log.Error("A world with the name ", name, " already exists!")
    return
  }
  log.Info("Adding world '", name, "'")
  world := NewWorld(w, name)
  if name == w.default_world_name {
    world.Settings.GenerateTerrain = true
    world.Settings.Seed = 0
  }
  world.LockSettings()
  w.worlds[name] = world
}

// This loads all worlds from disk or S3
func (w *WorldManager) Load() {
  world_dirs, _ := ioutil.ReadDir(w.path)
  has_default_world := false
  for _, world_dir := range world_dirs {
    if world_dir.Name() == w.default_world_name {
      has_default_world = true
    }
    w.AddWorld(world_dir.Name())
  }
  if !has_default_world {
    w.AddWorld(w.default_world_name)
  }
  for _, world := range w.worlds {
    world.Load()
  }
}

// This finds the player with the given username,
// by looping through all players.
// This is relatively slow, as it needs to (on average) check at least
// half of the players online. So avoid calling this function
// if you know which world they are in. If you know the world,
// then you can call w.GetPlayer(), which is a single map lookup.
// This will return nil if the player is not online.
func (w *WorldManager) FindPlayerByName(name string) *player.Player {
  for player, _ := range w.players {
    if player.Name() == name {
      return player
    }
  }
  return nil
}

// This gets the world that a player is in.
// This is very fast, as the world manager stores all players
// in a map of player -> world.
func (w *WorldManager) GetWorldOfPlayer(player *player.Player) *World {
  return w.worlds[w.players[player]]
}

// This moves the given player to a new world.
// If the player is not in any world, it will do nothing.
// If the world name is invalid, it log a warning, and return.
func (w *WorldManager) MovePlayerToWorld(player *player.Player, world_name string) {
  old_world := w.GetWorldOfPlayer(player)
  if old_world == nil {
    return
  }
  world, ok := w.worlds[world_name]
  if !ok {
    log.Warn("Unknown world " + world_name)
    return
  }

  old_world.DisconnectPlayer(player)

  // send them to the nether, then the overworld, to make them unload all chunks, and remove bugs with respawning players in the same world
  out := packet.NewOutgoingPacket(packet.Clientbound_Respawn)
  out.SetInt(0, 1) // nether
  out.SetByte(0, 1) // gamemode creative
  out.Send(player.PacketStream)

  out = packet.NewOutgoingPacket(packet.Clientbound_Respawn)
  out.SetInt(0, 0) // overworld
  out.SetByte(0, 1) // gamemode creative
  out.Send(player.PacketStream)

  // this is when a handoff should happen, if moving a player to another server
  w.players[player] = world_name
  world.AddPlayer(player)
}

// This generates a map of all players online. It is not very fast,
// as it needs to copy all players into a new map each time you call
// this function.
func (w *WorldManager) Players() map[util.UUID]*player.Player {
  out := make(map[util.UUID]*player.Player)
  for player, _ := range w.players {
    out[player.UUID] = player
  }
  return out
}

// This gets the number of online players.
func (w *WorldManager) LenPlayers() int {
  return len(w.players)
}

// This gets the maximum number of players.
func (w *WorldManager) MaxPlayers() int {
  return w.max_players
}

// This reserves a list of UUIDS, and returns true if all of those
// players can join. This is part of the handshake system for switching
// servers.
// This will make sure that the current number of online players,
// plus the new players passed in, is still <= max_players.
func (w *WorldManager) ReservePlayers(players []util.UUID) bool {
  if w.reserved_slots == nil {
    log.Warn("Reserving players on a server that does not need reservations ", players)
    return true
  }
  // If len of new players is 3, and there are 8 people online, but 2 reserved, and a max of 12,
  // then we want to deny the connection. (3 + 8 + 2 > 13 > 12)
  if len(players) + w.LenPlayers() + len(w.reserved_slots) > w.max_players {
    return false
  }
  for _, p := range players {
    _, ok := w.reserved_slots[p]
    if ok {
      log.Warn("Reserving a slot for a player who already has a slot reserved ", p)
    }
    // If the player is being reserved for a second time, we want to reset their timestamp anyway.
    w.reserved_slots[p] = time.Now()
  }
  return true
}

// This checks if a single player can join. If they are already online,
// it returns fals. If the server requires slot reservation, and this player
// has not reserved a slot, then it will return false. If they reserved a slot,
// but it has been too long since they reserved that slot, it will return false.
// Otherwise, it returns true.
func (w *WorldManager) PlayerCanJoin(id util.UUID, name string) bool {
  for _, world := range w.worlds {
    players := world.Players()
    _, ok := players[id]
    if ok {
      log.Warn("Player ", name, " tried to join while already connected")
      return false
    }
  }
  if w.reserved_slots != nil {
    t, ok := w.reserved_slots[id]
    if !ok {
      log.Warn("Player ", name, " tried to join before reserving a slot!")
      return false
    }
    delete(w.reserved_slots, id)
    if time.Since(t) > w.reserved_slot_timeout {
      log.Warn("Player ", name, " tried to join, but their reserved slot was timeout out")
      return false
    }
  }
  return true
}

// This adds the player into the default world, and ignores any slot reservation
// stuff.
func (w *WorldManager) AddPlayer(player *player.Player, send_login bool) {
  world := w.GetWorldOfPlayer(player)
  if world != nil {
    return
  }

  if send_login {
    out := packet.NewOutgoingPacket(packet.Clientbound_JoinGame) // login packet
    out.SetInt(0, int32(player.EID())) // player's EID
    out.SetInt(1, 0) // overworld
    out.SetInt(2, w.DefaultWorld().view_distance) // view distance
    out.SetByte(0, 1) // gamemode creative
    out.SetLong(0, 0) // sha256 of world seed
    out.SetString(0, "flat") // type of world
    out.SetBool(0, false) // reduced debug info
    out.SetBool(1, true) // enable respawn screen
    out.Send(player.PacketStream)
  }

  w.players[player] = w.DefaultWorld().Name()
  w.DefaultWorld().AddPlayer(player)

  // This must be after world.AddPlayer, since the player position and look
  // packet will hide the resource pack prompt.
  if w.resource_pack != "" {
    // Send resource pack
    out := packet.NewOutgoingPacket(packet.Clientbound_ResourcePack)
    out.SetString(0, w.resource_pack)
    out.SetString(1, "12345") // Should be the hash of the resource pack
    out.Send(player.PacketStream)
  }
}

// This removes the player from the world, and disconnects them.
func (w *WorldManager) DisconnectPlayer(player *player.Player) {
  world := w.GetWorldOfPlayer(player)
  if world != nil {
    delete(w.players, player)
    world.DisconnectPlayer(player)
  }
}

// This removes the player from the world, and sends them a disconnect message.
func (w *WorldManager) KickPlayer(player *player.Player, reason *util.Chat) {
  world := w.GetWorldOfPlayer(player)
  if world != nil {
    world.KickPlayer(player, reason)
    w.DisconnectPlayer(player)
  }
}
