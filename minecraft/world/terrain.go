package world

import (
  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/world/chunk"
  "gitlab.com/macmv/sugarcane/minecraft/world/terrain"
)

func (w *World) OnChunkSet(f func (pos block.ChunkPos)) {
  w.on_chunk_set = f
}

func (w *World) generate_chunk(chunk_pos block.ChunkPos) *chunk.MultiChunk {
  w.chunks_lock.Lock()
  _, ok := w.chunks[chunk_pos]
  if ok {
    log.Error(chunk_pos)
    panic("Chunk is already at position")
  }

  if w.generator == nil && w.locked_settings.GenerateTerrain {
    w.generator = terrain.NewVoronoi(w.locked_settings.Seed)
    // If the user has not set the world generator,
    // then we want to only use the default settings.
    w.generator.Init()
    w.generator.Lock()
  }

  chunk := chunk.NewMultiChunk()
  // log.Info("Saving chunk at ", x, z)
  // Generates land
  if w.Generator() != nil {
    w.Generator().FillChunk(chunk_pos, chunk)
  }
  w.chunks[chunk_pos] = chunk
  if w.on_chunk_set != nil {
    w.on_chunk_set(chunk_pos)
  }
  w.chunks_lock.Unlock()
  return chunk
}

func (w *World) Generator() terrain.WorldGenerator {
  return w.generator
}
