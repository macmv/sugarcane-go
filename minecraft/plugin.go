package minecraft

import (
  "plugin"
  "io/ioutil"

  "gitlab.com/macmv/log"
)

// This is never used. This will be removed in the near future.
// This was setup to be like Spigot's plugin system, but in golang, all plugins are statically linked.
// So it created these massive plugin files, that just made the server huge.
// Also the core server does nothing, so I thought there was no point in having it load plugins.
// Since the core server cannot do much, I though it would be best that people write their own main() funcitons,
// instead of using the server's main.
type Plugin interface {
  OnEnable(minecraft *Minecraft)
  OnDisable()
}

type PluginManager struct {
  plugins []Plugin
  minecraft *Minecraft
}

func NewPluginManager(minecraft *Minecraft) *PluginManager {
  m := PluginManager{}
  m.minecraft = minecraft
  return &m
}

func (m *PluginManager) LoadPlugins() {
  plugin_files, err := ioutil.ReadDir("plugins")
  if err != nil {
    panic(err)
  }
  for _, plugin_file := range plugin_files {
    if !plugin_file.IsDir() {
      log.Info("Found plugin: " + plugin_file.Name())
      plug, err := plugin.Open("plugins/" + plugin_file.Name())
      if err != nil {
        log.Warn("Could not load plugin " + plugin_file.Name())
        log.Error("Error: " + err.Error())
        continue
      }
      init_func, err := plug.Lookup("Init")
      if err != nil {
        log.Warn("Could not load plugin " + plugin_file.Name())
        log.Error("Error: " + err.Error())
        continue
      }
      init_func_typed, ok := init_func.(func() Plugin)
      if !ok {
        log.Warn("Could not load plugin " + plugin_file.Name())
        log.Error("Error: Init function in plugin is not the right type!")
        continue
      }
      new_plugin := init_func_typed()
      m.plugins = append(m.plugins, new_plugin)
    }
  }
}

func (m *PluginManager) EnablePlugins() {
  for _, plugin := range m.plugins {
    plugin.OnEnable(m.minecraft)
  }
}

func (m *PluginManager) DisablePlugins() {
  for _, plugin := range m.plugins {
    plugin.OnDisable()
  }
}
