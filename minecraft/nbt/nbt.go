package nbt

import (
  "fmt"
  "encoding/binary"
  "compress/gzip"
  "bytes"
  "io/ioutil"
  "io"
  "math"
  "strconv"
)

type Tag struct {
  tag_type byte
  name string
  data []byte
  children []*Tag
  length int32
}

func ReadFile(filename string) *Tag {
  data, err := ioutil.ReadFile(filename)
  if err != nil {
    panic(err)
  }
  return ReadData(data)
}

func ReadData(data []byte) *Tag {
  if len(data) == 0 {
    return Empty()
  }
  var parseData []byte
  if data[0] == 0x1f && data[1] == 0x8b { // gzipped
    b := bytes.NewBuffer(data)
    var r io.Reader
    r, err := gzip.NewReader(b)
    if err != nil {
      panic(err)
    }
    var buf bytes.Buffer
    _, err = buf.ReadFrom(r)
    if err != nil {
      panic(err)
    }
    parseData = buf.Bytes()
  } else {
    parseData = data
  }
  return parseNBT(parseData, 255)
}

// tag type is either a type of tag (for items in a list) or 255 to mean that the type is ine first byte in the data
func parseNBT(data []byte, tag_type byte) *Tag {
  t := Tag{}
  var payload []byte
  var name_length int32 = 0
  if tag_type == 255 {
    tag_type = data[0]
    t.tag_type = tag_type
    if tag_type == 0 {
      payload = data[1:]
      t.name = "None"
    } else {
      name_length = int32(binary.BigEndian.Uint16(data[1:3])) + 2
      t.name = string(data[3:name_length + 1])
      payload = data[name_length + 1:]
      t.length = t.calculateSize(true)
    }
  } else {
    payload = data
    t.name = "None"
    t.tag_type = tag_type
    t.length = t.calculateSize(false)
  }
  if t.tag_type == TAG_End { // 0x00
    t.data = []byte{}
  } else if t.tag_type == TAG_Byte { // 0x01
    t.data = payload[0:1]
  } else if t.tag_type == TAG_Short { // 0x02
    t.data = payload[0:2]
  } else if t.tag_type == TAG_Int { // 0x03
    t.data = payload[0:4]
  } else if t.tag_type == TAG_Long { // 0x04
    t.data = payload[0:8]
  } else if t.tag_type == TAG_Float { // 0x05
    t.data = payload[0:4]
  } else if t.tag_type == TAG_Double { // 0x06
    t.data = payload[0:8]
  } else if t.tag_type == TAG_ByteArray { // 0x07
    length := binary.BigEndian.Uint32(payload)
    t.data = payload[4:length + 4]
  } else if t.tag_type == TAG_String { // 0x08
    length := binary.BigEndian.Uint16(payload)
    t.data = payload[2:length + 2]
  } else if t.tag_type == TAG_List { // 0x09
    list_type := payload[0]
    list_length := int(binary.BigEndian.Uint32(payload[1:]))
    payload = payload[5:]
    for i := 0; i < list_length; i++ {
      child := parseNBT(payload, list_type)
      length := int(child.Size())
      payload = payload[length:]
      t.children = append(t.children, child)
    }
  } else if t.tag_type == TAG_Compound { // 0x0a
    for {
      child := parseNBT(payload, 255)
      if child.tag_type == 0 {
        break
      }
      length := int(child.Size()) + 1
      payload = payload[length:]
      t.children = append(t.children, child)
    }
  } else if t.tag_type == TAG_IntArray { // 0x0b
    length := binary.BigEndian.Uint32(payload) * 4
    t.data = payload[4:length + 4]
  } else if t.tag_type == TAG_LongArray { // 0x0c
    length := binary.BigEndian.Uint32(payload) * 8
    t.data = payload[4:length + 4]
  } else {
    panic("Invalid NBT data!")
  }
  return &t
}

func (t *Tag) Size() int32 {
  var size int32 = 0
  if t.tag_type == 0x07 { // byte array
    size += int32(len(t.data))
  } else if t.tag_type == 0x08 { //string
    size += int32(len(t.data))
  } else if t.tag_type == 0x09 { // list
    for i := 0; i < len(t.children); i++ {
      size += t.children[i].Size()
    }
  } else if t.tag_type == 0x0a { // compound
    for i := 0; i < len(t.children); i++ {
      size += t.children[i].Size() + 1
    }
  } else if t.tag_type == 0x0b { // int array
    size += int32(len(t.data))
  } else if t.tag_type == 0x0c { // long array
    size += int32(len(t.data))
  }
  return size + t.length
}

func (t *Tag) Print() {
  fmt.Println(t.String())
}

func (t *Tag) String() string {
  return t.StringIndent(0)
}

func (t *Tag) StringIndent(indent int) string {
  out := ""
  for i := 0; i < indent; i++ {
    out += " "
  }
  if t.tag_type == TAG_End { // 0x00
    out += fmt.Sprintf("TAG_End()\n")
  } else if t.tag_type == TAG_Byte { // 0x01
    out += fmt.Sprintf("TAG_Byte('%s'): %x\n", t.name, t.GetByte())
  } else if t.tag_type == TAG_Short { // 0x02
    out += fmt.Sprintf("TAG_Short('%s'): %d\n", t.name, t.GetShort())
  } else if t.tag_type == TAG_Int { // 0x03
    out += fmt.Sprintf("TAG_Int('%s'): %d\n", t.name, t.GetInt())
  } else if t.tag_type == TAG_Long { // 0x04
    out += fmt.Sprintf("TAG_Long('%s'): %d\n", t.name, t.GetLong())
  } else if t.tag_type == TAG_Float { // 0x05
    out += fmt.Sprintf("TAG_Float('%s'): %f\n", t.name, t.GetFloat())
  } else if t.tag_type == TAG_Double { // 0x06
    out += fmt.Sprintf("TAG_Double('%s'): %f\n", t.name, t.GetDouble())
  } else if t.tag_type == TAG_ByteArray { // 0x07
    out += fmt.Sprintf("TAG_ByteArray('%s'): %x\n", t.name, t.GetByteArray())
  } else if t.tag_type == TAG_String { // 0x08
    out += fmt.Sprintf("TAG_String('%s'): %s\n", t.name, t.GetString())
  } else if t.tag_type == TAG_List { // 0x09
    out += fmt.Sprintf("TAG_List('%s') {\n", t.name)
    for i := 0; i < len(t.children); i++ {
      out += t.children[i].StringIndent(indent + 2)
    }
    for i := 0; i < indent; i++ {
      out += " "
    }
    out += "}\n"
  } else if t.tag_type == TAG_Compound { // 0x0a
    out += fmt.Sprintf("TAG_Compound('%s') {\n", t.name)
    for i := 0; i < len(t.children); i++ {
      out += t.children[i].StringIndent(indent + 2)
    }
    for i := 0; i < indent; i++ {
      out += " "
    }
    out += "}\n"
    // length := binary.BigEndian.Uint16(payload)
    // t.data = payload[0:length]
  } else if t.tag_type == TAG_IntArray { // 0x0b
    out += fmt.Sprintf("TAG_IntArray('%s')\n", t.name)
  } else if t.tag_type == TAG_LongArray { // 0x0c
    out += fmt.Sprintf("TAG_LongArray('%s') {\n", t.name)
    for i := 0; i < len(t.data) / 8; i++ {
      for i := 0; i < indent + 2; i++ {
        out += " "
      }
      out += fmt.Sprintf("long: 0x%s\n", strconv.FormatUint(binary.BigEndian.Uint64(t.data[i * 8:i * 8 + 8]), 2))
    }
    for i := 0; i < indent; i++ {
      out += " "
    }
    out += "}\n"
  } else {
    panic("Invalid NBT data!")
  }
  return out
}

func (t *Tag) Type() byte {
  return t.tag_type
}

func (t *Tag) GetByte() byte {
  if t.tag_type != TAG_Byte {
    panic("Tag " + t.name + " is not a byte!")
  }
  return byte(t.data[0])
}

func (t *Tag) GetShort() int16 {
  if t.tag_type != TAG_Short {
    panic("Tag " + t.name + " is not a short!")
  }
  return int16(binary.BigEndian.Uint16(t.data))
}

func (t *Tag) GetInt() int32 {
  if t.tag_type != TAG_Int {
    panic("Tag " + t.name + " is not an int!")
  }
  return int32(binary.BigEndian.Uint32(t.data))
}

func (t *Tag) GetLong() int64 {
  if t.tag_type != TAG_Long {
    panic("Tag " + t.name + " is not a long!")
  }
  return int64(binary.BigEndian.Uint64(t.data))
}

func (t *Tag) GetFloat() float32 {
  if t.tag_type != TAG_Float {
    panic("Tag " + t.name + " is not a float!")
  }
  return math.Float32frombits(binary.BigEndian.Uint32(t.data))
}

func (t *Tag) GetDouble() float64 {
  if t.tag_type != TAG_Double {
    panic("Tag " + t.name + " is not a double!")
  }
  return math.Float64frombits(binary.BigEndian.Uint64(t.data))
}

func (t *Tag) GetByteArray() []byte {
  if t.tag_type != TAG_ByteArray {
    panic("Tag " + t.name + " is not a byte array!")
  }
  return t.data
}

func (t *Tag) GetString() string {
  if t.tag_type != TAG_String {
    panic("Tag " + t.name + " is not a string!")
  }
  return string(t.data)
}

func (t *Tag) GetChildren() []*Tag {
  if t.tag_type != TAG_List {
    panic("Tag " + t.name + " is not a list!")
  }
  return t.children
}

func (t *Tag) GetListType() byte {
  if t.tag_type != TAG_List {
    panic("Tag " + t.name + " is not a list!")
  }
  if len(t.children) > 0 {
    return t.children[0].tag_type
  } else {
    return TAG_End
  }
}

func (t *Tag) Child(name string) *Tag {
  if t.tag_type != TAG_Compound {
    panic("Tag " + t.name + " is not a compound!")
  }
  for _, child := range t.children {
    if child.name == name {
      return child
    }
  }
  return nil
}
