package nbt_test

import (
  "testing"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "encoding/hex"
)

func TestNBTReadFile(t *testing.T) {
  data, _ := hex.DecodeString("1f8b0800000000000000ed54cf4f1a41147ec202cb9682b1c41063ccabb584a5dbcd421189b188162c9a0d1ad8a83186b82bc3822ebb6677b0f1d44b7b6c7aeb3fd3237f43cfbdf6bfa0c32f7b69cfbdf032c9f7e6bd6fe67b6f2679020454724f2c0e78cbb14d8d78f4e370623e087b1dc7a593180f8247ddee840262b5a2aac778765c57cba8550f1bc8d61e6a9586860dad7e587b8f83cf834f836fcf03106e5b8e3ebea5384c64fd10eada74a62340dc662e69e1b5d3bb73fa760b29db0be0efe83d1e385bef110856f5de5ddf0b40e05eb7fa64b704008c414c73c608554cd3202e7da4c0c8c210b3bade580b53a3ee448e450330b127538c4cf1e914a3538c85e1d99fe3b3f24481a57c33ddd8bbc7aa75135f281c08d72ed1593faf1d1b602159dffaf105fec1cefc9dbd00bcf140c9f885424046fe9eebea0f933a688760bbeb3237a3280a8ebbf5d06963ca4edbe9ece6e62b3bbd25be6449093daabb94fd187ee8d20eda6f154cb1683e2be19b9c8499bc8405096559164500ff2f28ae2ff2c2b2a42e1d20775a3bb98ccae729df5141c916b5c56da12aad2cc5317fba7a928e5e9d5ff81205231bd1f6b777aacd9572bc9edf585d4b97ae9217b944d080c8fa3ebfb3dc54cb07756ea3b676599293a9dc5150996bcc35e61aff57230842cbe91bd678c2ecfefc7afb7d78d384dfd4f2a4fb08060000")

  tag := nbt.ReadData(data)
  expected := nbt.NewCompoundTag("Level")
  expected.CompoundAddChild("byteTest", nbt.TAG_Byte).WriteByte(127)
  expected.CompoundAddChild("intTest", nbt.TAG_Int).WriteInt(2147483647)
  expected.CompoundAddChild("longTest", nbt.TAG_Long).WriteLong(9223372036854775807)
  expected.CompoundAddChild("floatTest", nbt.TAG_Float).WriteFloat(0.49823147058486938)
  expected.CompoundAddChild("doubleTest", nbt.TAG_Double).WriteDouble(0.49312871321823148)
  expected.CompoundAddChild("byteArrayTest", nbt.TAG_ByteArray).WriteByteArray([]byte{0})
  expected.CompoundAddChild("stringTest", nbt.TAG_String).WriteString("HELLO WORLD THIS IS A TEST STRING \xc3\x85\xc3\x84\xc3\x96!")
  list := expected.CompoundAddChild("listTest (long)", nbt.TAG_List)
  list.ListAddFirst(nbt.TAG_Long).WriteLong(11)
  list.ListAddChild().WriteLong(12)
  list.ListAddChild().WriteLong(13)
  list.ListAddChild().WriteLong(14)
  list.ListAddChild().WriteLong(15)
  list = expected.CompoundAddChild("listTest (compound)", nbt.TAG_List)
  item := list.ListAddFirst(nbt.TAG_Compound)
  item.CompoundAddChild("name", nbt.TAG_String).WriteString("Compound Tag #0")
  item.CompoundAddChild("created-on", nbt.TAG_Long).WriteLong(1264099775885)
  item = list.ListAddChild()
  item.CompoundAddChild("name", nbt.TAG_String).WriteString("Compound Tag #1")
  item.CompoundAddChild("created-on", nbt.TAG_Long).WriteLong(1264099775885)
  item = expected.CompoundAddChild("nested compound test", nbt.TAG_Compound)
  item_child := item.CompoundAddChild("egg", nbt.TAG_Compound)
  item_child.CompoundAddChild("name", nbt.TAG_String).WriteString("Eggbert")
  item_child.CompoundAddChild("value", nbt.TAG_Float).WriteFloat(0.5)
  item_child = item.CompoundAddChild("ham", nbt.TAG_Compound)
  item_child.CompoundAddChild("name", nbt.TAG_String).WriteString("Hampus")
  item_child.CompoundAddChild("value", nbt.TAG_Float).WriteFloat(0.75)

  tag.Print()
  expected.Print()
}
