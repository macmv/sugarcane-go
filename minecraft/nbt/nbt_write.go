package nbt

import (
  "encoding/binary"
  "math"
)

func NewCompoundTag(name string) *Tag {
  t := Tag{}
  t.tag_type = TAG_Compound
  t.name = name
  t.length = t.calculateSize(true)
  return &t
}

func Empty() *Tag {
  t := Tag{}
  t.tag_type = TAG_End
  return &t
}

func (t *Tag) CompoundAddChild(name string, tag_type byte) *Tag {
  if t.tag_type != TAG_Compound {
    panic("Cannot add child to non compound child")
  }
  nt := Tag{}
  nt.name = name
  nt.tag_type = tag_type
  nt.length = t.calculateSize(true)
  t.children = append(t.children, &nt)
  return &nt
}

func (t *Tag) ListAddFirst(tag_type byte) *Tag {
  if t.tag_type != TAG_List {
    panic("Cannot add list item to non list tag")
  }
  if len(t.children) != 0 {
    panic("Please call ListAddChild after calling ListAddFirst")
  }
  list_type := tag_type
  nt := Tag{}
  nt.name = "None"
  nt.tag_type = list_type
  nt.length = nt.calculateSize(true)
  t.children = append(t.children, &nt)
  return &nt
}

func (t *Tag) ListAddChild() *Tag {
  if t.tag_type != TAG_List {
    panic("Cannot add list item to non list tag")
  }
  if len(t.children) == 0 {
    panic("Please call ListAddFirst before called ListAddChild")
  }
  list_type := t.children[0].tag_type
  nt := Tag{}
  nt.name = "None"
  nt.tag_type = list_type
  nt.length = t.calculateSize(false)
  t.children = append(t.children, &nt)
  return &nt
}

func (t *Tag) Write(data []byte) {
  if t.tag_type == TAG_List || t.tag_type == TAG_Compound {
    panic("Cannot write binary data to compound or list tags")
  }
  t.data = data
}

func (t *Tag) calculateSize(has_name bool) int32 {
  var name_length int32 = 0
  if has_name {
    name_length = int32(len(t.name)) + 2
  }
  if t.tag_type == TAG_End { // 0x00
    return 0
  } else if t.tag_type == TAG_Byte { // 0x01
    return 1 + name_length
  } else if t.tag_type == TAG_Short { // 0x02
    return 2 + name_length
  } else if t.tag_type == TAG_Int { // 0x03
    return 4 + name_length
  } else if t.tag_type == TAG_Long { // 0x04
    return 8 + name_length
  } else if t.tag_type == TAG_Float { // 0x05
    return 4 + name_length
  } else if t.tag_type == TAG_Double { // 0x06
    return 8 + name_length
  } else if t.tag_type == TAG_ByteArray { // 0x07
    return 4 + name_length
  } else if t.tag_type == TAG_String { // 0x08
    return 2 + name_length
  } else if t.tag_type == TAG_List { // 0x09
    return 5 + name_length // 1 byte for type of all tags in list, and 4 bytes for length of list
  } else if t.tag_type == TAG_Compound { // 0x0a
    return 1 + name_length // +1 for end tag
  } else if t.tag_type == TAG_IntArray { // 0x0b
    return 4 + name_length
  } else if t.tag_type == TAG_LongArray { // 0x0c
    return 4 + name_length
  } else {
    panic("Invalid NBT data!")
  }
}

func (t *Tag) Serialize() []byte {
  return t.serialize(true)
}

func (t *Tag) serialize(add_name bool) []byte {
  if t.tag_type == TAG_End {
    return []byte{0x00}
  }
  data := []byte{}
  if add_name {
    data = append(data, t.tag_type)
    value := make([]byte, 2)
    binary.BigEndian.PutUint16(value, uint16(len(t.name)))
    data = append(data, value...)
    data = append(data, []byte(t.name)...)
  }
  if t.tag_type == TAG_End { // compound end
    panic("TAG_End() Should not exist in nbt data!")
  } else if t.tag_type == TAG_Byte ||
  t.tag_type == TAG_Short ||
  t.tag_type == TAG_Int ||
  t.tag_type == TAG_Long ||
  t.tag_type == TAG_Float ||
  t.tag_type == TAG_Double { // 0x01 - 0x06 can all be serialized in the same way
    data = append(data, t.data...)
  } else if t.tag_type == TAG_ByteArray { // 0x07
    value := make([]byte, 4)
    binary.BigEndian.PutUint32(value, uint32(len(t.data)))
    data = append(data, value...)
    data = append(data, t.data...)
  } else if t.tag_type == TAG_String { // 0x08
    value := make([]byte, 2)
    binary.BigEndian.PutUint16(value, uint16(len(t.data)))
    data = append(data, value...)
    data = append(data, t.data...)
  } else if t.tag_type == TAG_List { // 0x09
    // fmt.Println("Parsing list")
    list_type := t.children[0].tag_type
    list_length := len(t.children)
    data = append(data, list_type)
    value := make([]byte, 4)
    binary.BigEndian.PutUint32(value, uint32(list_length))
    data = append(data, value...)
    for i := 0; i < list_length; i++ {
      data = append(data, t.children[i].serialize(false)...)
    }
  } else if t.tag_type == TAG_Compound { // 0x0a
    list_length := len(t.children)
    for i := 0; i < list_length; i++ {
      data = append(data, t.children[i].serialize(true)...)
    }
    data = append(data, 0x0) // TAG_End()
  } else if t.tag_type == TAG_IntArray { // 0x0b
    list_length := len(t.data) / 4
    value := make([]byte, 4)
    binary.BigEndian.PutUint32(value, uint32(list_length))
    data = append(data, value...)
    data = append(data, t.data...)
  } else if t.tag_type == TAG_LongArray { // 0x0c
    list_length := len(t.data) / 8
    value := make([]byte, 4)
    binary.BigEndian.PutUint32(value, uint32(list_length))
    data = append(data, value...)
    data = append(data, t.data...)
  } else {
    panic("Invalid NBT data!")
  }
  return data
}

func (t *Tag) WriteByte(value byte) {
  if t.tag_type != TAG_Byte {
    panic("Tag " + t.name + " is not a byte!")
  }
  t.data = make([]byte, 1)
  t.data[0] = value
}

func (t *Tag) WriteShort(value int16) {
  if t.tag_type != TAG_Short {
    panic("Tag " + t.name + " is not a short!")
  }
  t.data = make([]byte, 2)
  binary.BigEndian.PutUint16(t.data, uint16(value))
}

func (t *Tag) WriteInt(value int32) {
  if t.tag_type != TAG_Int {
    panic("Tag " + t.name + " is not an int!")
  }
  t.data = make([]byte, 4)
  binary.BigEndian.PutUint32(t.data, uint32(value))
}

func (t *Tag) WriteLong(value int64) {
  if t.tag_type != TAG_Long {
    panic("Tag " + t.name + " is not a long!")
  }
  t.data = make([]byte, 8)
  binary.BigEndian.PutUint64(t.data, uint64(value))
}

func (t *Tag) WriteFloat(value float32) {
  if t.tag_type != TAG_Float {
    panic("Tag " + t.name + " is not a float!")
  }
  t.data = make([]byte, 4)
  binary.BigEndian.PutUint32(t.data, math.Float32bits(value))
}

func (t *Tag) WriteDouble(value float64) {
  if t.tag_type != TAG_Double {
    panic("Tag " + t.name + " is not a double!")
  }
  t.data = make([]byte, 8)
  binary.BigEndian.PutUint64(t.data, math.Float64bits(value))
}

func (t *Tag) WriteByteArray(value []byte) {
  if t.tag_type != TAG_ByteArray {
    panic("Tag " + t.name + " is not a byte array!")
  }
  t.data = value
}

func (t *Tag) WriteString(value string) {
  if t.tag_type != TAG_String {
    panic("Tag " + t.name + " is not a string!")
  }
  t.data = []byte(value)
}

func (t *Tag) WriteIntArray(value []uint32) {
  if t.tag_type != TAG_IntArray {
    panic("Tag " + t.name + " is not an int array!")
  }
  for i := 0; i < len(value); i++ {
    tmp := make([]byte, 4)
    binary.BigEndian.PutUint32(tmp, value[i])
    t.data = append(t.data, tmp...)
  }
}

func (t *Tag) WriteLongArray(value []uint64) {
  if t.tag_type != TAG_LongArray {
    panic("Tag " + t.name + " is not a long array!")
  }
  for i := 0; i < len(value); i++ {
    tmp := make([]byte, 8)
    binary.BigEndian.PutUint64(tmp, value[i])
    t.data = append(t.data, tmp...)
  }
}
