package schedule

type Scheduler struct {
  tasks map[int32]*Task
  current_id int32
}

func NewScheduler() *Scheduler {
  s := Scheduler{}
  s.current_id = 0
  s.tasks = make(map[int32]*Task)
  return &s
}

func (s *Scheduler) RunSyncTask(delay int32, repeates bool, action func()) *Task {
  s.current_id++
  task := newTask(delay, repeates, action)
  s.tasks[s.current_id] = task
  return task
}

func (s *Scheduler) Update() {
  for id, task := range s.tasks {
    task.update()
    if task.done {
      delete(s.tasks, id)
    }
  }
}
