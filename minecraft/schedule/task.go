package schedule

type Task struct {
  action func()
  delay int32
  time_since_last_run int32
  repeates bool
  done bool
}

func newTask(delay int32, repeates bool, action func()) *Task {
  t := Task{}
  t.action = action
  t.delay = delay
  t.repeates = repeates
  return &t
}

func (t *Task) Finish() {
  t.done = true
}

func (t *Task) update() {
  if !t.done && t.time_since_last_run > t.delay {
    t.time_since_last_run = 0
    t.action()
    if !t.repeates {
      t.done = true
    }
  }
  t.time_since_last_run++
}


