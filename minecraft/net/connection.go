package net

import (
  "io"
  "fmt"
  "context"

  "google.golang.org/grpc/codes"
  "google.golang.org/grpc/status"
  "google.golang.org/protobuf/types/known/anypb"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/event"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type Connection struct {
  world_manager *world.WorldManager
  events *event.EventManager
  packet_stream *packet.ConnectionWithChan
  player *player.Player
  packet_handler *PacketHandler
}

type player_packet struct {
  packet *pb.Packet
  player *player.Player
  world *world.World
  ctx context.Context
  // Set if any errors come up while reading fields.
  err error
}

func NewConnection(conn pb.Minecraft_ConnectionServer,
world_manager *world.WorldManager,
events *event.EventManager,
packet_handler *PacketHandler) *Connection {
  c := Connection{}
  c.world_manager = world_manager
  c.events = events
  c.packet_stream = packet.NewConnectionWithChan(conn)
  go c.packet_stream.Listen()
  c.packet_handler = packet_handler
  return &c
}

func (c *Connection) RecieveLoop() {
  stream := c.packet_stream.GetStream()
  ctx := stream.Context()
  for {
    p, err := stream.Recv()
    if err != nil {
      // If it was cancelled, then that is normal behavior. We only log errors with other error codes
      if status.Code(err) != codes.Canceled {
        log.Error("Got err in stream recv: ", err)
      }
      break
    }
    over := false
    select {
    case <-ctx.Done():
      log.Info("Incoming packet listener is closing, as grpc connection has been closed")
      over = true
      break
    default:
    }
    if over {
      break
    }
    if err == io.EOF {
      break
    }
    if err != nil {
      log.Error("Got error while listening for packets from proxy: " + err.Error())
      break
    }
    if p.Id == 0xff {
      name := p.Strings[0]
      uuid := util.NewUUIDFromProto(p.Uuids[0])
      version := packet.Version(p.Ints[0])

      if !c.world_manager.PlayerCanJoin(uuid, name) {
        // disconnect_packet := packet.NewOutgoingPacket(packet.Clientbound_Disconnect)
        // disconnect_packet.SetString(0, "")
        // disconnect_packet.Send(c.packet_stream)

        break
      }

      eid := c.world_manager.DefaultWorld().NextEID()
      c.player = player.New(name, uuid, c.packet_stream, eid, version)
    } else if p.Id == 0xfe {
      break
    }
    // The packet handler needs to call wm.AddPlayer without causing concurrent map errors, so we still pass packet 0xff
    c.packet_handler.add(&player_packet{
      packet: p,
      player: c.player,
      world: c.world_manager.GetWorldOfPlayer(c.player),
    })
  }
  if c.player != nil {
    c.packet_handler.add(&player_packet{packet: &pb.Packet{Id: 0xfe}, player: c.player})
  }
}

// Call this when you have finished reading all values from p.packet.
// This will return true if any of those reads produced errors; it
// will also log those errors to the console. The usage of this function
// should just be if p.Done { return }
func (p *player_packet) Done() bool {
  if p.err != nil {
    log.Error("While handling a ", packet.Serverbound(p.packet.Id), " we encountered an error: ", p.err)
  }
  return p.err != nil
}

func (p *player_packet) ID() packet.Serverbound {
  return packet.Serverbound(p.packet.Id)
}

func (p *player_packet) Bool(index int) bool {
  if p.err != nil { return false }
  if index < 0 || index >= len(p.packet.Bools) {
    p.err = fmt.Errorf("Bool at index %d is out of range %d", index, len(p.packet.Bools))
    return false
  }
  return p.packet.Bools[index]
}

func (p *player_packet) Byte(index int) byte {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Bytes) {
    p.err = fmt.Errorf("Byte at index %d is out of range %d", index, len(p.packet.Bytes))
    return 0
  }
  return p.packet.Bytes[index]
}

func (p *player_packet) Short(index int) int16 {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Shorts) {
    p.err = fmt.Errorf("Short at index %d is out of range %d", index, len(p.packet.Shorts))
    return 0
  }
  return int16(p.packet.Shorts[index])
}

func (p *player_packet) Int(index int) int32 {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Ints) {
    p.err = fmt.Errorf("Int at index %d is out of range %d", index, len(p.packet.Ints))
    return 0
  }
  return p.packet.Ints[index]
}

func (p *player_packet) Long(index int) uint64 {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Longs) {
    p.err = fmt.Errorf("Long at index %d is out of range %d", index, len(p.packet.Longs))
    return 0
  }
  return p.packet.Longs[index]
}

func (p *player_packet) Float(index int) float32 {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Floats) {
    p.err = fmt.Errorf("Float at index %d is out of range %d", index, len(p.packet.Floats))
    return 0
  }
  return p.packet.Floats[index]
}

func (p *player_packet) Double(index int) float64 {
  if p.err != nil { return 0 }
  if index < 0 || index >= len(p.packet.Doubles) {
    p.err = fmt.Errorf("Double at index %d is out of range %d", index, len(p.packet.Doubles))
    return 0
  }
  return p.packet.Doubles[index]
}

func (p *player_packet) String(index int) string {
  if p.err != nil { return "" }
  if index < 0 || index >= len(p.packet.Strings) {
    p.err = fmt.Errorf("String at index %d is out of range %d", index, len(p.packet.Strings))
    return ""
  }
  return p.packet.Strings[index]
}

func (p *player_packet) UUID(index int) util.UUID {
  if p.err != nil { return util.NewUUID() }
  if index < 0 || index >= len(p.packet.Uuids) {
    p.err = fmt.Errorf("UUID at index %d is out of range %d", index, len(p.packet.Uuids))
    return util.NewUUID()
  }
  return util.NewUUIDFromProto(p.packet.Uuids[index])
}

func (p *player_packet) NBTTag(index int) *nbt.Tag {
  if p.err != nil { return nbt.Empty() }
  if index < 0 || index >= len(p.packet.NBTTags) {
    p.err = fmt.Errorf("NBTTag at index %d is out of range %d", index, len(p.packet.NBTTags))
    return nbt.Empty()
  }
  return nbt.ReadData(p.packet.NBTTags[index])
}

func (p *player_packet) ByteArray(index int) []byte {
  if p.err != nil { return []byte{} }
  if index < 0 || index >= len(p.packet.ByteArrays) {
    p.err = fmt.Errorf("Byte array at index %d is out of range %d", index, len(p.packet.ByteArrays))
    return []byte{}
  }
  return p.packet.ByteArrays[index]
}

func (p *player_packet) Position(index int) block.Pos {
  if p.err != nil { return block.Pos{0, 0, 0} }
  if index < 0 || index >= len(p.packet.Positions) {
    p.err = fmt.Errorf("Position at index %d is out of range %d", index, len(p.packet.Positions))
    return block.Pos{0, 0, 0}
  }
  return block.PosFromLong(p.packet.Positions[index])
}

func (p *player_packet) Other(index int) *anypb.Any {
  if p.err != nil { return nil }
  if index < 0 || index >= len(p.packet.Other) {
    p.err = fmt.Errorf("Other at index %d is out of range %d", index, len(p.packet.Positions))
    return nil
  }
  return p.packet.Other[index]
}

// repeated IntArray intArrays        = 14;
// repeated LongArray longArrays      = 15;
// repeated StringArray stringArrays  = 16;
