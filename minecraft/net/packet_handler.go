package net

import (
  "fmt"

  "github.com/golang/protobuf/ptypes"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/event"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type PacketHandler struct {
  stream chan *player_packet
  wm *world.WorldManager
  events *event.EventManager
}

func NewPacketHandler(world_manager *world.WorldManager, events *event.EventManager) *PacketHandler {
  h := PacketHandler{}
  h.stream = make(chan *player_packet, 100)
  h.wm = world_manager
  h.events = events
  return &h
}

func (h *PacketHandler) add(packet *player_packet) {
  h.stream <- packet
}

func (h *PacketHandler) Listen() {
  if packet_types == nil {
    log.Error("Please run net.SetupPacketTypes for calling Listen!")
    return
  }
  for {
    p := <- h.stream
    handler, ok := packet_types[p.ID()]
    if !ok {
      log.Warn(fmt.Sprintf("Got unknown packet id: %v", p.ID()))
      continue
    }
    handler(p, h)
  }
}

var packet_types map[packet.Serverbound]func(*player_packet, *PacketHandler)

func SetupPacketTypes() {
  packet_types = make(map[packet.Serverbound]func(*player_packet, *PacketHandler))
  packet_types[0xff] = func(p *player_packet, h *PacketHandler) { // player join
    log.Info(p.player.Name() + " has logged in!")
    h.wm.AddPlayer(p.player, p.Bool(0))
    p.Done()
    // Send player the active commands
    h.events.GetCommandTree().Send(p.player.PacketStream)
    h.events.HandleEvent("player-join", p.player)
  }
  packet_types[0xfe] = func(p *player_packet, h *PacketHandler) { // player leave
    log.Info(p.player.Name() + " has left the game")
    h.wm.DisconnectPlayer(p.player)
    h.events.HandleEvent("player-leave", p.player)
  }
  packet_types[packet.Serverbound_ClientStatus] = func(p *player_packet, h *PacketHandler) { // 0x04
    status := p.Int(0)
    if p.Done() { return }
    if status == 0 {
      p.player.Respawn()
    } else if status == 1 {
      // statistics
    }
  }
  packet_types[packet.Serverbound_ChatMessage] = func(p *player_packet, h *PacketHandler) { // 0x03
    msg := p.String(0)
    if p.Done() { return }
    if msg[0] == '/' {
      command := msg[1:]
      log.Info("Command: " + command)
      h.events.HandleCommand(command, p.player)
    } else {
      log.Info("Chat: <" + p.player.Name() + "> " + msg)
      chat := util.NewChat()
      chat.AddSection("<")
      chat.AddSectionColor(p.player.Name(), "red")
      chat.AddSection("> " + msg)
      out := packet.NewOutgoingPacket(packet.Clientbound_ChatMessage)
      out.SetString(0, chat.ToJSON())
      out.SetByte(0, 0)
      for _, player := range h.wm.Players() {
        out.Send(player.PacketStream)
      }
    }
  }
  packet_types[packet.Serverbound_ClientSettings] = func(p *player_packet, h *PacketHandler) { // 0x05
    lang := p.String(0)
    if p.Done() { return }
    // render_dist := packet.ReadByte()
    // chat_mode := packet.ReadVarInt()
    // chat_colors := packet.ReadBoolean()
    // skin_sections := packet.ReadByte()
    // main_hand := packet.ReadVarInt()
    log.Info("Lang: " + lang)
  }
  packet_types[packet.Serverbound_TabComplete] = func(p *player_packet, h *PacketHandler) { // 0x06
    index := p.Int(0)
    text := p.String(0)
    if p.Done() { return }
    h.events.HandleTabComplete(p.player, index, text[1:])
  }
  packet_types[packet.Serverbound_ClickWindow] = func(p *player_packet, h *PacketHandler) { // 0x09
    window_id := p.Byte(0)
    slot := p.Short(0)
    button := p.Byte(1)
    action := p.Short(1)
    mode := p.Int(0)
    other := p.Other(0)
    if p.Done() { return }
    proto_item := &pb.Item{}
    err := ptypes.UnmarshalAny(other, proto_item)
    if err != nil {
      panic(err)
    }
    is_cancelled := h.events.HandleEvent("click-window", slot, button, mode, item.NewItemFromProto(proto_item), p.player)
    confirm_packet := packet.NewOutgoingPacket(packet.Clientbound_WindowConfirm)
    confirm_packet.SetByte(0, window_id)
    confirm_packet.SetShort(0, int16(action))
    confirm_packet.SetBool(0, !is_cancelled)
    confirm_packet.Send(p.player.PacketStream)
  }
  packet_types[packet.Serverbound_PluginMessage] = func(p *player_packet, h *PacketHandler) { // 0x0b
    channel := p.String(0)
    if p.Done() { return }
    log.Info("Got plugin channel: " + channel)
    // if channel == "minecraft:brand" {
    //   brand := p.proto.ReadProtocolString()
    //   c.log.Info("Got brand: " + brand)
    // } else if channel == "minecraft:register" {
    //   customChannel := p.proto.ReadNullString()
    //   c.log.Info("Got custom channel: l" + customChannel)
    // }
  }
  packet_types[packet.Serverbound_InteractEntity] = func(p *player_packet, h *PacketHandler) { // 0x0e
    other_id := uint32(p.Int(0))
    action := p.Int(1)
    if p.Done() { return }
    other, ok := p.world.GetEntity(other_id)
    if !ok {
      return
    }
    switch action {
    case 0:
    case 1:
      p.player.Hit(other)
    case 2:
    }
  }
  packet_types[packet.Serverbound_KeepAlive] = func(p *player_packet, h *PacketHandler) { // 0x0f
    id := p.Int(0)
    if p.Done() { return }
    p.player.HandleKeepAlive(id)
  }
  packet_types[packet.Serverbound_PlayerPosition] = func(p *player_packet, h *PacketHandler) { // 0x11
    x, y, z := p.Double(0), p.Double(1), p.Double(2)
    on_ground := p.Bool(0)
    if p.Done() { return }
    p.player.SetNewPosition(x, y, z)
    p.player.SetOnGround(on_ground)
  }
  packet_types[packet.Serverbound_PlayerPositionAndRotation] = func(p *player_packet, h *PacketHandler) { // 0x12
    x, y, z := p.Double(0), p.Double(1), p.Double(2)
    yaw, pitch := p.Float(0), p.Float(1)
    on_ground := p.Bool(0)
    if p.Done() { return }
    p.player.SetNewPosition(x, y, z)
    p.player.SetRotation(yaw, pitch)
    p.player.SetOnGround(on_ground)
  }
  packet_types[packet.Serverbound_PlayerRotation] = func(p *player_packet, h *PacketHandler) { // 0x13
    yaw, pitch := p.Float(0), p.Float(1)
    on_ground := p.Bool(0)
    if p.Done() { return }
    p.player.SetRotation(yaw, pitch)
    p.player.SetOnGround(on_ground)
  }
  packet_types[packet.Serverbound_PlayerOnGround] = func(p *player_packet, h *PacketHandler) { // 0x14
    on_ground := p.Bool(0)
    if p.Done() { return }
    p.player.SetOnGround(on_ground)
  }
  packet_types[packet.Serverbound_PlayerAbilities] = func(p *player_packet, h *PacketHandler) { // 0x19
    flags := p.Byte(0)
    if p.Done() { return }
    p.player.SetAbilities(flags)
  }
  packet_types[packet.Serverbound_PlayerDigging] = func(p *player_packet, h *PacketHandler) { // 0x1a
    dig_status := p.Int(0)
    pos := p.Position(0)
    if p.Done() { return }
    w := h.wm.GetWorldOfPlayer(p.player)
    // Started digging
    time_in_ticks := p.player.TimeToBreakBlock(w.Block(pos).Type().Name())
    if dig_status == 0 && p.player.Gamemode() == 0 && time_in_ticks > 0 {
      chat := util.NewChatFromString(
        fmt.Sprintf("It will take %d ticks (%f seconds) to break that block", time_in_ticks, float32(time_in_ticks) / 20),
      )
      p.player.SendChat(chat)

    // Finished digging
    } else if (dig_status == 0 && p.player.Gamemode() == 1) || // Creative move
    (dig_status == 2 && p.player.Gamemode() == 0) ||  // When a player finises digging
    (dig_status == 0 && p.player.Gamemode() == 0 && time_in_ticks == 0) { // When a block is instant break (snow with diamond shovel, stone with pickaxe and haste, etc)
      bs := w.Block(pos)
      is_cancelled := h.events.HandleEvent("left-click", w, bs, p.player)
      if !is_cancelled {
        if w.PlayerCanPlaceBlock(p.player, pos) {
          is_cancelled = h.events.HandleEvent(
            "block-change",
            block.GetKind("minecraft:air").DefaultType(),
            w,
            bs,
            p.player)
          if !is_cancelled {
            w.BreakBlock(p.player, pos)
          }
        }
      }
      block_break_confirm_packet := packet.NewOutgoingPacket(packet.Clientbound_AcknowledgePlayerDigging)
      block_break_confirm_packet.SetPosition(0, pos)
      block_break_confirm_packet.SetInt(0, int32(w.BlockState(pos)))
      block_break_confirm_packet.SetInt(1, 0)
      block_break_confirm_packet.SetBool(0, !is_cancelled)
      block_break_confirm_packet.Send(p.player.PacketStream)
    }
    // Drop single item
    // TODO: Broken, nil causes panic
    // if dig_status == 4 {
    //   // vx := -math.Sin(p.player.YawAngle())
    //   // vz := math.Cos(p.player.YawAngle())
    //   ent := w.SpawnObject(entity.Type_Item,
    //     p.player.X(), p.player.Y(), p.player.Z(),
    //     0, 0,
    //     0, 0, 0,
    //     1,
    //   )
    //   meta := metadata.NewEntityMetadata()
    //   item, _ := item.NewItemStack("grass_block", 1, nil)
    //   meta.Write(7, metadata.EntityType_Item, item)
    //   out := packet.NewOutgoingPacket(packet.Clientbound_EntityMetadata)
    //   out.SetInt(0, int32(ent.EID()))
    //   out.SetEntityMetadata(meta)
    //   out.Send(p.player.PacketStream)
    // }
  }
  packet_types[packet.Serverbound_EntityAction] = func(p *player_packet, h *PacketHandler) { // 0x1b
    // We ignore this eid value, as we already know who it came from
    // eid := p.proto.Ints[0]
    action := p.Int(1)
    // Used for starting a horse jump
    // jump_amount := p.proto.Ints[2]
    if p.Done() { return }
    switch action {
    case 0:
      p.player.Sneak(true)
    case 1:
      p.player.Sneak(false)
    }
  }
  packet_types[packet.Serverbound_HeldItemChange] = func(p *player_packet, h *PacketHandler) {
    slot := uint16(p.Short(0))
    if p.Done() { return }
    p.player.Inventory().SetHeldItemSlot(slot)
  }
  packet_types[packet.Serverbound_CreativeInventoryAction] = func(p *player_packet, h *PacketHandler) {
    slot := uint16(p.Short(0))
    other := p.Other(0)
    if p.Done() { return }
    item_stack := &pb.Item{}
    err := ptypes.UnmarshalAny(other, item_stack)
    if err != nil {
      log.Error(err)
      return
    }
    if item_stack.Present {
      id := uint16(item_stack.ID)
      amount := byte(item_stack.Count)
      p.player.Inventory().SetItem(slot, item.NewItemStackID(id, amount, nbt.Empty()))
    } else {
      p.player.Inventory().SetItem(slot, nil)
    }
  }
  packet_types[packet.Serverbound_Animation] = func(p *player_packet, h *PacketHandler) { // player arm swing
    hand := p.Int(0)
    if p.Done() { return }
    var animation byte
    if hand == 0 {
      animation = 0
    } else if hand == 1 {
      animation = 3
    } else {
      log.Error("Invalid hand sent from client in arm swing packet!")
      return
    }
    arm_swing_packet := packet.NewOutgoingPacket(packet.Clientbound_EntityAnimation)
    arm_swing_packet.SetInt(0, int32(p.player.EID()))
    arm_swing_packet.SetByte(0, animation)
    for id, player := range h.wm.Players() {
      if id != p.player.UUID {
        arm_swing_packet.Send(player.PacketStream)
      }
    }
  }
  packet_types[packet.Serverbound_PlayerBlockPlace] = func(p *player_packet, h *PacketHandler) { // player arm swing
    hand := p.Int(0)
    is_main_hand := hand == 0
    clicked_pos := p.Position(0)
    block_face := block.Face(p.Int(1))
    if p.Done() { return }
    // cursor_x := p.proto.Floats[0]
    // cursor_y := p.proto.Floats[1]
    // cursor_z := p.proto.Floats[2]
    // inside_block := p.proto.Bools[0]
    player_world := h.wm.GetWorldOfPlayer(p.player)
    stack := p.player.Inventory().HeldItem()
    is_cancelled := h.events.HandleEvent(
      "right-click",
      is_main_hand,
      stack,
      player_world,
      player_world.Block(clicked_pos),
      p.player,
    )
    if stack != nil && !is_cancelled {
      stack.Item().OnClick(player_world, p.player, clicked_pos, block_face)
      // kind := block.GetKind(item.Name())
      // if kind != nil {
      //   new_block_type := kind.DefaultType()
      //   old_block := player_world.Block(block_x, block_y, block_z)
      //   if !is_cancelled {
      //     if player_world.PlayerCanPlaceBlock(p.player, block_x, block_y, block_z) {
      //       is_cancelled = h.events.HandleEvent(
      //         "block-change",
      //         new_block_type,
      //         player_world,
      //         old_block,
      //         p.player,
      //       )
      //       if is_cancelled {
      //         block_place_packet := packet.NewOutgoingPacket(packet.Clientbound_BlockChange)
      //         block_place_packet.SetPosition(0, block_x, block_y, block_z)
      //         block_place_packet.SetInt(0, int32(old_block.State()))
      //         block_place_packet.Send(p.player.PacketStream)
      //       } else {
      //         player_world.PlaceBlock(block_x, block_y, block_z, new_block_type)
      //       }
      //     }
      //   } else {
      //     // reset block
      //   }
      // } else {
      //   log.Warn("Unknown block for item: ", item.Name())
      // }
    }
  }
}

