package item

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type BlockItem struct {
  *Basic
  block *block.Kind
  name string
}

func NewBlock(name string) *BlockItem {
  return &BlockItem{NewBasic(name), block.GetKind(name), name}
}

func (b *BlockItem) Block() *block.Kind {
  return b.block
}

func (b *BlockItem) OnClick(w block.World, p Player, pos block.Pos, face block.Face) {
  pos = pos.OffsetFace(face)
  if b.block == nil {
    chat := util.NewChatFromStringColor("The block ", "red")
    chat.AddSectionColor(b.name, "white")
    chat.AddSectionColor(" has not been added yet!", "red")
    p.SendChat(chat)
    // Makes sure there are no ghost blocks for the client
    w.SetBlock(pos, block.Air())
  } else {
    w.SetBlock(pos, b.block.DefaultType())
  }
}
