package item

type attribute struct {
  damage, speed float64
}

var item_attributes map[string]attribute

func setupItemTypes() {
  item_attributes = make(map[string]attribute)
  item_attributes["minecraft:wooden_sword"]      = attribute{damage: 4, speed: 1.6}
  item_attributes["minecraft:gold_sword"]        = attribute{damage: 4, speed: 1.6}
  item_attributes["minecraft:stone_sword"]       = attribute{damage: 5, speed: 1.6}
  item_attributes["minecraft:iron_sword"]        = attribute{damage: 6, speed: 1.6}
  item_attributes["minecraft:diamond_sword"]     = attribute{damage: 7, speed: 1.6}
  item_attributes["minecraft:netherite_sword"]   = attribute{damage: 8, speed: 1.6}

  item_attributes["minecraft:wooden_axe"]        = attribute{damage: 7, speed: 0.8}
  item_attributes["minecraft:gold_axe"]          = attribute{damage: 7, speed: 1.0}
  item_attributes["minecraft:stone_axe"]         = attribute{damage: 9, speed: 0.8}
  item_attributes["minecraft:iron_axe"]          = attribute{damage: 9, speed: 0.9}
  item_attributes["minecraft:diamond_axe"]       = attribute{damage: 9, speed: 1.0}
  item_attributes["minecraft:netherite_axe"]     = attribute{damage: 10, speed: 1.0}

  item_attributes["minecraft:wooden_shovel"]     = attribute{damage: 2.5, speed: 1.0}
  item_attributes["minecraft:gold_shovel"]       = attribute{damage: 2.5, speed: 1.0}
  item_attributes["minecraft:stone_shovel"]      = attribute{damage: 3.5, speed: 1.0}
  item_attributes["minecraft:iron_shovel"]       = attribute{damage: 4.5, speed: 1.0}
  item_attributes["minecraft:diamond_shovel"]    = attribute{damage: 5.5, speed: 1.0}
  item_attributes["minecraft:netherite_shovel"]  = attribute{damage: 6.5, speed: 1.0}

  item_attributes["minecraft:wooden_hoe"]        = attribute{damage: 1, speed: 1.0}
  item_attributes["minecraft:gold_hoe"]          = attribute{damage: 1, speed: 1.0}
  item_attributes["minecraft:stone_hoe"]         = attribute{damage: 1, speed: 2.0}
  item_attributes["minecraft:iron_hoe"]          = attribute{damage: 1, speed: 3.0}
  item_attributes["minecraft:diamond_hoe"]       = attribute{damage: 1, speed: 4.0}
  item_attributes["minecraft:netherite_hoe"]     = attribute{damage: 1, speed: 4.0}

  item_attributes["minecraft:wooden_pickaxe"]    = attribute{damage: 2, speed: 1.2}
  item_attributes["minecraft:gold_pickaxe"]      = attribute{damage: 2, speed: 1.2}
  item_attributes["minecraft:stone_pickaxe"]     = attribute{damage: 3, speed: 1.2}
  item_attributes["minecraft:iron_pickaxe"]      = attribute{damage: 4, speed: 1.2}
  item_attributes["minecraft:diamond_pickaxe"]   = attribute{damage: 5, speed: 1.2}
  item_attributes["minecraft:netherite_pickaxe"] = attribute{damage: 6, speed: 1.2}

  item_attributes["minecraft:trident"]           = attribute{damage: 9, speed: 1.1}
}
