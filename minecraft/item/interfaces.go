package item

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  // pb "gitlab.com/macmv/sugarcane/proto"
)

type InventoryWindow interface {
  SetItem(index uint16, stack *Stack) error
}

type PlayerInventory interface {
  HeldItem() *Stack
  SetHeldItem(stack *Stack)
  SetHeldItemSlot(slot uint16)
  SetItem(index uint16, item *Stack)
  SetHotbarItem(index uint16, item *Stack)

  Equipment() map[int]*Stack
  NeedsEquipmentChange() bool
  ClearEquipmentChange()
  OpenWindow(id byte, title *util.Chat) InventoryWindow
}

type Player interface {
  Name() string
  Inventory() PlayerInventory
  SendChat(msg *util.Chat)

  AxisFacing() block.Face
}
