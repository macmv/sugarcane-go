package item

import (
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type Basic struct {
  name string
}

func NewBasic(name string) *Basic {
  return &Basic{name}
}

func (b *Basic) Name() string {
  return b.name
}

func (b *Basic) OnClick(w block.World, p Player, pos block.Pos, face block.Face) {

}
