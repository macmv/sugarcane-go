package item

import (
)

var items []Item
var item_names map[string]uint16

func ClearItems() {
  items = []Item{}
  item_names = make(map[string]uint16)
}

func RegisterItem(item Item) {
  id := uint16(len(items))
  items = append(items, item)
  item_names[item.Name()] = id
}

// func LoadItemNames(filename string) {
//   if item_ids != nil {
//     panic("Already loaded item names!")
//   }
//   item_ids = make(map[uint16]string)
//   item_names = make(map[string]uint16)
//   string_data, err := ioutil.ReadFile(filename)
//   if err != nil {
//     panic(err)
//   }
//   var data map[string]interface{}
//   err = json.Unmarshal(string_data, &data)
//   if err != nil {
//     panic(err)
//   }
//   json_ids := data["minecraft:item"].(map[string]interface{})["entries"].(map[string]interface{})
//   for name, item := range json_ids {
//     item_id := uint16(item.(map[string]interface{})["protocol_id"].(float64))
//     item_ids[item_id] = name
//     item_names[name] = item_id
//   }
//   setupItemTypes()
// }

func GetItem(name string) Item {
  if items == nil {
    panic("Haven't loaded item names yet!")
  }
  return items[item_names[name]]
}

func GetItemID(item Item) uint16 {
  if items == nil {
    panic("Haven't loaded item names yet!")
  }
  return item_names[item.Name()]
}

func GetItemFromID(id uint16) Item {
  if items == nil {
    panic("Haven't loaded item names yet!")
  }
  return items[id]
}

func ValidItem(name string) bool {
  if items == nil {
    panic("Haven't loaded item names yet!")
  }
  _, ok := item_names[name]
  return ok
}
