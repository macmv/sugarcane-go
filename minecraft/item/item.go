package item

import (
  "fmt"

  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

type Item interface {
  Name() string
  OnClick(w block.World, p Player, pos block.Pos, face block.Face)
}

type Stack struct {
  item Item
  count byte
  nbt *nbt.Tag
}

func NewItemStack(item Item, count byte, nbt *nbt.Tag) *Stack {
  if !ValidItem(item.Name()) {
    panic("Invalid item name " + item.Name())
  }
  s := Stack{}
  s.item = item
  s.count = count
  s.nbt = nbt
  return &s
}

func NewItemStackID(id uint16, count byte, nbt *nbt.Tag) *Stack {
  s := Stack{}
  s.item = GetItemFromID(id)
  s.count = count
  s.nbt = nbt
  return &s
}

func NewItemFromProto(proto *pb.Item) *Stack {
  if !proto.Present {
    return nil
  }
  return NewItemStackID(uint16(proto.ID), byte(proto.Count), nbt.ReadData(proto.NBT))
}

func (i *Stack) ToProto() *pb.Item {
  p := pb.Item{}
  p.Present = true
  p.ID = int32(GetItemID(i.Item()))
  p.Count = int32(i.count)
  if i.nbt == nil {
    p.NBT = []byte{0}
  } else {
    p.NBT = i.nbt.Serialize()
  }
  return &p
}

func (i *Stack) Item() Item {
  return i.item
}

func (i *Stack) ID() uint16 {
  return GetItemID(i.Item())
}

func (i *Stack) Count() byte {
  return i.count
}

func (i *Stack) NBT() *nbt.Tag {
  return i.nbt
}

func (i *Stack) String() string {
  out := fmt.Sprintf("Item{name: %s, id: %d, count: %d", i.Item().Name(), i.ID(), i.count)
  if i.nbt != nil {
    out += ", nbt:\n" + i.nbt.StringIndent(2) + "}"
  } else {
    out += "}"
  }
  return out
}

func (i *Stack) AttackDamage() float64 {
  attrib, ok := item_attributes[i.Item().Name()]
  if ok {
    return attrib.damage
  } else {
    return 1
  }
}

func (i *Stack) AttackSpeed() float64 {
  attrib, ok := item_attributes[i.Item().Name()]
  if ok {
    return attrib.speed
  } else {
    return 4
  }
}

