package player

import (
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
)

// Speed multipliers for all tools.
const (
  NormalMultiplier = 1
  WoodMultiplier = 2
  StoneMultiplier = 4
  IronMultiplier = 6
  DiamondMultiplier = 8
  NethertiteMultiplier = 9
  GoldMultiplier = 16
)

type tool_type struct {
  mult int32
  ttype string
  level int32
}

var tools map[string]tool_type

// This creates a map of all tools, and what multipliers they have. This is automatically called on mc.Load().
func SetupToolTypes() {
  tools = make(map[string]tool_type)

  tools["minecraft:wooden_axe"]        = tool_type{mult: WoodMultiplier, level: 1, ttype: "axe"}
  tools["minecraft:wooden_pickaxe"]    = tool_type{mult: WoodMultiplier, level: 1, ttype: "pickaxe"}
  tools["minecraft:wooden_shovel"]     = tool_type{mult: WoodMultiplier, level: 1, ttype: "shovel"}
  tools["minecraft:wooden_hoe"]        = tool_type{mult: WoodMultiplier, level: 1, ttype: "hoe"}
  tools["minecraft:wooden_sword"]      = tool_type{mult: WoodMultiplier, level: 1, ttype: "sword"}

  tools["minecraft:stone_axe"]         = tool_type{mult: StoneMultiplier, level: 2, ttype: "axe"}
  tools["minecraft:stone_pickaxe"]     = tool_type{mult: StoneMultiplier, level: 2, ttype: "pickaxe"}
  tools["minecraft:stone_shovel"]      = tool_type{mult: StoneMultiplier, level: 2, ttype: "shovel"}
  tools["minecraft:stone_hoe"]         = tool_type{mult: StoneMultiplier, level: 2, ttype: "hoe"}
  tools["minecraft:stone_sword"]       = tool_type{mult: StoneMultiplier, level: 2, ttype: "sword"}

  tools["minecraft:iron_axe"]          = tool_type{mult: IronMultiplier, level: 3, ttype: "axe"}
  tools["minecraft:iron_pickaxe"]      = tool_type{mult: IronMultiplier, level: 3, ttype: "pickaxe"}
  tools["minecraft:iron_shovel"]       = tool_type{mult: IronMultiplier, level: 3, ttype: "shovel"}
  tools["minecraft:iron_hoe"]          = tool_type{mult: IronMultiplier, level: 3, ttype: "hoe"}
  tools["minecraft:iron_sword"]        = tool_type{mult: IronMultiplier, level: 3, ttype: "sword"}

  tools["minecraft:diamond_axe"]       = tool_type{mult: DiamondMultiplier, level: 4, ttype: "axe"}
  tools["minecraft:diamond_pickaxe"]   = tool_type{mult: DiamondMultiplier, level: 4, ttype: "pickaxe"}
  tools["minecraft:diamond_shovel"]    = tool_type{mult: DiamondMultiplier, level: 4, ttype: "shovel"}
  tools["minecraft:diamond_hoe"]       = tool_type{mult: DiamondMultiplier, level: 4, ttype: "hoe"}
  tools["minecraft:diamond_sword"]     = tool_type{mult: DiamondMultiplier, level: 4, ttype: "sword"}

  tools["minecraft:netherite_axe"]     = tool_type{mult: NethertiteMultiplier, level: 4, ttype: "axe"}
  tools["minecraft:netherite_pickaxe"] = tool_type{mult: NethertiteMultiplier, level: 4, ttype: "pickaxe"}
  tools["minecraft:netherite_shovel"]  = tool_type{mult: NethertiteMultiplier, level: 4, ttype: "shovel"}
  tools["minecraft:netherite_hoe"]     = tool_type{mult: NethertiteMultiplier, level: 4, ttype: "hoe"}
  tools["minecraft:netherite_sword"]   = tool_type{mult: NethertiteMultiplier, level: 4, ttype: "sword"}

  tools["minecraft:gold_axe"]          = tool_type{mult: GoldMultiplier, level: 1, ttype: "axe"}
  tools["minecraft:gold_pickaxe"]      = tool_type{mult: GoldMultiplier, level: 1, ttype: "pickaxe"}
  tools["minecraft:gold_shovel"]       = tool_type{mult: GoldMultiplier, level: 1, ttype: "shovel"}
  tools["minecraft:gold_hoe"]          = tool_type{mult: GoldMultiplier, level: 1, ttype: "hoe"}
  tools["minecraft:gold_sword"]        = tool_type{mult: GoldMultiplier, level: 1, ttype: "sword"}
}

// This can take any item name, and will give you the speed multiplier of that item.
// This will return 1 for anything that is not a tool.
//
// Used in the TimeToBreakBlock function.
func GetBaseToolMultiplier(tool string) int32 {
  ttype, ok := tools[tool]
  if ok {
    return ttype.mult
  } else {
    return NormalMultiplier
  }
}

// Gets the block's "hardness" value.
// This is all hardcoded, copied from a table on the minecraft wiki.
// This will return 0 if the block passed in is not a valid block.
//
// Used in the TimeToBreakBlock function.
func GetBlockHardness(block_type string) float32 {
  return block_types[block_type].hardness
}

// This is the mining level of the tool.
// Used for checking if a block is harvestable by the given tool.
// This will return 0 if you pass in an item that is not a tool.
//
// Used in the TimeToBreakBlock function.
func GetToolLevel(tool string) int32 {
  return tools[tool].level
}

// This checks if a tool's level >= the min level of the block that they are trying to harvest.
//
// Used in the TimeToBreakBlock function.
func CanHarvest(tool_level int32, block_type string) bool {
  required := block_types[block_type].min_level
  return tool_level >= required
}

// This checks if they are using the correct tool type for the given block.
// This will return true if they are using an axe on wood, for example.
//
// Used in the TimeToBreakBlock function.
func ToolIsCorrectType(tool, block string) bool {
  block_type := block_types[block].tooltype
  ttype := tools[tool].ttype
  // Means that any tool works
  if block_type == "" {
    return true
  }
  return block_type == ttype
}

// This calculates the amount of time it will take the player to break a block of the given type.
// This does not account for the player changing on_ground in the middle of the break,
// but otherwise it takes all implemented features into account. Exceptions:
//
// It does not use the haste level yet, as status effects have not been added.
//
// It does not check if the player is in water, as that has not been implemented.
//
// Returns a value in ticks
func (p *Player) TimeToBreakBlock(block_type string) int32 {
  seconds := GetBlockHardness(block_type)
  item := p.Inventory().HeldItem()
  name := ""
  if item != nil {
    name = item.Item().Name()
  }
  if CanHarvest(GetToolLevel(name), block_type) {
    seconds *= 1.5
  } else {
    seconds *= 5
  }
  speed_multiplier := float32(1)
  if ToolIsCorrectType(name, block_type) {
    speed_multiplier = float32(GetBaseToolMultiplier(name))
    if item != nil {
      tag := item.NBT()
      if tag != nil && tag.Type() == nbt.TAG_Compound {
        list := tag.Child("Enchantments")
        if list != nil && list.GetListType() == nbt.TAG_Compound {
          for _, item := range list.GetChildren() {
            id := item.Child("id")
            level := item.Child("lvl")
            if id != nil && level != nil &&
            id.Type() == nbt.TAG_String && level.Type() == nbt.TAG_Int &&
            id.GetString() == "efficiency" {
              speed_multiplier += float32(level.GetInt() * level.GetInt()) + 1
            }
          }
        }
      }
    }
  }
  seconds /= speed_multiplier

  // if p.InWater() {
  //   seconds *= 5
  // }
  if !p.OnGround() {
    seconds *= 5
  }

  return int32(seconds * 20)
}
