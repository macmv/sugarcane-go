package player

import (
  "math"
  "errors"
  "net/http"
  "math/rand"
  "encoding/json"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/entity"
  "gitlab.com/macmv/sugarcane/minecraft/inventory"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"
)

// This is the player struct. There is one created per player, and it should be passed around by pointer.
// Each player has a UUID. This is their mojang account UUID. This will always be the same for the same account.
// If you need to track someone who logs off, then logs back in, you should use the UUID.
// Do not change the UUID on the player. That will break things.
//
// If you need to send the player a packet, use the PacketStream.
// To modify the scoreboard (this will be displayed on the sidebar) use the Scoreboard.
// To modify their inventory, and show them windows (chest GUIS, etc) use Inventory.
type Player struct {
  entity.LivingBase
  name string
  eid uint32
  UUID util.UUID
  PacketStream *packet.ConnectionWithChan
  last_sent_keep_alive int32
  last_recieved_keep_alive int32
  keep_alive_id int32
  // Is set whenever they send a position update packet. Can be from any fraction of a tick ago.
  new_x, new_y, new_z float64
  // Is set on each player tick. Will always be 1 tick behind x, y, z
  prev_x, prev_y, prev_z float64
  yaw, pitch float32
  // Set when SetNewPosition is called
  // needs_pos_update bool
  needs_rot_update bool
  on_ground bool
  prev_on_ground bool
  inventory *inventory.PlayerInventory
  Scoreboard *Scoreboard
  abilities byte
  fly_speed float32
  gamemode byte
  needs_position_set bool
  last_attack_time int
  tick int
  last_hit_time int
  version packet.Version
}

type mojang_api_uuid struct {
  Name string
  Id string
}

// This is an old function, that is never used.
// It gets the player's UUID based on their username.
// However, now that the proxy does mojang authentication for you, this is no longer needed.
// I am leaving this here, as it might be useful to get the UUID of someone who is offline.
func GetUUIDFromName(name string) *util.UUID {
  client := &http.Client{}
  req, err := http.NewRequest("GET", "https://api.mojang.com:443/users/profiles/minecraft/" + name, nil)
  // mojang api hates freedom. Will give 403 if you don't use this
  req.Header.Add("User-Agent", "Wget/1.20.3")
  resp, err := client.Do(req)
  if (err != nil) {
    panic(err)
  }

  defer resp.Body.Close()

  if (err != nil) {
    panic(err)
  } else {
    if resp.StatusCode != 200 {
      panic("Could not get player UUID from mojand api!")
    } else {
      uuid_json_str := make([]byte, 1024)
      length, err := resp.Body.Read(uuid_json_str)
      uuid_json_str = uuid_json_str[:length]
      var uuid_json mojang_api_uuid
      err = json.Unmarshal(uuid_json_str, &uuid_json)
      if err != nil {
        panic(err)
      }
      id := util.NewUUIDFromString(uuid_json.Id)
      return &id
    }
  }
  return nil
}

// Used internally when a player joins. Should never by called.
func New(name string,
uuid util.UUID,
conn *packet.ConnectionWithChan,
eid uint32,
version packet.Version) *Player {
  p := &Player{}
  p.name = name
  p.eid = eid
  p.UUID = uuid
  p.version = version

  var x float64 = 0
  var y float64 = 100
  var z float64 = 0
  var pitch byte = 0
  var yaw byte = 0
  var head_pitch byte = 0
  var vx int16 = 0
  var vy int16 = 0
  var vz int16 = 0

  living := entity.NewLivingBase(entity.Attributes{}, entity.Type_Player, eid, uuid, x, y, z, pitch, yaw, head_pitch, vx, vy, vz)
  p.LivingBase = *living

  p.PacketStream = conn

  p.inventory = inventory.NewPlayerInventory(eid, p.PacketStream)
  p.Scoreboard = newScoreboard(p)

  p.gamemode = 1

  p.fly_speed = 1
  p.SetNewPosition(x, y, z)
  p.SetCanFly(true)
  return p
}

// We shrink the player's hitbox, to make sure they don't get rubber-banded
// all the time. The newer client sends the player's position as an average
// between physics updates, which can make the player appear inside a block.
// 0.6 and 1.8 are is the real size of the player, but that caused to many
// issues on corners of blocks.
// func (p *Player) Width() float64 { return 0.6 }
// func (p *Player) Height() float64 { return 1.8 }
func (p *Player) Width() float64 { return 0.4 }
func (p *Player) Height() float64 { return 1.5 }

// Gets the player's username.
func (p *Player) Name() string {
  return p.name
}
// Gets the player's current inventory
func (p *Player) Inventory() item.PlayerInventory {
  return p.inventory
}
// This is the protocol version that this player is using.
// Used when generating chunk packets.
func (p *Player) Version() packet.Version {
  return p.version
}
// This is called whenever the player is inside a block.
// It will update the position of the player, such that they are now outside the block.
// It will also set the needs_position_set flag, which will cause them to rubberband next tick.
func (p *Player) Collide(info *entity.CollisionInfo) {
  if info.DidCollide {
    p.needs_position_set = true
  }
  p.Base.Collide(info)
}
// This is an update function for the player.
// It is called once per tick by the world update loop.
// Should never be called.
func (p *Player) Tick() {
  p.last_recieved_keep_alive++
  p.last_sent_keep_alive++
  p.tick++
  if p.last_sent_keep_alive > 20 {
    p.keep_alive_id = int32(rand.Uint32())
    out := packet.NewOutgoingPacket(packet.Clientbound_KeepAlive)
    out.SetInt(0, p.keep_alive_id)
    out.Send(p.PacketStream)
    p.last_sent_keep_alive = 0
  }
}
// Updates the prev position. Should never be called.
func (p *Player) SetPrevPosition() {
  p.prev_x = p.X()
  p.prev_y = p.Y()
  p.prev_z = p.Z()
  p.prev_on_ground = p.on_ground
}
// Called once all packets have been sent to the other client.
// This will clear things like needs_position_set, and needs_rot_update.
// Should never be called.
func (p *Player) ClearFlags() {
  p.needs_position_set = false
  p.needs_rot_update = false
}

// This updates the player's rotation.
// This is used internally when they send rotation update packets.
// Should never be called.
func (p *Player) SetRotation(yaw float32, pitch float32) {
  p.yaw = yaw
  p.pitch = pitch
  p.needs_rot_update = true
}
// This updates the player's on_ground status. Fall damage is applied when this happens.
// This is used internally when they send on_ground change packets.
// Should never be called.
func (p *Player) SetOnGround(on_ground bool) {
  p.on_ground = on_ground
}

// Called when a player hits something
func (p *Player) Hit(other entity.Entity) {
  log.Info("Player ", p.name, " is hitting ", other.EID())
  liv, ok := other.(entity.Living)
  if ok {
    main_hand := p.Inventory().HeldItem()
    damage := 1.0
    speed := 4.0
    if main_hand != nil {
      damage = main_hand.AttackDamage()
      speed = main_hand.AttackSpeed()
    }
    last_attack := float64(p.last_attack_time - p.Time())
    multiplier := 0.2 + math.Pow((last_attack + 0.5) / (20 / speed), 2) * 0.8
    // Keeps the multiplier in the range 0.2-1
    multiplier = math.Min(multiplier, 1)
    multiplier = math.Max(multiplier, 0.2)

    actual_damage := float32(damage) * float32(multiplier)
    log.Info("Dealing", actual_damage, "damage")
    if !liv.ImmuneToAttacks() {
      liv.Damage(actual_damage)
      liv.SetVelocity(0, 2000, 0)
    }
    p.last_attack_time = p.Time()
  }
}

// Call this to damage the player. Amount is the number health (half hearts) to take away from their health.
func (p *Player) Damage(amount float32) {
  p.LivingBase.Damage(amount)
  p.last_hit_time = p.Time()
  p.Metadata().Write(8, metadata.EntityType_Float, p.Health())
  out := packet.NewOutgoingPacket(packet.Clientbound_UpdateHealth)
  out.SetFloat(0, p.Health())
  out.SetInt(0, 20)
  out.SetFloat(1, 1.5)
  out.Send(p.PacketStream)
}

// Gets the player's yaw rotation.
// This is how much they are rotated on the X axis, in degrees.
// This is a weird measurement, and is essentially an inverted y axis unit circle.
// Refer to AxisAngle() to get a north-south-east-west value for the player yaw.
// This will always return a value from 0 - 360
func (p *Player) Yaw() float32 {
  return float32(math.Mod(math.Mod(float64(p.yaw), 360) + 360, 360))
}
// Gets the player's yaw rotation.
// This is how much they are rotated on the X axis, from 0 to 256
func (p *Player) YawByte() byte {
  // any value of p.yaw is valid, so the % 360 + 360 thing is to get a positive value between 0-360
  return byte(p.Yaw() * (256.0 / 360))
}
// This will return a block face, which is one of north, south, east or west, depending on the player's yaw.
func (p *Player) AxisFacing() block.Face {
  v := p.Yaw()
  if v < 45 {
    return block.FACE_SOUTH
  } else if v < 135 {
    return block.FACE_WEST
  } else if v < 225 {
    return block.FACE_NORTH
  } else if v < 315 {
    return block.FACE_EAST
  } else {
    // This is for the 315-360 range, which is the same range as 0-45
    return block.FACE_SOUTH
  }
}
// Gets the player's pitch rotation. This is how much they are rotated on the Y axis, from -90 to 90 degrees.
func (p *Player) Pitch() float32 {
  return p.pitch
}
// Gets the player's pitch rotation. This is how much they are rotated on the Y axis, from -64 to 64.
func (p *Player) PitchByte() byte {
  return byte(p.pitch * (64.0 / 90))
}
// Gets the player's yaw rotation in radians.
func (p *Player) YawAngle() float64 {
  return float64(p.yaw) / 180 * math.Pi
}
// Gets the player's pitch rotation in radians.
func (p *Player) PitchAngle() float64 {
  return float64(p.pitch) / 180 * math.Pi
}
// Gets whether or not the player is on the ground.
func (p *Player) OnGround() bool {
  return p.on_ground
}
// Get's their delta X, since the last time Tick() was called.
// Is used internally by the world update loop.
// There is not harm in calling it.
func (p *Player) DX() int16 {
  return int16((p.X() * 32 - p.prev_x * 32) * 128)
}
// Get's their delta Y, since the last time Tick() was called.
// Is used internally by the world update loop.
// There is not harm in calling it.
func (p *Player) DY() int16 {
  return int16((p.Y() * 32 - p.prev_y * 32) * 128)
}
// Get's their delta Z, since the last time Tick() was called.
// Is used internally by the world update loop.
// There is not harm in calling it.
func (p *Player) DZ() int16 {
  return int16((p.Z() * 32 - p.prev_z * 32) * 128)
}
// This checks if the player's delta position is greater than 8 blocks in any direction.
// This is because the DX, DY and DZ values are int16s, and will overflow if the delta is greater than 8 blocks.
// This is used internally to check if it needs to send a PlayerTeleport packet to everyone,
// or if it can just send position deltas.
// There is no harm in calling it.
func (p *Player) NeedsTeleport() bool {
  return math.Abs(p.X() - p.prev_x) > 8 || math.Abs(p.Y() - p.prev_y) > 8 || math.Abs(p.Z() - p.prev_z) > 8
}
// Used to check if the player has moved incorrectly.
// This is called in the world update loop, and if it returns true,
// then the player will be rubberbanded.
func (p *Player) NeedsPositionSet() bool {
  return p.needs_position_set
}
// Checks if the player has sent a rotation packet since the last time Tick() was called.
// Used internally to know if it needs to send rotation packets to all the other nearby clients.
// There is no harm in calling it.
func (p *Player) NeedsRotUpdate() bool {
  return p.needs_rot_update
}
func (p *Player) PrevX() float64 { return p.prev_x }
func (p *Player) PrevY() float64 { return p.prev_y }
func (p *Player) PrevZ() float64 { return p.prev_z }
// Will get X coordinate of the block they are standing in.
func (p *Player) BlockX() int32 {
  block_x := int32(p.X())
  if p.X() < 0 {
    block_x--
  }
  return block_x
}
// Will get Y coordinate of the block they are standing in.
// This is their feet position, not their eye position.
func (p *Player) BlockY() int32 {
  block_y := int32(p.Y())
  if p.Y() < 0 {
    block_y--
  }
  return block_y
}
// Will get Z coordinate of the block they are standing in.
func (p *Player) BlockZ() int32 {
  block_z := int32(p.Z())
  if p.Z() < 0 {
    block_z--
  }
  return block_z
}
// Will get the X coordinate of the block they were in before the current tick.
// This is only updated when Tick() is called.
// This is used to see if the players have travelled across chunk borders.
func (p *Player) PrevBlockX() int32 {
  block_x := int32(p.prev_x)
  if p.prev_x < 0 {
    block_x--
  }
  return block_x
}
// Will get the Y coordinate of the block they were in before the current tick.
// This is only updated when Tick() is called.
// This is used to see if the players have travelled across chunk borders.
func (p *Player) PrevBlockY() int32 {
  block_y := int32(p.prev_y)
  if p.prev_y < 0 {
    block_y--
  }
  return block_y
}
// Will get the Z coordinate of the block they were in before the current tick.
// This is only updated when Tick() is called.
// This is used to see if the players have travelled across chunk borders.
func (p *Player) PrevBlockZ() int32 {
  block_z := int32(p.prev_z)
  if p.prev_z < 0 {
    block_z--
  }
  return block_z
}
// Will get the X coordinate of the chunk they are in.
func (p *Player) ChunkX() int32 {
  block_x := p.BlockX()
  chunk_x := block_x / 16
  if block_x < 0 {
    chunk_x = (block_x - 15) / 16
  }
  return chunk_x
}
// Will get the Z coordinate of the chunk they are in.
func (p *Player) ChunkZ() int32 {
  block_z := p.BlockZ()
  chunk_z := block_z / 16
  if block_z < 0 {
    chunk_z = (block_z - 15) / 16
  }
  return chunk_z
}
// Will get the X coordinate of the chunk they were in during the previous tick.
func (p *Player) PrevChunkX() int32 {
  block_x := p.PrevBlockX()
  chunk_x := block_x / 16
  if block_x < 0 {
    chunk_x = (block_x - 15) / 16
  }
  return chunk_x
}
// Will get the Z coordinate of the chunk they were in during the previous tick.
func (p *Player) PrevChunkZ() int32 {
  block_z := p.PrevBlockZ()
  chunk_z := block_z / 16
  if block_z < 0 {
    chunk_z = (block_z - 15) / 16
  }
  return chunk_z
}

// Used whenever the client sends a keep alive packet.
// Should never be called.
func (p *Player) HandleKeepAlive(keep_alive_id int32) {
  if keep_alive_id == p.keep_alive_id {
    p.last_recieved_keep_alive = 0
  } else {
    log.Warn("Player ", p.Name(), " sent wrong keep alive id!")
  }
}

// This will send the player a chat message.
// This is not prefixed by a player's username or anything, it just sends them a chat message
// with the exact text as defined in the Chat.
// This works with all formatting options of packet.Chat.
func (p *Player) SendChat(chat *util.Chat) {
  out := packet.NewOutgoingPacket(packet.Clientbound_ChatMessage)
  out.SetString(0, chat.ToJSON())
  out.SetByte(0, 0)
  out.Send(p.PacketStream)
}

// This will spawn a redstone dust particle at the given location, with the given color.
// It should not be used, as a proper particle api will be introduced later.
// It is used by the worldedit server to display a bounding box around your selection.
func (p *Player) SpawnParticleDust(x, y, z float64, r, g, b float32, scale float32) {
  out := packet.NewOutgoingPacket(packet.Clientbound_Particle)
  out.SetBool(0, false)
  out.SetInt(0, 14)
  out.SetInt(1, 1) // count
  out.SetFloat(0, 0) // random offset
  out.SetFloat(1, 0) // random offset
  out.SetFloat(2, 0) // random offset
  out.SetFloat(3, 0) // data
  out.SetFloat(4, r)
  out.SetFloat(5, g)
  out.SetFloat(6, b)
  out.SetFloat(7, scale)
  out.SetDouble(0, x)
  out.SetDouble(1, y)
  out.SetDouble(2, z)
  out.Send(p.PacketStream)
}

// Sends the player a packet about their current abilities.
// Is called automatically whenever you call SetCanFly, SetAbilities, etc.
func (p *Player) UpdateAbilities() {
  out := packet.NewOutgoingPacket(packet.Clientbound_PlayerAbilities)
  out.SetByte(0, p.abilities)
  out.SetFloat(0, 0.05 * p.fly_speed)
  out.SetFloat(1, 0.1)
  out.Send(p.PacketStream)
}

// Sets whether or not the player can fly.
func (p *Player) SetCanFly(value bool) {
  if value {
    p.abilities |= 0x04
  } else {
    p.abilities &= ^byte(0x04)
  }
  p.UpdateAbilities()
}

// Sets whether or not the player is flying.
// Used internally when the player sends a packet to start/stop flying.
func (p *Player) SetIsFlying(value bool) {
  if value {
    p.abilities |= 0x02
  } else {
    p.abilities &= ^byte(0x02)
  }
  p.UpdateAbilities()
}

// Sets the fly speed. This is a multiplier, not the actually value.
// So 1 is the base fly speed.
func (p *Player) SetFlySpeed(value float32) {
  p.fly_speed = value
  p.UpdateAbilities()
}

// Sets the abilities byte.
// You shouldn't need to call this, as wrapper functions like SetCanFly will do this for you.
// This is used internally when the client sends an ability update packet.
func (p *Player) SetAbilities(value byte) {
  // TODO: Validate these abilities, i.e., check if the player can actually start flying
  p.abilities = value
  p.UpdateAbilities()
}

// Gets the player's gamemode.
//   0 -> survival
//   1 -> creative
//   2 -> adventure
//   3 -> spectator
func (p *Player) Gamemode() byte {
  return p.gamemode
}

// Sets the player's gamemode.
// If the value is > 3, this will return an error.
// This also checks if they are being set to/from creative, and updates their abilities as such.
func (p *Player) SetGamemode(value byte) error {
  if value > 3 {
    return errors.New("Invalid gamemode!")
  }
  if p.gamemode == value {
    return nil
  }
  p.gamemode = value
  // Survival or adventure
  if p.gamemode == 0 || p.gamemode == 2 {
    // Is not creative, is not currently flying, cannot fly, and damage is enabled
    p.SetAbilities(0)
  // Creative of spectator
  } else if p.gamemode == 1 || p.gamemode == 3 {
    // Is creative, is not flying, can fly, and damage is disabled
    p.SetAbilities(0x01 | 0x04 | 0x08)
  }
  out := packet.NewOutgoingPacket(packet.Clientbound_ChangeGameState)
  out.SetByte(0, 3) // change gamemode
  out.SetFloat(0, float32(p.gamemode))
  out.Send(p.PacketStream)
  return nil
}

// This will teleport the player to a set location, facing 0,0.
// Use this when you want to move players around, instead of SetPosition.
// A TeleportFacing function will be added in the future, which will let you set the direction they are facing.
func (p *Player) Teleport(x, y, z float64) {
  p.SetNewPosition(x, y, z)
  out := packet.NewOutgoingPacket(packet.Clientbound_PlayerPositionAndLook)
  out.SetDouble(0, p.X())
  out.SetDouble(1, p.Y())
  out.SetDouble(2, p.Z())
  out.Send(p.PacketStream)
}

// This will send the player to a server, at the given ip.
// It will not reserve slots on that server, it will only send the packet to the proxy, which will redirect them.
func (p *Player) SendToServer(ip string) {
  out := packet.NewOutgoingPacket(0xfe)
  out.SetString(0, ip)
  out.Send(p.PacketStream)
}

func (p *Player) SendTitle(title, subtitle *util.Chat, fadein, stay, fadeout int) {
  out := packet.NewOutgoingPacket(0x50)
  out.SetInt(0, 5) // reset
  out.Send(p.PacketStream)
  if title != nil {
    out := packet.NewOutgoingPacket(0x50)
    out.SetInt(0, 0) // set title
    out.SetString(0, title.ToJSON())
    out.Send(p.PacketStream)
  }
  if subtitle != nil {
    out := packet.NewOutgoingPacket(0x50)
    out.SetInt(0, 1) // set subtitle
    out.SetString(0, subtitle.ToJSON())
    out.Send(p.PacketStream)
  }
  out = packet.NewOutgoingPacket(0x50)
  out.SetInt(0, 3) // set times
  out.SetInt(1, int32(fadein))
  out.SetInt(2, int32(stay))
  out.SetInt(3, int32(fadeout))
  out.Send(p.PacketStream)
}

// Updates the player's entity metadata to reflect if they are sneaking.
// This will be sent to all players in view distance on the next player tick.
func (p *Player) Sneak(sneak bool) {
  prev := p.Metadata().GetType(0, metadata.EntityType_Byte).(byte)
  if sneak {
    p.Metadata().Write(0, metadata.EntityType_Byte, prev | 0x02)
    p.Metadata().Write(6, metadata.EntityType_Pose, metadata.Pose_Sneaking)
  } else {
    // Inverts the mask, then ands that with the previous value
    p.Metadata().Write(0, metadata.EntityType_Byte, prev & ^byte(0x02))
    p.Metadata().Write(6, metadata.EntityType_Pose, metadata.Pose_Standing)
  }
}

// Is the same as the entity.Time function, but it needs to be overridden here, as the Tick() function is also
// overridden.
func (p *Player) Time() int {
  return p.tick
}

// Needs to be overridden, as player.Tick() was overridden.
func (p *Player) ImmuneToAttacks() bool {
  return p.Time() - p.last_hit_time <= 10
}

// Gets the equipment for the player (held items/armor. Things that are visible to other players)
func (p *Player) Equipment() map[int]*item.Stack {
  return p.Inventory().Equipment()
}

// Used to check if an equipment change packet is needed
func (p *Player) NeedsEquipmentChange() bool {
  return p.Inventory().NeedsEquipmentChange()
}

// Sets the needs equipment change field to false
func (p *Player) ClearEquipmentChange() {
  p.Inventory().ClearEquipmentChange()
}

func (p *Player) Respawn() {
  out := packet.NewOutgoingPacket(packet.Clientbound_Respawn)
  out.SetInt(0, 0)  // dimension
  out.SetByte(0, 1) // gamemode
  out.Send(p.PacketStream)

  p.SetHealth(20)

  p.Metadata().Write(6, metadata.EntityType_Pose, metadata.Pose_Standing)
}

func (p *Player) SetHealth(val float32) {
  p.LivingBase.SetHealth(val)
  out := packet.NewOutgoingPacket(packet.Clientbound_UpdateHealth)
  out.SetFloat(0, p.Health())
  out.SetInt(0, 20)
  out.SetFloat(1, 1.5)
  out.Send(p.PacketStream)
}
