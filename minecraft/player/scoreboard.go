package player

import (
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
)

var objective_name string = "scoreboard"

// This is a wrapper class for the sidebar scoreboard.
// One is automatically created whenever a player joins, but it is not displayed.
// You can call Show() to show the scoreboard to the player.
//
// In the future, it will be able to also do tab list scores, and above name things.
// But right now, it can only do sidebar information.
type Scoreboard struct {
  lines []string
  title *util.Chat
  player *Player
}

func newScoreboard(player *Player) *Scoreboard {
  s := Scoreboard{}
  s.player = player
  s.lines = make([]string, 15)
  s.title = util.NewChatFromString("Scoreboard")
  return &s
}

// This sets all lines to be blank.
func (s *Scoreboard) Clear() {
  for i := int32(1); i < 16; i++ {
    s.SetLine(i, util.NewChatFromString(" "))
  }
}

// Will set the given line to the text passed in.
// Will panic if
//   line < 1 || line > 15
func (s *Scoreboard) SetLine(line int32, text_chat *util.Chat) {
  if line < 1 || line > 15 {
    panic("Invalid scoreboard line! (range is 1-15, which is the same number as the red number on the right of the scoreboard)")
  }
  index := line - 1 // because we cannot have a score of 0 be shown, the indexes in the s.lines array are all off by one
  text := text_chat.ToFormatCodes()
  if s.lines[index] == text {
    return
  }
  for {
    contains_text := false
    for _, other_text := range s.lines {
      if text == other_text {
        contains_text = true
        break
      }
    }
    if contains_text {
      text = text + " "
    } else {
      break
    }
  }
  old_text := s.lines[index]
  s.lines[index] = text

  out := packet.NewOutgoingPacket(packet.Clientbound_UpdateScore) // give points
  out.SetByte(0, 1) // removing item
  out.SetString(0, old_text)
  out.SetString(1, objective_name)
  out.Send(s.player.PacketStream)

  out = packet.NewOutgoingPacket(packet.Clientbound_UpdateScore) // give points
  out.SetByte(0, 0) // adding item
  out.SetInt(0, line)
  out.SetString(0, text)
  out.SetString(1, objective_name)
  out.Send(s.player.PacketStream)
}

// Sets the title above the scoreboard.
func (s *Scoreboard) SetTitle(new_title *util.Chat) {
  s.title = new_title

  out := packet.NewOutgoingPacket(packet.Clientbound_ScoreboardObjective)
  out.SetByte(0, 2) // changing name
  out.SetString(0, objective_name)
  out.SetString(1, s.title.ToJSON())
  out.SetInt(0, 0) // numbers, not hearts
  out.Send(s.player.PacketStream)
}

// Will start displaying the scoreboard to the player.
func (s *Scoreboard) Show() {
  out := packet.NewOutgoingPacket(packet.Clientbound_ScoreboardObjective)
  out.SetByte(0, 0) // creating
  out.SetString(0, objective_name)
  out.SetString(1, s.title.ToJSON())
  out.SetInt(0, 0) // numbers, not hearts
  out.Send(s.player.PacketStream)

  out = packet.NewOutgoingPacket(packet.Clientbound_DisplayScoreboard)
  out.SetByte(0, 1) // sidebar
  out.SetString(0, objective_name)
  out.Send(s.player.PacketStream)
}
