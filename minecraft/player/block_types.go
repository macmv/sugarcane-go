package player

type block_break_type struct {
  hardness float32
  tooltype string
  min_level int32
}

var block_types map[string]block_break_type

// This creates a map that is a list of all blocks in the game,
// and what tools/mining level/hardness they are.
//
// This is called automatically when you call mc.Load(). Should not be called manually.
//
// Note: This was created manually. It is ~200 lines of assignments into a map. It took forever. Feel free to copy it.
func SetupBlockTypes() {
  block_types = make(map[string]block_break_type)

  // All stone things
  block_types["minecraft:obsidian"]                       = block_break_type{hardness: 50, tooltype: "pickaxe", min_level: 4}
  block_types["minecraft:ender_chest"]                    = block_break_type{hardness: 22, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:anvil"]                          = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:coal_block"]                     = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:diamond_block"]                  = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:emerald_block"]                  = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:iron_block"]                     = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 2}
  block_types["minecraft:redstone_block"]                 = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:enchanting_table"]               = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:iron_bars"]                      = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:iron_door"]                      = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:iron_trapdoor"]                  = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:spawner"]                        = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:bell"]                           = block_break_type{hardness: 5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:cobweb"]                         = block_break_type{hardness: 4, tooltype: "sword"}

  block_types["minecraft:dispenser"]                      = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:dropper"]                        = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:furnace"]                        = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:observer"]                       = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:stonecutter"]                    = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:blast_furnace"]                  = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:smoker"]                         = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:lantern"]                        = block_break_type{hardness: 3.5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:beacon"]                         = block_break_type{hardness: 3}
  block_types["minecraft:dragon_egg"]                     = block_break_type{hardness: 3}
  block_types["minecraft:conduit"]                        = block_break_type{hardness: 3}

  block_types["minecraft:gold_block"]                     = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:coal_ore"]                       = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:diamond_ore"]                    = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:emerald_ore"]                    = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:end_stone"]                      = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:gold_ore"]                       = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 3}
  block_types["minecraft:hopper"]                         = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:iron_ore"]                       = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 2}
  block_types["minecraft:lapis_block"]                    = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 2}
  block_types["minecraft:lapis_ore"]                      = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 2}
  block_types["minecraft:nether_quartz_ore"]              = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:redstone_ore"]                   = block_break_type{hardness: 3, tooltype: "pickaxe", min_level: 3}

  block_types["minecraft:blue_ice"]                       = block_break_type{hardness: 2.8, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:bone_block"]                     = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:brick_stairs"]                   = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:bricks"]                         = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cauldron"]                       = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cobblestone"]                    = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cobblestone_stairs"]             = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cobblestone_slab"]               = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cobblestone_wall"]               = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:grindstone"]                     = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:shulker_box"]                    = block_break_type{hardness: 2, tooltype: "pickaxe"}
  block_types["minecraft:mossy_cobblestone"]              = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:nether_bricks"]                  = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:red_nether_bricks"]              = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:nether_brick_fence"]             = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:nether_brick_stairs"]            = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:smooth_stone"]                   = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:stone_slab"]                     = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:smooth_stone_slab"]              = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:sandstone_slab"]                 = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cut_sandstone_slab"]             = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:smooth_cobblestone_slab"]        = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:red_sandstone_slab"]             = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cut_red_sandstone_slab"]         = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:smooth_red_sandstone_slab"]      = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:cobblestone_slab"]               = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:mossy_cobblestone_slab"]         = block_break_type{hardness: 2, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:COLOR_concrete"]                 = block_break_type{hardness: 1.8, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:andesite"]                       = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:CORAL_block"]                    = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:dark_prismarine"]                = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:diorite"]                        = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:granite"]                        = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:piston"]                         = block_break_type{hardness: 1.5, tooltype: "pickaxe"}
  block_types["minecraft:prismarine"]                     = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:prismarine_bricks"]              = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:stone"]                          = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:stone_bricks"]                   = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:stone_brick_stairs"]             = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:purpur_block"]                   = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:purpur_pillar"]                  = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:purpur_stairs"]                  = block_break_type{hardness: 1.5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:COLOR_glazed_terracotta"]        = block_break_type{hardness: 1.4, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:terracotta"]                     = block_break_type{hardness: 1.25, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:quartz_block"]                   = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:quartz_stairs"]                  = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:sandstone"]                      = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:sandstone_stairs"]               = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:red_sandstone"]                  = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:red_sandstone_stairs"]           = block_break_type{hardness: 0.8, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:rail"]                           = block_break_type{hardness: 0.7, tooltype: "pickaxe"}
  block_types["minecraft:activator_rail"]                 = block_break_type{hardness: 0.7, tooltype: "pickaxe"}
  block_types["minecraft:detecor_rail"]                   = block_break_type{hardness: 0.7, tooltype: "pickaxe"}
  block_types["minecraft:powered_rail"]                   = block_break_type{hardness: 0.7, tooltype: "pickaxe"}
  block_types["minecraft:rail"]                           = block_break_type{hardness: 0.7, tooltype: "pickaxe"}
  block_types["minecraft:rail"]                           = block_break_type{hardness: 0.7, tooltype: "pickaxe"}

  block_types["minecraft:stone_button"]                   = block_break_type{hardness: 0.5, tooltype: "pickaxe"}
  block_types["minecraft:ice"]                            = block_break_type{hardness: 0.5, tooltype: "pickaxe"}
  block_types["minecraft:packed_ice"]                     = block_break_type{hardness: 0.5, tooltype: "pickaxe"}
  block_types["minecraft:frosted_ice"]                    = block_break_type{hardness: 0.5, tooltype: "pickaxe"}
  block_types["minecraft:brewing_stand"]                  = block_break_type{hardness: 0.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:magma_block"]                    = block_break_type{hardness: 0.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:stone_presssure_plate"]          = block_break_type{hardness: 0.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:light_weighted_presssure_plate"] = block_break_type{hardness: 0.5, tooltype: "pickaxe", min_level: 1}
  block_types["minecraft:heavy_weighted_presssure_plate"] = block_break_type{hardness: 0.5, tooltype: "pickaxe", min_level: 1}

  block_types["minecraft:netherrack"]                     = block_break_type{hardness: 0.4, tooltype: "pickaxe", min_level: 1}


  // All wooden things are next
  block_types["minecraft:WOOD_trapdoor"]                  = block_break_type{hardness: 3, tooltype: "axe"}
  block_types["minecraft:WOOD_door"]                      = block_break_type{hardness: 3, tooltype: "axe"}
  block_types["minecraft:chest"]                          = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:trapped_chest"]                  = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:crafting_table"]                 = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:cartography_table"]              = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:lectern"]                        = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:fletching_table"]                = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:smithing_table"]                 = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:loom"]                           = block_break_type{hardness: 2.5, tooltype: "axe"}
  block_types["minecraft:barrel"]                         = block_break_type{hardness: 2.5, tooltype: "axe"}

  block_types["minecraft:WOOD_fence"]                     = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:WOOD_fence_gate"]                = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:jukebox"]                        = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:campfire"]                       = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:WOOD_log"]                       = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:WOOD_wood"]                      = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:stripped_WOOD_log"]              = block_break_type{hardness: 2, tooltype: "axe"}
  block_types["minecraft:stripped_WOOD_wood"]             = block_break_type{hardness: 2, tooltype: "axe"}

  block_types["minecraft:bookshelf"]                      = block_break_type{hardness: 1.5, tooltype: "axe"}

  block_types["minecraft:bamboo"]                         = block_break_type{hardness: 1, tooltype: "axe"}
  block_types["minecraft:COLOR_banner"]                   = block_break_type{hardness: 1, tooltype: "axe"}
  block_types["minecraft:jack_o_lantern"]                 = block_break_type{hardness: 1, tooltype: "axe"}
  block_types["minecraft:melon"]                          = block_break_type{hardness: 1, tooltype: "axe"}
  block_types["minecraft:player_head"]                    = block_break_type{hardness: 1}
  block_types["minecraft:zombie_head"]                    = block_break_type{hardness: 1}
  block_types["minecraft:creeper_head"]                   = block_break_type{hardness: 1}
  block_types["minecraft:skeleton_skull"]                 = block_break_type{hardness: 1}
  block_types["minecraft:wither_skeleton_skull"]          = block_break_type{hardness: 1}
  block_types["minecraft:pumpkin"]                        = block_break_type{hardness: 1, tooltype: "axe"}
  block_types["minecraft:WOOD_sign"]                      = block_break_type{hardness: 1, tooltype: "axe"}

  block_types["minecraft:note_block"]                     = block_break_type{hardness: 0.8, tooltype: "axe"}
  block_types["minecraft:COLOR_wool"]                     = block_break_type{hardness: 0.8}

  block_types["minecraft:infested_stone"]                 = block_break_type{hardness: 0.75}
  block_types["minecraft:infested_cobblestone"]           = block_break_type{hardness: 0.75}
  block_types["minecraft:infested_stone_bricks"]          = block_break_type{hardness: 0.75}
  block_types["minecraft:infested_mossy_stone_bricks"]    = block_break_type{hardness: 0.75}
  block_types["minecraft:infested_cracked_stone_bricks"]  = block_break_type{hardness: 0.75}
  block_types["minecraft:infested_chiseled_stone_bricks"] = block_break_type{hardness: 0.75}

  block_types["minecraft:beehive"]                        = block_break_type{hardness: 0.6, tooltype: "axe"}
  block_types["minecraft:composter"]                      = block_break_type{hardness: 0.6, tooltype: "axe"}
  block_types["minecraft:clay"]                           = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:farmland"]                       = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:grass_block"]                    = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:grass_path"]                     = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:gravel"]                         = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:mycelium"]                       = block_break_type{hardness: 0.6, tooltype: "shovel"}
  block_types["minecraft:sponge"]                         = block_break_type{hardness: 0.6, tooltype: "hoe"}
  block_types["minecraft:wet_sponge"]                     = block_break_type{hardness: 0.6, tooltype: "hoe"}
  block_types["minecraft:honeycomb"]                      = block_break_type{hardness: 0.6}

  block_types["minecraft:WOOD_presssure_plate"]           = block_break_type{hardness: 0.5, tooltype: "axe"}
  block_types["minecraft:coarse_dirt"]                    = block_break_type{hardness: 0.5, tooltype: "shovel"}
  block_types["minecraft:COLOR_concrete_powder"]          = block_break_type{hardness: 0.5, tooltype: "shovel"}
  block_types["minecraft:dirt"]                           = block_break_type{hardness: 0.5, tooltype: "shovel"}
  block_types["minecraft:hay_block"]                      = block_break_type{hardness: 0.5, tooltype: "hoe"}
  block_types["minecraft:dried_kelp_block"]               = block_break_type{hardness: 0.5, tooltype: "hoe"}
  block_types["minecraft:cake"]                           = block_break_type{hardness: 0.5}
  block_types["minecraft:lever"]                          = block_break_type{hardness: 0.5}
  block_types["minecraft:turtle_egg"]                     = block_break_type{hardness: 0.5}

  block_types["minecraft:ladder"]                         = block_break_type{hardness: 0.4, tooltype: "axe"}
  block_types["minecraft:chorus_plant"]                   = block_break_type{hardness: 0.4, tooltype: "axe"}
  block_types["minecraft:chorus_flower"]                  = block_break_type{hardness: 0.4, tooltype: "axe"}
  block_types["minecraft:cactus"]                         = block_break_type{hardness: 0.4}

  block_types["minecraft:bee_nest"]                       = block_break_type{hardness: 0.3, tooltype: "axe"}
  block_types["minecraft:glass"]                          = block_break_type{hardness: 0.3}
  block_types["minecraft:glass_pane"]                     = block_break_type{hardness: 0.3}
  block_types["minecraft:glowstone"]                      = block_break_type{hardness: 0.3}
  block_types["minecraft:redstone_lamp"]                  = block_break_type{hardness: 0.3}
  block_types["minecraft:sea_lantern"]                    = block_break_type{hardness: 0.3}
  block_types["minecraft:COLOR_stained_glass"]            = block_break_type{hardness: 0.3}
  block_types["minecraft:COLOR_stained_glass_pane"]       = block_break_type{hardness: 0.3}

  block_types["minecraft:cocoa"]                          = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:daylight_detector"]              = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:brown_mushroom_block"]           = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:red_mushroom_block"]             = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:mushroom_stem"]                  = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:vines"]                          = block_break_type{hardness: 0.2, tooltype: "axe"}
  block_types["minecraft:snow_block"]                     = block_break_type{hardness: 0.2, tooltype: "shovel", min_level: 1}
  block_types["minecraft:WOOD_leaves"]                    = block_break_type{hardness: 0.2, tooltype: "hoe"}
  block_types["minecraft:COLOR_bed"]                      = block_break_type{hardness: 0.2}

  block_types["minecraft:COLOR_carpet"]                   = block_break_type{hardness: 0.1}
  block_types["minecraft:snow"]                           = block_break_type{hardness: 0.1, tooltype: "shovel", min_level: 1}

  // All undefined blocks are instant break
}
