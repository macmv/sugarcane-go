package packet

import (
  "encoding/hex"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/entity/metadata"

  "github.com/golang/protobuf/ptypes"
  "github.com/golang/protobuf/ptypes/any"

  pb "gitlab.com/macmv/sugarcane/proto"
  proto_v1 "github.com/golang/protobuf/proto"
  proto_v2 "google.golang.org/protobuf/proto"
)

var packet_types map[Clientbound]func(*out)

type out struct {
  other_type string
  p *pb.Packet
}

type OutgoingPacket struct {
  id Clientbound
  out *out
}

func SetupPacketTypes() {
  packet_types = make(map[Clientbound]func(*out))
  packet_types[0xff] = func(o *out) { // login success
    o.p.Uuids = make([]*pb.UUID, 1)
    o.p.Strings = make([]string, 1)
  }
  packet_types[0xfe] = func(o *out) { // switch server
    o.p.Strings = make([]string, 1)
  }
  packet_types[Clientbound_SpawnEntity] = func(o *out) { // 0x00
    o.p.Bytes = make([]byte, 2)
    o.p.Shorts = make([]int32, 3)
    o.p.Ints = make([]int32, 3)
    o.p.Doubles = make([]float64, 3)
    o.p.Uuids = make([]*pb.UUID, 1)
  }
  packet_types[Clientbound_SpawnExpOrb] = func(o *out) { // 0x01
    o.p.Ints = make([]int32, 2)
    o.p.Doubles = make([]float64, 3)
  }
  packet_types[Clientbound_SpawnWeatherEntity] = func(o *out) { // 0x02
    o.p.Ints = make([]int32, 1)
    o.p.Doubles = make([]float64, 3)
  }
  packet_types[Clientbound_SpawnLivingEntity] = func(o *out) { // 0x03
    o.p.Bytes = make([]byte, 3)
    o.p.Ints = make([]int32, 2)
    o.p.Shorts = make([]int32, 3)
    o.p.Doubles = make([]float64, 3)
    o.p.Uuids = make([]*pb.UUID, 1)
  }
  packet_types[Clientbound_SpawnPainting] = func(o *out) { // 0x04
    o.p.Bytes = make([]byte, 1)
    o.p.Ints = make([]int32, 2)
    o.p.Positions = make([]uint64, 1)
    o.p.Uuids = make([]*pb.UUID, 1)
  }
  packet_types[Clientbound_SpawnPlayer] = func(o *out) { // 0x05
    o.p.Bytes = make([]byte, 2)
    o.p.Ints = make([]int32, 1)
    o.p.Doubles = make([]float64, 3)
    o.p.Uuids = make([]*pb.UUID, 1)
  }
  packet_types[Clientbound_EntityAnimation] = func(o *out) { // 0x06
    o.p.Bytes = make([]byte, 1)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_AcknowledgePlayerDigging] = func(o *out) { // 0x08
    o.p.Ints = make([]int32, 2)
    o.p.Positions = make([]uint64, 1)
    o.p.Bools = make([]bool, 1)
  }
  packet_types[Clientbound_BlockAction] = func(o *out) { // 0x0b
    o.p.Positions = make([]uint64, 1)
    o.p.Bytes = make([]byte, 2)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_BlockChange] = func(o *out) { // 0x0c
    o.p.Positions = make([]uint64, 1)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_BossBar] = func(o *out) { // 0x0d
    o.p.Ints = make([]int32, 1)
    o.other_type = "proto.BossBar"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_ChatMessage] = func(o *out) { // 0x0f
    o.p.Strings = make([]string, 1)
    o.p.Bytes = make([]byte, 1)
    o.p.Uuids = make([]*pb.UUID, 1)
  }
  packet_types[Clientbound_TabComplete] = func(o *out) { // 0x11
    o.p.Ints = make([]int32, 3)
    o.other_type = "proto.TabCompleteSection"
    // node length can vary
  }
  packet_types[Clientbound_DeclareCommands] = func(o *out) { // 0x12
    o.p.Ints = make([]int32, 1)
    o.other_type = "proto.Node"
    // node length can vary
  }
  packet_types[Clientbound_WindowConfirm] = func(o *out) { // 0x13
    o.p.Bools = make([]bool, 1)
    o.p.Bytes = make([]byte, 1)
    o.p.Shorts = make([]int32, 1)
  }
  packet_types[Clientbound_SetSlot] = func(o *out) { // 0x17
    o.p.Bytes = make([]byte, 1)
    o.p.Shorts = make([]int32, 1)
    o.other_type = "proto.Item"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_EntityStatus] = func(o *out) { // 0x1c
    o.p.Ints = make([]int32, 1)
    o.p.Bytes = make([]byte, 1)
  }
  packet_types[Clientbound_UnloadChunk] = func(o *out) { // 0x1e
    o.p.Ints = make([]int32, 2)
  }
  packet_types[Clientbound_ChangeGameState] = func(o *out) { // 0x1f
    o.p.Bytes = make([]byte, 1)
    o.p.Floats = make([]float32, 1)
  }
  packet_types[Clientbound_KeepAlive] = func(o *out) { // 0x21
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_ChunkData] = func(o *out) { // 0x22
    o.other_type = "proto.Chunk"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_Effect] = func(o *out) { // 0x23
    o.p.Ints = make([]int32, 2)
    o.p.Positions = make([]uint64, 1)
    o.p.Bools = make([]bool, 1)
  }
  packet_types[Clientbound_UpdateLight] = func(o *out) { // 0x25
    o.p.Ints = make([]int32, 6)
    o.p.ByteArrays = make([][]byte, 2)
  }
  packet_types[Clientbound_JoinGame] = func(o *out) { // 0x26
    o.p.Bools = make([]bool, 2)
    o.p.Bytes = make([]byte, 2)
    o.p.Ints = make([]int32, 3)
    o.p.Longs = make([]uint64, 1)
    o.p.Strings = make([]string, 1)
  }
  packet_types[Clientbound_MapData] = func(o *out) { // 0x27
    o.other_type = "proto.Map"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_EntityPosition] = func(o *out) { // 0x29
    o.p.Ints = make([]int32, 1)
    o.p.Shorts = make([]int32, 3)
    o.p.Bools = make([]bool, 1)
  }
  packet_types[Clientbound_EntityPositionAndRotation] = func(o *out) { // 0x2a
    o.p.Ints = make([]int32, 1)
    o.p.Shorts = make([]int32, 3)
    o.p.Bytes = make([]byte, 2)
    o.p.Bools = make([]bool, 1)
  }
  packet_types[Clientbound_EntityRotation] = func(o *out) { // 0x2b
    o.p.Ints = make([]int32, 1)
    o.p.Bytes = make([]byte, 2)
    o.p.Bools = make([]bool, 1)
  }
  packet_types[Clientbound_OpenWindow] = func(o *out) { // 0x2f
    o.p.Ints = make([]int32, 2)
    o.p.Strings = make([]string, 1)
  }
  packet_types[Clientbound_PlayerAbilities] = func(o *out) { // 0x32
    o.p.Bytes = make([]byte, 1)
    o.p.Floats = make([]float32, 2)
  }
  packet_types[Clientbound_PlayerInfo] = func(o *out) { // 0x34
    o.p.Ints = make([]int32, 2)
    o.p.ByteArrays = make([][]byte, 1)
  }
  packet_types[Clientbound_PlayerPositionAndLook] = func(o *out) { // 0x36
    o.p.Bytes = make([]byte, 1)
    o.p.Ints = make([]int32, 1)
    o.p.Floats = make([]float32, 2)
    o.p.Doubles = make([]float64, 4)
  }
  packet_types[Clientbound_DestroyEntity] = func(o *out) { // 0x38
    o.p.IntArrays = make([]*pb.IntArray, 1)
  }
  packet_types[Clientbound_ResourcePack] = func(o *out) { // 0x3a
    o.p.Strings = make([]string, 2)
  }
  packet_types[Clientbound_Respawn] = func(o *out) { // 0x3b
    o.p.Ints = make([]int32, 1)
    o.p.Bytes = make([]byte, 1)
  }
  packet_types[Clientbound_EntityHeadLook] = func(o *out) { // 0x3c
    o.p.Ints = make([]int32, 1)
    o.p.Bytes = make([]byte, 1)
  }
  packet_types[Clientbound_UpdateViewPosition] = func(o *out) { // 0x41
    o.p.Ints = make([]int32, 2)
  }
  packet_types[Clientbound_DisplayScoreboard] = func(o *out) { // 0x43
    o.p.Bytes = make([]byte, 1)
    o.p.Strings = make([]string, 1)
  }
  packet_types[Clientbound_EntityMetadata] = func(o *out) { // 0x44
    o.p.Ints = make([]int32, 1)
    o.other_type = "proto.EntityMetadata"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_EntityVelocity] = func(o *out) { // 0x46
    o.p.Ints = make([]int32, 1)
    o.p.Shorts = make([]int32, 3)
  }
  packet_types[Clientbound_EntityEquipment] = func(o *out) { // 0x47
    o.p.Ints = make([]int32, 2)
    o.other_type = "proto.Item"
    o.p.Other = make([]*any.Any, 1)
  }
  packet_types[Clientbound_UpdateHealth] = func(o *out) { // 0x49
    o.p.Floats = make([]float32, 2)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_ScoreboardObjective] = func(o *out) { // 0x4a
    o.p.Strings = make([]string, 2)
    o.p.Bytes = make([]byte, 1)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_UpdateScore] = func(o *out) { // 0x4d
    o.p.Strings = make([]string, 2)
    o.p.Bytes = make([]byte, 1)
    o.p.Ints = make([]int32, 1)
  }
  packet_types[Clientbound_SpawnPosition] = func(o *out) { // 0x4e
    o.p.Positions = make([]uint64, 1)
  }
  packet_types[Clientbound_Title] = func(o *out) { // 0x50
    o.p.Ints = make([]int32, 4)
    o.p.Strings = make([]string, 1)
  }
  packet_types[Clientbound_PlayerListHeader] = func(o *out) { // 0x54
    o.p.Strings = make([]string, 2)
  }
  packet_types[Clientbound_EntityTeleport] = func(o *out) { // 0x57
    o.p.Bools = make([]bool, 1)
    o.p.Bytes = make([]byte, 2)
    o.p.Ints = make([]int32, 1)
    o.p.Doubles = make([]float64, 3)
  }
  packet_types[Clientbound_EntityProperties] = func(o *out) { // 0x59
    o.p.Ints = make([]int32, 1)
    o.other_type = "proto.EntityProperty"
    // There can be any number of entity properties, so we don't set Other here
  }
}

func NewOutgoingPacket(id Clientbound) *OutgoingPacket {
  p := OutgoingPacket{}
  p.id = id
  p.out = &out{p: &pb.Packet{Id: int32(id)}}
  function, ok := packet_types[id]
  if !ok {
    panic("Tried to send unknown packet type: 0x" + hex.EncodeToString([]byte{byte(id)}))
  }
  function(p.out)
  return &p
}

func (p *OutgoingPacket) SetBool(index int32, value bool) {
  p.out.p.Bools[index] = value
}

func (p *OutgoingPacket) SetByte(index int32, value byte) {
  p.out.p.Bytes[index] = value
}

func (p *OutgoingPacket) SetShort(index int32, value int16) {
  p.out.p.Shorts[index] = int32(value)
}

func (p *OutgoingPacket) SetInt(index int32, value int32) {
  p.out.p.Ints[index] = value
}

func (p *OutgoingPacket) SetLong(index int32, value uint64) {
  p.out.p.Longs[index] = value
}

func (p *OutgoingPacket) SetFloat(index int32, value float32) {
  p.out.p.Floats[index] = value
}

func (p *OutgoingPacket) SetDouble(index int32, value float64) {
  p.out.p.Doubles[index] = value
}

func (p *OutgoingPacket) SetString(index int32, value string) {
  p.out.p.Strings[index] = value
}

func (p *OutgoingPacket) SetUUID(index int32, value util.UUID) {
  p.out.p.Uuids[index] = &pb.UUID{BigEndianData: value.GetBytes()}
}

func (p *OutgoingPacket) SetNBT(index int32, value *nbt.Tag) {
  p.out.p.NBTTags[index] = value.Serialize()
}

func (p *OutgoingPacket) SetByteArray(index int32, value []byte) {
  p.out.p.ByteArrays[index] = value
}

func (p *OutgoingPacket) SetIntArray(index int32, value []int32) {
  p.out.p.IntArrays[index] = &pb.IntArray{Ints: value}
}

func (p *OutgoingPacket) SetLongArray(index int32, value []uint64) {
  p.out.p.LongArrays[index] = &pb.LongArray{Longs: value}
}

func (p *OutgoingPacket) SetPosition(index int32, pos block.Pos) {
  value :=((uint64(pos.X) & 0x3FFFFFF) << 38) |
  ((uint64(pos.Z) & 0x3FFFFFF) << 12) |
  (uint64(pos.Y) & 0xFFF)
  p.out.p.Positions[index] = value
}

// Item can be nil, if you want to set the slot to be empty
func (p *OutgoingPacket) SetItem(index int32, value *item.Stack) {
  // Get type of message using this:
  // _, desc := descriptor.MessageDescriptorProto(v)
  // fmt.Println("Got type of message: ", *desc.Name)
  if p.out.other_type != "proto.Item" {
    panic("Other type is not item!")
  }
  proto := &pb.Item{}
  if value != nil {
    proto = value.ToProto()
  }
  any, err := ptypes.MarshalAny(proto)
  if err != nil {
    panic(err)
  }
  p.out.p.Other[index] = any
}

func (p *OutgoingPacket) SetNodes(value []*pb.Node) {
  if p.out.other_type != "proto.Node" {
    panic("Other type is not node!")
  }
  nodes := []*any.Any{}
  for _, v := range value {
    any, err := ptypes.MarshalAny(v)
    if err != nil {
      panic(err)
    }
    nodes = append(nodes, any)
  }
  p.out.p.Other = nodes
}

func (p *OutgoingPacket) SetEntityMetadata(value *metadata.Metadata) {
  if p.out.other_type != "proto.EntityMetadata" {
    panic("Other type is not entity metadata!")
  }
  proto := &pb.EntityMetadata{}
  if value != nil {
    proto = value.ToProto()
  }
  any, err := ptypes.MarshalAny(proto)
  if err != nil {
    panic(err)
  }
  p.out.p.Other[0] = any
}

func (p *OutgoingPacket) SetTabCompleteSections(value []*pb.TabCompleteSection) {
  if p.out.other_type != "proto.TabCompleteSection" {
    panic("Other type is not tab complete section!")
  }
  nodes := []*any.Any{}
  for _, v := range value {
    any, err := ptypes.MarshalAny(v)
    if err != nil {
      panic(err)
    }
    nodes = append(nodes, any)
  }
  p.out.p.Other = nodes
}

func (p *OutgoingPacket) AddEntityProperty(name string, val float64) {
  if p.out.other_type != "proto.EntityProperty" {
    panic("Other type is not entity property!")
  }
  proto := &pb.EntityProperty{}
  proto.Name = name
  proto.Value = val
  any, err := ptypes.MarshalAny(proto)
  if err != nil {
    panic(err)
  }
  p.out.p.Other = append(p.out.p.Other, any)
}

func (p *OutgoingPacket) SetOther(index int32, value proto_v2.Message) {
  any, err := ptypes.MarshalAny(proto_v1.MessageV1(value))
  if err != nil {
    panic(err)
  }
  name, err := ptypes.AnyMessageName(any)
  if err != nil {
    panic(err)
  }
  if name != p.out.other_type {
    panic("Other type does not match passed in protobuf (" + name + " is not " + p.out.other_type + ")")
  }
  p.out.p.Other[index] = any
}

type ConnectionWithChan struct {
  stream pb.Minecraft_ConnectionServer
  channel chan *OutgoingPacket
}

func NewConnectionWithChan(conn pb.Minecraft_ConnectionServer) *ConnectionWithChan {
  c := ConnectionWithChan{}
  c.stream = conn
  c.channel = make(chan *OutgoingPacket, 8000)
  return &c
}

func (c *ConnectionWithChan) Listen() {
  for {
    packet := <- c.channel
    c.stream.Send(packet.out.p)
  }
}

func (c *ConnectionWithChan) GetStream() pb.Minecraft_ConnectionServer {
  return c.stream
}

func (p *OutgoingPacket) Send(conn *ConnectionWithChan) {
  if p == nil {
    return
  }
  conn.channel <- p
}
