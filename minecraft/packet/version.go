package packet

import (
  "fmt"

  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

type VersionsMap map[Version]map[string]uint32

type Version int32

// All minecraft protocol versions. Snapshots are not going to be implemented.
// Even though some if these are the same (1.9.3, 1.9.4) is is better to be clear
// about which version is which. All 1.8 versions are the same, so I didn't bother
// making the subversions of that. Anything below 1.8 will not be supported.
// This is because there is no real reason to use 1.7, ever. 1.9 added the new combat,
// so 1.8 is the latest version that does not have that. Plus it's very fast. So
// 1.8 will always be supported, but nothing below that.
const (
  VINVALID Version = 0

  V1_8    Version = 47

  V1_9    Version = 107
  V1_9_1  Version = 109
  V1_9_2  Version = 109
  V1_9_3  Version = 110
  V1_9_4  Version = 110

  V1_10   Version = 210
  V1_10_1 Version = 210
  V1_10_2 Version = 210

  V1_11   Version = 315
  V1_11_1 Version = 316
  V1_11_2 Version = 316

  V1_12   Version = 335
  V1_12_1 Version = 338
  V1_12_2 Version = 340

  V1_13   Version = 393
  V1_13_1 Version = 401
  V1_13_2 Version = 404

  V1_14   Version = 477
  V1_14_1 Version = 480
  V1_14_2 Version = 485
  V1_14_3 Version = 490
  V1_14_4 Version = 498

  V1_15   Version = 573
  V1_15_1 Version = 575
  V1_15_2 Version = 578

  V1_16   Version = 735
  V1_16_1 Version = 736
  V1_16_2 Version = 751
  V1_16_3 Version = 753
  V1_16_4 Version = 754
  V1_16_5 Version = 754

  // Latest block version to use. Should only be a minor version
  // if the block was changed in that version.
  VLATEST Version = V1_16
)

func (v Version) String() string {
  switch v {
    case V1_8   : return "1.8"
    case V1_9   : return "1.9"
    case V1_9_1 : return "1.9.1-1.9.2"
    case V1_9_3 : return "1.9.3-1.9.4"
    case V1_10  : return "1.10-1.10.2"
    case V1_11  : return "1.11"
    case V1_11_1: return "1.11.1-1.11.2"
    case V1_12  : return "1.12"
    case V1_12_1: return "1.12.1"
    case V1_12_2: return "1.12.2"
    case V1_13  : return "1.13"
    case V1_13_1: return "1.13.1"
    case V1_13_2: return "1.13.2"
    case V1_14  : return "1.14"
    case V1_14_1: return "1.14.1"
    case V1_14_2: return "1.14.2"
    case V1_14_3: return "1.14.3"
    case V1_14_4: return "1.14.4"
    case V1_15  : return "1.15"
    case V1_15_1: return "1.15.1"
    case V1_15_2: return "1.15.2"
    case V1_16  : return "1.16"
    case V1_16_1: return "1.16.1"
    case V1_16_2: return "1.16.2"
    case V1_16_3: return "1.16.3"
    case V1_16_4: return "1.16.4-1.16.5"
    default:      return fmt.Sprint("Protocol version", int32(v))
  }
}

func (v Version) BlockVersion() block.Version {
  switch v {
    case V1_8:                                      return block.V1_8
    // These versions will probably never be implemented
    // case V1_9, V1_9_1, V1_9_3:                      return block.V1_9
    // case V1_10:                                     return block.V1_10
    // case V1_11, V1_11_2:                            return block.V1_11
    case V1_12, V1_12_1, V1_12_2:                   return block.V1_12
    case V1_13, V1_13_1, V1_13_2:                   return block.V1_13
    case V1_14, V1_14_1, V1_14_2, V1_14_3, V1_14_4: return block.V1_14
    case V1_15, V1_15_1, V1_15_2:                   return block.V1_15
    case V1_16, V1_16_1, V1_16_2, V1_16_3, V1_16_4: return block.V1_16
    default:                                        return block.VINVALID
  }
}
