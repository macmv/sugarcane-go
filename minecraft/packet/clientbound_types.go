package packet

import (
  "fmt"
)

const (
  // Special none type; used as a placeholder to when a packet is defined
  // in an older version, but not in the grpc version.
  Clientbound_None Clientbound = iota
  Clientbound_SpawnEntity
  Clientbound_SpawnExpOrb
  Clientbound_SpawnWeatherEntity
  Clientbound_SpawnLivingEntity
  Clientbound_SpawnPainting
  Clientbound_SpawnPlayer
  Clientbound_EntityAnimation
  Clientbound_Statistics
  Clientbound_AcknowledgePlayerDigging
  Clientbound_BlockBreakAnimation
  Clientbound_BlockEntityData
  Clientbound_BlockAction
  Clientbound_BlockChange
  Clientbound_BossBar
  Clientbound_ServerDifficulty
  Clientbound_ChatMessage
  Clientbound_MultiBlockChange
  Clientbound_TabComplete
  Clientbound_DeclareCommands
  Clientbound_WindowConfirm
  Clientbound_CloseWindow
  Clientbound_WindowItems
  Clientbound_WindowProperty
  Clientbound_SetSlot
  Clientbound_SetCooldown
  Clientbound_PluginMessage
  Clientbound_NamedSoundEffect
  Clientbound_Disconnect
  Clientbound_EntityStatus
  Clientbound_Explosion
  Clientbound_UnloadChunk
  Clientbound_ChangeGameState
  Clientbound_OpenHorseWindow
  Clientbound_KeepAlive
  Clientbound_ChunkData
  Clientbound_Effect
  Clientbound_Particle
  Clientbound_UpdateLight
  Clientbound_JoinGame
  Clientbound_MapData
  Clientbound_TradeList
  Clientbound_EntityPosition
  Clientbound_EntityPositionAndRotation
  Clientbound_EntityRotation
  Clientbound_EntityOnGround
  Clientbound_VehicleMove
  Clientbound_OpenBook
  Clientbound_OpenWindow
  Clientbound_OpenSignEditor
  Clientbound_CraftRecipeResponse
  Clientbound_PlayerAbilities
  Clientbound_EnterCombat
  Clientbound_PlayerInfo
  Clientbound_FacePlayer
  Clientbound_PlayerPositionAndLook
  Clientbound_UnlockRecipies
  Clientbound_DestroyEntity
  Clientbound_RemoveEntityEffect
  Clientbound_ResourcePack
  Clientbound_Respawn
  Clientbound_EntityHeadLook
  Clientbound_SelectAdvancementTab
  Clientbound_WorldBorder
  Clientbound_Camera
  Clientbound_HeldItemChange
  Clientbound_UpdateViewPosition
  Clientbound_UpdateViewDistance
  Clientbound_DisplayScoreboard
  Clientbound_EntityMetadata
  Clientbound_AttachEntity
  Clientbound_EntityVelocity
  Clientbound_EntityEquipment
  Clientbound_SetExp
  Clientbound_UpdateHealth
  Clientbound_ScoreboardObjective
  Clientbound_SetPassengers
  Clientbound_Teams
  Clientbound_UpdateScore
  Clientbound_SpawnPosition
  Clientbound_TimeUpdate
  Clientbound_Title
  Clientbound_EntitySoundEffect
  Clientbound_SoundEffect
  Clientbound_StopSound
  Clientbound_PlayerListHeader
  Clientbound_NBTQueryResponse
  Clientbound_CollectItem
  Clientbound_EntityTeleport
  Clientbound_Advancements
  Clientbound_EntityProperties
  Clientbound_EntityEffect
  Clientbound_DeclareRecipies
  Clientbound_Tags
  // Custom packet; should be intercepted by the proxy
  Clientbound_Login
)

func init() {
  c(Clientbound_None,                      "Clientbound_None")
  c(Clientbound_SpawnEntity,               "Clientbound_SpawnEntity")
  c(Clientbound_SpawnExpOrb,               "Clientbound_SpawnExpOrb")
  c(Clientbound_SpawnWeatherEntity,        "Clientbound_SpawnWeatherEntity") // Lightning
  c(Clientbound_SpawnLivingEntity,         "Clientbound_SpawnLivingEntity")
  c(Clientbound_SpawnPainting,             "Clientbound_SpawnPainting")
  c(Clientbound_SpawnPlayer,               "Clientbound_SpawnPlayer")
  c(Clientbound_EntityAnimation,           "Clientbound_EntityAnimation")
  c(Clientbound_Statistics,                "Clientbound_Statistics")
  c(Clientbound_AcknowledgePlayerDigging,  "Clientbound_AcknowledgePlayerDigging")
  c(Clientbound_BlockBreakAnimation,       "Clientbound_BlockBreakAnimation")
  c(Clientbound_BlockEntityData,           "Clientbound_BlockEntityData")
  c(Clientbound_BlockAction,               "Clientbound_BlockAction")
  c(Clientbound_BlockChange,               "Clientbound_BlockChange")
  c(Clientbound_BossBar,                   "Clientbound_BossBar")
  c(Clientbound_ServerDifficulty,          "Clientbound_ServerDifficulty")
  c(Clientbound_ChatMessage,               "Clientbound_ChatMessage")
  c(Clientbound_MultiBlockChange,          "Clientbound_MultiBlockChange")
  c(Clientbound_TabComplete,               "Clientbound_TabComplete")
  c(Clientbound_DeclareCommands,           "Clientbound_DeclareCommands")
  c(Clientbound_WindowConfirm,             "Clientbound_WindowConfirm")
  c(Clientbound_CloseWindow,               "Clientbound_CloseWindow")
  c(Clientbound_WindowItems,               "Clientbound_WindowItems")
  c(Clientbound_WindowProperty,            "Clientbound_WindowProperty")
  c(Clientbound_SetSlot,                   "Clientbound_SetSlot") // Sets the item in a window (chest, player inventory, etc)
  c(Clientbound_SetCooldown,               "Clientbound_SetCooldown") // Enderpearl cooldown thingy. Can be used with any item
  c(Clientbound_PluginMessage,             "Clientbound_PluginMessage")
  c(Clientbound_NamedSoundEffect,          "Clientbound_NamedSoundEffect")
  c(Clientbound_Disconnect,                "Clientbound_Disconnect")
  c(Clientbound_EntityStatus,              "Clientbound_EntityStatus")
  c(Clientbound_Explosion,                 "Clientbound_Explosion")
  c(Clientbound_UnloadChunk,               "Clientbound_UnloadChunk")
  c(Clientbound_ChangeGameState,           "Clientbound_ChangeGameState") // Used for setting rain, enabling respawn screen, changing gamemode, and more
  c(Clientbound_OpenHorseWindow,           "Clientbound_OpenHorseWindow")
  c(Clientbound_KeepAlive,                 "Clientbound_KeepAlive")
  c(Clientbound_ChunkData,                 "Clientbound_ChunkData")
  c(Clientbound_Effect,                    "Clientbound_Effect") // Particles with sounds. Includes block break, door open/close, and more
  c(Clientbound_Particle,                  "Clientbound_Particle")
  c(Clientbound_UpdateLight,               "Clientbound_UpdateLight")
  c(Clientbound_JoinGame,                  "Clientbound_JoinGame")
  c(Clientbound_MapData,                   "Clientbound_MapData")
  c(Clientbound_TradeList,                 "Clientbound_TradeList")
  c(Clientbound_EntityPosition,            "Clientbound_EntityPosition")
  c(Clientbound_EntityPositionAndRotation, "Clientbound_EntityPositionAndRotation")
  c(Clientbound_EntityRotation,            "Clientbound_EntityRotation")
  c(Clientbound_EntityOnGround,            "Clientbound_EntityOnGround")
  c(Clientbound_VehicleMove,               "Clientbound_VehicleMove")
  c(Clientbound_OpenBook,                  "Clientbound_OpenBook")
  c(Clientbound_OpenWindow,                "Clientbound_OpenWindow")
  c(Clientbound_OpenSignEditor,            "Clientbound_OpenSignEditor")
  c(Clientbound_CraftRecipeResponse,       "Clientbound_CraftRecipeResponse")
  c(Clientbound_PlayerAbilities,           "Clientbound_PlayerAbilities")
  c(Clientbound_EnterCombat,               "Clientbound_EnterCombat") // Used for entering/exiting combat. No longer used by the vanilla server
  c(Clientbound_PlayerInfo,                "Clientbound_PlayerInfo") // Tab list update
  c(Clientbound_FacePlayer,                "Clientbound_FacePlayer") // Makes the player face a block
  c(Clientbound_PlayerPositionAndLook,     "Clientbound_PlayerPositionAndLook") // Will set the look direction of the players screen. Usually very jarring
  c(Clientbound_UnlockRecipies,            "Clientbound_UnlockRecipies") // Adds/removes recipes from the recipie book
  c(Clientbound_DestroyEntity,             "Clientbound_DestroyEntity")
  c(Clientbound_RemoveEntityEffect,        "Clientbound_RemoveEntityEffect")
  c(Clientbound_ResourcePack,              "Clientbound_ResourcePack")
  c(Clientbound_Respawn,                   "Clientbound_Respawn")
  c(Clientbound_EntityHeadLook,            "Clientbound_EntityHeadLook")
  c(Clientbound_SelectAdvancementTab,      "Clientbound_SelectAdvancementTab")
  c(Clientbound_WorldBorder,               "Clientbound_WorldBorder")
  c(Clientbound_Camera,                    "Clientbound_Camera") // When the player clicks an entity in spectator mode
  c(Clientbound_HeldItemChange,            "Clientbound_HeldItemChange")
  c(Clientbound_UpdateViewPosition,        "Clientbound_UpdateViewPosition") // Sent when a player crosses chunk borders
  c(Clientbound_UpdateViewDistance,        "Clientbound_UpdateViewDistance") // Used when changing the chunk view distance. Never used by the vanilla server
  c(Clientbound_DisplayScoreboard,         "Clientbound_DisplayScoreboard")
  c(Clientbound_EntityMetadata,            "Clientbound_EntityMetadata")
  c(Clientbound_AttachEntity,              "Clientbound_AttachEntity") // When using a lead
  c(Clientbound_EntityVelocity,            "Clientbound_EntityVelocity")
  c(Clientbound_EntityEquipment,           "Clientbound_EntityEquipment")
  c(Clientbound_SetExp,                    "Clientbound_SetExp")
  c(Clientbound_UpdateHealth,              "Clientbound_UpdateHealth")
  c(Clientbound_ScoreboardObjective,       "Clientbound_ScoreboardObjective")
  c(Clientbound_SetPassengers,             "Clientbound_SetPassengers") // For vehicles
  c(Clientbound_Teams,                     "Clientbound_Teams")
  c(Clientbound_UpdateScore,               "Clientbound_UpdateScore")
  c(Clientbound_SpawnPosition,             "Clientbound_SpawnPosition") // Compass target
  c(Clientbound_TimeUpdate,                "Clientbound_TimeUpdate")
  c(Clientbound_Title,                     "Clientbound_Title")
  c(Clientbound_EntitySoundEffect,         "Clientbound_EntitySoundEffect") // Same as sound effect, but the position is the entity's position
  c(Clientbound_SoundEffect,               "Clientbound_SoundEffect")
  c(Clientbound_StopSound,                 "Clientbound_StopSound")
  c(Clientbound_PlayerListHeader,          "Clientbound_PlayerListHeader") // Header and footer
  c(Clientbound_NBTQueryResponse,          "Clientbound_NBTQueryResponse")
  c(Clientbound_CollectItem,               "Clientbound_CollectItem") // Is the animation. Does not affect inventory
  c(Clientbound_EntityTeleport,            "Clientbound_EntityTeleport")
  c(Clientbound_Advancements,              "Clientbound_Advancements")
  c(Clientbound_EntityProperties,          "Clientbound_EntityProperties")
  c(Clientbound_EntityEffect,              "Clientbound_EntityEffect")
  c(Clientbound_DeclareRecipies,           "Clientbound_DeclareRecipies") // Is a list of all recipies. Does not affect recipe book
  c(Clientbound_Tags,                      "Clientbound_Tags")
  c(Clientbound_Login,                     "Clientbound_Login")
}

type Clientbound int32

var (
  clientbound_names = make(map[Clientbound]string)
)

// Adds a clientbound packet to the names list
// It's named c so that the init() function above is more readable
func c(p Clientbound, name string) {
  clientbound_names[p] = name
}

func (c Clientbound) String() string {
  s, ok := clientbound_names[c]
  if ok {
    return fmt.Sprintf("%s (0x%02x)", s, int32(c))
  }  else {
    return fmt.Sprintf("Unknown (0x%02x)", int32(c))
  }
}
