package packet

import (
  "fmt"
)

const (
  // Special none type. Should be handled within the proxy, but it is valid
  // to send one of these to the server (it should be ignored).
  Serverbound_None Serverbound = iota
  Serverbound_TeleportConfirm
  Serverbound_QueryBlockNBT
  Serverbound_SetDifficulty
  Serverbound_ChatMessage
  Serverbound_ClientStatus
  Serverbound_ClientSettings
  Serverbound_TabComplete
  Serverbound_WindowConfirmation
  Serverbound_ClickWindowButton
  Serverbound_ClickWindow
  Serverbound_CloseWindow
  Serverbound_PluginMessage
  Serverbound_EditBook
  Serverbound_EntityNBTRequest
  Serverbound_InteractEntity
  Serverbound_KeepAlive
  Serverbound_LockDifficulty
  Serverbound_PlayerPosition
  Serverbound_PlayerPositionAndRotation
  Serverbound_PlayerRotation
  Serverbound_PlayerOnGround
  Serverbound_VehicleMove
  Serverbound_SteerBoat
  Serverbound_PickItem
  Serverbound_CraftRecipeRequest
  Serverbound_PlayerAbilities
  Serverbound_PlayerDigging
  Serverbound_EntityAction
  Serverbound_SteerVehicle
  Serverbound_RecipeBookData
  Serverbound_NameItem
  Serverbound_ResourcePackStatus
  Serverbound_AdvancementTab
  Serverbound_SelectTrade
  Serverbound_SetBeaconEffect
  Serverbound_HeldItemChange
  Serverbound_UpdateCommandBlock
  Serverbound_UpdateCommandBlockMinecart
  Serverbound_CreativeInventoryAction
  Serverbound_UpdateJigsawBlock
  Serverbound_UpdateStructureBlock
  Serverbound_UpdateSign
  Serverbound_Animation
  Serverbound_Spectate
  Serverbound_PlayerBlockPlace
  Serverbound_UseItem
)

func init() {
  s(Serverbound_None,                       "Serverbound_None")
  s(Serverbound_TeleportConfirm,            "Serverbound_TeleportConfirm")
  s(Serverbound_QueryBlockNBT,              "Serverbound_QueryBlockNBT")
  s(Serverbound_SetDifficulty,              "Serverbound_SetDifficulty")
  s(Serverbound_ChatMessage,                "Serverbound_ChatMessage")
  s(Serverbound_ClientStatus,               "Serverbound_ClientStatus")
  s(Serverbound_ClientSettings,             "Serverbound_ClientSettings")
  s(Serverbound_TabComplete,                "Serverbound_TabComplete")
  s(Serverbound_WindowConfirmation,         "Serverbound_WindowConfirmation")
  s(Serverbound_ClickWindowButton,          "Serverbound_ClickWindowButton")
  s(Serverbound_ClickWindow,                "Serverbound_ClickWindow")
  s(Serverbound_CloseWindow,                "Serverbound_CloseWindow")
  s(Serverbound_PluginMessage,              "Serverbound_PluginMessage")
  s(Serverbound_EditBook,                   "Serverbound_EditBook")
  s(Serverbound_EntityNBTRequest,           "Serverbound_EntityNBTRequest")
  s(Serverbound_InteractEntity,             "Serverbound_InteractEntity")
  s(Serverbound_KeepAlive,                  "Serverbound_KeepAlive")
  s(Serverbound_LockDifficulty,             "Serverbound_LockDifficulty")
  s(Serverbound_PlayerPosition,             "Serverbound_PlayerPosition")
  s(Serverbound_PlayerPositionAndRotation,  "Serverbound_PlayerPositionAndRotation")
  s(Serverbound_PlayerRotation,             "Serverbound_PlayerRotation")
  s(Serverbound_PlayerOnGround,             "Serverbound_PlayerOnGround")
  s(Serverbound_VehicleMove,                "Serverbound_VehicleMove")
  s(Serverbound_SteerBoat,                  "Serverbound_SteerBoat")
  s(Serverbound_PickItem,                   "Serverbound_PickItem") // Survival mode only. Creative mode users just sends set inventory packets,
  s(Serverbound_CraftRecipeRequest,         "Serverbound_CraftRecipeRequest")
  s(Serverbound_PlayerAbilities,            "Serverbound_PlayerAbilities")
  s(Serverbound_PlayerDigging,              "Serverbound_PlayerDigging")
  s(Serverbound_EntityAction,               "Serverbound_EntityAction")
  s(Serverbound_SteerVehicle,               "Serverbound_SteerVehicle")
  s(Serverbound_RecipeBookData,             "Serverbound_RecipeBookData")
  s(Serverbound_NameItem,                   "Serverbound_NameItem")
  s(Serverbound_ResourcePackStatus,         "Serverbound_ResourcePackStatus")
  s(Serverbound_AdvancementTab,             "Serverbound_AdvancementTab")
  s(Serverbound_SelectTrade,                "Serverbound_SelectTrade")
  s(Serverbound_SetBeaconEffect,            "Serverbound_SetBeaconEffect")
  s(Serverbound_HeldItemChange,             "Serverbound_HeldItemChange")
  s(Serverbound_UpdateCommandBlock,         "Serverbound_UpdateCommandBlock")
  s(Serverbound_UpdateCommandBlockMinecart, "Serverbound_UpdateCommandBlockMinecart")
  s(Serverbound_CreativeInventoryAction,    "Serverbound_CreativeInventoryAction")
  s(Serverbound_UpdateJigsawBlock,          "Serverbound_UpdateJigsawBlock")
  s(Serverbound_UpdateStructureBlock,       "Serverbound_UpdateStructureBlock")
  s(Serverbound_UpdateSign,                 "Serverbound_UpdateSign")
  s(Serverbound_Animation,                  "Serverbound_Animation") // Hand animation
  s(Serverbound_Spectate,                   "Serverbound_Spectate") // Spectator select entity
  s(Serverbound_PlayerBlockPlace,           "Serverbound_PlayerBlockPlace")
  s(Serverbound_UseItem,                    "Serverbound_UseItem")
}

type Serverbound int32

var (
  serverbound_names = make(map[Serverbound]string)
)

// Adds a serverbound packet to the names list
// It's named s so that the init() function above is more readable
func s(p Serverbound, name string) {
  serverbound_names[p] = name
}

func (s Serverbound) String() string {
  n, ok := serverbound_names[s]
  if ok {
    return fmt.Sprintf("%s (0x%02x)", n, int32(s))
  }  else {
    return fmt.Sprintf("Unknown (0x%02x)", int32(s))
  }
}
