package minecraft

import (
  "os"
  "context"
  "crypto/rsa"
  "crypto/rand"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/net"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  pb "gitlab.com/macmv/sugarcane/proto"

  "google.golang.org/grpc"
  // _ "google.golang.org/grpc/encoding/gzip"
)

type connection_manager struct {
  pb.MinecraftServer

  minecraft *Minecraft
  privateKey *rsa.PrivateKey
  packet_handler *net.PacketHandler
}

func newConnectionManager(minecraft *Minecraft, packet_handler *net.PacketHandler) *connection_manager {
  c := connection_manager{}
  c.minecraft = minecraft
  var err error
  c.privateKey, err = rsa.GenerateKey(rand.Reader, 1024)
  if err != nil {
    log.Error("Could not Could not generate private key pair!")
    os.Exit(1)
  }
  c.packet_handler = packet_handler
  return &c
}

func setup_connection(port string, minecraft *Minecraft) *grpc.Server {
  log.Info("Listening on port ", port)
  packet_handler := net.NewPacketHandler(minecraft.WorldManager, minecraft.Events)
  net.SetupPacketTypes()
  go packet_handler.Listen()
  packet.SetupPacketTypes()
  grpc_server := grpc.NewServer()
  pb.RegisterMinecraftServer(grpc_server, newConnectionManager(minecraft, packet_handler))
  return grpc_server
}

func (c *connection_manager) Connection(stream pb.Minecraft_ConnectionServer) error {
  log.Info("Opened new connection with client!")
  connection := net.NewConnection(stream, c.minecraft.WorldManager, c.minecraft.Events, c.packet_handler)
  connection.RecieveLoop()
  log.Info("Connection with client has been closed")
  return nil
}

func (c *connection_manager) ReserveSlots(ctx context.Context, req *pb.ReserveSlotsRequest) (*pb.ReserveSlotsResponse, error) {
  ids := []util.UUID{}
  for _, id := range req.Players {
    ids = append(ids, util.NewUUIDFromProto(id))
  }
  res := pb.ReserveSlotsResponse{}
  if c.minecraft.WorldManager.ReservePlayers(ids) {
    res.CanJoin = true
  }
  return &res, nil
}

func (c *connection_manager) Status(ctx context.Context, req *pb.StatusRequest) (*pb.StatusResponse, error) {
  println("Got status")
  return nil, nil
}
