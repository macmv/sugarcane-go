package event

import (
  "gitlab.com/macmv/sugarcane/minecraft/event/command"
)

func addCommands(m *command.CommandManager) {
  m.Add(command.New("clearchunk", handleClearChunk))

  m.Add(command.New("loadchunk", handleLoadChunk))

  // Reuse chunk/world args (reduces size of final command tree)
  chunk := command.LiteralNode("chunk")
  world := command.LiteralNode("world",
    command.ArgumentNode("world",
      command.Parser(command.STRING).Greedy(),
    ).Suggestion("minecraft:ask_server"),
  )
  m.Add(command.New("save", handleSave,
    command.LiteralNode("disk", chunk, world),
    command.LiteralNode("s3", chunk, world),
  ).SetTabCompleter(handleSaveTabComplete))

  m.Add(command.New("stp", handleServerTeleport,
    command.ArgumentNode("server", command.Parser(command.STRING).Greedy()),
  ))

  m.Add(command.New("wtp", handleWorldTeleport,
    command.ArgumentNode("world", command.Parser(command.STRING).Greedy()).
    Suggestion("minecraft:ask_server"),
  ).SetTabCompleter(handleWorldTeleportTabComplete))

  m.Add(command.New("flyspeed", handleFlySpeed,
    command.ArgumentNode("speed", command.Parser(command.FLOAT).Min(0)),
  ))

  m.Add(command.New("bossbar", handleShowBossBar))

  m.Add(command.New("test", handleTest))

  m.Add(command.New("gamemode", handleGamemode,
    command.ArgumentNode("gamemode", command.Parser(command.INTEGER).Min(0).Max(3)),
  ))

  m.Add(command.New("clearvoid", handleClearVoid))
}
