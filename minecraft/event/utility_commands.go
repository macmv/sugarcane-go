package event

import (
  "strconv"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"

  pb "gitlab.com/macmv/sugarcane/proto"
)

func handleClearChunk(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  world := world_manager.GetWorldOfPlayer(player)
  _, chunk_pos, is_default := world.ChunkAtBlock(block.Pos{player.BlockX(), 0, player.BlockZ()})
  if is_default {
    chat := util.NewChat()
    chat.AddSectionColor("You are already standing in a default chunk!", "red")
    player.SendChat(chat)
  } else {
    chat := util.NewChat()
    chat.AddSectionColor("Clearing chunk at position ", "yellow")
    chat.AddSection(strconv.Itoa(int(chunk_pos.X)) + ", " + strconv.Itoa(int(chunk_pos.Z)))
    player.SendChat(chat)
    world.Unload(chunk_pos)
  }
  return true
}

func handleLoadChunk(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  return true
}

func handleSave(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) < 2 {
    return false
  }
  if args[0] == "disk" || args[0] == "s3" {
    if args[1] == "chunk" {
      world := world_manager.GetWorldOfPlayer(player)
      chunk_pos := block.ChunkPos{player.ChunkX(), player.ChunkZ()}
      _, is_default := world.Chunk(chunk_pos)
      if is_default {
        chat := util.NewChat()
        chat.AddSectionColor("You are standing in a default chunk!", "red")
        player.SendChat(chat)
      } else {
        chat := util.NewChat()
        chat.AddSectionColor("Saving chunk at position ", "yellow")
        chat.AddSection(strconv.Itoa(int(chunk_pos.X)) + ", " + strconv.Itoa(int(chunk_pos.Z)))
        chat.AddSectionColor(" to " + args[0], "yellow")
        player.SendChat(chat)
        if args[0] == "disk" {
          world.SaveChunkToDisk(chunk_pos)
        } else {
          world.SaveChunkToS3(chunk_pos, "cubiness-data")
        }
      }
    } else if args[1] == "world" {
      world := world_manager.GetWorldOfPlayer(player)
      if len(args) == 2 {
        if args[0] == "disk" {
          world.SaveToDisk()
        } else {
          world.SaveToS3()
        }
        chat := util.NewChat()
        chat.AddSectionColor("Saved world to " + args[0], "yellow")
        player.SendChat(chat)
      } else if len(args) == 3 {
        if args[0] == "disk" {
          world.SaveToDiskDir(args[2])
        } else {
          world.SaveToS3Dir("cubiness-data", args[2])
        }
        chat := util.NewChat()
        chat.AddSectionColor("Saved world to " + args[0] + " under directory ", "yellow")
        chat.AddSection(args[2])
        player.SendChat(chat)
      } else {
        return false
      }
    }
  } else {
    return false
  }
  return true
}

func handleSaveTabComplete(world_manager *world.WorldManager, player *player.Player, args []string)[]string {
  if len(args) == 3 && args[1] == "world" {
    worlds := world_manager.Worlds()
    world_list := []string{}
    for name, _ := range worlds {
      world_list = append(world_list, name)
    }
    return world_list
  }
  return []string{}
}

func handleServerTeleport(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) == 0 {
    player.SendChat(util.NewChatFromStringColor("Please enter a valid ip!", "red"))
    return true
  }
  out := packet.NewOutgoingPacket(0xfe) // Switch server packet (custom to proxy)
  out.SetString(0, args[0])
  out.Send(player.PacketStream)
  return true
}

func handleWorldTeleport(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) < 1 {
    return false
  }
  worlds := world_manager.Worlds()
  _, ok := worlds[args[0]]
  if !ok {
    chat := util.NewChat()
    chat.AddSectionColor("Cannot find a world with the name ", "red")
    chat.AddSection(args[0])
    player.SendChat(chat)
    return true
  }
  chat := util.NewChat()
  chat.AddSectionColor("Teleporting to world ", "yellow")
  chat.AddSection(args[0])
  player.SendChat(chat)
  world_manager.MovePlayerToWorld(player, args[0])
  return true
}

func handleWorldTeleportTabComplete(world_manager *world.WorldManager, player *player.Player, args []string) []string {
  if len(args) == 1 {
    worlds := world_manager.Worlds()
    world_list := []string{}
    for name, _ := range worlds {
      world_list = append(world_list, name)
    }
    return world_list
  }
  return []string{}
}

func handleShowBossBar(world_manager *world.WorldManager, player *player.Player, args[] string) bool {
  boss_bar := pb.BossBar{}
  boss_bar.Value = 0.5
  boss_bar.Title = util.NewChatFromStringColor("Hello", "green").ToJSON()
  boss_bar.Uuid = &pb.UUID{}
  out := packet.NewOutgoingPacket(packet.Clientbound_BossBar)
  out.SetInt(0, 0)
  out.SetOther(0, &boss_bar)
  out.Send(player.PacketStream)
  return true
}

func handleFlySpeed(world_manager *world.WorldManager, player *player.Player, args[] string) bool {
  if len(args) < 1 {
    return false
  }
  speed, _ := strconv.ParseFloat(args[0], 32)
  player.SetFlySpeed(float32(speed))
  return true
}

func handleTest(world_manager *world.WorldManager, player *player.Player, args[] string) bool {
  player.Scoreboard.Show()
  player.Scoreboard.Clear()
  return true
}

func handleGamemode(world_manager *world.WorldManager, player *player.Player, args[] string) bool {
  if len(args) < 1 {
    return false
  }
  gamemode, err := strconv.ParseInt(args[0], 10, 8)
  if err != nil || gamemode < 0 || gamemode > 3 {
    player.SendChat(util.NewChatFromStringColor("Please enter a gamemode from 0 to 3", "red"))
    return true
  }
  player.SetGamemode(byte(gamemode))
  return true
}

func handleClearVoid(world_manager *world.WorldManager, player *player.Player, args[] string) bool {
  w := world_manager.GetWorldOfPlayer(player)
  w.ClearVoid()
  player.Scoreboard.Show()
  player.Scoreboard.Clear()
  return true
}

