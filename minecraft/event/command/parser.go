package command

import (
  "math"
  "encoding/binary"
)

// This creates a parser for a node. You should use
// the constants for the parser_kind. To change the
// way the values are parsed, call functions like
// Min() or Max(). See the individual functions for more
// info. All functions will return copies of the
// original parser, with the new settings applied. They
// are designed to be used inline as arguments to
// NewArgumentNode().
func Parser(k parser_kind) parser {
  return parser{k: k, data: k.initial_data()}
}

// A parser for a node. See Parser() for more.
type parser struct {
  // The type of parser. Is an arbitrary constant.
  k parser_kind
  // Extra data. Format determined by t.
  data []byte
}

// Sets the minimum value. Will panic of the parser kind
// is not an int, float, or double. If you are setting
// this minimum on an int, then value will be cast to an
// int32.
func (p parser) Min(value float64) parser {
  zero_value := []byte{}
  switch p.k {
  case INTEGER, FLOAT:
    zero_value = make([]byte, 4)
  case DOUBLE:
    zero_value = make([]byte, 8)
  default:
    panic("Invalid call Min() on type " + p.k.String())
  }
  had_min := p.data[0] & 0x01 != 0
  had_max := p.data[0] & 0x02 != 0
  // Make sure the min flag is set
  p.data[0] |= 0x01
  if !had_min {
    p.data = append(p.data, zero_value...)
    if had_max {
      // Moves max over
      copy(p.data[len(zero_value) + 1:], p.data[1:])
    }
  }
  switch p.k {
  case INTEGER:
    binary.BigEndian.PutUint32(p.data[1:], uint32(value))
  case FLOAT:
    binary.BigEndian.PutUint32(p.data[1:], math.Float32bits(float32(value)))
  case DOUBLE:
    binary.BigEndian.PutUint64(p.data[1:], math.Float64bits(value))
  }
  return p
}

// Sets the maximum value. Will panic of the parser kind
// is not an int, float, or double. If you are setting
// this maximum on an int, then value will be cast to an
// int32.
func (p parser) Max(value float64) parser {
  zero_value := []byte{}
  switch p.k {
  case INTEGER, FLOAT:
    zero_value = make([]byte, 4)
  case DOUBLE:
    zero_value = make([]byte, 8)
  default:
    panic("Invalid call Max() on type " + p.k.String())
  }
  had_max := p.data[0] & 0x02 != 0
  // Make sure the min flag is set
  p.data[0] |= 0x02
  if !had_max {
    // We can always append here; max will always be last in the list.
    // So we don't need to shift min over or anything.
    p.data = append(p.data, zero_value...)
  }
  index := len(p.data) - len(zero_value)
  switch p.k {
  case INTEGER:
    binary.BigEndian.PutUint32(p.data[index:], uint32(value))
  case FLOAT:
    binary.BigEndian.PutUint32(p.data[index:], math.Float32bits(float32(value)))
  case DOUBLE:
    binary.BigEndian.PutUint64(p.data[index:], math.Float64bits(value))
  }
  return p
}

// This changes how strings are parsed. With this parse
// mode, this node will only read one word. Will panic if
// called on a value that is not a string.
func (p parser) Word() parser {
  if p.k != STRING {
    panic("Invalid call Word() on type " + p.k.String())
  }
  p.data[0] = 0x00
  return p
}

// This changes how strings are parsed. With this parse
// mode, this node will only read one word, unless there
// are "". With quotes, this will parse all the way until
// another quote. Quotes can be escaped with \. Will panic
// if called on a value that is not a string.
func (p parser) Quotable() parser {
  if p.k != STRING {
    panic("Invalid call Quotable() on type " + p.k.String())
  }
  p.data[0] = 0x01
  return p
}

// This changes how strings are parsed. With this parse
// mode, this node will consume all remaining data in the
// command. " will not be removed. Will panic if
// called on a value that is not a string.
func (p parser) Greedy() parser {
  if p.k != STRING {
    panic("Invalid call Greedy() on type " + p.k.String())
  }
  p.data[0] = 0x02
  return p
}

// Only valid for ENTITY. If true, then this node will
// only allow a single entity/player (it disallows @e/@a).
func (p parser) OneEntity(val bool) parser {
  if p.k != ENTITY {
    panic("Invalid call OneEntity() on type " + p.k.String())
  }
  if val {
    p.data[0] |= 0x01
  } else {
    p.data[0] &^= 0x01
  }
  return p
}

// Only valid for ENTITY. If true, then this node will
// only allow players. This can be used in combination
// with OneEntity().
func (p parser) OnlyPlayers(val bool) parser {
  if p.k != ENTITY {
    panic("Invalid call OnlyPlayers() on type " + p.k.String())
  }
  if val {
    p.data[0] |= 0x02
  } else {
    p.data[0] &^= 0x02
  }
  return p
}

// Only valid for RANGE. If true, then this node will
// allow decimals to be used.
func (p parser) Decimals(val bool) parser {
  if p.k != RANGE {
    panic("Invalid call Decimals() on type " + p.k.String())
  }
  if val {
    p.data[0] |= 0x01
  } else {
    p.data[0] &^= 0x01
  }
  return p
}
