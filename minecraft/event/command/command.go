package command

import (
  "strings"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  pb "gitlab.com/macmv/sugarcane/proto"
)

type Command struct {
  name string
  tree *Node
  runner func(*world.WorldManager, *player.Player, []string) bool
  tab_completer func(*world.WorldManager, *player.Player, []string) []string
}

// Creates a new command. The name will be the command
// without the /. The running is called whenever the client
// runs this command. The children are the arguments to the
// command. See Node for more information on command nodes.
func New(name string, runner func(*world.WorldManager, *player.Player, []string) bool, children ...*Node) *Command {
  c := Command{}
  c.name = name
  c.runner = runner
  c.tab_completer = nil
  c.tree = new_node(1, children)
  c.tree.Name(name)
  return &c
}

// Changes the function that is called when the command is run.
// This will override the previous runner.
func (c *Command) SetRunner(runner func(*world.WorldManager, *player.Player, []string) bool) *Command {
  c.runner = runner
  return c
}

// Changes the function that is called whenever the client requests
// tab completes. This will only be called if one if the nodes
// has the 'minecraft:ask_server' suggestion type.
func (c *Command) SetTabCompleter(runner func(*world.WorldManager, *player.Player, []string) []string) *Command {
  c.tab_completer = runner
  return c
}

// Adds the given nodes as children. These are top
// level arguments to the command.
func (c *Command) AddNode(child ...*Node) *Command {
  c.tree.AddNode(child...)
  return c
}

// Calls the runner. Will panic if the runner is nil.
func (c *Command) execute(world_manager *world.WorldManager, player *player.Player, args []string) {
  c.runner(world_manager, player, args)
}

// Calls the tab completer. Prints a warning if it is
// not set.
func (c *Command) tab_complete(world_manager *world.WorldManager, player *player.Player, id int32, args []string, last_arg_start int32) {
  if c.tab_completer == nil {
    log.Warn("No tab completer set for command", c.name)
    return
  }
  options := c.tab_completer(world_manager, player, args)
  out := packet.NewOutgoingPacket(packet.Clientbound_TabComplete)
  last_arg := args[len(args) - 1]
  out.SetInt(0, id) // needs to match from the packet the client sent
  out.SetInt(1, last_arg_start + 1) // starts replacing text at the start of the next arg
  out.SetInt(2, int32(len(last_arg))) // replaces the whole word they were typing
  sections := []*pb.TabCompleteSection{}
  for _, val := range options {
    if strings.HasPrefix(val, last_arg) { // we only want to show them the options that are completions of what they have already typed
      section := &pb.TabCompleteSection{}
      section.Match = val
      sections = append(sections, section)
    }
  }
  out.SetTabCompleteSections(sections)
  out.Send(player.PacketStream)
}
