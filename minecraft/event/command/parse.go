package command

import (
  "strconv"
  "gitlab.com/macmv/sugarcane/minecraft/player"
)

// This parses a block position in the world, and is just three PositionNums in a row
func ParsePosition(player *player.Player, x_str, y_str, z_str string) (x, y, z int32, err error) {
  err = nil
  x, err = ParsePositionNum(player.BlockX(), x_str)
  if err != nil {
    return
  }
  y, err = ParsePositionNum(player.BlockY(), y_str)
  if err != nil {
    return
  }
  z, err = ParsePositionNum(player.BlockZ(), z_str)
  if err != nil {
    return
  }
  return
}

// Parses a number in either of these formats:
// 10   -5    -123
// ~    ~-10  ~123
// The ~ means relative positioning, and that is relative to the base parameter
func ParsePositionNum(base int32, val string) (int32, error) {
  var pos int32
  if val[0] == '~' {
    pos = base
    if len(val) > 1 {
      pos64, err := strconv.ParseInt(val[1:], 10, 32)
      if err != nil {
        return 0, err
      }
      pos += int32(pos64)
    }
  } else {
    pos64, err := strconv.ParseInt(val, 10, 32)
    if err != nil {
      return 0, err
    }
    pos = int32(pos64)
  }
  return pos, nil
}

// Shorthand for any float
func ParseFloat(arg string) (float32, error) {
  val, err := strconv.ParseFloat(arg, 32)
  return float32(val), err
}
