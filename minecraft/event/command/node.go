package command

import (
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  pb "gitlab.com/macmv/sugarcane/proto"
)

// A command node. Since minecraft 1.13, all possible commands are
// represented with a tree. Every node on the tree is one argument
// in a command. Every one of these nodes has a type: Root, literal,
// or argument. The root node is created automatically. Literal nodes
// are for things like keyworks; they only allow a certain string
// to be inserted at that point. Argument nodes allow for any text,
// and are parsed with a node parser. See parser.go for more information
// on parsers.
//
// To create a command, use the New() function. To add arguments, call
// AddNode(Node). To create a node, use the ArgumentNode() or LiteralNode()
// functions.
type Node struct {
  // Is a byte with this bitmask:
  // 0x03: Node type. 0 -> root, 1 -> literal, 2 -> argument
  // 0x04: Is executable. If set, then this is a valid command with the current number of arguments.
  // 0x08: Has redirect. If set, then this just redirects to another node.
  // 0x10: Has suggestion. Only present for argument nodes.
  flags byte

  // The name of the node.
  name string
  // All child nodes.
  children []*Node

  // If flags & 0x08, then this will be non nil.
  redirect *Node
  // If flags & 0x10, then this will be set.
  suggestion string

  // The parser that will be used.
  parser parser
}

// Low level function for creating a new command. Will panic if the kind is invalid.
func new_node(kind byte, children []*Node) *Node {
  if kind > 2 {
    panic("Invalid kind passed to command node!")
  }
  n := Node{}
  n.flags = kind
  n.children = children
  return &n
}

// Creates a new argument node. The name is what the users sees when they are entering
// the value of this arguemnt. The parser should be one of the parsers from parser.go.
func ArgumentNode(name string, parser parser, children ...*Node) *Node {
  n := new_node(2, children)
  n.Name(name)
  n.Parser(parser)
  return n
}

// Creates a new literal node. The name is the only text that will be accepted for this
// node.
func LiteralNode(name string, children ...*Node) *Node {
  n := new_node(1, children)
  n.Name(name)
  return n
}

// Only valid on the root node. Will return an outgoing packet that contains the entire
// command tree. You should node call this function, as it will panic on everything that
// is not a root node.
func (n *Node) ToPacket() *packet.OutgoingPacket {
  kind := n.flags & 0x03
  if kind != 0 {
    panic("Can only convert root tags to a packet!")
  }
  nodes := []*Node{}
  tracked_nodes := make(map[*Node]int32)
  nodes = n.makeList(nodes, tracked_nodes)
  protos := []*pb.Node{}
  for _, node := range nodes {
    proto := &pb.Node{}
    proto.Flags = int32(node.flags)
    if len(node.children) == 0 { // All tip nodes are executable
      proto.Flags |= 0x04
    }
    children_list := []int32{}
    for _, child := range node.children {
      children_list = append(children_list, tracked_nodes[child])
    }
    proto.Children = children_list
    proto.Redirect = tracked_nodes[node.redirect]
    proto.Name = node.name
    proto.Parser = node.parser.k.String()
    proto.Properties = node.parser.data
    proto.Suggestion = node.suggestion
    protos = append(protos, proto)
  }
  // fmt.Println("From node tree:", *n)
  // fmt.Println("Generated proto list:", protos)
  // fmt.Println("Generated proto list length:", len(protos))
  out := packet.NewOutgoingPacket(packet.Clientbound_DeclareCommands)
  out.SetNodes(protos)
  out.SetInt(0, 0)
  return out
}

func (n *Node) makeList(nodes []*Node, tracked_nodes map[*Node]int32) []*Node {
  _, ok := tracked_nodes[n]
  if ok {
    return nodes
  }
  tracked_nodes[n] = int32(len(nodes))
  nodes = append(nodes, n)
  for _, child := range n.children {
    nodes = child.makeList(nodes, tracked_nodes)
  }
  return nodes
}

// Makes the node executable. This means that
// at the end of this node, this is a valid command.
// All nodes that have no children are automatically
// set to be executable, so this function should
// only be used if you command is valid with only partial
// arguments.
func (n *Node) Executable() *Node {
  n.flags |= 0x04
  return n
}

// Adds another node as a child. Will return
// itself.
func (n *Node) AddNode(other ...*Node) *Node {
  n.children = append(n.children, other...)
  return n
}

// Sets this node to redirect to another node.
// If other is nil, then the redirect will be
// cleared.
func (n *Node) Redirect(other *Node) *Node {
  n.redirect = other
  if other == nil {
    n.flags &^= 0x08
  } else {
    n.flags |= 0x08
  }
  return n
}

// Changes the name of the node.
func (n *Node) Name(name string) *Node {
  kind := n.flags & 0x03
  if kind == 1 || kind == 2 {
    n.name = name
  } else {
    panic("Cannot set name of root node!")
  }
  return n
}

// Changes the parser for the node. Will
// panic if this is not an argument node.
func (n *Node) Parser(parser parser) *Node {
  kind := n.flags & 0x03
  if kind == 2 {
    n.parser = parser
  } else {
    panic("Can only set parser on argument nodes!")
  }
  return n
}

// Changes the suggestion of the node. If
// this is an empty string, the suggestion
// will be cleared.
func (n *Node) Suggestion(suggestion string) *Node {
  n.suggestion = suggestion
  if suggestion == "" {
    n.flags &^= 0x10
  } else {
    n.flags |= 0x10
  }
  return n
}
