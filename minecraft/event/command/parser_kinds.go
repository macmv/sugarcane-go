package command

// A kind of parser. See the constants for more.
type parser_kind int

// See https://wiki.vg/Command_Data for more information.
const (
  BOOL parser_kind = iota // Boolean value
  DOUBLE                  // Double
  FLOAT                   // Float
  INTEGER                 // Integer
  STRING                  // A string
  ENTITY                  // A selector, player name, or UUID.
  GAME_PROFILE            // A player, online or not. Can also use a selector, which may match one or more players (but not entities).
  BLOCK_POS               // A location, represented as 3 numbers (which must be integers). May use relative locations with ~.
  COLUMN_POS              // A column location, represented as 3 numbers (which must be integers). May use relative locations with ~.
  VEC3                    // A location, represented as 3 numbers (which may have a decimal point, but will be moved to the center of a block if none is specified). May use relative locations with ~.
  VEC2                    // A location, represented as 2 numbers (which may have a decimal point, but will be moved to the center of a block if none is specified). May use relative locations with ~.
  BLOCK_STATE             // A block state, optionally including NBT and state information.
  BLOCK_PREDICATE         // A block, or a block tag.
  ITEM_STACK              // An item, optionally including NBT.
  ITEM_PREDICATE          // An item, or an item tag.
  COLOR                   // A chat color. One of the names from Chat#Colors, or reset. Case-insensitive.
  COMPONENT               // A JSON Chat component.
  MESSAGE                 // A regular message, potentially including selectors.
  NBT                     // An NBT value, parsed using JSON-NBT rules.
  NBT_PATH                // A path within an NBT value, allowing for array and member accesses.
  OBJECTIVE               // A scoreboard objective.
  OBJECTIVE_CRITERIA      // A single score criterion.
  OPERATION               // A scoreboard operator.
  PARTICLE                // A particle effect (an identifier with extra information following it for specific particles, mirroring the Particle packet)
  ROTATION                // An angle, represented as 2 numbers (which may have a decimal point, but will be moved to the center of a block if none is specified). May use relative locations with ~.
  ANGLE                   // minecraft:angle
  SCOREBOARD_SLOT         // A scoreboard display position slot. list, sidebar, belowName, and sidebar.team.${color} for all chat colors (reset is not included)
  SCORE_HOLDER            // Something that can join a team. Allows selectors and *.
  SWIZZLE                 // A collection of up to 3 axes.
  TEAM                    // The name of a team. Parsed as an unquoted string.
  ITEM_SLOT               // A name for an inventory slot.
  RESOURCE_LOCATION       // An Identifier.
  MOB_EFFECT              // A potion effect.
  FUNCTION                // A function.
  ENTITY_ANCHOR           // The entity anchor related to the facing argument in the teleport command, is feet or eyes.
  RANGE                   // A range of values with a min and a max.
  INT_RANGE               // An integer range of values with a min and a max.
  FLOAT_RANGE             // A floating-point range of values with a min and a max.
  ITEM_ENCHANTMENT        // Represents a item enchantment.
  ENTITY_SUMMON           // Represents an entity summon.
  DIMENSION               // Represents a dimension.
  UUID                    // Represents a UUID value.
  NBT_TAG                 // Represents a partial nbt tag, usable in data modify command.
  NBT_COMPOUND_TAG        // Represents a full nbt tag.
  TIME                    // Represents a time duration.
)

func (k parser_kind) String() string {
  switch k {
    case BOOL:               return "brigadier:bool"
    case DOUBLE:             return "brigadier:double"
    case FLOAT:              return "brigadier:float"
    case INTEGER:            return "brigadier:integer"
    case STRING:             return "brigadier:string"
    case ENTITY:             return "minecraft:entity"
    case GAME_PROFILE:       return "minecraft:game_profile"
    case BLOCK_POS:          return "minecraft:block_pos"
    case COLUMN_POS:         return "minecraft:column_pos"
    case VEC3:               return "minecraft:vec3"
    case VEC2:               return "minecraft:vec2"
    case BLOCK_STATE:        return "minecraft:block_state"
    case BLOCK_PREDICATE:    return "minecraft:block_predicate"
    case ITEM_STACK:         return "minecraft:item_stack"
    case ITEM_PREDICATE:     return "minecraft:item_predicate"
    case COLOR:              return "minecraft:color"
    case COMPONENT:          return "minecraft:component"
    case MESSAGE:            return "minecraft:message"
    case NBT:                return "minecraft:nbt"
    case NBT_PATH:           return "minecraft:nbt_path"
    case OBJECTIVE:          return "minecraft:objective"
    case OBJECTIVE_CRITERIA: return "minecraft:objective_criteria"
    case OPERATION:          return "minecraft:operation"
    case PARTICLE:           return "minecraft:particle"
    case ROTATION:           return "minecraft:rotation"
    case ANGLE:              return "minecraft:angle"
    case SCOREBOARD_SLOT:    return "minecraft:scoreboard_slot"
    case SCORE_HOLDER:       return "minecraft:score_holder"
    case SWIZZLE:            return "minecraft:swizzle"
    case TEAM:               return "minecraft:team"
    case ITEM_SLOT:          return "minecraft:item_slot"
    case RESOURCE_LOCATION:  return "minecraft:resource_location"
    case MOB_EFFECT:         return "minecraft:mob_effect"
    case FUNCTION:           return "minecraft:function"
    case ENTITY_ANCHOR:      return "minecraft:entity_anchor"
    case RANGE:              return "minecraft:range"
    case INT_RANGE:          return "minecraft:int_range"
    case FLOAT_RANGE:        return "minecraft:float_range"
    case ITEM_ENCHANTMENT:   return "minecraft:item_enchantment"
    case ENTITY_SUMMON:      return "minecraft:entity_summon"
    case DIMENSION:          return "minecraft:dimension"
    case UUID:               return "minecraft:uuid"
    case NBT_TAG:            return "minecraft:nbt_tag"
    case NBT_COMPOUND_TAG:   return "minecraft:nbt_compound_tag"
    case TIME:               return "minecraft:time"
    default:                 return ""
  }
}

func (k parser_kind) initial_data() []byte {
  switch k {
    case INTEGER, FLOAT, DOUBLE: return make([]byte, 1) // A single flags byte
    case STRING: return make([]byte, 1) // Varint
    case ENTITY: return make([]byte, 1) // A single flags byte
    case SCORE_HOLDER: return make([]byte, 1) // A single flags byte
    case RANGE: return make([]byte, 1) // A single bool
    default: return nil
  }
}
