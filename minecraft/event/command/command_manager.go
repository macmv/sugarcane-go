package command

import (
  "fmt"
  "strings"

  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
)

type CommandManager struct {
  commands map[string]*Command
  world_manager *world.WorldManager
}

func NewCommandManager(world_manager *world.WorldManager) *CommandManager {
  c := CommandManager{}
  c.commands = make(map[string]*Command)
  c.world_manager = world_manager
  return &c
}

func (c *CommandManager) Add(command *Command) {
  c.commands[command.name] = command
}

func (c *CommandManager) Execute(player *player.Player, text string) *util.Chat {
  sections := strings.Split(text, " ")
  command, ok := c.commands[sections[0]]
  if !ok {
    out := util.NewChat()
    out.AddSectionColor("Unknown command", "red")
    return out
  }
  command.execute(c.world_manager, player, sections[1:])
  return nil
}

func (c *CommandManager) TabComplete(player *player.Player, id int32, text string) *util.Chat {
  sections := strings.Split(text, " ")
  fmt.Println("Got tab complete with sections:", sections)
  command, ok := c.commands[sections[0]]
  if !ok {
    out := util.NewChat()
    out.AddSectionColor("Unknown command", "red")
    return out
  }
  command.tab_complete(c.world_manager, player, id, sections[1:], int32(len(text) - len(sections[len(sections) - 1])))
  return nil
}

func (c *CommandManager) GetCommandTree() *packet.OutgoingPacket {
  root := new_node(0, nil)
  for _, command := range c.commands {
    root.AddNode(command.tree)
  }
  return root.ToPacket()
}
