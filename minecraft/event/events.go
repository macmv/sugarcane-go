package event

import (
  "reflect"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/event/command"
)

type EventManager struct {
  // key is the event type
  handlers map[string][]reflect.Value
  commands *command.CommandManager
}

func NewEventManager(world_manager *world.WorldManager) *EventManager {
  m := EventManager{}
  m.handlers = make(map[string][]reflect.Value)
  m.commands = command.NewCommandManager(world_manager)
  return &m
}

func (m *EventManager) EnableBasicHandlers() {
  m.AddEventHandler("right-click", reflect.ValueOf(blockEdit))
  m.AddEventHandler("click-window", reflect.ValueOf(blockEditClick))
  addCommands(m.commands)
}

func (m *EventManager) AddEventType(event_type string) {
  _, ok := m.handlers[event_type]
  if ok {
    panic("Event type has already been registered! Type: " + event_type)
  }
  m.handlers[event_type] = []reflect.Value{}
}

func (m *EventManager) AddEventHandler(event_type string, handler reflect.Value) {
  _, ok := m.handlers[event_type]
  if !ok {
    panic("Cannot find event type " + event_type)
  }
  m.handlers[event_type] = append(m.handlers[event_type], handler)
}

// The params passed into this function should match the params of the functions added in AddEventHandler
func (m *EventManager) HandleEvent(event_type string, params ...interface{}) bool {
  handlers, ok := m.handlers[event_type]
  if !ok {
    panic("Cannot find event type " + event_type)
  }
  is_cancelled := false
  for _, function := range handlers {
    ftype := function.Type()
    if ftype.NumIn() != len(params) {
      log.Error("Function params: ", ftype.NumIn())
      log.Error("Passed params: ", len(params))
      panic("Function passed in for handler " + event_type + " takes the wrong number of parameters")
    }
    in := make([]reflect.Value, ftype.NumIn())
    for i, p := range params {
      if ftype.In(i).ConvertibleTo(reflect.TypeOf(p)) {
        in[i] = reflect.ValueOf(p)
      } else {
        log.Error("Function type: ", ftype.In(i))
        log.Error("Passed type: ", reflect.TypeOf(p))
        panic("Function passed in for handler " + event_type + " takes the wrong type of parameter")
      }
    }
    out := function.Call(in)
    // If the function returns a bool, we expect that to be an is_cancelled flag
    // If it returns anything else, we ignore it
    if len(out) > 0 && out[0].Kind() == reflect.Bool && out[0].Bool() {
      is_cancelled = true
    }
  }
  return is_cancelled
}

func (m *EventManager) HandleCommand(command string, player *player.Player) {
  m.commands.Execute(player, command)
}

func (m *EventManager) HandleTabComplete(player *player.Player, id int32, command string) {
  m.commands.TabComplete(player, id, command)
}

func (m *EventManager) AddCommand(c *command.Command) {
  m.commands.Add(c)
}

func (m *EventManager) GetCommandTree() *packet.OutgoingPacket {
  return m.commands.GetCommandTree()
}

