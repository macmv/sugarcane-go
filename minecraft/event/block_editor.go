package event

import (
  "fmt"
  "sort"
  "gitlab.com/macmv/sugarcane/minecraft/nbt"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/inventory"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
)

var blockEditWindows map[*player.Player]*blockEditWindow = make(map[*player.Player]*blockEditWindow)

type blockEditWindow struct {
  items map[uint16]map[uint16]*blockEditItem
  window item.InventoryWindow
  loc block.Pos
  world *world.World
}

type blockEditItem struct {
  category string
  name string
}

func blockEdit(hand bool, stack *item.Stack, wo *world.World, b *block.State, player *player.Player) bool {
  // main hand only
  if hand {
    if stack != nil {
      if stack.Item().Name() == "minecraft:diamond_hoe" {
        all_properties := b.AllProperties()
        if len(all_properties) == 0 {
          return false
        }

        window := player.Inventory().OpenWindow(inventory.WindowType_Generic_9x6, util.NewChatFromString("Edit block state"))

        w := &blockEditWindow{}
        w.items = make(map[uint16]map[uint16]*blockEditItem)
        w.window = window
        w.loc = block.Pos{b.X(), b.Y(), b.Z()}
        w.world = wo

        row := uint16(0)
        category_list := []string{}
        item_list := make(map[string][]string)
        for category, properties := range all_properties {
          category_list = append(category_list, category)
          item_list[category] = []string{}
          for prop := range properties {
            item_list[category] = append(item_list[category], prop)
          }
          sort.Strings(item_list[category])
        }
        sort.Strings(category_list)

        for _, category := range category_list {
          row++
          col := uint16(1)
          tag := nbt.NewCompoundTag("")
          display := tag.CompoundAddChild("display", 0x0a)
          display.CompoundAddChild("Name", 0x08).WriteString("{\"text\":\"" + category + "\",\"italic\":\"false\"}")

          i := item.NewItemStack(item.GetItem("minecraft:oak_sign"), 1, tag)
          window.SetItem(uint16(row * 9 + col), i)
          // set_items := packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
          // set_items.SetByte(0, blockEditId)
          // set_items.SetShort(0, int16(row * 9 + col))
          // set_items.SetItem(0, i)
          // set_items.Send(e.Player.PacketStream)

          w.items[row] = make(map[uint16]*blockEditItem)
          for _, prop := range item_list[category] {
            col++
            tag := nbt.NewCompoundTag("")
            display := tag.CompoundAddChild("display", 0x0a)
            display.CompoundAddChild("Name", 0x08).WriteString("{\"text\":\"" + prop + "\",\"italic\":\"false\"}")

            i = item.NewItemStack(item.GetItem("minecraft:paper"), 1, tag)
            window.SetItem(uint16(row * 9 + col), i)

            w.items[row][col] = &blockEditItem{category: category, name: prop}
          }
        }
        blockEditWindows[player] = w
      }
    }
  }
  return false
}

func blockEditClick(slot int32, button byte, mode int32, stack *item.Stack, player *player.Player) bool {
  w, ok := blockEditWindows[player]
  if !ok {
    return false
  }

  if stack != nil {
    set_item := packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
    set_item.SetByte(0, byte(1))
    set_item.SetShort(0, int16(slot))
    set_item.SetItem(0, stack)
    set_item.Send(player.PacketStream)

    set_item = packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
    set_item.SetByte(0, 255)
    set_item.SetShort(0, -1)
    set_item.SetItem(0, nil)
    set_item.Send(player.PacketStream)

    if slot > 9 && slot < 54 {
      row := uint16(slot) / 9
      col := uint16(slot) % 9
      item_row, ok := w.items[row]
      if !ok {
        return true
      }
      item, ok := item_row[col]
      if !ok {
        return true
      }
      w.world.SetBlockProperty(w.loc, item.category, item.name)
      fmt.Println("You have clicked on paper, with item:", item, w.loc.X, w.loc.Y, w.loc.Z)
    }
  }
  return true
}
