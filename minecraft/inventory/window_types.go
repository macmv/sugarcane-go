package inventory

import (
  "gitlab.com/macmv/sugarcane/minecraft/item"
)

var window_types map[byte]func(*Window)

const (
  WindowType_Generic_9x1      = 0
  WindowType_Generic_9x2      = 1
  WindowType_Generic_9x3      = 2
  WindowType_Generic_9x4      = 3
  WindowType_Generic_9x5      = 4
  WindowType_Generic_9x6      = 5
  WindowType_Generic_3x3      = 6
  WindowType_Anvil            = 7
  WindowType_Beacon           = 8
  WindowType_BlastFurnace     = 9
  WindowType_BrewingStand     = 10
  WindowType_Crafting         = 11
  WindowType_Enchantment      = 12
  WindowType_Furnace          = 13
  WindowType_Grindstone       = 14
  WindowType_Hopper           = 15
  WindowType_Lectern          = 16
  WindowType_Loom             = 17
  WindowType_Merchant         = 18
  WindowType_ShulkerBox       = 19
  WindowType_Smoker           = 20
  WindowType_CartographyTable = 21
  WindowType_Stonecutter      = 22
)

func SetupWindowTypes() {
  window_types = make(map[byte]func(*Window))
  window_types[WindowType_Generic_9x1] = func(window *Window) {
    window.items = make([]*item.Stack, 45)
    window.inventory_start = 9
  }
  window_types[WindowType_Generic_9x2] = func(window *Window) {
    window.items = make([]*item.Stack, 54)
    window.inventory_start = 18
  }
  window_types[WindowType_Generic_9x3] = func(window *Window) {
    window.items = make([]*item.Stack, 63)
    window.inventory_start = 27
  }
  window_types[WindowType_Generic_9x4] = func(window *Window) {
    window.items = make([]*item.Stack, 72)
    window.inventory_start = 36
  }
  window_types[WindowType_Generic_9x5] = func(window *Window) {
    window.items = make([]*item.Stack, 81)
    window.inventory_start = 45
  }
  window_types[WindowType_Generic_9x6] = func(window *Window) {
    window.items = make([]*item.Stack, 90)
    window.inventory_start = 54
  }
  window_types[WindowType_Generic_3x3] = func(window *Window) {
    window.items = make([]*item.Stack, 45)
    window.inventory_start = 9
  }
  window_types[WindowType_Anvil] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_Beacon] = func(window *Window) {
    window.items = make([]*item.Stack, 37)
    window.inventory_start = 1
  }
  window_types[WindowType_BlastFurnace] = func(window *Window) {
    window.items = make([]*item.Stack, 37)
    window.inventory_start = 3
  }
  window_types[WindowType_BrewingStand] = func(window *Window) {
    window.items = make([]*item.Stack, 41)
    window.inventory_start = 5
  }
  window_types[WindowType_Crafting] = func(window *Window) {
    window.items = make([]*item.Stack, 46)
    window.inventory_start = 10
  }
  window_types[WindowType_Enchantment] = func(window *Window) {
    window.items = make([]*item.Stack, 38)
    window.inventory_start = 2
  }
  window_types[WindowType_Furnace] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_Grindstone] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_Hopper] = func(window *Window) {
    window.items = make([]*item.Stack, 41)
    window.inventory_start = 5
  }
  window_types[WindowType_Lectern] = func(window *Window) {
    window.items = make([]*item.Stack, 0)
    window.inventory_start = 0
  }
  window_types[WindowType_Loom] = func(window *Window) {
    window.items = make([]*item.Stack, 40)
    window.inventory_start = 4
  }
  window_types[WindowType_Merchant] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_ShulkerBox] = func(window *Window) {
    window.items = make([]*item.Stack, 63)
    window.inventory_start = 27
  }
  window_types[WindowType_Smoker] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_CartographyTable] = func(window *Window) {
    window.items = make([]*item.Stack, 39)
    window.inventory_start = 3
  }
  window_types[WindowType_Stonecutter] = func(window *Window) {
    window.items = make([]*item.Stack, 38)
    window.inventory_start = 2
  }
}
