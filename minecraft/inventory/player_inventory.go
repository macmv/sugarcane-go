package inventory

import (
  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
)

type PlayerInventory struct {
  items [46]*item.Stack
  open_window *Window
  packet_stream *packet.ConnectionWithChan
  current_hotbar_slot uint16
  needs_equipment_change bool
  eid uint32
}

func NewPlayerInventory(eid uint32, packet_stream *packet.ConnectionWithChan) *PlayerInventory {
  i := PlayerInventory{}
  i.packet_stream = packet_stream
  i.eid = eid
  return &i
}

func (i *PlayerInventory) SetHeldItemSlot(slot uint16) {
  if slot > 8 {
    panic("Invalid slot!")
  }
  prev := i.HeldItem()
  i.current_hotbar_slot = slot
  if i.HeldItem() != prev {
    i.needs_equipment_change = true
    i.on_change_main_hand()
  }
}

func (i *PlayerInventory) HeldItem() *item.Stack {
  return i.items[i.current_hotbar_slot + 36]
}

func (i *PlayerInventory) SetHeldItem(t *item.Stack) {
  i.SetItem(i.current_hotbar_slot + 36, t)
}

func (i *PlayerInventory) SetItem(slot uint16, stack *item.Stack) {
  if slot > 45 {
    panic("Invalid slot!")
  }
  window_id := byte(0)
  if i.open_window != nil {
    window_id = 1
  }
  prev := i.HeldItem()
  i.items[slot] = stack
  if i.HeldItem() != prev {
    i.needs_equipment_change = true
    i.on_change_main_hand()
  }
  out := packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
  out.SetByte(0, window_id)
  out.SetShort(0, int16(slot))
  out.SetItem(0, stack)
  out.Send(i.packet_stream)
}

func (i *PlayerInventory) on_change_main_hand() {
  speed := 4.0
  if i.HeldItem() != nil {
    speed = i.HeldItem().AttackSpeed()
  }
  out := packet.NewOutgoingPacket(packet.Clientbound_EntityProperties)
  out.SetInt(0, int32(i.eid))
  out.AddEntityProperty("generic.attackSpeed", speed)
  out.Send(i.packet_stream)
}

func (i *PlayerInventory) SetHotbarItem(slot uint16, stack *item.Stack) {
  index := slot
  if slot > 8 {
    panic("Invalid slot!")
  }
  if i.open_window == nil {
    i.SetItem(index + 36, stack)
  } else {
    i.SetItem(index + 27, stack)
  }
}

func (i *PlayerInventory) OpenWindow(kind byte, title *util.Chat) item.InventoryWindow {
  window, err := newWindow(kind, title, i, i.packet_stream)
  if err != nil {
    log.Error("Error while creating window: ", err.Error())
    return nil
  }
  i.open_window = window
  return window
}

func (i *PlayerInventory) Equipment() map[int]*item.Stack {
  e := make(map[int]*item.Stack)
  e[0] = i.HeldItem()
  return e
}

func (i *PlayerInventory) NeedsEquipmentChange() bool {
  return i.needs_equipment_change
}

func (i *PlayerInventory) ClearEquipmentChange() {
  i.needs_equipment_change = false
}
