package inventory

import (
  "fmt"
  "errors"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
)

type Window struct {
  items []*item.Stack
  kind byte
  packet_stream *packet.ConnectionWithChan
  player_inventory *PlayerInventory
  inventory_start uint16
}

func newWindow(kind byte, title *util.Chat, player_inventory *PlayerInventory, packet_stream *packet.ConnectionWithChan) (*Window, error) {
  w := Window{}
  w.kind = kind
  w.player_inventory = player_inventory
  w.packet_stream = packet_stream
  if window_types == nil {
    panic("Please call SetupWindowTypes before creating a window!")
  }
  setup, ok := window_types[kind]
  if !ok {
    return nil, errors.New("Invalid window type: " + fmt.Sprintf("%d", kind))
  }
  setup(&w)
  out := packet.NewOutgoingPacket(packet.Clientbound_OpenWindow)
  out.SetInt(0, 1)
  out.SetInt(1, int32(kind))
  out.SetString(0, title.ToJSON())
  out.Send(w.packet_stream)
  return &w, nil
}

func (w *Window) SetItem(slot uint16, item *item.Stack) error {
  if slot >= w.inventory_start {
    return errors.New("Cannot set a slot that is the player's inventory on a window. Please use SetInventoryItem for that")
  }
  w.items[slot] = item
  out := packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
  out.SetByte(0, 1)
  out.SetShort(0, int16(slot))
  out.SetItem(0, item)
  out.Send(w.packet_stream)
  return nil
}

func (w *Window) setInventoryItem(slot uint16, item *item.Stack) error {
  if w.inventory_start == 0 {
    return errors.New("Cannot set inventory item, as inventory is now shown on this window type")
  }
  if slot > 35 {
    return errors.New("Invalid slot passed to SetInventoryItem")
  }
  w.player_inventory.SetItem(slot, item)
  w.items[slot + w.inventory_start] = item
  return nil
}

func (w *Window) GetItem(slot uint16) *item.Stack {
  return w.items[slot]
}

func (w *Window) GetPlayerInventory() *PlayerInventory {
  return w.player_inventory
}
